'use strict';

import React from 'react';
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import Styles from '../styles/Styles';
import IMAGES from '../styles/Images';
import {useNavigation} from '@react-navigation/native';

const NavigationBar = props => {
  const navigation = useNavigation();

  return (
    <View style={style.mainView}>
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={Styles.back_button}>
        <Image
          style={{height: 35, width: 35, alignSelf: 'flex-start'}}
          source={IMAGES.blue_left_arrow}
          resizeMode={'contain'}
        />
      </TouchableOpacity>
      <View>
        {props.title ? (
          <Text
            style={[
              Styles.subheading_label,
              {marginLeft: 15, color: COLOR.WHITE, marginRight: 20},
            ]}>
            {props.title}
          </Text>
        ) : (
          <Image
            style={{height: 35, marginLeft: 10, width: 120}}
            resizeMode={'contain'}
            source={IMAGES.logo}
          />
        )}
        {props.secondTitle && (
          <Text style={[Styles.extra_small_label, style.second_title]}>
            {props.secondTitle}
          </Text>
        )}
      </View>
    </View>
  );
};

export default NavigationBar;

const style = StyleSheet.create({
  mainView: {
    marginVertical: 0,
    width: wp(90),
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  second_title: {
    marginLeft: 10,
    color: COLOR.YELLOW,
    textDecorationLine: 'underline',
    marginRight: 10,
    fontWeight: '500',
  },
});
