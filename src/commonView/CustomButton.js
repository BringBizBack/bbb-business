import React from 'react';
import {TouchableOpacity, Text, StyleSheet, Image} from 'react-native';
import COLOR from '../styles/Color';
import Styles from '../styles/Styles';

export const CustomButton = props => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={[
        styles.button_content,
        {
          backgroundColor: props.bg ? props.bg : COLOR.YELLOW,
          flexDirection: 'row',
          height: props.height ? props.height:45
        },
      ]}>
      {props.image && (
        <Image
          source={props.image}
          style={styles.image_view}
          resizeMode={'contain'}
        />
      )}
      <Text
        style={[
          Styles.bold_body_label,
          {
            color: props.labelColor ? props.labelColor : COLOR.BLACK,
            fontWeight: 'normal',
          },
        ]}>
        {props.text}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button_content: {
    height: 45,
    width: '100%',
    borderRadius: 22.5,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  image_view: {height: 15, width: 15, marginRight: 10},
});
