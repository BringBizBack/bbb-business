'use strict';
//URLS
export const U_BASE = 'https://application-acc.fastusers.com/credits-api/';
export const U_IMAGE_BASE =
  'https://application-acc.fastusers.com/credits-api/images/';
export const U_FILE_UPLOAD = 'file';
export const U_APP_STORE_LINK = 'https://itunes.apple.com/app/id=1582367657';
export const U_PLAY_STORE_LINK = '';
export const U_CUSTOMER_APP_STORE_LINK =
  'https://itunes.apple.com/app/id=1572394806';
export const U_CUSTOMER_PLAY_STORE_LINK = '';

export const U_GOOGLE_API_KEY = 'AIzaSyBvOO-m6sV_jFWGUWIx5aPqBGfgI8d3HeI';

export const U_LOGIN = 'auth/business/login';
export const U_LOGOUT = 'logout-user/';
export const U_SIGNUP = 'v2/auth/business/signup';

export const U_FORGOT_PASS = 'forgot-password';
export const U_RESET_PASS = 'reset-password';
export const U_GENERATE_OTP = 'generate-otp/';
export const U_VERIFY_OTP = 'v2/verify-otp/';

export const U_GET_REDEEM_REQUEST = 'redeem-non-approved';
export const U_GIVE_REDEEM_TO_REQUEST = 'approve-redeem/';
export const U_GET_CREDIT_REQUEST = 'credits-non-approved';
export const U_GIVE_CREDIT_TO_REQUEST = 'approve-credit/';

export const U_COMPLETE_PROFILE = 'user';
export const U_GET_COMPLETE_PROFILE = 'user-business-info/';
export const U_GET_NOTIFICATION = 'system-notifications';
export const U_GET_BUSINESS_IMAGES = 'get-all-images';
export const U_ADD_BUSINESS_IMAGES = 'add-new-images';
export const U_DELETE_BUSINESS_IMAGE = 'remove-image';
export const U_REMOVE_CAMPAIGN = 'remove-campaign/';

export const U_UPDATE_LEGAL_INFO = 'legal-info';
export const U_GET_LEGAL_INFO = 'get-legal-info-user';

export const U_UPDATE_TRADE_INFO = 'trading-info';
export const U_GET_TRADE_INFO = 'trading-info-user';
export const U_SUBMIT_BUSINESS_FOR_APPROVAL = 'submit-for-approval';

export const U_GET_ALL_TEMPLATE = 'all-campaign-templates';

export const U_ADD_CAMPAIGN = 'campaign';
export const U_GET_CAMPAIGN = 'campaigns-user';
export const U_UPDATE_CAMPAIGN = 'update-campaign/';
export const U_GET_CAMPAIGN_LEADERBOARD = 'leaderboard/';

export const U_ADD_MEMBER = 'add-member';
export const U_REMOVE_MEMBER = 'remove-member/';
export const U_GET_MEMBER = 'get-members';

export const U_ADD_BUSINESS_HOUR = 'add-business-hour';
export const U_GET_BUSINESS_HOUR = 'business-hour';

export const U_GIVE_CREDIT = 'add-credit';
export const U_REDEEM_CREDIT = 'redeem-credits-business';
export const U_GET_CAMPAIGN_FOR_REDEEM = 'get-campaign-credits';

export const U_SEARCH_USER = 'user-record';
export const U_CREDIT_USER = 'credit-user';

export const U_GET_CREDIT_HISTORY = 'combined-credit-history';

export const U_GET_NATION = 'nations';
export const U_GET_PROVINCES = 'province/';
export const U_GET_DISTRICT = 'districts/';
export const U_GET_SUB_DISTRICT = 'sub-districts/';

export const U_ADD_PAYMENT_METHOD = 'add-card';
export const U_GET_PAYMENT_METHOD = 'cards';
export const U_SET_AS_DEFAULT_CARD = 'card-default/';
export const U_REMOVE_CARD = 'remove-card/';

//Userdefaults
export const AS_INITIAL_ROUTE = 'initial_route_name';
export const AS_USER_DETAIL = 'user_details';
export const AS_USER_TOKEN = 'user_access_token';
export const AS_FCM_TOKEN = 'fcm_token';
export const AS_USER_EMAIL = 'AS_USER_EMAIL';
export const AS_USER_PASSWORD = 'AS_USER_PASSWORD';

//Selection Data

export const K_REMEMBER_ME =
  "Click this so you don't have to enter your email address and password each time you use the app.  It's stored on your device. You can always Logoff in Settings at the bottom.";
export const K_CAMPAIGN_INFO =
  'If this restaurant receives the number of visits indicated as its GOAL after the START time and before the END time it will give PRIZES to WINNERS.  If it reaches the END time and the GOAL is not achieved no PRIZES will be given out.  This event is a Goalprize. (more info)';
export const K_WINNERS_INFO =
  'WINNERS of prizes will be participants using the Goalprize app who spent the most money at this restaurant, as indicated on the LEADERBOARD, after the START time and before the Goalprize campaign finished because the GOAL was successfuly achieved before the END time.  The number of winners is indicated in Step 2, WINNERS. (more info)';
export const K_PRIZE_INFO =
  'The PRIZE is a digital gift card equal to what the winner spent during the Goalprize campaign. It can only be used before the expiration date only at that restaurant to reduce the bill on eligible items. (more info)\n' +
  '\n' +
  'This is only a summary. Many more rules apply. Click to\n' +
  'READ ALL GOALPRIZE RULES & TERMS.';
export const K_LEADERBOARD_INFO =
  'This Goalprize LEADERBOARD shows the total points each participant has earned during this Goalprize which equals the total eligible spending they did at this restaurant. Customers must use the Goalprize app, create an account, pay a bill, request and receive points for eligible spending.  Eligible spending is spending on most items but not alcohol, tobacco, VAT (sales tax) and service charge (tip).  If a Goalprize is active the LEADERBOARD shows the current totals and rankings of all participants and is updated after each participant pays their bills and requests and receives points from the restaurant.  \n' +
  '\n' +
  'If a Goalprize is finished the LEADERBOARD shows the total that was spent at the time the Goalprize finished because the GOAL was achieved before the END time (a successful Goalprize) or because the END time was reached before the GOAL was achieved (a failed Goalprize).\n' +
  '\n' +
  'This is only a summary.  Many more rules apply.  Click to \n' +
  'READ ALL GOALPRIZE RULES & TERMS. ';
