'use strict';

import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import COLOR from '../styles/Color';
import Styles from '../styles/Styles';
import {useNavigation} from '@react-navigation/native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import ProgressCircle from 'react-native-progress-circle';

const BottomView = props => {
  const navigation = useNavigation();
  return (
    <View style={style.main_view}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <TouchableWithoutFeedback
          onPress={() => navigation.navigate('SignupScreen')}>
          <Text
            style={[
              Styles.small_label,
              {
                color: COLOR.WHITE,
                alignSelf: 'center',
                textAlign: 'center',
              },
            ]}>
            Create account
          </Text>
        </TouchableWithoutFeedback>
      </View>
      <View style={style.line_view} />
      {/*<View*/}
      {/*  style={{*/}
      {/*    flex: 1,*/}
      {/*    alignItems: 'center',*/}
      {/*    justifyContent: 'center',*/}
      {/*  }}>*/}
      {/*  <TouchableWithoutFeedback*/}
      {/*    onPress={() =>*/}
      {/*      navigation.navigate('CampaignDetail', {isTabbar: false})*/}
      {/*    }>*/}
      {/*    <Text*/}
      {/*      style={[*/}
      {/*        Styles.small_label,*/}
      {/*        {*/}
      {/*          color: COLOR.WHITE,*/}
      {/*          alignSelf: 'center',*/}
      {/*          textAlign: 'center',*/}
      {/*        },*/}
      {/*      ]}>*/}
      {/*      Campaigns*/}
      {/*    </Text>*/}
      {/*  </TouchableWithoutFeedback>*/}
      {/*</View>*/}
      <View style={style.line_view} />
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <TouchableWithoutFeedback
          onPress={() => navigation.navigate('LoginScreen')}>
          <Text
            style={[
              Styles.small_label,
              {
                color: COLOR.WHITE,
                alignSelf: 'center',
                textAlign: 'center',
              },
            ]}>
            Login
          </Text>
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
};

export default BottomView;

const style = StyleSheet.create({
  main_view: {
    flexDirection: 'row',
    height: 50,
    width: wp(100),
    alignSelf: 'center',
    alignItems: 'center',
    backgroundColor: COLOR.DARK_BLUE,
    justifyContent: 'space-between',
  },
  create_account_text: {
    color: COLOR.DARK_BLUE,
    alignSelf: 'center',
    textAlign: 'center',
    backgroundColor: COLOR.YELLOW,
    borderRadius: 5,
    padding: 5,
    overflow: 'hidden',
  },
  line_view: {width: 1, height: 25, backgroundColor: COLOR.WHITE},
});
