'use strict';

import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import COLOR from '../styles/Color';
import Styles from '../styles/Styles';
import {useNavigation} from '@react-navigation/native';

const TopView = props => {
  const navigation = useNavigation();
  return (
    <View style={style.mainView}>
      <TouchableWithoutFeedback
        style={{zIndex: 1000}}
        onPress={() => navigation.navigate('')}>
        <View
          style={[
            style.inner_circle,
            {
              backgroundColor:
                props.viewType == 1 ? COLOR.LIGHT_BLUE : COLOR.YELLOW,
            },
          ]}>
          <View
            style={[
              style.yellow_view,
              {
                backgroundColor:
                  props.viewType == 1 ? COLOR.YELLOW : 'transparent',
              },
            ]}>
            <Text
              style={[
                props.viewType == 1
                  ? Styles.extra_small_label
                  : Styles.body_label,
                {
                  fontSize: props.viewType == 1 ? 10 : 14,
                  margin: 5,
                },
              ]}>
              {props.title}
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

export default TopView;

const style = StyleSheet.create({
  mainView: {
    height: 250,
    width: 250,
    borderRadius: 125,
    top: -125,
    right: -125,
    position: 'absolute',
    backgroundColor: COLOR.TOP_CIRCLE,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inner_circle: {
    height: 200,
    width: 200,
    borderRadius: 100,
    backgroundColor: COLOR.LIGHT_BLUE,
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
  },

  yellow_view: {
    height: 25,
    width: 75,
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
    marginLeft: 30,
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
