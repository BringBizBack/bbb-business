import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Modal,
  SafeAreaView,
  TouchableWithoutFeedback,
} from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../styles/Color';
import {CustomButton} from './CustomButton';
import QRCodeScanner from 'react-native-qrcode-scanner';
import {RNCamera} from 'react-native-camera';
import Styles from '../styles/Styles';

export default QRCodeView = props => {
  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={props.qrModalVisible}
      onRequestClose={() => {
        props.handleQRModal(1);
      }}>
      <View style={styles.modal}>
        <SafeAreaView style={{flex: 1}}>
          <View style={styles.white_view}>
            <TouchableWithoutFeedback onPress={() => props.handleQRModal(1)}>
              <Text
                style={{alignSelf: 'flex-end', marginRight: 10, marginTop: 10}}>
                close
              </Text>
            </TouchableWithoutFeedback>
            <QRCodeScanner
              fadeIn={true}
              flashMode={RNCamera.Constants.FlashMode.off}
              reactivate={false}
              onRead={val => props.handleQRModal(val)}
              cameraStyle={{
                width: wp(90),
                alignSelf: 'center',
                margin: 0,
              }}
              cameraContainerStyle={{width: wp(90)}}
              topContent={
                <Text style={Styles.heading_label}>Scan QR code.</Text>
              }
            />
          </View>
        </SafeAreaView>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: '#000000bb',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    flex: 1,
  },
  appicon_image: {
    height: 90,
    width: 90,
    marginTop: 50,
    marginLeft: 20,
    overflow: 'hidden',
  },
  white_view: {
    width: wp(90),
    height: hp(70),
    alignSelf: 'center',
    alignItems: 'center',
    backgroundColor: COLOR.WHITE,
    borderRadius: 20,
    flex: 1,
  },
});
