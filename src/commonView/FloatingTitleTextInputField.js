import React, {Component} from 'react';
import {
  View,
  Animated,
  StyleSheet,
  TextInput,
  Image,
  Platform,
} from 'react-native';
import {string, func, object, number, bool} from 'prop-types';
import COLOR from '../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import FONTS from '../styles/Fonts';
import IMAGES from '../styles/Images';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';

export class FloatingTitleTextInputField extends Component {
  static propTypes = {
    attrName: string.isRequired,
    title: string.isRequired,
    updateMasterState: func.isRequired,
    floatKeyboardType: string,
    titleActiveSize: number, // to control size of title when field is active
    titleInActiveSize: number, // to control size of title when field is inactive
    textInputStyles: object,
    otherTextInputProps: object,
    isWhite: bool,
    image: string,
    editable: bool,
    isPasswordVisible: bool,
    handlePasswordVisibility: func,
    handleFieldEmpty: bool,
    multiline: bool,
  };

  static defaultProps = {
    floatKeyboardType: 'default',
    isWhite: true,
    titleActiveSize: 12,
    titleInActiveSize: 12,
    textInputStyles: {},
    otherTextInputAttributes: {},
    image: '',
    editable: true,
    isPasswordVisible: false,
    handleFieldEmpty: false,
    multiline: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      isFieldActive: false,
      isPasswordVisible: bool,
    };
    const {value} = this.props;
    this.position = new Animated.Value(value ? 1 : 0);
    this.initFields = this.initFields.bind(this);
  }

  componentDidMount() {
    this.setState({
      isPasswordVisible: this.props.isPasswordVisible,
    });
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ) {
    if (prevProps != this.props) {
      this.initFields();
    }
  }

  initFields = () => {
    if (this.props.value) {
      this.setState({isFieldActive: true});
      Animated.timing(this.position, {
        toValue: 1,
        duration: 150,
        useNativeDriver: false,
      }).start();
    }
  };

  _handleFocus = () => {
    if (!this.state.isFieldActive) {
      this.setState({isFieldActive: true});
      Animated.timing(this.position, {
        toValue: 1,
        duration: 150,
        useNativeDriver: false,
      }).start();
    }
  };

  _handleBlur = () => {
    if (this.state.isFieldActive && !this.props.value) {
      this.setState({isFieldActive: false});
      Animated.timing(this.position, {
        toValue: 0,
        duration: 150,
        useNativeDriver: false,
      }).start();
    }
  };

  _onChangeText = updatedValue => {
    const {attrName, updateMasterState} = this.props;
    updateMasterState(attrName, updatedValue);
  };

  handlePasswordVisibility = updateValue => {
    if (this.props && this.props.image) {
      this.setState({
        isPasswordVisible: !this.state.isPasswordVisible,
      });
    }
  };

  _returnAnimatedTitleStyles = () => {
    const {isFieldActive} = this.state;
    const {titleActiveSize, titleInActiveSize} = this.props;

    return {
      top: this.position.interpolate({
        inputRange: [0, 1],
        outputRange: [14, 0],
      }),
      fontSize: isFieldActive ? titleActiveSize : titleInActiveSize,
      color: isFieldActive
        ? this.props.isWhite == false
          ? COLOR.BLACK
          : COLOR.WHITE
        : this.props.isWhite == false
        ? COLOR.BLACK
        : COLOR.WHITE,
    };
  };

  render() {
    return (
      <View style={Styles.container}>
        <Animated.Text
          style={[
            Styles.titleStyles,
            this._returnAnimatedTitleStyles(),
            {
              color: this.props.handleFieldEmpty
                ? COLOR.ORANGE
                : this.props.isWhite == false
                ? COLOR.LIGHT_TEXT
                : COLOR.WHITE,
            },
          ]}>
          {this.props.title}
        </Animated.Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <TextInput
            value={this.props.value}
            selectionColor={
              this.props.isWhite == false ? COLOR.BLACK : COLOR.WHITE
            }
            style={[
              Styles.textInput,
              this.props.textInputStyles,
              {
                marginRight: -20,
                flex: 1,
                color: this.props.isWhite == false ? COLOR.BLACK : COLOR.WHITE,
                borderBottomColor: this.props.handleFieldEmpty
                  ? COLOR.ORANGE
                  : this.props.isWhite == false
                  ? COLOR.LIGHT_TEXT
                  : COLOR.WHITE,
              },
            ]}
            underlineColorAndroid="transparent"
            onFocus={this._handleFocus}
            onBlur={this._handleBlur}
            editable={this.props.editable}
            secureTextEntry={this.state.isPasswordVisible}
            onChangeText={this._onChangeText}
            keyboardType={this.props.floatKeyboardType}
            returnKeyLabel="Done"
            returnKeyType="done"
            multiline={this.props.multiline}
            placeholderTextColor={
              this.props.isWhite == false ? COLOR.BLACK : COLOR.WHITE
            }
            {...this.props.otherTextInputProps}
          />

          <TouchableWithoutFeedback onPress={this.handlePasswordVisibility}>
            <Image
              style={{
                height: this.props.image == IMAGES.down_arrow ? 8 : 20,
                width: this.props.image == IMAGES.down_arrow ? 8 : 25,
                paddingRight: 20,

                tintColor:
                  this.props.isWhite == false ? COLOR.BLACK : COLOR.WHITE,
              }}
              source={
                this.props.image
                  ? this.props.image == IMAGES.down_arrow
                    ? IMAGES.down_arrow
                    : this.state.isPasswordVisible == false
                    ? IMAGES.visibility
                    : IMAGES.visibility_hidden
                  : ''
              }
              resizeMode={'contain'}
            />
          </TouchableWithoutFeedback>
        </View>
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  container: {
    width: '100%',
    borderWidth: 0.0,
    height: Platform.OS == 'android' ? 45 : 55,
    marginBottom: 15,
  },
  textInput: {
    fontSize: 16,
    fontWeight: '500',
    fontFamily: FONTS.FAMILY_REGULAR,
    marginTop: Platform.OS == 'android' ? 10 : 20,
    marginLeft: Platform.OS == 'android' ? -5 : 0,
    borderBottomWidth: 1,
    paddingBottom: 0,
  },
  titleStyles: {
    position: 'absolute',
    margin: 0,
    fontSize: 13,
    fontFamily: FONTS.FAMILY_REGULAR,
  },
});
