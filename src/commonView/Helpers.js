import {PermissionsAndroid, Alert, Platform, Linking} from 'react-native';
import moment from 'moment';
import {showMessage} from 'react-native-flash-message';
import {CommonActions} from '@react-navigation/native';
import memoize from 'lodash.memoize';
import i18n from 'i18n-js';
import EnglishLocale from '../translation/en.json';
import ThaiLocale from '../translation/th.json';

export function validateEmail(email) {
  var re =
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

export function validatePassword(pass) {
  var re = /(?=.*[a-z])(?=.*\d)/i;
  return re.test(pass);
}

export async function getDate(timestamp, format) {
  return;
}

export async function requestCameraPermission() {
  //Calling the permission function
  const granted = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.CAMERA,
    {
      title: 'Allow Camera',
      message: 'App needs access to your camera to capture images',
    },
  );
  if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    return true;
  } else {
    return false;
  }
}

export async function requestGalleryPermission() {
  //Calling the permission function
  const granted = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.requestGalleryPermission,
    {
      title: 'Allow Gallery',
      message: 'App needs access to your gallery to pick images',
    },
  );
  if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    return true;
  } else {
    return false;
  }
}

export const createFormData = (photo, body) => {
  const data = new FormData();

  data.append('photo', {
    name: photo.fileName,
    type: photo.type,
    uri:
      Platform.OS === 'android' ? photo.uri : photo.uri.replace('file://', ''),
  });

  Object.keys(body).forEach(key => {
    data.append(key, body[key]);
  });

  return data;
};

export const insertComma = val => {
  if (Platform.OS === 'android') {
    return Number(val)
      .toLocaleString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  } else {
    let num = Number(parseFloat(val).toFixed(2)).toLocaleString('en', {
      minimumFractionDigits: 0,
    });
    return num;
  }
};

export const getRemainingTime = t2 => {
  const t1 = new Date().getTime();
  return t1 > t2 ? 0 : t2 - t1;
};

export const getDateFromTimeStamp = (val, format) => {
  if (val) {
    if (String(val).length > 10) {
      let time = moment(new Date(val ? val : Date.now())).format(format);
      return time;
    } else {
      let time = moment(new Date(val ? val * 1000 : Date.now())).format(format);
      return time;
    }
  } else {
    return null;
  }
};

export const getTimestampFromDate = val => {
  return Date.parse(val) / 1000;
};

export const showAlertMessage = (msg, type) => {
  if (msg && msg != '') {
    let height = 0;
    if (msg.length > 80) {
      height = 90;
    } else if (msg.length > 41) {
      height = 70;
    } else {
      height: 40;
    }
    if (Platform.OS === 'android') {
      Alert.alert(msg);
    } else {
      showMessage({
        message: msg,
        type: type == 1 ? 'success' : 'failure',
        style: {padding: 12, height: height},
      });
    }
  }
};

export const handlingGPNumber = number => {
  let pattern = number
    .replace(/\s?/g, '')
    .replace(/(\d{3})/g, '$1-')
    .trim();
  return pattern.slice(0, -1);
};

export const handleDispatch = val => {
  val.props.navigation.dispatch(
    CommonActions.reset({
      index: 0,
      routes: [{name: 'MyTabs'}],
    }),
  );
};

export let navigationRef;

export const setNavigationRef = navigator => {
  navigationRef = navigator;
};

export function navigate(name, params) {
  navigationRef.navigate(name, params);
}

export function redirectToLink(url) {
  console.log('url to redirect is:-', url);
  if (url && url !== '') {
    Linking.canOpenURL(url).then(
      supported => {
        supported && Linking.openURL(url);
      },
      err => console.log(err),
    );
  }
}

//Methods for Localization
export const Locale = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key),
);

export const translationGetters = {
  en: () => EnglishLocale,
  th: () => ThaiLocale,
};

export const setI18nConfig = lang => {
  Locale.cache.clear();
  i18n.translations = {[lang ?? 'en']: translationGetters[lang ?? 'en']()};
  i18n.locale = lang ?? 'en';
};

export const withoutTime = dateTime => {
  var date = new Date(dateTime.getTime());
  date.setHours(0, 0, 0, 0);
  return date;
};
