import React, {useCallback, useState} from 'react';
import {Alert, Platform, StyleSheet, Text, View, Keyboard} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import COLOR from '../styles/Color';
import Styles from '../styles/Styles';
export function Dropdown(props) {
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  return (
    <DropDownPicker
      value={value}
      defaultValue={props.value}
      items={props.items ? props.items : null}
      searchableError={() => <Text>Not Found</Text>}
      setOpen={setOpen}
      open={open}
      setValue={setValue}
      dropDownDirection={'bottom'}
      maxHeight={300}
      zIndex={((props.zIndex ? props.zIndex : 1000): null)}
      disabled={props.disabled ? props.disabled : false}
      onOpen={() => {
        Keyboard.dismiss();
      }}
      style={[
        style.dropdown_style,
        props.dropdown_style,
        {
          ...(props.dropdown_style
            ? null
            : {borderBottomWidth: 1, borderBottomColor: COLOR.WHITE}),
        },
      ]}
      textStyle={[
        Styles.small_label,
        {
          color: COLOR.WHITE,
          marginLeft: props.dropdown_style ? 0 : -9,
        },
      ]}
      placeholderStyle={[Styles.body_label, {color: COLOR.WHITE}]}
      placeholder={
        props.value
          ? props.value != ''
            ? props.value
            : props.placeholder
          : props.placeholder
      }
      showTickIcon={false}
      arrowIconStyle={{tintColor: COLOR.WHITE}}
      searchable={false}
      dropDownContainerStyle={[style.dropDownContainerStyle]}
      onChangeValue={item => {
        props.handleDropdown(item, props.type);
      }}
      listMode="SCROLLVIEW"
      scrollViewProps={{
        nestedScrollEnabled: true,
      }}
    />
  );
}

const style = StyleSheet.create({
  dropdown_style: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderWidth: 0,
    height: Platform.OS == 'android' ? 40 : 45,
    color: COLOR.WHITE,
  },
  container_style: {
    color: COLOR.WHITE,
  },
  dropDownContainerStyle: {
    color: COLOR.BLACK,
    backgroundColor: COLOR.DARK_BLUE,
    borderColor: 'transparent',
    borderWidth: 0,
    paddingHorizontal: 10,
    marginBottom: 15,
  },
});
