import types from '../Types';
import {showAlertMessage} from '../../commonView/Helpers';
import API from '../../api/Api';
import {Alert} from 'react-native';

let api = new API();

const getMemberData = val => {
  return async (dispatch, getStore) => {
    let memberData = await getStore().businessReducer.membersData;
    if (!memberData || val == 1) {
      try {
        api
          .getMember()
          .then(json => {
            console.log('business detail is:-', json);
            if (json.data.status == 200) {
              console.log('json data.response', json.data.response);
              dispatch({
                type: types.MEMBER_DATA,
                data: json.data.response,
              });
            } else {
              showAlertMessage(json.data.message);
            }
          })
          .catch(function (error) {
            showAlertMessage(error.response.data.message);
          });
      } catch (error) {
        console.log('error getting user business detail');
      }
    } else {
      return dispatch({
        type: types.MEMBER_DATA,
        data: memberData,
      });
    }
  };
};

const getBusinessHourData = val => {
  return async (dispatch, getStore) => {
    let hourData = await getStore().businessReducer.businessHourData;
    if (!hourData || val == 1) {
      try {
        api
          .getBusinessHours()
          .then(json => {
            console.log('business hour data is:-', json);
            if (json.data.status == 200) {
              console.log('json data.response', json.data.response);
              dispatch({
                type: types.BUSINESS_HOUR_DATA,
                data: json.data.response,
              });
            } else {
              //showAlertMessage(json.data.message);
            }
          })
          .catch(function (error) {
            showAlertMessage(error.response.data.message);
          });
      } catch (error) {
        console.log('error getting user business detail');
      }
    } else {
      return dispatch({
        type: types.BUSINESS_HOUR_DATA,
        data: hourData,
      });
    }
  };
};

const getPaymentMethod = val => {
  return async (dispatch, getStore) => {
    let cardData = await getStore().businessReducer.paymentCardData;
    if (!cardData || val == 1) {
      try {
        api
          .getPaymentData()
          .then(json => {
            console.log('business hour data is:-', json);
            if (json.data.status == 200) {
              console.log('json data.response', json.data.response);
              dispatch({
                type: types.PAYMENT_METHODS,
                data: json.data.response,
              });
            } else {
              //showAlertMessage(json.data.message);
            }
          })
          .catch(function (error) {
            showAlertMessage(error.response.data.message);
          });
      } catch (error) {
        console.log('error getting user business detail');
      }
    } else {
      return dispatch({
        type: types.PAYMENT_METHODS,
        data: cardData,
      });
    }
  };
};

export default {
  getMemberData,
  getBusinessHourData,
  getPaymentMethod,
};
