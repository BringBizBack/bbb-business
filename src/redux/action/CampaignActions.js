import types from '../Types';
import {showAlertMessage} from '../../commonView/Helpers';
import API from '../../api/Api';
import {Alert} from 'react-native';

let api = new API();

const getCampaignData = (type, val, city, sort) => {
  return async (dispatch, getStore) => {
    let campaignDetail = null;
    if (type == 2) {
      campaignDetail = await getStore().campaignReducer.currentCampaign;
    } else if (type == 3) {
      campaignDetail = await getStore().campaignReducer.pastCampaign;
    } else if (type == 4) {
      campaignDetail = await getStore().campaignReducer.future;
    }
    if (!campaignDetail || val == 1) {
      try {
        api
          .getCampaign(
            JSON.stringify({
              type: type,
              nation: 'Thailand',
              city: city,
              sort: sort,
            }),
          )
          .then(json => {
            console.log('campaign data is:-', json, type);
            if (json.data.status == 200) {
              if (type == 2) {
                return dispatch({
                  type: types.CURRENT_CAMPAIGNS,
                  data: json.data.response,
                });
              } else if (type == 3) {
                return dispatch({
                  type: types.PAST_CAMPAIGNS,
                  data: json.data.response,
                });
              } else if (type == 4) {
                return dispatch({
                  type: types.FUTURE_CAMPAIGNS,
                  data: json.data.response,
                });
              }
            } else {
              if (json.data.message) {
                showAlertMessage(json.data.message);
              }
            }
          })
          .catch(function (error) {
            console.log('error getting campaign', error);
          });
      } catch (error) {
        console.log('error getting campaign detail');
      }
    } else {
      if (type == 2) {
        return dispatch({
          type: types.CURRENT_CAMPAIGNS,
          data: campaignDetail,
        });
      } else if (type == 3) {
        return dispatch({
          type: types.PAST_CAMPAIGNS,
          data: campaignDetail,
        });
      } else if (type == 4) {
        return dispatch({
          type: types.FUTURE_CAMPAIGNS,
          data: campaignDetail,
        });
      }
    }
  };
};

const getTemplatenData = val => {
  return async (dispatch, getStore) => {
    let templateDetail = await getStore().campaignReducer.templateData;
    if (!templateDetail || val == 1) {
      try {
        api
          .getTemplates()
          .then(json => {
            console.log('tes data is:-', json);
            if (json.data.status == 200) {
              console.log('json data.response', json.data.response);
              return dispatch({
                type: types.TEMPLATE_DETAIL,
                data: json.data.response,
              });
            } else {
              showAlertMessage(json.data.message);
            }
          })
          .catch(function (error) {
            showAlertMessage(error.response.data.message);
          });
      } catch (error) {
        console.log('error getting campaign detail');
      }
    } else {
      return dispatch({
        type: types.TEMPLATE_DETAIL,
        data: templateDetail,
      });
    }
  };
};

export default {
  getCampaignData,
  getTemplatenData,
};
