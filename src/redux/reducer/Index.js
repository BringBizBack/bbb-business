import {combineReducers} from 'redux';
import {Alert} from 'react-native';
import AuthenticationReducer from './AuthenticationReducer';
import BusinessReducer from './BusinessReducer';
import CampaignReducer from './CampaignReducer';

const appReducer = combineReducers({
  authReducer: AuthenticationReducer,
  businessReducer: BusinessReducer,
  campaignReducer: CampaignReducer,
});

const rootReducer = (state, action) => {
  if (action.type === 'USER_LOGOUT') {
    state = undefined;
  }
  return appReducer(state, action);
};

export default rootReducer;
