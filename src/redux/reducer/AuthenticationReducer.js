import types from '../Types';

let newState = {
  userDetail: null,
  userBusinessDetail: null,
  userNotification: null,
  businessImages: null,
};

let cloneObject = function (obj) {
  return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
  switch (action.type) {
    case types.USER_DETAIL:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          userDetail: action.data,
        });
      }
      return newState;
    case types.USER_BUSINESS_DETAIL:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          userBusinessDetail: action.data,
        });
      }
      return newState;
    case types.USER_NOTIFICATION:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          userNotification: action.data,
        });
      }
      return newState;
    case types.USER_LOGOUT:
      newState = Object.assign({}, newState, {
        userDetail: null,
        userBusinessDetail: null,
        businessImages: null,
        userNotification: null,
      });
      return newState;
    case types.BUSINESS_IMAGES:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          businessImages: action.data,
        });
      }
      return newState;
    default:
      return state || newState;
  }
}
