import types from '../Types';

let newState = {
  membersData: null,
  businessHourData: null,
  paymentCardData: null,
};

let cloneObject = function (obj) {
  return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
  switch (action.type) {
    case types.MEMBER_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          membersData: action.data,
        });
      }
      return newState;
    case types.BUSINESS_HOUR_DATA:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          businessHourData: action.data,
        });
      }
      return newState;
    case types.PAYMENT_METHODS:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          paymentCardData: action.data,
        });
      }
      return newState;
    case types.USER_LOGOUT:
      newState = Object.assign({}, newState, {
        membersData: null,
        businessHourData: null,
        paymentCardData: null,
      });
      return newState;
    default:
      return state || newState;
  }
}
