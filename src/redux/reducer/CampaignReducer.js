import types from '../Types';

let newState = {
  currentCampaign: null,
  pastCampaign: null,
  futureCampaign: null,
  templateData: null,
};

let cloneObject = function (obj) {
  return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
  switch (action.type) {
    case types.CURRENT_CAMPAIGNS:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          currentCampaign: action.data,
        });
      }
      return newState;
    case types.PAST_CAMPAIGNS:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          pastCampaign: action.data,
        });
      }
      return newState;
    case types.FUTURE_CAMPAIGNS:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          futureCampaign: action.data,
        });
      }
      return newState;
    case types.TEMPLATE_DETAIL:
      newState = cloneObject(state);
      if (action.data) {
        newState = Object.assign({}, newState, {
          templateData: action.data,
        });
      }
      return newState;
    case types.USER_LOGOUT:
      newState = Object.assign({}, newState, {
        currentCampaign: null,
        pastCampaign: null,
        futureCampaign: null,
        templateData: null,
      });
      return newState;
    default:
      return state || newState;
  }
}
