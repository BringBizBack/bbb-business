/*
 * Copyright (c) 2011-2018, Zingaya, Inc. All rights reserved.
 */

'use strict';

import {StyleSheet} from 'react-native';
import COLOR from './Color';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import FONTS from './Fonts';

export default StyleSheet.create({
  //UIView
  container: {
    flex: 1,
  },
  line_view: {
    borderBottomColor: '#e8e8e8',
    width: '100%',
    alignSelf: 'center',
    borderBottomWidth: 1,
  },
  yellow_view: {
    height: 30,
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLOR.YELLOW,
    paddingHorizontal: 15,
  },

  //UIBUttons
  button_font: {
    fontSize: FONTS.MEDIUM,
    fontWeight: '500',
    fontFamily: FONTS.FAMILY_REGULAR,
    alignSelf: 'center',
    color: COLOR.BLACK,
  },
  button_content: {
    height: 50,
    width: wp(90),
    backgroundColor: COLOR.GREEN,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  back_button: {
    height: 45,
    width: 35,
    alignItems: 'center',
    justifyContent: 'center',
  },

  //UILabel
  heading_label: {
    fontSize: FONTS.LARGE,
    fontWeight: '600',
    fontFamily: FONTS.FAMILY_REGULAR,
    color: COLOR.BLACK,
  },
  subheading_label: {
    fontSize: FONTS.HEADING,
    fontWeight: '500',
    fontFamily: FONTS.FAMILY_REGULAR,
    color: COLOR.BLACK,
  },
  body_label: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.REGULAR,
    fontWeight: '400',
    color: COLOR.BLACK,
  },
  bold_body_label: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.REGULAR,
    fontWeight: '500',
    color: COLOR.BLACK,
  },
  small_label: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.SMALL,
    color: COLOR.LIGHT_TEXT,
  },
  extra_small_label: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.EXTRA_SMALL,
    color: COLOR.BLACK,
  },

  //UIText field
  text_input_text: {
    color: COLOR.BLACK,
    marginLeft: -15,
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: FONTS.REGULAR,
  },
  text_input_heading: {
    fontSize: FONTS.SMALL,
    marginLeft: -15,
    fontFamily: FONTS.FAMILY_REGULAR,
    color: COLOR.LIGHT_TEXT,
  },

  //UIImage
  backgroundImage: {
    flex: 1,
    width: null,
    height: null,
    overflow: 'hidden',
  },
  shadow_view: {
    shadowColor: COLOR.GRAY,
    shadowOffset: {width: 0.2, height: 0.2},
    shadowOpacity: 0.2,
    elevation: 4,
  },

  mono_view: {
    width: '90%',
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
  },

  flatlist_leader_row: {
    height: 30,
    width: 30,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLOR.LIGHT_BLUE,
  },
  flatlist_leaderboard_view: {
    marginBottom: 10,
    marginHorizontal: 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  left_corner_view: {
    height: 25,
    borderTopRightRadius: 12.5,
    borderBottomRightRadius: 12.5,
    marginLeft: 0,
    backgroundColor: COLOR.WHITE,
    justifyContent: 'center',
    alignSelf: 'flex-start',
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  right_corner_view: {
    height: 25,
    borderTopLeftRadius: 12.5,
    borderBottomLeftRadius: 12.5,
    marginRight: 0,
    backgroundColor: COLOR.WHITE,
    justifyContent: 'center',
    alignSelf: 'flex-start',
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
});
