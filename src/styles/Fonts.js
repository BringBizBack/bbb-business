'use strict';
import {Dimensions} from 'react-native';
const {width} = Dimensions.get('window');

const FONTS = {
  FAMILY_BOLD: 'Poppins-Bold',
  FAMILY_SEMIBOLD: 'Poppins-SemiBold',
  FAMILY_MEDIUM: 'Poppins-Medium',
  FAMILY_REGULAR: 'Poppins-Regular',
  LARGE: 22,
  HEADING: 18,
  MEDIUM: 16,
  REGULAR: 14,
  SMALL: 13,
  EXTRA_SMALL: 12,
};

// function fontSizer(type, width) {
//   if (width > 400) {
//     switch (type) {
//       case 1:
//         return 22;
//         break;
//       case 2:
//         return 26;
//         break;
//       case 3:
//         return 18;
//         break;
//       case 4:
//         return 16;
//         break;
//       case 5:
//         return 13;
//         break;
//       case 6:
//         return 22;
//       case 7:
//         return 12;
//         break;
//       default:
//         break;
//     }
//   } else if (width > 250) {
//     switch (type) {
//       case 1:
//         return 22;
//         break;
//       case 2:
//         return 24;
//         break;
//       case 3:
//         return 18;
//         break;
//       case 4:
//         return 14;
//         break;
//       case 5:
//         return 13;
//       case 6:
//         return 20;
//         break;
//       case 7:
//         return 12;
//         break;
//       default:
//         break;
//     }
//   } else {
//     switch (type) {
//       case 1:
//         return 22;
//         break;
//       case 2:
//         return 22;
//         break;
//       case 3:
//         return 16;
//         break;
//       case 4:
//         return 14;
//         break;
//       case 5:
//         return 13;
//         break;
//       case 6:
//         return 20;
//       case 7:
//         return 12;
//         break;
//       default:
//         break;
//     }
//   }
// }

export default FONTS;
