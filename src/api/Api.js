import axios from 'axios';
import Auth from '../auth/index';
import {
  AS_USER_TOKEN,
  U_ADD_BUSINESS_HOUR,
  U_ADD_CAMPAIGN,
  U_ADD_MEMBER,
  U_ADD_PAYMENT_METHOD,
  U_BASE,
  U_COMPLETE_PROFILE,
  U_DELETE_BUSINESS_IMAGE,
  U_DELETE_BUSINESS_IMAGES,
  U_FORGOT_PASS,
  U_GENERATE_OTP,
  U_GET_ALL_TEMPLATE,
  U_GET_BUSINESS_HOUR,
  U_GET_BUSINESS_IMAGES,
  U_GET_CAMPAIGN,
  U_GET_CAMPAIGN_FOR_REDEEM,
  U_GET_CAMPAIGN_LEADERBOARD,
  U_GET_COMPLETE_PROFILE,
  U_GET_CREDIT_HISTORY,
  U_GET_CREDIT_REQUEST,
  U_GET_DISTRICT,
  U_GET_MEMBER,
  U_GET_NATION,
  U_GET_NOTIFICATION,
  U_GET_PAYMENT_METHOD,
  U_GET_PROVINCES,
  U_GET_REDEEM_REQUEST,
  U_GET_SUB_DISTRICT,
  U_GIVE_CREDIT,
  U_GIVE_CREDIT_TO_REQUEST,
  U_GIVE_REDEEM_TO_REQUEST,
  U_LOGIN,
  U_LOGOUT,
  U_REDEEM_CREDIT,
  U_REMOVE_CAMPAIGN,
  U_REMOVE_CARD,
  U_REMOVE_MEMBER,
  U_RESET_PASS,
  U_SEARCH_USER,
  U_SET_AS_DEFAULT_CARD,
  U_SIGNUP,
  U_SUBMIT_BUSINESS_FOR_APPROVAL,
  U_UPDATE_LEGAL_INFO,
  U_UPDATE_TRADE_INFO,
  U_VERIFY_OTP,
} from '../commonView/Constants';

const headers = async () => {
  const headers = {
    'Content-Type': 'application/json',
  };

  const auth = new Auth();
  const token = await auth.getValue(AS_USER_TOKEN);

  await console.log('user token is:-', token);
  if (token) {
    headers.Authorization = `Bearer ${token}`;
  }
  return headers;
};

const request = async (method, path, body) => {
  const url = `${U_BASE}${path}`;
  const options = {method, url, headers: await headers()};
  if (body) {
    options.data = body;
  }
  console.log('options are:-', options, body);

  return axios(options);
};

export default class API {
  loginAPI(data) {
    return request('POST', U_LOGIN, data);
  }

  logoutUser(data) {
    return request('GET', U_LOGOUT + data);
  }

  signupAPI(data) {
    return request('POST', U_SIGNUP, data);
  }

  getUserDetail() {
    return request('GET', U_GET_COMPLETE_PROFILE);
  }

  getBusinessImages() {
    return request('GET', U_GET_BUSINESS_IMAGES);
  }

  getUserNotification() {
    return request('GET', U_GET_NOTIFICATION);
  }

  forgotPassword(data) {
    return request('POST', U_FORGOT_PASS, data);
  }

  resetPassword(data) {
    return request('POST', U_RESET_PASS, data);
  }

  generateOtp(data) {
    return request('POST', U_GENERATE_OTP + data);
  }

  verifyOtp(data) {
    return request('POST', U_VERIFY_OTP, data);
  }

  addCamapaign(data) {
    return request('POST', U_ADD_CAMPAIGN, data);
  }

  getCampaign(data) {
    return request('POST', U_GET_CAMPAIGN, data);
  }

  getCampaignCreditRequest(data) {
    return request('GET', U_GET_CREDIT_REQUEST);
  }

  getCampaignRedeemRequest(data) {
    return request('GET', U_GET_REDEEM_REQUEST);
  }

  addBusinessHours(data) {
    return request('POST', U_ADD_BUSINESS_HOUR, data);
  }

  getBusinessHours() {
    return request('GET', U_GET_BUSINESS_HOUR);
  }

  getTemplates() {
    return request('GET', U_GET_ALL_TEMPLATE);
  }

  addMember(data) {
    return request('POST', U_ADD_MEMBER, data);
  }

  removeMember(data) {
    return request('POST', U_REMOVE_MEMBER + data);
  }

  searchUserRecord(data) {
    return request('POST', U_SEARCH_USER, data);
  }

  getCampaingForRedeem(data) {
    return request('POST', U_GET_CAMPAIGN_FOR_REDEEM, data);
  }

  giveCreditCustomer(data, type, customerRequest) {
    if (customerRequest) {
      return request(
        'GET',
        type == 2
          ? U_GIVE_REDEEM_TO_REQUEST + data
          : U_GIVE_CREDIT_TO_REQUEST + data,
      );
    } else {
      return request('POST', type == 2 ? U_REDEEM_CREDIT : U_GIVE_CREDIT, data);
    }
  }

  getMember() {
    return request('GET', U_GET_MEMBER);
  }

  addPaymentMethod(data) {
    return request('POST', U_ADD_PAYMENT_METHOD, data);
  }

  getPaymentData() {
    return request('GET', U_GET_PAYMENT_METHOD);
  }

  handlePaymentMethod(id, type) {
    if (type == 1) {
      return request('GET', U_REMOVE_CARD + id);
    } else {
      return request('GET', U_SET_AS_DEFAULT_CARD + id);
    }
  }

  getRedeemCreditHistoryCampaign(data) {
    return request('POST', U_GET_CREDIT_HISTORY, data);
  }

  getProvince(type, id) {
    if (type == 1) {
      return request('GET', U_GET_PROVINCES + (id ?? 1));
    } else if (type == 2) {
      return request('GET', U_GET_DISTRICT + id);
    } else if (type == 3) {
      return request('GET', U_GET_SUB_DISTRICT + id);
    } else if (type == 8) {
      return request('GET', U_GET_NATION);
    }
  }

  updatePersonalDetail(data, type) {
    if (type == 1) {
      return request('POST', U_COMPLETE_PROFILE, data);
    } else if (type == 2) {
      return request('POST', U_UPDATE_LEGAL_INFO, data);
    } else {
      return request('POST', U_UPDATE_TRADE_INFO, data);
    }
  }

  submitBusinessForApproval() {
    return request('GET', U_SUBMIT_BUSINESS_FOR_APPROVAL);
  }

  deleteBusinessImage(data) {
    return request('DELETE', U_DELETE_BUSINESS_IMAGE, data);
  }

  getCampaignLeaderboard(data) {
    return request('GET', U_GET_CAMPAIGN_LEADERBOARD + data);
  }

  deletePendingCampaign(data) {
    return request('DELETE', U_REMOVE_CAMPAIGN + data);
  }
}
