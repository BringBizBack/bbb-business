import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  ScrollView,
  FlatList,
  Alert,
} from 'react-native';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import NavigationBar from '../../commonView/NavigationBar';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import BottomView from '../../commonView/BottomView';
import {TopWhiteView} from './TopWhiteView';
import {Pages} from 'react-native-pages';
import FastImage from 'react-native-fast-image';
import {
  handlingGPNumber,
  insertComma,
  Locale,
  showAlertMessage,
} from '../../commonView/Helpers';
import API from '../../api/Api';
import database from '@react-native-firebase/database';
import AuthenticationAction from '../../redux/action/AuthenticationAction';

import {connect} from 'react-redux';

let api = new API();

class LeaderboardScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      leaderboardData: null,
      isTabbar: false,
      campaignDetail: null,
      userDetail: null,
      userRanking: null,
    };
    this.getLeaderboardDate = this.getLeaderboardDate.bind(this);
    this.getWinnerIndex = this.getWinnerIndex.bind(this);
  }

  componentDidMount(props) {
    this.handleRealtimeDB();
    if (this.props.route.params) {
      if (this.props.route.params.tabScreen != null) {
        this.setState(
          {
            isTabbar: this.props.route.params.tabScreen,
            campaignDetail: this.props.route.params.data,
          },
          () => {
            this.getLeaderboardDate();
          },
        );
      }
    }
  }

  handleRealtimeDB() {
    let that = this;
    database()
      .ref('campaigns/')
      .on('value', snapshot => {
        setTimeout(function () {
          let campaign = that.state.campaignDetail;
          if (campaign && campaign.length && snapshot.hasChildren()) {
            snapshot.forEach((childSnapshot, i) => {
              if (campaign.id == childSnapshot.key) {
                that.getLeaderboardDate();
              }
            });
          }
        }, 1000);
      });
  }

  getLeaderboardDate() {
    try {
      console.log('campaign detail is:-', this.state.campaignDetail);
      api
        .getCampaignLeaderboard(this.state.campaignDetail.id)
        .then(json => {
          console.log('leaderboard data is:-', json);
          if (json.data.status == 200) {
            this.setState({leaderboardData: json.data.response});
            if (json.data.response.leaderboard) {
              let rank = null;
              let user = json.data.response.leaderboard.filter((val, index) => {
                if (val.id == this.props.userDetail.id) {
                  rank = index + 1;
                }
                return val.id == this.props.userDetail.id;
              });
              console.log('user is', user);
              if (user.length > 0) {
                user[0].rank = rank;
                this.setState({userDetail: user[0]});
              }
            }
          } else {
            showAlertMessage(json.data.message);
          }
        })
        .catch(function (error) {
          showAlertMessage(error.response.data.message);
        });
    } catch (error) {
      console.log('error getting user business detail');
    }
  }

  commonView = props => {
    return (
      <View>
        <TouchableWithoutFeedback
          onPress={() => this.handleRedirection(props.id)}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              paddingHorizontal: 5,
            }}>
            <Image
              source={props.image}
              style={{height: 40, width: 40}}
              resizeMode={'cover'}
            />
            <View style={{flex: 1, marginHorizontal: 15}}>
              <TouchableWithoutFeedback
                onPress={() =>
                  this.props.navigation.navigate('PrizeDetailScreen')
                }>
                <Text
                  style={[
                    Styles.small_label,
                    {color: COLOR.BLACK, alignSelf: 'flex-start'},
                  ]}>
                  {Locale('Terms & Conditions')}
                </Text>
              </TouchableWithoutFeedback>
              <Text
                style={[
                  Styles.bold_body_label,
                  {
                    alignSelf: 'flex-start',
                  },
                ]}>
                {props.title}
              </Text>
              {props.description && (
                <Text
                  style={[
                    Styles.small_label,
                    {color: COLOR.BLACK, alignSelf: 'flex-start'},
                  ]}>
                  {props.description}
                </Text>
              )}
            </View>
            <Image
              source={IMAGES.keyboard_arrow_right}
              style={{height: 12, width: 12}}
              resizeMode={'contain'}
            />
          </View>
        </TouchableWithoutFeedback>
        {props.showLine && (
          <View
            style={[
              Styles.line_view,
              {
                borderBottomWidth: 0.3,
                marginVertical: 15,
                borderBottomColor: COLOR.DARK_BLUE,
              },
            ]}
          />
        )}
      </View>
    );
  };

  handleRedirection = id => {
    switch (id) {
      case 1:
        this.props.navigation.navigate('PrizeDetailScreen');
        break;
      case 2:
        this.props.navigation.navigate('CampaignRules');
        break;
      default:
        break;
    }
  };

  getWinnerIndex(data) {
    console.log('this.state.campaignDetail', this.state.campaignDetail);
    if (data) {
      if (!this.state.campaignDetail) {
        return 0;
      } else if (
        this.state.campaignDetail &&
        this.state.campaignDetail.isFinished == 0
      ) {
        if (
          this.state.campaignDetail.endTime <
          parseInt(new Date().getTime() / 1000)
        ) {
          return 0;
        } else {
          let winnerCount = (data.leaderboard.length * data.winners) / 100;
          return Math.ceil(winnerCount);
        }
      } else {
        let winnerCount = (data.leaderboard.length * data.winners) / 100;
        return Math.ceil(winnerCount);
      }
    } else {
      return 0;
    }
  }

  render() {
    const {isTabbar, campaignDetail, leaderboardData} = this.state;
    return (
      <View style={[Styles.container, {backgroundColor: COLOR.DARK_BLUE}]}>
        <SafeAreaView style={Styles.container}>
          <NavigationBar title={Locale('   Goal Prize\n   Leaderboard')} />
          <ScrollView>
            <View style={[Styles.shadow_view, style.main_view]}>
              <TouchableWithoutFeedback>
                <TopWhiteView
                  image={
                    campaignDetail
                      ? campaignDetail.trading
                        ? campaignDetail.trading.logo
                        : ''
                      : ''
                  }
                  name={
                    campaignDetail
                      ? campaignDetail.trading
                        ? campaignDetail.trading.name
                        : ''
                      : ''
                  }
                  address={
                    campaignDetail
                      ? campaignDetail.trading
                        ? campaignDetail.trading.address
                        : ''
                      : ''
                  }
                  isTabbar={isTabbar}
                />
              </TouchableWithoutFeedback>
            </View>
            <View style={{height: 150, alignSelf: 'center', width: '100%'}}>
              {campaignDetail ? (
                campaignDetail.trading.images ? (
                  campaignDetail.trading.images.length > 0 && (
                    <View style={{flex: 1, width: '100%', height: '100%'}}>
                      <Pages
                        indicatorColor={COLOR.LIGHT_BLUE}
                        indicatorOpacity={0.2}>
                        {campaignDetail.trading.images.map(element => {
                          return (
                            <FastImage
                              resizeMode={FastImage.resizeMode.cover}
                              style={{
                                overflow: 'hidden',
                                width: '100%',
                                alignSelf: 'center',
                                height: '100%',
                              }}
                              source={{
                                uri: element.image,
                                priority: FastImage.priority.high,
                              }}
                            />
                          );
                        })}
                      </Pages>
                    </View>
                  )
                ) : (
                  <View />
                )
              ) : (
                <View />
              )}
            </View>
            <View
              style={[
                Styles.shadow_view,
                style.shadowView,
                {
                  marginTop: -35,
                  paddingHorizontal: 0,
                  paddingVertical: 10,
                },
              ]}>
              <View
                style={{
                  alignItem: 'center',
                  flexDirection: 'row',
                  marginBottom: 10,
                  justifyContent: 'space-between',
                }}>
                <Text
                  style={[
                    Styles.subheading_label,
                    {
                      marginHorizontal: 10,
                      color: COLOR.WHITE,
                      marginTop: 10,
                    },
                  ]}>
                  {Locale('Leaderboard')}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  height: 40,
                  width: wp(90),
                  alignSelf: 'center',
                  backgroundColor: COLOR.DARK_BLUE,
                  alignItems: 'center',
                  paddingHorizontal: 10,
                }}>
                <Text
                  style={[
                    Styles.extra_small_label,
                    style.text_one,
                    {width: 45},
                  ]}>
                  {Locale('RANK')}
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    style.text_one,
                    {textAlign: 'left', flex: 1},
                  ]}>
                  {Locale('PARTICIPATING SPENDERS')}
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    style.text_one,
                    {color: COLOR.WHITE, width: 100},
                  ]}>
                  {Locale('TOTAL SPENT')}
                </Text>
              </View>
              <FlatList
                style={style.flatlist_view}
                data={
                  this.state.leaderboardData
                    ? this.state.leaderboardData.leaderboard
                    : ''
                }
                renderItem={({item, index}) => (
                  <TouchableWithoutFeedback
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor:
                        this.getWinnerIndex(this.state.leaderboardData) != 0
                          ? index <
                            this.getWinnerIndex(this.state.leaderboardData)
                            ? COLOR.GREEN
                            : 'transparent'
                          : 'transparent',
                    }}>
                    <View
                      style={[
                        Styles.shadow_view,
                        {
                          width: '100%',

                          alignItems: 'center',
                          justifyContent: 'center',
                          paddingHorizontal: 15,
                          marginTop: 10,
                        },
                      ]}>
                      <View style={[Styles.flatlist_leaderboard_view]}>
                        <View style={Styles.flatlist_leader_row}>
                          <Text
                            style={[
                              Styles.bold_body_label,
                              {color: COLOR.WHITE},
                            ]}>
                            {index + 1}
                          </Text>
                        </View>
                        <View
                          style={{
                            margin: 0,
                            flex: 1,
                            marginLeft: 15,
                          }}>
                          <Text
                            style={[
                              Styles.button_font,
                              {
                                color: COLOR.WHITE,
                                margin: 0,
                                alignSelf: 'flex-start',
                              },
                            ]}>
                            {item.bbb_id ? handlingGPNumber(item.bbb_id) : ''}
                          </Text>
                          {item.emailPrivate == false && (
                            <Text
                              style={[
                                Styles.extra_small_label,
                                {
                                  color: COLOR.WHITE,
                                  margin: 0,
                                  alignSelf: 'flex-start',
                                },
                              ]}>
                              {Locale('Email:')}{' '}
                              {item.email
                                ? item.email.substring(0, 5) + '***'
                                : ''}
                            </Text>
                          )}
                        </View>
                        <View style={{margin: 0}}>
                          <Text
                            style={[
                              Styles.button_font,
                              {
                                margin: 0,
                                color: COLOR.WHITE,
                                fontWeight: '600',
                              },
                            ]}>
                            {item.totalCredits
                              ? insertComma(item.totalCredits)
                              : '0'}{' '}
                            THB
                          </Text>
                        </View>
                      </View>
                    </View>
                  </TouchableWithoutFeedback>
                )}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          </ScrollView>

          {!isTabbar && <BottomView />}
        </SafeAreaView>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    paddingVertical: 15,
    paddingHorizontal: 15,
    width: wp(90),
    alignSelf: 'center',
  },
  flatlist_view: {
    marginVertical: 0,
    width: wp(90),
    marginHorizontal: 0,
    alignSelf: 'center',
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    paddingVertical: 15,
    paddingHorizontal: 0,
  },
  date_yellow_view: {
    height: 25,
    borderTopLeftRadius: 12.5,
    borderBottomLeftRadius: 12.5,
    marginTop: 10,
    marginRight: 0,
    backgroundColor: COLOR.YELLOW,
    justifyContent: 'center',
    alignSelf: 'flex-start',
  },
  common_view: {
    flex: 1,
    backgroundColor: COLOR.MEDIUM_BLUE,
    borderRadius: 10,
    padding: 7,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text_one: {
    color: COLOR.WHITE,
    textAlign: 'center',
    fontWeight: '500',
  },
  text_two: {
    color: COLOR.WHITE,
    textAlign: 'center',
    alignSelf: 'center',
    fontWeight: '700',
    marginVertical: 5,
  },
  text_three: {
    color: COLOR.WHITE,
    fontSize: 10,
    textAlign: 'center',
  },
  main_view: {
    marginTop: 10,
    marginBottom: 0,
    paddingVertical: 10,
    width: wp(100),
    backgroundColor: COLOR.WHITE,
    alignSelf: 'center',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.authReducer.userInfo,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserInfo: val => {
      dispatch(AuthenticationAction.getUserDetail(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LeaderboardScreen);
