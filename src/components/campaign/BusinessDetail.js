import React from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  ScrollView,
  Text,
  Linking,
} from 'react-native';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import NavigationBar from '../../commonView/NavigationBar';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import FONTS from '../../styles/Fonts';
import LinearGradient from 'react-native-linear-gradient';
import {TopWhiteView} from './TopWhiteView';
import {Pages} from 'react-native-pages';
import FastImage from 'react-native-fast-image';
import {getDateFromTimeStamp, Locale} from '../../commonView/Helpers';

export default class BusinessDetail extends React.Component {
  constructor() {
    super();
    this.state = {
      isTabbar: false,
      campaignDetail: null,
      weekName: ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'],
    };

    this.getTimeSlots = this.getTimeSlots.bind(this);
    this.handleLinkingButton = this.handleLinkingButton.bind(this);
  }

  componentDidMount(props) {
    if (this.props.route.params) {
      if (this.props.route.params.tabScreen != null) {
        this.setState({
          isTabbar: this.props.route.params.tabScreen,
          campaignDetail: this.props.route.params.data,
        });

        console.log('capaign timing is:', this.props.route.params.data);
      }
    }
  }

  handleLinkingButton(val, id) {
    if (id == 4) {
      Linking.openURL(`tel:${val}`);
    } else if (id == 5) {
      Linking.canOpenURL(val).then(supported => {
        if (supported) {
          Linking.openURL(val);
        }
      });
    }
  }

  commonView = props => {
    return (
      <View>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Image
            source={props.image}
            style={{height: 25, width: 25}}
            resizeMode={'cover'}
          />
          <View style={{flex: 1, marginHorizontal: 15}}>
            <Text
              style={[
                Styles.bold_body_label,
                {
                  color: COLOR.BLACK,
                  fontFamily: FONTS.FAMILY_MEDIUM,
                },
              ]}>
              {props.heading}
            </Text>
            <Text
              style={[
                Styles.extra_small_label,
                {color: COLOR.LIGHT_TEXT, marginTop: 5},
              ]}>
              {props.description}
            </Text>
          </View>
          <TouchableWithoutFeedback
            onPress={() =>
              this.handleLinkingButton(props.description, props.id)
            }>
            <Text style={[Styles.extra_small_label, style.underline_label]}>
              {props.buttonLabel}
            </Text>
          </TouchableWithoutFeedback>
        </View>
        <View
          style={[
            Styles.line_view,
            {
              marginVertical: 15,
            },
          ]}
        />
      </View>
    );
  };

  getTimeSlots() {
    let campaign = this.state.campaignDetail;
    if (campaign && campaign.days) {
      let timeslot = '';
      for (let i = 0; i < campaign.days.length; i++) {
        timeslot = timeslot + '\n' + `${this.state.weekName[i]} - `;
        if (campaign.days[i].length == 0) {
          timeslot = timeslot + 'CLOSED';
        }
        for (let j = 0; j < campaign.days[i].length; j++) {
          if (campaign.days[i][j] && campaign.days[i][j].is_opened) {
            let format = `${getDateFromTimeStamp(
              campaign.days[i][j].opening,
              'h:mm a',
            )}-${getDateFromTimeStamp(campaign.days[i][j].closing, 'h:mm a')},`;
            timeslot = timeslot + (format ? format : 'CLOSED');
          } else {
            timeslot = timeslot + 'closed';
          }
        }
      }
      return timeslot;
    } else {
      return (
        'MON-THUR. 11 AM TO 9 PM\n' +
        'FRI-SAT 11 AM TO 11:30 PM\n' +
        'SUNDAY: 9 AM TO 9 PM'
      );
    }
  }

  render() {
    const {isTabbar, campaignDetail} = this.state;
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <NavigationBar title={Locale('K_LOCATION_HOURS')} />
            <ScrollView>
              <View style={[Styles.shadow_view, style.main_view]}>
                <TopWhiteView
                  image={campaignDetail ? campaignDetail.logo : null}
                  name={
                    campaignDetail
                      ? campaignDetail.name
                      : "Archie's Bar & Grill"
                  }
                  address={
                    campaignDetail
                      ? campaignDetail.address
                      : Locale('hours & info')
                  }
                  isTabbar={isTabbar}
                />
              </View>
              <View
                style={{
                  height: 150,
                  alignSelf: 'center',
                  width: '100%',
                  borderRadius: 10,
                }}>
                {campaignDetail ? (
                  campaignDetail.images.length > 0 && (
                    <View
                      style={{
                        flex: 1,
                        width: '90%',
                        height: '100%',
                        alignSelf: 'center',
                      }}>
                      <Pages
                        indicatorColor={COLOR.LIGHT_BLUE}
                        indicatorOpacity={0.2}>
                        {campaignDetail &&
                          campaignDetail.images.map(element => {
                            return (
                              <FastImage
                                resizeMode={FastImage.resizeMode.cover}
                                style={{
                                  overflow: 'hidden',
                                  width: '100%',
                                  alignSelf: 'center',
                                  height: '100%',
                                }}
                                source={{
                                  uri: element.image,
                                  priority: FastImage.priority.high,
                                }}
                              />
                            );
                          })}
                      </Pages>
                    </View>
                  )
                ) : (
                  <Image
                    style={{width: '100%', height: 150}}
                    source={IMAGES.main_banner}
                  />
                )}
              </View>
              <View style={[style.shadowView, Styles.shadow_view]}>
                <this.commonView
                  id={1}
                  image={IMAGES.surya_bulb}
                  heading={Locale('Style')}
                  description={
                    campaignDetail
                      ? campaignDetail.style
                      : 'Thai restaurant & bar'
                  }
                  buttonLabel={''}
                />
                <this.commonView
                  id={2}
                  image={IMAGES.direction}
                  heading={Locale('Location')}
                  description={
                    campaignDetail ? campaignDetail.address : 'kingdom palace'
                  }
                />
                <this.commonView
                  id={3}
                  image={IMAGES.alarm_clock}
                  heading={Locale('Hours')}
                  description={this.getTimeSlots()}
                  buttonLabel={''}
                />
                <this.commonView
                  id={4}
                  image={IMAGES.phone_blue}
                  heading={Locale('Phone')}
                  description={
                    campaignDetail ? campaignDetail.phone : '123-456-789'
                  }
                  buttonLabel={Locale('Call now')}
                />
                <this.commonView
                  id={5}
                  image={IMAGES.website}
                  heading={'Website'}
                  description={
                    campaignDetail ? campaignDetail.website : 'archies.com'
                  }
                  buttonLabel={Locale('Go to URL   ')}
                />

                <this.commonView
                  id={6}
                  image={IMAGES.specialization}
                  heading={Locale('Specials')}
                  description={
                    campaignDetail
                      ? campaignDetail.specials
                      : 'Karoake Saturday 8 pm to 11 pm Free billiards before 5 pm'
                  }
                  buttonLabel={''}
                />
              </View>
            </ScrollView>
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 10,
    padding: 15,
  },
  underline_label: {
    color: COLOR.MEDIUM_BLUE,
    textDecorationLine: 'underline',
    width: 90,
    textAlign: 'right',
  },
  main_view: {
    marginTop: 10,
    marginBottom: 0,
    paddingVertical: 10,
    width: wp(100),
    backgroundColor: COLOR.WHITE,
    alignSelf: 'center',
  },
  bottom_view: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    backgroundColor: '#00000050',
    paddingVertical: 5,
  },
});
