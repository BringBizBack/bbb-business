import React from 'react';
import {
  View,
  ImageBackground,
  SafeAreaView,
  StyleSheet,
  Image,
  ScrollView,
  Text,
} from 'react-native';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import NavigationBar from '../../commonView/NavigationBar';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import FONTS from '../../styles/Fonts';
import LinearGradient from 'react-native-linear-gradient';
import {Locale} from '../../commonView/Helpers';

export default class PrizeDetailScreen extends React.Component {
  constructor(ptops) {
    super();
    this.state = {
      isTabbar: false,
      campaignName: '',
      redirectionFromLogin: false,
    };
    this.updateMasterState = this.updateMasterState.bind(this);
  }

  componentDidMount(props) {
    if (this.props.route.params) {
      if (this.props.route.params.tabScreen != null) {
        this.setState({
          isTabbar: this.props.route.params.tabScreen,
          campaignName: this.props.route.params.campaignName
            ? this.props.route.params.campaignName
            : '',
        });
      }
    }
  }

  updateMasterState = (props, value) => {
    this.setState({
      [props]: value,
    });
  };

  render() {
    const {isTabbar} = this.state;
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <NavigationBar title={'Terms & Condition'} />
            <ScrollView>
              <View style={[style.shadowView, Styles.shadow_view]}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingHorizontal: 5,
                  }}>
                  <Image
                    source={IMAGES.trophy}
                    style={{height: 35, width: 35}}
                    resizeMode={'cover'}
                  />
                  <View style={{flex: 1, marginHorizontal: 15}}>
                    <Text
                      style={[
                        Styles.button_font,
                        {fontWeight: '500', alignSelf: 'flex-start'},
                      ]}>
                      {`Prize: ${this.state.campaignName}`}
                    </Text>
                    <Text
                      style={[Styles.extra_small_label, {fontWeight: '500'}]}>
                      {Locale('Terms and condition')}
                    </Text>
                  </View>
                </View>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      marginTop: 15,
                    },
                  ]}>
                  What is Prize?
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  The prize is a "digital gift card" in the form of credits you
                  can use at the same business hosting the GP campaign where you
                  won them until the expiration date.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                    },
                  ]}>
                  How much is the prize?
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  The prize equals what you spent at the business during the BBB
                  campaign time period not counting taxes and service charges
                  and no higher than the prize limit.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                    },
                  ]}>
                  How do i redeem the prize?
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  You register on this website to participate in the campaign so
                  we have a place to put your credits. If the Goal is reached
                  before the Campaign Deadline and you are a Winner then when
                  you visit the business after the Campaign has ended you can
                  use your credits to eliminate the amount on your bill before
                  the server charges and taxes.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  Is there a prize expiration date?
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  Yes. It is indicated on the Campaign Leaderboard
                </Text>
              </View>
            </ScrollView>
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 20,
    paddingVertical: 30,
    paddingHorizontal: 15,
  },
});
