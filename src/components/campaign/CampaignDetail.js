import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  ScrollView,
  Platform,
  Alert,
} from 'react-native';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import NavigationBar from '../../commonView/NavigationBar';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {TopWhiteView} from './TopWhiteView';
import FONTS from '../../styles/Fonts';
import FastImage from 'react-native-fast-image';
import {Pages} from 'react-native-pages';
import {
  getDateFromTimeStamp,
  insertComma,
  Locale,
  showAlertMessage,
} from '../../commonView/Helpers';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import database from '@react-native-firebase/database';
import AlertPopup from '../../commonView/AlertPopup';
import {
  AS_FCM_TOKEN,
  AS_INITIAL_ROUTE,
  AS_USER_DETAIL,
  AS_USER_TOKEN,
  K_CAMPAIGN_INFO,
  K_LEADERBOARD_INFO,
  K_PRIZE_INFO,
  K_WINNERS_INFO,
} from '../../commonView/Constants';
import {CustomButton} from '../../commonView/CustomButton';
import API from '../../api/Api';
import {CommonActions} from '@react-navigation/native';

let api = new API();

export default class CampaignDetail extends React.Component {
  constructor(props) {
    super();
    this.state = {
      campaignData: [],
      isTabbar: false,
      alertModalVisible: false,
      campaignDetail: null,
      title: '',
      description: '',
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.closeVerifyModal = this.closeVerifyModal.bind(this);
    this.handleCancelRequest = this.handleCancelRequest.bind(this);
  }

  componentDidMount() {
    this.handleRealtimeDB();
    if (this.props.route.params != null) {
      if (this.props.route.params.tabScreen) {
        console.log('campaign detail is:-', this.props.route.params.data);
        this.setState({
          isTabbar: this.props.route.params.tabScreen,
          campaignDetail: this.props.route.params.data,
        });
      }
    }
  }

  handleRealtimeDB() {
    let that = this;
    database()
      .ref('campaigns/')
      .on('value', snapshot => {
        setTimeout(function () {
          let campaign = that.state.campaignDetail;
          if (campaign && campaign.length && snapshot.hasChildren()) {
            snapshot.forEach((childSnapshot, i) => {
              if (campaign.id == childSnapshot.key) {
                campaign.currentVisits = childSnapshot.val().visits;
              }
            });
            that.setState({
              campaignDetail: campaign,
            });
          }
        }, 1000);
      });
  }

  updateMasterState = (props, value) => {
    this.setState({
      [props]: value,
    });
  };

  viewOne = props => {
    return (
      <TouchableWithoutFeedback
        onPress={() =>
          this.props.navigation.navigate('PrizeDetailScreen', {
            tabScreen: true,
            campaignName: this.state.campaignDetail
              ? this.state.campaignDetail.campaignTemplate
                ? this.state.campaignDetail.campaignTemplate.name
                : ''
              : '',
            redirectionFromLogin: this.state.redirectionFromLogin,
          })
        }>
        <View
          style={{
            flexDirection: 'row',
            alignSelf: 'center',
          }}>
          <View style={style.count_view}>
            <Text style={[Styles.subheading_label, {textAlign: 'center'}]}>
              {props.count}
            </Text>
          </View>
          <View
            style={{
              borderWidth: 1,
              borderColor: COLOR.BLACK,
              borderRadius: 10,
              padding: 10,
              flex: 1,
            }}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={[Styles.subheading_label, {color: COLOR.WHITE}]}>
                {Locale('PRIZES IF:')}
              </Text>
              <Image
                resizeMode={'contain'}
                source={IMAGES.right_arrow_streach}
                style={{height: 20, marginLeft: 10, flex: 1}}
              />
              <View
                style={{
                  borderRightWidth: 1,
                  borderRightColor: COLOR.BLACK,
                }}
              />
            </View>
            <View>
              <View style={style.slider_view}>
                <View
                  style={{
                    marginVertical: 0,
                    marginLeft: 0,
                    width: props.data
                      ? `${
                          (props.data.currentVisits / props.data.goal) * 100 ??
                          20
                        }%`
                      : '0%',
                    backgroundColor: '#03AC0060',
                    flex: 1,
                    height: 60,
                  }}
                />
              </View>
              <Text
                style={[
                  Styles.bold_body_label,
                  {marginTop: -60, marginHorizontal: 10},
                ]}>
                {`${props.data ? props.data.trading.name : ''} ${Locale(
                  'gets',
                )} ${props.data ? props.data.goal : ''} ${Locale(
                  'customer visits before',
                )} ${
                  props.data
                    ? getDateFromTimeStamp(props.data.endDate, 'MMM DD, YYYY')
                    : 'Jan 22, 2023'
                } `}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginTop: Platform.OS == 'ios' ? 20 : 35,
                width: '100%',
              }}>
              <Text
                style={[
                  Styles.small_label,
                  {color: COLOR.WHITE, flex: 1, marginRight: 10},
                ]}>
                {Locale('CURRENT:')}{' '}
                {props.data ? props.data.currentVisits : '0'} {Locale('VISITS')}
              </Text>
              <Text style={[Styles.small_label, {color: COLOR.WHITE, flex: 1}]}>
                {Locale('GOAL:')} {props.data ? props.data.goal : '0'}{' '}
                {Locale('VISITS')}
              </Text>
            </View>
            <View style={[Styles.line_view, {borderBottomWidth: 0.5}]} />
            <View
              style={{
                flexDirection: 'row',
                marginTop: 10,
                width: '100%',
                justifyContent: 'center',
              }}>
              <Text
                style={[
                  Styles.extra_small_label,
                  {
                    color: COLOR.WHITE,
                    flex: 1,
                    textAlign: 'center',
                    fontSize: 10,
                  },
                ]}>
                {Locale('STARTS:')}
                {'\n'}
                {props.data
                  ? getDateFromTimeStamp(
                      props.data.startTime,
                      'MMM DD, YYYY h:mm a',
                    )
                  : 'Jan 20, 2022'}
              </Text>
              <View
                style={{
                  borderRightColor: '#ffffff50',
                  borderRightWidth: 1,
                  marginTop: -10,
                  marginHorizontal: 5,
                }}
              />
              <Text
                style={[
                  Styles.extra_small_label,
                  {
                    color: COLOR.WHITE,
                    flex: 1,
                    textAlign: 'center',
                    fontSize: 10,
                  },
                ]}>
                {Locale('ENDS:')}
                {'\n'}
                {props.data
                  ? getDateFromTimeStamp(
                      props.data.endTime,
                      'MMM DD, YYYY h:mm a',
                    )
                  : 'Feb 20, 2021'}
              </Text>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  viewTwo = props => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignSelf: 'center',
          marginTop: 20,
        }}>
        <View style={style.count_view}>
          <Text style={[Styles.subheading_label, {textAlign: 'center'}]}>
            {props.count}
          </Text>
        </View>
        <View
          style={{
            borderWidth: 1,
            borderColor: COLOR.BLACK,
            borderRadius: 10,
            padding: 10,
            flex: 1,
          }}>
          <Text
            style={[
              Styles.subheading_label,
              {color: COLOR.WHITE, fontFamily: FONTS.FAMILY_BOLD},
            ]}>
            {Locale('WINNERS:')}
            <Text
              style={[
                Styles.bold_body_label,
                {color: COLOR.WHITE, fontFamily: FONTS.FAMILY_BOLD},
              ]}>
              {' '}
              Top {props.data ? props.data.winners : ' 50'}% of participating
              Spenders if campaign hits goal before end.
            </Text>
          </Text>
          <View style={{flexDirection: 'row'}}>
            <TouchableWithoutFeedback
              onPress={() => {
                if (
                  props.data &&
                  (props.data.status != 1 || props.data.status != 2)
                ) {
                  this.props.navigation.navigate('LeaderboardScreen', {
                    tabScreen: true,
                    data: this.state.campaignDetail,
                  });
                } else {
                  showAlertMessage('Campaign not started');
                }
              }}>
              <View
                style={[
                  Styles.shadow_view,
                  {
                    borderRadius: 5,
                    backgroundColor: COLOR.YELLOW,
                    width: '100%',
                    paddingVertical: 5,
                    marginVertical: 10,
                    paddingHorizontal: 10,
                  },
                ]}>
                <Text
                  style={{
                    fontFamily: FONTS.FAMILY_BOLD,
                    fontSize: 24,
                    textAlign: 'center',
                    color: COLOR.BLACK,
                  }}>
                  {Locale('LEADERBOARD')}
                </Text>
              </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginLeft: 10,
                flex: 1,
              }}
              onPress={() => {
                this.setState({
                  alertModalVisible: true,
                  title: Locale('Leaderboard'),
                  description: K_LEADERBOARD_INFO,
                });
              }}>
              <Image
                source={IMAGES.info}
                resizeMode={'contain'}
                style={{
                  height: 15,
                  width: 15,
                }}
              />
            </TouchableWithoutFeedback>
          </View>
        </View>
        <TouchableWithoutFeedback
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            marginLeft: 10,
            flex: 1,
          }}
          onPress={() => {
            this.setState({
              alertModalVisible: true,
              title: Locale('About winners'),
              description: K_WINNERS_INFO,
            });
          }}>
          <Image
            source={IMAGES.info}
            resizeMode={'contain'}
            style={{
              height: 15,
              width: 15,
            }}
          />
        </TouchableWithoutFeedback>
      </View>
    );
  };

  viewThree = props => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignSelf: 'center',
          marginTop: 20,
        }}>
        <View style={style.count_view}>
          <Text style={[Styles.subheading_label, {textAlign: 'center'}]}>
            {props.count}
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            flex: 1,
            borderWidth: 1,
            borderColor: COLOR.BLACK,
            borderRadius: 10,
            padding: 10,
          }}>
          <View
            style={{
              flex: 1,
            }}>
            <Text
              style={[
                Styles.subheading_label,
                {color: COLOR.WHITE, fontFamily: FONTS.FAMILY_BOLD},
              ]}>
              {Locale('PRIZE:')}
              <Text
                style={[
                  Styles.bold_body_label,
                  {color: COLOR.WHITE, fontFamily: FONTS.FAMILY_BOLD},
                ]}>
                {` Digital gift card with ${
                  props.data
                    ? props.data.campaignTemplate
                      ? props.data.campaignTemplate.pricePercentage
                      : ''
                    : ''
                }% of what you spent.`}
              </Text>
            </Text>
          </View>
          <View
            style={{
              backgroundColor: COLOR.WHITE,
              flex: 1,
              borderRadius: 10,
              alignItems: 'center',
              padding: 10,
              flexDirection: 'row',
            }}>
            <Image
              style={{height: 40, width: 40, borderRadius: 20}}
              source={props.data ? {uri: props.data.trading.logo} : IMAGES.mono}
            />
            <Text style={[Styles.bold_body_label, {flex: 1, marginLeft: 5}]}>
              {props.data ? props.data.trading.name : "Archie's Bar & Grill"}
            </Text>
          </View>
        </View>
        <TouchableWithoutFeedback
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            marginLeft: 10,
            flex: 1,
          }}
          onPress={() => {
            this.setState({
              alertModalVisible: true,
              title: Locale('Prizes'),
              description: K_PRIZE_INFO,
            });
          }}>
          <Image
            source={IMAGES.info}
            resizeMode={'contain'}
            style={{
              height: 15,
              width: 15,
            }}
          />
        </TouchableWithoutFeedback>
      </View>
    );
  };

  closeVerifyModal = val => {
    this.setState({alertModalVisible: false});
  };

  handleCancelRequest() {
    if (this.state.campaignDetail) {
      Alert.alert(
        'Alert',
        'Are you sure you want to delete campaign request?',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: 'OK',
            onPress: () => {
              api
                .deletePendingCampaign(this.state.campaignDetail.id)
                .then(json => {
                  showAlertMessage('Successfully removed campaign');
                  this.props.navigation.dispatch(
                    CommonActions.reset({
                      index: 0,
                      routes: [{name: 'MyTabs'}],
                    }),
                  );
                })
                .catch(error => {
                  this.setState({isLoading: false});
                  console.log('error is:-', error);
                  showAlertMessage(error.response.data.message, 2);
                });
            },
          },
        ],
      );
    }
  }

  render() {
    const {isTabbar, campaignDetail} = this.state;
    return (
      <View style={[Styles.container, {backgroundColor: COLOR.DARK_BLUE}]}>
        <SafeAreaView style={Styles.container}>
          <AlertPopup
            image={IMAGES.security}
            title={this.state.title}
            buttonText={Locale('ok')}
            desc={this.state.description}
            closeVerifyModal={val => this.closeVerifyModal(val)}
            modalVisible={this.state.alertModalVisible}
          />

          <NavigationBar
            title={
              campaignDetail
                ? campaignDetail.trading
                  ? campaignDetail.trading.name
                  : ''
                : ''
            }
          />

          <View
            style={{
              height: 35,
              width: 85,
              borderColor: COLOR.WHITE,
              borderWidth: 1,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 10,
              alignSelf: 'flex-end',
              marginRight: 15,
              marginTop: -40,
            }}>
            <TouchableWithoutFeedback
              onPress={() =>
                this.props.navigation.navigate('AddCampaignDetail', {
                  isTabbar: isTabbar,
                  data: campaignDetail,
                  isShowDetail: true,
                })
              }>
              <Text style={[Styles.small_label, {color: COLOR.WHITE}]}>
                Details >>
              </Text>
            </TouchableWithoutFeedback>
          </View>

          <ScrollView>
            <View style={[Styles.shadow_view, style.main_view]}>
              <TouchableWithoutFeedback
                onPress={() => {
                  this.props.navigation.navigate('BusinessDetail', {
                    data: campaignDetail ? campaignDetail.trading : '',
                    tabScreen: this.state.isTabbar,
                  });
                }}>
                <TopWhiteView
                  image={
                    campaignDetail
                      ? campaignDetail.trading
                        ? campaignDetail.trading.logo
                        : ''
                      : ''
                  }
                  name={
                    campaignDetail
                      ? campaignDetail.trading
                        ? campaignDetail.trading.name
                        : "Archie's Bar & Grill"
                      : "Archie's Bar & Grill"
                  }
                  address={Locale('Location hours & info >>')}
                  isTabbar={isTabbar}
                />
              </TouchableWithoutFeedback>
            </View>
            <View
              style={{
                height: 150,
                alignSelf: 'center',
                width: '100%',
              }}>
              {campaignDetail ? (
                campaignDetail.trading.images ? (
                  campaignDetail.trading.images.length > 0 && (
                    <View style={{flex: 1, width: '100%', height: '100%'}}>
                      <Pages
                        indicatorColor={COLOR.LIGHT_BLUE}
                        indicatorOpacity={0.2}>
                        {campaignDetail.trading.images.map(element => {
                          return (
                            <FastImage
                              resizeMode={FastImage.resizeMode.cover}
                              style={{
                                overflow: 'hidden',
                                width: '100%',
                                alignSelf: 'center',
                                height: '100%',
                              }}
                              source={{
                                uri: element.image,
                                priority: FastImage.priority.high,
                              }}
                            />
                          );
                        })}
                      </Pages>
                    </View>
                  )
                ) : (
                  <View />
                )
              ) : (
                <Image
                  style={{width: '100%', height: 150}}
                  source={IMAGES.main_banner}
                />
              )}
            </View>
            <View style={{flexDirection: 'row'}}>
              <View
                style={[
                  Styles.shadow_view,
                  {
                    borderRadius: 5,
                    marginTop: -15,
                    backgroundColor: COLOR.YELLOW,
                    marginRight: 10,
                    marginLeft: 60,
                    paddingVertical: 5,
                    paddingHorizontal: 10,
                  },
                ]}>
                <Text
                  style={{
                    fontFamily: FONTS.FAMILY_BOLD,
                    fontSize: 24,
                    color: COLOR.BLACK,
                    textAlign: 'center',
                  }}>
                  {campaignDetail
                    ? campaignDetail.campaignTemplate
                      ? campaignDetail.campaignTemplate.name
                      : Locale('Earn what you spent')
                    : Locale('Earn what you spent')}
                </Text>
              </View>
              <TouchableWithoutFeedback
                style={{
                  height: 25,
                  width: 25,
                }}
                onPress={() => {
                  this.setState({
                    alertModalVisible: true,
                    title: Locale('About campaign'),
                    description: K_CAMPAIGN_INFO,
                  });
                }}>
                <Image
                  source={IMAGES.info}
                  resizeMode={'contain'}
                  style={{
                    height: 15,
                    width: 15,
                  }}
                />
              </TouchableWithoutFeedback>
            </View>
            <View style={[style.campaignDetailView]}>
              <this.viewOne data={campaignDetail} count={1} />
              <this.viewTwo data={campaignDetail} count={2} />
              <this.viewThree data={campaignDetail} count={3} />
              <View
                style={[
                  Styles.shadow_view,
                  {
                    backgroundColor: COLOR.MEDIUM_BLUE,
                    padding: 10,
                    borderRadius: 10,
                    marginTop: 15,
                  },
                ]}>
                <Text style={[Styles.small_label, {color: COLOR.WHITE}]}>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.props.navigation.navigate('TermsConditionScreen')
                    }>
                    <Text
                      style={{
                        textDecorationLine: 'underline',
                        color: COLOR.WHITE,
                      }}>
                      {Locale('Click to read all terms & condition:')}
                    </Text>
                  </TouchableWithoutFeedback>
                  {`\n ${Locale('• Minimum spent to count as visit is')} ${
                    campaignDetail ? campaignDetail.minSpent : ''
                  } THB.\n ${Locale('• Maximum prize is')} ${
                    campaignDetail ? insertComma(campaignDetail.limit) : ''
                  } THB ${Locale('digital gift card')}\n ${Locale(
                    '• Being in Top',
                  )} ${campaignDetail ? campaignDetail.winners : ''}% ${Locale(
                    'before end does not guarantee business',
                  )}`}
                </Text>
              </View>
            </View>
          </ScrollView>
          {campaignDetail &&
            (campaignDetail.status == 1 || campaignDetail.status == 2) && (
              <View style={{width: wp(90), alignSelf: 'center'}}>
                <CustomButton
                  text={'Cancel Request'}
                  onPress={() => this.handleCancelRequest()}
                />
              </View>
            )}
        </SafeAreaView>
      </View>
    );
  }
}

const style = StyleSheet.create({
  campaignDetailView: {
    alignSelf: 'center',
    margin: 20,
  },
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    paddingVertical: 15,
    paddingHorizontal: 15,
    width: wp(90),
    alignSelf: 'center',
  },
  flatlist_view: {
    marginVertical: 0,
    width: wp(90),
    marginHorizontal: 15,
    alignSelf: 'center',
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    paddingTop: 10,
    paddingHorizontal: 10,
  },
  common_view: {
    flex: 1,
    backgroundColor: COLOR.MEDIUM_BLUE,
    borderRadius: 10,
    padding: 7,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text_one: {
    color: COLOR.WHITE,
    textAlign: 'center',
    fontWeight: '500',
  },
  text_two: {
    color: COLOR.WHITE,
    textAlign: 'center',
    alignSelf: 'center',
    fontWeight: '700',
    marginVertical: 5,
  },
  text_three: {
    color: COLOR.WHITE,
    fontSize: 10,
    textAlign: 'center',
  },
  main_view: {
    marginTop: 10,
    marginBottom: 0,
    paddingVertical: 10,
    width: wp(100),
    backgroundColor: COLOR.WHITE,
    alignSelf: 'center',
  },
  rank_view: {
    width: wp(90),
    alignSelf: 'center',
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  total_spent_view: {
    flexDirection: 'row',
    height: 40,
    borderRadius: 5,
    alignSelf: 'center',
    backgroundColor: COLOR.DARK_BLUE,
    alignItems: 'center',
  },
  view_all: {
    padding: 5,
    color: COLOR.BLACK,
    borderRadius: 5,
    overflow: 'hidden',
    backgroundColor: COLOR.YELLOW,
  },
  count_view: {
    height: 50,
    width: 50,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: COLOR.BLACK,
    backgroundColor: COLOR.WHITE,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
    marginLeft: 0,
    marginTop: 20,
  },
  slider_view: {
    borderWidth: 1,
    borderColor: COLOR.BLACK,
    borderRadius: 10,
    marginVertical: 10,
    backgroundColor: COLOR.WHITE,
    overflow: 'hidden',
  },
});
