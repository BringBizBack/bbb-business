'use strict';

import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Alert,
  Image,
} from 'react-native';
import COLOR from '../../styles/Color';
import Styles from '../../styles/Styles';
import {useNavigation} from '@react-navigation/native';
import IMAGES from '../../styles/Images';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import FONTS from '../../styles/Fonts';

export const TopWhiteView = props => {
  const navigation = useNavigation();
  return (
    <View style={Styles.mono_view}>
      <Image
        source={props.image ? {uri: props.image} : IMAGES.mono}
        style={{height: 40, width: 40, borderRadius: 20}}
        resizeMode={'cover'}
      />
      <View style={{flex: 1, marginHorizontal: 10}}>
        <Text
          style={[
            Styles.button_font,
            {
              marginHorizontal: 10,
              textAlign: 'center',
              fontSize: 18,
              fontFamily: FONTS.FAMILY_BOLD,
            },
          ]}>
          {props.name}
        </Text>
        <Text
          style={[Styles.extra_small_label, {margin: 0, textAlign: 'center'}]}>
          {props.address}
        </Text>
      </View>
    </View>
  );
};

export const MobileView = props => {
  return (
    <View style={[Styles.shadow_view, style.shadowView, style.mobile_view]}>
      <View
        style={[
          Styles.left_corner_view,
          {backgroundColor: COLOR.GREEN, alignSelf: 'center'},
        ]}>
        <Text style={[Styles.extra_small_label, {color: COLOR.WHITE}]}>
          {props.title}
        </Text>
      </View>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          marginLeft: 10,
          flex: 1,
        }}>
        <View style={{flexDirection: 'row'}}>
          <Text
            style={[
              Styles.heading_label,
              {color: COLOR.WHITE, marginRight: 5},
            ]}>
            {props.date}
          </Text>
          <Text style={[Styles.extra_small_label, {color: COLOR.WHITE}]}>
            {props.time}
          </Text>
        </View>
        {props.subTitle && (
          <Text style={[Styles.extra_small_label, {color: COLOR.WHITE}]}>
            {props.subTitle}
          </Text>
        )}
      </View>
      <Image
        style={{height: 45, width: 30, alignSelf: 'flex-end'}}
        source={props.image}
      />
    </View>
  );
};

const style = StyleSheet.create({
  mainView: {
    height: 250,
    width: 250,
    borderRadius: 125,
    top: -125,
    right: -125,
    position: 'absolute',
    backgroundColor: COLOR.TOP_CIRCLE,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inner_circle: {
    height: 200,
    width: 200,
    borderRadius: 100,
    backgroundColor: COLOR.LIGHT_BLUE,
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
  },

  yellow_view: {
    height: 25,
    width: 75,
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
    marginLeft: 30,
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mobile_view: {
    width: wp(90),
    flexDirection: 'row',
    paddingRight: 15,
    height: 60,
    alignItems: 'center',
    backgroundColor: COLOR.MEDIUM_BLUE,
    marginTop: 10,
    alignSelf: 'center',
    borderRadius: 5,
  },
});
