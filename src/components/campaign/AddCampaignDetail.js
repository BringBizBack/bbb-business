import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import NavigationBar from '../../commonView/NavigationBar';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {CustomButton} from '../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import {MobileView} from './TopWhiteView';
import AlertPopup from '../../commonView/AlertPopup';
import {
  getDateFromTimeStamp,
  insertComma,
  Locale,
  navigate,
  showAlertMessage,
} from '../../commonView/Helpers';
import ActivityIndicator from '../../commonView/ActivityIndicator';
import API from '../../api/Api';
import AuthenticationAction from '../../redux/action/AuthenticationAction';
import {connect} from 'react-redux';
import CampaignActions from '../../redux/action/CampaignActions';

let api = new API();

class AddCampaignDetail extends React.Component {
  constructor() {
    super();
    this.state = {
      alertModalVisible: false,
      campaignData: null,
      termsTick: false,
      isLoading: false,
      isShowDetail: false,
      templateData: null,
    };

    this.handleSubmitButton = this.handleSubmitButton.bind(this);
  }

  componentDidMount() {
    this.props.getUserBusinessDetail();
    if (this.props.route.params) {
      if (this.props.route.params.data != null) {
        console.log(
          'this.props.route.params.data',
          this.props.route.params.data,
        );
        this.setState({
          campaignData: this.props.route.params.data,
          isShowDetail: this.props.route.params.isShowDetail
            ? this.props.route.params.isShowDetail
            : false,
          templateData: this.props.route.params.templateData,
        });
      }
    }
  }

  closeVerifyModal = () => {
    this.setState({alertModalVisible: false});
  };

  handleSubmitButton = () => {
    if (this.state.termsTick == false) {
      showAlertMessage(Locale('Please agree our terms & condition'));
    } else {
      let that = this;
      let campaign = this.state.campaignData;
      let data = JSON.stringify({
        trading: this.props.userBusinessDetail.tradingInfo.id,
        campaignTemplate: this.state.templateData.id,
        maxPrize: campaign.prizeLimit,
        startDate: campaign.startDate,
        endDate: campaign.endDate,
        goal: campaign.goal,
        startTime: campaign.startTime,
        endTime: campaign.endTime,
      });

      that.setState({isLoading: true});
      api
        .addCamapaign(data)
        .then(json => {
          console.log('campaign response is:-', json);
          that.setState({isLoading: false});
          if (json.status == 200) {
            this.props.getCampaignData(1, 1);
            this.setState({alertModalVisible: true});
          } else if (json.status == 400) {
            showAlertMessage(json.data.message, 2);
          } else {
            showAlertMessage(json.data.message, 2);
          }
        })
        .catch(error => {
          that.setState({isLoading: false});
          if (error.response.data) {
            showAlertMessage(error.response.data.message, 2);
          }
        });
    }
  };

  closeVerifyModal = () => {
    this.setState({alertModalVisible: false}, () => {
      this.props.navigation.navigate('MyTabs');
    });
  };

  campaignDetail = props => {
    return (
      <View style={{marginBottom: 15}}>
        <Text style={[Styles.extra_small_label, {color: COLOR.WHITE}]}>
          {props.title}
        </Text>
        <Text style={[Styles.bold_body_label, {color: COLOR.WHITE}]}>
          {props.detail}
        </Text>
      </View>
    );
  };

  getBusinessStatus = props => {
    if (props) {
      switch (props.campaignStatus) {
        case 1:
          return 'Waiting';
          break;
        case 2:
          return 'Approved';
          break;
        case 3:
          return 'Rejected';
          break;
        default:
          return 'N/A';
          break;
      }
    } else {
      return 'Add Business';
    }
  };

  render() {
    let {campaignData, isLoading} = this.state;
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <AlertPopup
            image={IMAGES.security}
            title={Locale('THANK YOU')}
            buttonText={Locale('Home')}
            desc={Locale('K_GOALPRIZE_SUBMITTED')}
            closeVerifyModal={() => this.closeVerifyModal()}
            modalVisible={this.state.alertModalVisible}
          />
          <ActivityIndicator loading={isLoading} />
          <AlertPopup
            image={IMAGES.security}
            title={Locale('THANK YOU')}
            buttonText={Locale('Ok')}
            desc={Locale('K_GOALPRIZE_SUBMITTED')}
            closeVerifyModal={() => this.closeVerifyModal()}
            modalVisible={this.state.alertModalVisible}
          />
          <SafeAreaView style={Styles.container}>
            <NavigationBar
              title={
                this.state.isShowDetail
                  ? Locale('Campaign Details')
                  : Locale('Start New Campaign')
              }
            />
            {campaignData && campaignData.startTime < Date.now() / 1000 ? (
              <TouchableWithoutFeedback
                onPress={() =>
                  this.props.navigation.navigate('BillingHistory', {
                    campaignData: this.state.campaignData,
                    tabScreen: true,
                  })
                }>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.WHITE,
                      alignSelf: 'flex-end',
                      marginTop: -32,
                      textDecorationLine: 'underline',
                      marginRight: 15,
                    },
                  ]}>
                  {Locale('Billing history')}
                </Text>
              </TouchableWithoutFeedback>
            ) : null}
            <ScrollView>
              {this.state.isShowDetail && (
                <MobileView
                  title={Locale('Submitted')}
                  date={
                    campaignData &&
                    campaignData &&
                    getDateFromTimeStamp(
                      campaignData.startDate
                        ? campaignData.startDate
                        : 'Jul 23, 2021',
                      'DD',
                    )
                  }
                  time={
                    campaignData &&
                    getDateFromTimeStamp(
                      campaignData.startDate
                        ? campaignData.startDate
                        : '1614567890',
                      'MMM, yyyy',
                    ) +
                      '\n' +
                      getDateFromTimeStamp(
                        campaignData.startTime
                          ? campaignData.startTime
                          : '1614567890',
                        'h:mm a',
                      )
                  }
                  image={IMAGES.rocket}
                />
              )}
              {this.state.isShowDetail && (
                <MobileView
                  title={this.getBusinessStatus(campaignData)}
                  date={
                    campaignData
                      ? campaignData.campaignStatus == 2
                        ? getDateFromTimeStamp(
                            campaignData.trading
                              ? campaignData.campaignApprovalDate
                              : 'Jul 23, 2021',
                            'DD',
                          )
                        : ''
                      : 'N/A'
                  }
                  time={
                    campaignData
                      ? campaignData.campaignApprovalDate
                        ? getDateFromTimeStamp(
                            campaignData.campaignApprovalDate
                              ? campaignData.campaignApprovalDate
                              : 'Jul 23, 2021',
                            'MMM, YYYY',
                          ) +
                          '\n' +
                          getDateFromTimeStamp(
                            campaignData.campaignApprovalDate
                              ? campaignData.campaignApprovalDate
                              : '1614567890',
                            'h:mm a',
                          )
                        : Locale('Not Approved')
                      : 'N/A'
                  }
                  image={IMAGES.mobile_tick}
                />
              )}
              {campaignData && campaignData.campaignStatus == 3 ? (
                <View
                  style={[
                    Styles.shadow_view,
                    style.shadowView,
                    {backgroundColor: COLOR.RED, padding: 15},
                  ]}>
                  <Text style={[Styles.bold_body_label, {color: COLOR.WHITE}]}>
                    {campaignData ? campaignData.reason : ''}
                  </Text>
                </View>
              ) : null}
              <MobileView
                title={Locale('Start')}
                date={
                  campaignData &&
                  getDateFromTimeStamp(
                    campaignData.startDate
                      ? campaignData.startDate
                      : 'Jul 23, 2021',
                    'DD',
                  )
                }
                time={
                  campaignData &&
                  getDateFromTimeStamp(
                    campaignData.startDate
                      ? campaignData.startDate
                      : '1614567890',
                    'MMM, YYYY',
                  ) +
                    '\n' +
                    getDateFromTimeStamp(
                      campaignData.startTime
                        ? campaignData.startTime
                        : '6234567890',
                      'h:mm a',
                    )
                }
                image={IMAGES.management}
              />
              <View style={[Styles.shadow_view, style.shadowView]}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <View style={[Styles.left_corner_view, {marginVertical: 15}]}>
                    <Text style={[Styles.extra_small_label]}>
                      {Locale('Details')}
                    </Text>
                  </View>
                  {/*<TouchableOpacity*/}
                  {/*  onPress={() => this.props.navigation.goBack()}>*/}
                  {/*  <Text*/}
                  {/*    style={[*/}
                  {/*      Styles.small_label,*/}
                  {/*      {marginRight: 15, color: COLOR.WHITE},*/}
                  {/*    ]}>*/}
                  {/*    Edit*/}
                  {/*  </Text>*/}
                  {/*</TouchableOpacity>*/}
                </View>
                <View style={{paddingHorizontal: 15, alignItems: 'center'}}>
                  <View style={{width: '100%'}}>
                    <this.campaignDetail
                      title={Locale('Starts')}
                      detail={
                        campaignData &&
                        getDateFromTimeStamp(
                          campaignData.startDate
                            ? campaignData.startDate
                            : '1614567890',
                          'MMM DD, YYYY',
                        ) +
                          ' ' +
                          getDateFromTimeStamp(
                            campaignData.startTime
                              ? campaignData.startTime
                              : '1614567890',
                            'h:mm a',
                          )
                      }
                    />
                    <this.campaignDetail
                      title={Locale('Ends')}
                      detail={
                        campaignData &&
                        getDateFromTimeStamp(
                          campaignData.endDate
                            ? campaignData.endDate
                            : '1614567890',
                          'MMM DD, YYYY',
                        ) +
                          ' ' +
                          getDateFromTimeStamp(
                            campaignData.endTime
                              ? campaignData.endTime
                              : '1614567890',
                            'h:mm a',
                          )
                      }
                    />
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1, marginRight: 10}}>
                        <this.campaignDetail
                          title={Locale('Goal')}
                          detail={
                            campaignData
                              ? campaignData.goal + Locale(' customer visits')
                              : '1614567890'
                          }
                        />
                      </View>
                      <TouchableWithoutFeedback
                        onPress={() => {
                          showAlertMessage(Locale('Goals targeted'));
                        }}>
                        <Image
                          style={{height: 15, width: 15}}
                          resizeMode={'contain'}
                          source={IMAGES.info}
                        />
                      </TouchableWithoutFeedback>
                    </View>
                    <this.campaignDetail
                      title={Locale('Minimum spend')}
                      detail={
                        campaignData
                          ? insertComma(
                              campaignData.minSpent ??
                                campaignData.sliderTwo ??
                                '2000',
                            ) + ' THB'
                          : '2000'
                      }
                    />
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1, marginRight: 10}}>
                        <this.campaignDetail
                          title={Locale('Winners')}
                          detail={`Top ${
                            campaignData
                              ? campaignData.winners ??
                                campaignData.sliderOne ??
                                '20'
                              : '20'
                          }% of participating Spenders if campaign hits goal before end.`}
                        />
                      </View>
                      <TouchableWithoutFeedback
                        onPress={() => {
                          showAlertMessage(Locale('Participating spendors'));
                        }}>
                        <Image
                          style={{height: 15, width: 15}}
                          resizeMode={'contain'}
                          source={IMAGES.info}
                        />
                      </TouchableWithoutFeedback>
                    </View>
                    <this.campaignDetail
                      title={Locale('Prize')}
                      detail={`${
                        campaignData
                          ? campaignData.campaignTemplate
                            ? campaignData.campaignTemplate.name
                            : this.state.templateData
                            ? this.state.templateData.name
                            : Locale('Win what you spent')
                          : Locale('Win what you spent')
                      }\nDigital gift card with ${
                        campaignData
                          ? campaignData.campaignTemplate
                            ? campaignData.campaignTemplate.pricePercentage ??
                              ''
                            : this.state.templateData
                            ? this.state.templateData.pricePercentage
                            : ''
                          : ''
                      }% of what you spent.`}
                    />
                    <this.campaignDetail
                      title={Locale('Maximum Prize Amount')}
                      detail={
                        campaignData
                          ? insertComma(
                              campaignData.limit ??
                                campaignData.prizeLimit ??
                                '20000',
                            ) + ' THB'
                          : ''
                      }
                    />
                    <this.campaignDetail
                      title={Locale('Credit expiration')}
                      detail={`${
                        campaignData
                          ? campaignData.campaignTemplate
                            ? campaignData.campaignTemplate.creditExpiry
                            : this.state.templateData
                            ? this.state.templateData.creditExpiry
                            : '120'
                          : ''
                      } days after campaign starts`}
                    />
                    {/*<this.campaignDetail*/}
                    {/*  title={'Expiration date of credits'}*/}
                    {/*  detail={'Oct 10, 2021 (unless extended)'}*/}
                    {/*/>*/}
                  </View>
                </View>
              </View>

              {this.state.isShowDetail == false && (
                <View style={{width: '90%', alignSelf: 'center'}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginVertical: 15,
                    }}>
                    <TouchableWithoutFeedback
                      onPress={() => {
                        this.setState({termsTick: !this.state.termsTick});
                      }}>
                      <Image
                        style={{height: 15, width: 15, marginRight: 10}}
                        resizeMode={'contain'}
                        source={
                          this.state.termsTick
                            ? IMAGES.react_selected
                            : IMAGES.rectangle7
                        }
                      />
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback
                      onPress={() => {
                        navigate('TermsConditionScreen');
                      }}>
                      <Text
                        style={[
                          Styles.extra_small_label,
                          {color: COLOR.WHITE},
                        ]}>
                        I agree with{' '}
                        <Text style={{textDecorationLine: 'underline'}}>
                          {Locale('Campaign agreement')}
                        </Text>
                      </Text>
                    </TouchableWithoutFeedback>
                  </View>
                  <CustomButton
                    onPress={this.handleSubmitButton}
                    text={Locale('Submit for approval')}
                  />
                </View>
              )}
            </ScrollView>
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginTop: 15,
  },
  bottomButton: {marginVertical: 20, width: wp(90), alignSelf: 'center'},
  titleLabel: {color: COLOR.WHITE, width: 120, margin: 0, fontSize: 13},
  icon_image: {
    height: 40,
    width: 40,
  },
  purple_view: {
    backgroundColor: '#BA9AFF',
    margin: 10,
    padding: 15,
    borderRadius: 10,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    userBusinessDetail: state.authReducer.userBusinessDetail,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserBusinessDetail: val => {
      dispatch(AuthenticationAction.getBusinessDetail(val));
    },
    getCampaignData: val => {
      dispatch(CampaignActions.getCampaignData(4, 1));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddCampaignDetail);
