import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  Alert,
  Platform,
} from 'react-native';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import NavigationBar from '../../commonView/NavigationBar';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {CustomButton} from '../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import {FloatingTitleTextInputField} from '../../commonView/FloatingTitleTextInputField';
import Slider from '@react-native-community/slider';
import moment from 'moment';
import {
  getDateFromTimeStamp,
  Locale,
  showAlertMessage,
  withoutTime,
} from '../../commonView/Helpers';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import AuthenticationAction from '../../redux/action/AuthenticationAction';
import {connect} from 'react-redux';
import CampaignActions from '../../redux/action/CampaignActions';
import {Pages} from 'react-native-pages';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Swiper from 'react-native-swiper';

class CreateNewCampaign extends React.Component {
  constructor() {
    super();
    this.state = {
      startTime: null,
      endTime: null,
      startDate: null,
      endDate: null,
      goal: '',
      sliderOne: 0,
      sliderTwo: 0,
      prizeLimit: 0,
      templateData: null,
      index: 0,
      datePickerMode: 'date',
      openDatePicker: false,
      minDate: new Date(),
      selectedPick: 1,
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.handleSliderVal = this.handleSliderVal.bind(this);
    this.handleSaveButton = this.handleSaveButton.bind(this);
    this.initializeView = this.initializeView.bind(this);
    this.onPickupTimePick = this.onPickupTimePick.bind(this);
  }

  componentDidMount() {
    this.props.getTemplates();
    this.props.getCampaignData(2, 1);
    this.props.getCampaignData(3, 1);
    this.initializeView();
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.initializeView();
    });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ) {
    if (prevProps.templateData != this.props.templateData) {
      this.setState(
        {
          templateData: this.props.templateData,
        },
        () => {
          this.initializeView();
        },
      );
    }
  }

  updateMasterState = (props, value) => {
    this.setState({
      [props]: value,
    });
  };

  initializeView = val => {
    if (this.props.templateData && this.props.templateData.length) {
      let selectedTemplate = this.props.templateData[this.state.index];
      this.setState({
        sliderOne: selectedTemplate ? selectedTemplate.winnersPercentage : '',
        sliderTwo: selectedTemplate ? selectedTemplate.minimumSpending : '',
        prizeLimit: selectedTemplate ? selectedTemplate.maxPrize : '',
        startTime: null,
        endTime: null,
        startDate: null,
        endDate: null,
      });
    }
  };

  sliderView = props => {
    return (
      <View style={{flexDirection: 'row', marginBottom: 20}}>
        <View style={{flex: 1, marginRight: 10}}>
          <Text style={[Styles.extra_small_label, {color: COLOR.WHITE}]}>
            {props.name}
          </Text>
          <Slider
            style={{width: '100%', height: 40}}
            minimumValue={0}
            maximumValue={props.id == 1 ? 100 : 1000}
            disabled={true}
            thumbTintColor={COLOR.YELLOW}
            minimumTrackTintColor={COLOR.WHITE}
            maximumTrackTintColor={COLOR.WHITE}
            value={props.value}
            onValueChange={val => this.handleSliderVal(val, props.id)}
          />
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={[Styles.extra_small_label, {color: COLOR.WHITE}]}>
              {props.id == 1 ? '0 %' : '0'}
            </Text>
            <Text style={[Styles.extra_small_label, {color: COLOR.WHITE}]}>
              {props.id == 1 ? '100 %' : '1000'}
            </Text>
          </View>
        </View>
        <TouchableWithoutFeedback
          onPress={() => {
            showAlertMessage(props.name);
          }}>
          <Image
            style={{height: 15, width: 15, marginTop: 20}}
            resizeMode={'contain'}
            source={IMAGES.info}
          />
        </TouchableWithoutFeedback>
      </View>
    );
  };

  handleSliderVal = (val, id) => {
    if (id == 1) {
      this.setState({
        sliderOne: parseInt(val),
      });
    } else if (id == 2) {
      this.setState({
        sliderTwo: parseInt(val),
      });
    }
  };

  onPickupTimePick = val => {
    console.log('value is:-', val);
    switch (this.state.selectedPick) {
      case 1:
        this.setState({
          startDate: withoutTime(val),
          startTime: null,
          endDate: null,
          endTime: null,
          openDatePicker: false,
        });

        break;
      case 2:
        console.log(val);
        this.setState({
          startTime: val,

          openDatePicker: false,
        });
        break;
      case 3:
        this.setState({
          endDate: withoutTime(val),
          endTime: null,
          openDatePicker: false,
        });
        break;
      case 4:
        this.setState({
          endTime: val,
          openDatePicker: false,
        });
        break;
      default:
        break;
    }
  };

  getTimestamp = async val => {
    let dateVar = moment(val);
    let timestamp = Date.parse(dateVar.format());
    return timestamp / 1000;
  };

  handleSaveButton = async () => {
    if (this.props.templateData == null) {
      showAlertMessage(Locale('No template found'));
    } else if (this.state.startDate == null) {
      showAlertMessage(Locale('Select start date'));
    } else if (this.state.startTime == null) {
      showAlertMessage(Locale('Select start time'));
    } else if (this.state.endDate == null) {
      showAlertMessage(Locale('Select end date'));
    } else if (this.state.endTime == null) {
      showAlertMessage(Locale('Select end time'));
    } else if (!this.state.goal) {
      showAlertMessage('Enter number of visits');
    } else if (this.state.prizeLimit == '') {
      showAlertMessage(Locale('Enter prize limit'));
    } else {
      let data = {};
      data.sliderOne = this.state.sliderOne;
      data.sliderTwo = this.state.sliderTwo;
      data.prizeLimit = this.state.prizeLimit;
      data.goal = this.state.goal;
      data.templateData = this.state.templateData;
      data.index = this.state.index;

      let date = parseInt((await this.state.startDate.getTime()) / 1000);

      data.startTime = parseInt(this.state.startTime / 1000);
      data.endTime = parseInt(this.state.endTime / 1000);
      let todayDate = new Date().valueOf() / 1000;
      if (data.startTime < todayDate) {
        showAlertMessage('Start time cannot be less than current time');
        return;
      } else if (data.startTime > data.endTime) {
        console.log(data);
        this.setState({
          startTime: null,
          endTime: null,
        });
        showAlertMessage('End time cannot be less than start time');
        return;
      }

      data.endDate = parseInt((await this.state.endDate.getTime()) / 1000);
      data.startDate = date;
      console.log('data last', data);
      this.props.navigation.navigate('AddCampaignDetail', {
        data: data,
        templateData: this.props.templateData[this.state.index],
      });
    }
  };

  render() {
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <NavigationBar title={Locale('Start New Campaign')} />
            <KeyboardAwareScrollView>
              <View style={{height: 335, justifyContent: 'center'}}>
                {this.props.templateData && this.props.templateData.length ? (
                  <View style={{flex: 1}}>
                    <View
                      style={[Styles.shadow_view, style.shadowView, {flex: 1}]}>
                      <View
                        style={[Styles.left_corner_view, {marginVertical: 10}]}>
                        <Text style={[Styles.extra_small_label]}>
                          {Locale('Prize: You will give this to the Winners')}{' '}
                        </Text>
                      </View>
                      <View style={{flex: 1, alignSelf: 'center'}}>
                        <Swiper
                          key={
                            this.props.templateData
                              ? this.props.templateData.length
                              : 0
                          }
                          keyboardShouldPersistTaps="handled"
                          showsPagination={true}
                          containerStyle={{flex: 1}}
                          loadMinimalSize={1}
                          loop={false}
                          onIndexChanged={index => {
                            this.setState(
                              {
                                index: index,
                              },
                              () => {
                                this.initializeView();
                              },
                            );
                          }}
                          paginationStyle={{
                            bottom: 20,
                          }}
                          loadMinimal={true}
                          activeDotColor="#FFFFFF"
                          bounce={false}
                          dotColor="#FFFFFF50">
                          {this.props.templateData &&
                            this.props.templateData.map((val, key) => {
                              return (
                                <View style={{flex: 1, margin: 0}}>
                                  <View
                                    key={key}
                                    style={[
                                      Styles.shadow_view,
                                      Styles.shadowView,
                                      style.purple_view,
                                      {flex: 1, marginLeft: 10},
                                    ]}>
                                    <View
                                      style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                      }}>
                                      <Text
                                        style={[
                                          Styles.subheading_label,
                                          {color: COLOR.WHITE},
                                        ]}>
                                        {val.name ? val.name : ''}
                                      </Text>
                                      <Image
                                        style={{height: 25, width: 25}}
                                        source={IMAGES.green_tick}
                                        resizeMode={'contain'}
                                      />
                                    </View>
                                    <Text
                                      style={[
                                        Styles.extra_small_label,
                                        {color: COLOR.WHITE, marginTop: 10},
                                      ]}>
                                      {`${
                                        val.pricePercentage
                                          ? val.pricePercentage
                                          : ''
                                      }`}
                                      % of what winner spent in successful
                                      campaign will become credits on a digital
                                      gift card that the winning participating
                                      customer will automatically receive in
                                      their GOALPRIZE application immediately
                                      after a successful campaign ends.{'\n'}
                                      Credits expire:{' '}
                                      {`${
                                        val.creditExpiry ? val.creditExpiry : ''
                                      }`}{' '}
                                      days after campaign ends.{'\n'}
                                    </Text>
                                  </View>
                                </View>
                              );
                            })}
                        </Swiper>
                      </View>
                      <Text
                        style={{
                          marginHorizontal: 0,
                          marginVertical: 10,
                          color: COLOR.WHITE,
                          textAlign: 'center',
                        }}>
                        {Locale('Showing')} {`${this.state.index + 1}`}/
                        {`${this.props.templateData.length}`}{' '}
                        {Locale('see other options')}
                      </Text>
                    </View>
                  </View>
                ) : (
                  <Text style={[Styles.button_font, {color: COLOR.WHITE}]}>
                    {Locale('No template(s) found')}
                  </Text>
                )}
              </View>
              <View style={[Styles.shadow_view, style.shadowView]}>
                <View style={[Styles.left_corner_view, {marginVertical: 15}]}>
                  <Text style={[Styles.extra_small_label]}>
                    {Locale('Campaign details')}
                  </Text>
                </View>
                <View style={{paddingHorizontal: 15, alignItems: 'center'}}>
                  <View style={{width: '100%'}}>
                    <View
                      style={{
                        flex: 1,
                        marginRight: 10,
                        marginTop: 15,
                        marginBottom: 15,
                      }}>
                      <Text style={[Styles.body_label, {color: COLOR.WHITE}]}>
                        {this.props.templateData
                          ? this.props.templateData[this.state.index].name
                          : ''}
                      </Text>
                      <View
                        style={[
                          Styles.line_view,
                          {
                            color: COLOR.WHITE,
                            marginBottom: 10,
                            marginTop: 5,
                          },
                        ]}
                      />
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1}}>
                        <TouchableWithoutFeedback
                          onPress={() =>
                            this.setState({
                              openDatePicker: true,
                              minDate: new Date(),
                              selectedPick: 1,
                              datePickerMode: 'date',
                            })
                          }>
                          <View
                            style={{
                              flex: 1,
                              marginRight: 10,
                              marginBottom: 10,
                            }}>
                            <Text
                              style={[Styles.body_label, {color: COLOR.WHITE}]}>
                              {this.state.startDate
                                ? getDateFromTimeStamp(
                                    this.state.startDate,
                                    'YYYY-MM-DD',
                                  )
                                : 'Start Date'}
                            </Text>
                            <View
                              style={[
                                Styles.line_view,
                                {
                                  color: COLOR.WHITE,
                                  marginBottom: 10,
                                  marginTop: 5,
                                },
                              ]}
                            />
                          </View>
                        </TouchableWithoutFeedback>
                      </View>
                      <View style={{flex: 1, marginHorizontal: 10}}>
                        <TouchableWithoutFeedback
                          onPress={() => {
                            this.setState({
                              openDatePicker: true,
                              selectedPick: 2,
                              datePickerMode: 'time',
                              minDate: this.state.startDate
                                ? this.state.startDate
                                : null,
                            });
                          }}>
                          <View
                            style={{
                              flex: 1,
                              marginRight: 10,
                              marginBottom: 10,
                            }}>
                            <Text
                              style={[Styles.body_label, {color: COLOR.WHITE}]}>
                              {this.state.startTime
                                ? getDateFromTimeStamp(
                                    this.state.startTime,
                                    'h:mm a',
                                  )
                                : 'Start Time'}
                            </Text>
                            <View
                              style={[
                                Styles.line_view,
                                {
                                  color: COLOR.WHITE,
                                  marginBottom: 10,
                                  marginTop: 5,
                                },
                              ]}
                            />
                          </View>
                        </TouchableWithoutFeedback>
                      </View>
                      <TouchableWithoutFeedback
                        onPress={() => {
                          showAlertMessage(
                            Locale('Select campaign start date and time'),
                          );
                        }}>
                        <Image
                          style={{height: 15, width: 15, marginTop: 5}}
                          resizeMode={'contain'}
                          source={IMAGES.info}
                        />
                      </TouchableWithoutFeedback>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 15}}>
                      <View style={{flex: 1}}>
                        <TouchableWithoutFeedback
                          onPress={() =>
                            this.setState({
                              openDatePicker: true,
                              minDate: this.state.startDate
                                ? this.state.startDate
                                : null,
                              selectedPick: 3,
                              datePickerMode: 'date',
                            })
                          }>
                          <View
                            style={{
                              flex: 1,
                              marginRight: 10,
                              marginBottom: 10,
                            }}>
                            <Text
                              style={[Styles.body_label, {color: COLOR.WHITE}]}>
                              {this.state.endDate
                                ? getDateFromTimeStamp(
                                    this.state.endDate,
                                    'YYYY-MM-DD',
                                  )
                                : 'End Date'}
                            </Text>
                            <View
                              style={[
                                Styles.line_view,
                                {
                                  color: COLOR.WHITE,
                                  marginBottom: 10,
                                  marginTop: 5,
                                },
                              ]}
                            />
                          </View>
                        </TouchableWithoutFeedback>
                      </View>
                      <View style={{flex: 1, marginHorizontal: 10}}>
                        <TouchableWithoutFeedback
                          onPress={() =>
                            this.setState({
                              openDatePicker: true,
                              selectedPick: 4,
                              datePickerMode: 'time',
                            })
                          }>
                          <View
                            style={{
                              flex: 1,
                              marginRight: 10,
                              marginBottom: 10,
                            }}>
                            <Text
                              style={[Styles.body_label, {color: COLOR.WHITE}]}>
                              {this.state.endTime
                                ? getDateFromTimeStamp(
                                    this.state.endTime,
                                    'h:mm a',
                                  )
                                : 'End Time'}
                            </Text>
                            <View
                              style={[
                                Styles.line_view,
                                {
                                  color: COLOR.WHITE,
                                  marginBottom: 10,
                                  marginTop: 5,
                                },
                              ]}
                            />
                          </View>
                        </TouchableWithoutFeedback>
                      </View>
                      <TouchableWithoutFeedback
                        onPress={() => {
                          showAlertMessage(
                            Locale('Select campaign end date and time'),
                          );
                        }}>
                        <Image
                          style={{height: 15, width: 15, marginTop: 5}}
                          resizeMode={'contain'}
                          source={IMAGES.info}
                        />
                      </TouchableWithoutFeedback>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1, marginRight: 10}}>
                        <FloatingTitleTextInputField
                          isWhite={true}
                          attrName={'goal'}
                          floatKeyboardType={'number-pad'}
                          value={this.state.goal}
                          updateMasterState={this.updateMasterState}
                          title={Locale('Goal: number of visits')}
                        />
                      </View>
                      <TouchableWithoutFeedback
                        onPress={() => {
                          showAlertMessage(Locale('Enter number of visits'));
                        }}>
                        <Image
                          style={{height: 15, width: 15, marginTop: 20}}
                          resizeMode={'contain'}
                          source={IMAGES.info}
                        />
                      </TouchableWithoutFeedback>
                    </View>
                    <this.sliderView
                      id={1}
                      value={this.state.sliderOne}
                      name={`Winners: Top ${this.state.sliderOne}% Spenders if campaign hits goal before end.`}
                    />
                    <this.sliderView
                      id={2}
                      value={this.state.sliderTwo}
                      name={`${Locale('Minimum spent is')} ${
                        this.state.sliderTwo
                      } THB ${Locale('(to count as a visit)')}`}
                    />
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1, marginRight: 10}}>
                        <FloatingTitleTextInputField
                          isWhite={true}
                          attrName={'prizeLimit'}
                          editable={true}
                          floatKeyboardType={'number-pad'}
                          value={String(this.state.prizeLimit)}
                          updateMasterState={this.updateMasterState}
                          title={Locale('Maximum Prize Amount')}
                        />
                      </View>
                      <TouchableWithoutFeedback
                        onPress={() => {
                          showAlertMessage(Locale('Maximum Prize Amount'));
                        }}>
                        <Image
                          style={{height: 15, width: 15, marginTop: 20}}
                          resizeMode={'contain'}
                          source={IMAGES.info}
                        />
                      </TouchableWithoutFeedback>
                    </View>
                  </View>
                </View>
              </View>

              <View style={{width: '90%', marginTop: 50, alignSelf: 'center'}}>
                <CustomButton
                  onPress={() => this.handleSaveButton()}
                  text={Locale('Review the info')}
                />
              </View>
            </KeyboardAwareScrollView>
            <DateTimePicker
              isVisible={this.state.openDatePicker}
              mode={this.state.datePickerMode}
              date={
                this.state.selectedPick == 1
                  ? new Date()
                  : this.state.selectedPick == 2
                  ? this.state.startDate ?? new Date()
                  : this.state.selectedPick == 3
                  ? this.state.startDate ?? new Date()
                  : this.state.endDate ?? new Date()
              }
              minimumDate={
                Platform.OS == 'ios'
                  ? this.state.minDate
                    ? this.state.selectedPick == 2
                      ? new Date()
                      : this.state.selectedPick == 3 ||
                        this.state.selectedPick == 1
                      ? this.state.minDate ?? null
                      : null
                    : null
                  : null
              }
              onConfirm={this.onPickupTimePick.bind(this)}
              onCancel={() => this.setState({openDatePicker: false})}
            />
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginTop: 15,
  },
  bottomButton: {marginVertical: 20, width: wp(90), alignSelf: 'center'},
  titleLabel: {color: COLOR.WHITE, width: 120, margin: 0, fontSize: 13},
  icon_image: {
    height: 40,
    width: 40,
  },
  purple_view: {
    backgroundColor: '#BA9AFF',
    margin: 10,
    padding: 15,
    borderRadius: 10,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.authReducer.userDetail,
    userBusinessDetail: state.authReducer.userBusinessDetail,
    templateData: state.campaignReducer.templateData,
    currentCampaign: state.campaignReducer.currentCampaign,
    pastCampaign: state.campaignReducer.pastCampaign,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserDetail: val => {
      dispatch(AuthenticationAction.getUserInfo(val));
    },
    getUserBusinessDetail: val => {
      dispatch(AuthenticationAction.getBusinessDetail(val));
    },
    getTemplates: val => {
      dispatch(CampaignActions.getTemplatenData(val));
    },
    getCampaignData: (type, val, city) => {
      dispatch(CampaignActions.getCampaignData(type, val, city));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateNewCampaign);
