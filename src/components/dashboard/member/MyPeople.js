import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  FlatList,
} from 'react-native';
import Styles from '../../../styles/Styles';
import IMAGES from '../../../styles/Images';
import NavigationBar from '../../../commonView/NavigationBar';
import COLOR from '../../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {CustomButton} from '../../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import AlertPopup from '../../../commonView/AlertPopup';
import {connect} from 'react-redux';
import BusinessAction from '../../../redux/action/BusinessAction';
import {Locale, showAlertMessage} from '../../../commonView/Helpers';
import API from '../../../api/Api';
import ActivityIndicator from '../../../commonView/ActivityIndicator';
import AuthenticationAction from '../../../redux/action/AuthenticationAction';

let api = new API();

class MyPeople extends React.Component {
  constructor() {
    super();
    this.state = {
      startTime: '',
      endTime: '',
      alertModalVisible: false,
      operationType: '',
      isLoading: false,
      selectedMember: null,
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.closeVerifyModal = this.closeVerifyModal.bind(this);
    this.handleDeleteMember = this.handleDeleteMember.bind(this);
  }

  componentDidMount() {
    this.props.getMemberData();
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ) {
    if (prevProps.membersData != this.props.membersData) {
      this.setState({memberData: this.props.membersData, isLoading: false});
    }
  }

  updateMasterState = (props, value) => {
    this.setState({
      [props]: value,
    });
  };

  closeVerifyModal(val) {
    this.setState({alertModalVisible: false, isLoading: false});
    console.log(val);
    if (val == 1) {
      if (this.state.operationType == 2) {
        this.handleDeleteMember(this.state.selectedMember);
      }
    }
  }

  handleDeleteMember = async member => {
    let that = this;
    // that.setState({isLoading: true});
    api
      .removeMember(member.id)
      .then(json => {
        console.log('member response is:-', json);
        that.setState({isLoading: false});
        let stringData = JSON.stringify(json.data.response);
        if (json.status == 200) {
          that.setState({isLoading: false});
          showAlertMessage(Locale('Successfully removed member'), 1);

          this.props.getMemberData(1);
        } else if (json.status == 400) {
          showAlertMessage(json.data.message, 2);
        } else {
          showAlertMessage(json.data.message, 2);
        }
      })
      .catch(error => {
        that.setState({isLoading: false});
        showAlertMessage(error.response.data.message, 2);
      });
  };

  noMemberMessage() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',

          flex: 1,
        }}>
        <Text style={[Styles.subheading_label, {color: COLOR.WHITE}]}>
          No member(s) added!
        </Text>
      </View>
    );
  }

  render() {
    let {operationType} = this.state;
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <AlertPopup
            title={Locale('ARE YOU SURE')}
            buttonText={Locale('Confirm')}
            desc={
              operationType == 1 ? Locale('Edit user') : Locale('Delete user')
            }
            closeVerifyModal={this.closeVerifyModal}
            modalVisible={this.state.alertModalVisible}
          />
          <ActivityIndicator loading={this.state.isLoading} />
          <SafeAreaView style={Styles.container}>
            <NavigationBar title={'My people'} />

            {this.props.membersData && this.props.membersData.length ? (
              <FlatList
                style={style.flatlist_view}
                data={this.props.membersData}
                renderItem={({item, index}) => (
                  <TouchableWithoutFeedback
                    onPress={() => this.props.navigation.navigate('')}
                    style={{justifyContent: 'center', alignItems: 'center'}}>
                    <View style={[Styles.shadow_view, style.shadowView]}>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                        }}>
                        <View
                          style={[
                            Styles.left_corner_view,
                            {marginVertical: 15},
                          ]}>
                          <Text style={[Styles.extra_small_label]}>
                            {item.role == 1 ? 'Server' : 'Manager'}
                          </Text>
                        </View>
                        {/*<Text*/}
                        {/*  style={[*/}
                        {/*    Styles.extra_small_label,*/}
                        {/*    {color: COLOR.WHITE, marginHorizontal: 15, flex: 1},*/}
                        {/*  ]}>*/}
                        {/*  {item.isConfirmed ? 'Approved' : 'Pending'}*/}
                        {/*</Text>*/}
                        <TouchableOpacity
                          onPress={() => {
                            this.setState({
                              alertModalVisible: true,
                              operationType: 2,
                              selectedMember: item,
                            });
                          }}
                          style={[Styles.back_button, {marginHorizontal: 10}]}>
                          <Image
                            resizeMode={'contain'}
                            style={{height: 30, width: 30}}
                            source={IMAGES.trash}
                          />
                        </TouchableOpacity>
                      </View>
                      <View style={{margin: 15}}>
                        <Text
                          style={[
                            Styles.subheading_label,
                            {color: COLOR.WHITE},
                          ]}>
                          {item.firstName + ' ' + item.lastName}
                        </Text>
                        <Text
                          style={[
                            Styles.bold_body_label,
                            {color: COLOR.WHITE},
                          ]}>
                          {item.email}
                        </Text>
                      </View>
                    </View>
                  </TouchableWithoutFeedback>
                )}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
              />
            ) : (
              <this.noMemberMessage />
            )}
            {this.props.userDetail && this.props.userDetail.role ? (
              this.props.userDetail.role != 1 ? (
                <View style={[style.bottom_button, {marginVertical: 15}]}>
                  <CustomButton
                    onPress={() => this.props.navigation.navigate('AddMember')}
                    text={Locale('Add new member')}
                  />
                </View>
              ) : null
            ) : (
              <View style={[style.bottom_button, {marginVertical: 15}]}>
                <CustomButton
                  onPress={() => this.props.navigation.navigate('AddMember')}
                  text={Locale('Add new member')}
                />
              </View>
            )}
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginTop: 15,
  },
  flatlist_view: {
    marginVertical: 0,
    width: wp(90),
    marginHorizontal: 15,
    alignSelf: 'center',

    borderRadius: 5,
    paddingVertical: 15,
    paddingHorizontal: 10,
  },

  bottom_button: {width: wp(90), marginTop: 20, alignSelf: 'center'},
});

const mapStateToProps = (state, ownProps) => {
  console.log(
    'state.businessReducer.membersData',
    state.businessReducer.membersData,
  );
  return {
    membersData: state.businessReducer.membersData,
    userDetail: state.authReducer.userDetail,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getMemberData: val => {
      dispatch(BusinessAction.getMemberData(val));
    },
    getUserDetail: val => {
      dispatch(AuthenticationAction.getUserInfo(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyPeople);
