import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  ScrollView,
} from 'react-native';
import Styles from '../../../styles/Styles';
import IMAGES from '../../../styles/Images';
import NavigationBar from '../../../commonView/NavigationBar';
import COLOR from '../../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {FloatingTitleTextInputField} from '../../../commonView/FloatingTitleTextInputField';
import {CustomButton} from '../../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import AlertPopup from '../../../commonView/AlertPopup';
import {
  Locale,
  showAlertMessage,
  validateEmail,
} from '../../../commonView/Helpers';
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import BusinessAction from '../../../redux/action/BusinessAction';
import API from '../../../api/Api';
import ActivityIndicator from '../../../commonView/ActivityIndicator';
import AuthenticationAction from '../../../redux/action/AuthenticationAction';

let api = new API();

class AddMember extends React.Component {
  constructor() {
    super();
    this.state = {
      email: '',
      fName: '',
      lName: '',
      role: 1,
      alertModalVisible: false,
      isLoading: false,
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.closeVerifyModal = this.closeVerifyModal.bind(this);
  }

  updateMasterState = (props, value) => {
    this.setState({
      [props]: value,
    });
  };

  closeVerifyModal = val => {
    if (val == 1) {
      this.setState({alertModalVisible: false, isLoading: false});
      this.handleAddmember();
    } else {
      this.setState({alertModalVisible: false, isLoading: false});
    }
  };

  handleAddmember = () => {
    const {email, fName, lName, role} = this.state;
    if (fName.length == 0) {
      showAlertMessage(Locale('Enter first name'), 2);
    } else if (lName.length == 0) {
      showAlertMessage(Locale('Enter last name'), 2);
    } else if (email.length == 0) {
      showAlertMessage(Locale('Enter email address'), 2);
    } else if (validateEmail(email) == false) {
      showAlertMessage(Locale('Enter valid email address'), 2);
    } else {
      let data = JSON.stringify({
        email: email,
        firstName: fName,
        lastName: lName,
        role: role,
      });

      this.setState({isLoading: false});
      api
        .addMember(data)
        .then(json => {
          console.log('member response is:-', json);
          this.setState({isLoading: true});
          let stringData = JSON.stringify(json.data.response);
          if (json.status == 200) {
            if (json.data.status == 200) {
              showAlertMessage(Locale('Successfully added member'), 1);
              this.props.getMemberData(1);
              this.props.navigation.goBack();
            } else {
              showAlertMessage(json.data.message, 2);
              this.setState({isLoading: false});
            }
          } else if (json.status == 400) {
            showAlertMessage(json.data.message, 2);
          } else {
            showAlertMessage(json.data.message, 2);
          }
        })
        .catch(error => {
          this.setState({isLoading: false});
          showAlertMessage(error.response.data.message, 2);
        });
    }
  };

  render() {
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <AlertPopup
            image={IMAGES.security}
            title={Locale('CONFIRMATION')}
            buttonText={Locale('Confirm')}
            desc={
              Locale("You're about to send invitation to ") +
              `${this.state.email}` +
              Locale(' to join your business as ') +
              `${this.state.role == 1 ? Locale('Server') : Locale('Manager')}`
            }
            closeVerifyModal={val => this.closeVerifyModal(val)}
            modalVisible={this.state.alertModalVisible}
          />
          <SafeAreaView style={Styles.container}>
            <NavigationBar title={Locale('Add new member')} />
            <ScrollView style={Styles.container}>
              <View style={[style.shadowView, Styles.shadow_view]}>
                <View
                  style={{
                    flexDirection: 'row',
                    height: 30,
                    alignItems: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({role: 1});
                    }}>
                    <Image
                      source={
                        this.state.role == 1
                          ? IMAGES.react_selected
                          : IMAGES.rectangle7
                      }
                      resizeMode={'contain'}
                      style={{
                        height: 15,
                        width: 15,
                        marginRight: 10,
                        tintColor: COLOR.YELLOW,
                      }}
                    />
                  </TouchableOpacity>
                  <Text style={[Styles.small_label, {color: COLOR.WHITE}]}>
                    {Locale('Server')}
                  </Text>
                  <TouchableWithoutFeedback
                    onPress={() => showAlertMessage(Locale('Server'))}>
                    <Image
                      source={IMAGES.info}
                      resizeMode={'contain'}
                      style={{
                        height: 10,
                        width: 10,
                        marginLeft: 5,
                      }}
                    />
                  </TouchableWithoutFeedback>
                </View>

                {this.props.userDetail && this.props.userDetail.role ? null : (
                  <View
                    style={{
                      flexDirection: 'row',
                      height: 30,
                      alignItems: 'center',
                    }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({role: 2});
                      }}>
                      <Image
                        source={
                          this.state.role == 2
                            ? IMAGES.react_selected
                            : IMAGES.rectangle7
                        }
                        resizeMode={'contain'}
                        style={{
                          height: 15,
                          width: 15,
                          marginRight: 10,
                          tintColor: COLOR.YELLOW,
                        }}
                      />
                    </TouchableOpacity>
                    <Text style={[Styles.small_label, {color: COLOR.WHITE}]}>
                      {Locale('Manager')}
                    </Text>
                    <TouchableWithoutFeedback
                      onPress={() => showAlertMessage(Locale('Manager'))}>
                      <Image
                        source={IMAGES.info}
                        resizeMode={'contain'}
                        style={{
                          height: 10,
                          width: 10,
                          marginLeft: 5,
                        }}
                      />
                    </TouchableWithoutFeedback>
                  </View>
                )}
              </View>
              <View style={[style.shadowView, Styles.shadow_view]}>
                <FloatingTitleTextInputField
                  attrName={'fName'}
                  value={this.state.fName}
                  updateMasterState={this.updateMasterState}
                  title={Locale('First Name')}
                />
                <FloatingTitleTextInputField
                  attrName={'lName'}
                  value={this.state.lName}
                  updateMasterState={this.updateMasterState}
                  title={Locale('Last Name')}
                />
                <FloatingTitleTextInputField
                  attrName={'email'}
                  value={this.state.email}
                  floatKeyboardType={'email-address'}
                  updateMasterState={this.updateMasterState}
                  title={Locale('Email')}
                />
              </View>
              <View style={{width: wp(90), alignSelf: 'center', marginTop: 50}}>
                <CustomButton
                  onPress={() => {
                    this.setState({alertModalVisible: true});
                  }}
                  text={Locale('Send invitation email')}
                />
              </View>
            </ScrollView>
            <ActivityIndicator loading={this.state.isLoading} />
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 10,
    width: wp(90),
    alignSelf: 'center',
    marginTop: 20,
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  topbar: {
    height: 45,
    width: wp(100),
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    membersData: state.businessReducer.membersData,
    userDetail: state.authReducer.userDetail,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getMemberData: val => {
      dispatch(BusinessAction.getMemberData(val));
    },
    getUserDetail: val => {
      dispatch(AuthenticationAction.getUserInfo(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddMember);
