import React from 'react';
import {View, SafeAreaView, StyleSheet, Text, Image} from 'react-native';
import Styles from '../../../../styles/Styles';
import COLOR from '../../../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import FONTS from '../../../../styles/Fonts';
import NavigationBar from '../../../../commonView/NavigationBar';
import LinearGradient from 'react-native-linear-gradient';
import AuthenticationAction from '../../../../redux/action/AuthenticationAction';
import {connect} from 'react-redux';
import {Locale, showAlertMessage} from '../../../../commonView/Helpers';
import API from '../../../../api/Api';
import AlertPopup from '../../../../commonView/AlertPopup';
import IMAGES from '../../../../styles/Images';
import CampaignActions from '../../../../redux/action/CampaignActions';
import ActivityIndicator from '../../../../commonView/ActivityIndicator';
import ActionSheet from '../../../../commonView/ActionSheet';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

let api = new API();

class CreditHistoryDetails extends React.Component {
  constructor() {
    super();
    this.state = {
      campaignDetail: null,
      actionSheetVisible: false,
      alertModalVisible: false,
      isLoading: false,
      disputedText: '',
    };
    this.onTextChange = this.onTextChange.bind(this);
    this.callRequestAPI = this.callRequestAPI.bind(this);
    this.closeVerifyModal = this.closeVerifyModal.bind(this);
    this.handleSheet = this.handleSheet.bind(this);
  }

  componentDidMount() {
    this.props.getUserDetail();
    if (this.props.route.params) {
      if (this.props.route.params.data) {
        console.log('campaign detial:- ', this.props.route.params.data);
        this.setState({
          campaignDetail: this.props.route.params.data,
        });
      }
    }
  }

  onTextChange(val) {
    console.log('text value:-', val);

    this.setState({disputedText: val});
  }

  commonView = props => {
    return (
      <View
        style={{
          flexDirection: 'row',
          width: '90%',
          alignSelf: 'center',
          marginTop: 10,
        }}>
        <Text
          style={[Styles.small_label, {width: 110, alignSelf: 'flex-start'}]}>
          {props.attr}
        </Text>
        <Text
          style={[
            Styles.body_label,
            {flex: 1, alignSelf: 'flex-start', color: COLOR.DARK_BLUE},
          ]}>
          {props.value}
        </Text>
      </View>
    );
  };

  callRequestAPI() {
    if (this.state.disputedText == '') {
      showAlertMessage(Locale('Enter reason for raising dispute'));
    } else {
      this.setState({isLoading: false, actionSheetVisible: false});
      let data = JSON.stringify({
        credit: this.state.campaignDetail.id,
        description: this.state.disputedText,
      });
      api
        .raiseDispute(data)
        .then(json => {
          console.log('dispute response is:-', json);
          this.setState({isLoading: false});
          if (json.status == 200) {
            this.props.getCreditRedeemHistoryData(1);
            this.setState({alertModalVisible: true});
          } else if (json.status == 400) {
            showAlertMessage(json.data.message, 2);
          } else {
            showAlertMessage(json.data.message, 2);
          }
        })
        .catch(error => {
          this.setState({isLoading: false});
          if (error.response.data) {
            console.log('dispute response is:-', error);
            showAlertMessage(error.response.data.message, 2);
          }
        });
    }
  }

  closeVerifyModal = () => {
    this.setState({alertModalVisible: false}, () => {
      this.setState({
        amountRequested: '',
        billNumber: '',
        billImage: null,
        memberCount: 1,
      });
    });
  };

  handleSheet = type => {
    if (type == 2) {
      this.setState({actionSheetVisible: false});
    } else if (type == 4) {
      this.callRequestAPI();
    }
  };

  render() {
    let {campaignDetail} = this.state;
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <AlertPopup
            image={IMAGES.security}
            title={Locale('REQUEST DISPUTE')}
            buttonText={'Home'}
            prop={this.props}
            desc={Locale('Dispute response send successfully')}
            closeVerifyModal={this.closeVerifyModal}
            modalVisible={this.state.alertModalVisible}
          />

          <ActivityIndicator loading={this.state.isLoading} />
          <SafeAreaView style={Styles.container}>
            <NavigationBar />
            <KeyboardAwareScrollView>
              <ActionSheet
                isDispute={true}
                onChange={this.onTextChange}
                handleSheet={this.handleSheet}
                modalVisible={this.state.actionSheetVisible}
              />
              {campaignDetail && (campaignDetail.bill ?? '') != '' ? (
                <View
                  style={[
                    style.shadowView,
                    Styles.shadow_view,
                    {marginTop: 20, marginBottom: 0},
                  ]}>
                  <View
                    style={{
                      alignItems: 'center',
                      alignSelf: 'center',
                      margin: 10,
                    }}>
                    <Image
                      source={
                        campaignDetail
                          ? {uri: campaignDetail.bill}
                          : IMAGES.visibility
                      }
                      style={{height: 200, width: 250}}
                      resizeMode={'cover'}
                    />
                  </View>

                  <Text
                    style={[
                      Styles.small_label,
                      {
                        alignSelf: 'center',
                        width: wp(50),
                        textAlign: 'center',
                        marginTop: 0,
                        fontFamily: FONTS.FAMILY_SEMIBOLD,
                      },
                    ]}>
                    {Locale('Bill Image')}
                  </Text>
                </View>
              ) : null}
              <View
                style={[style.shadowView, Styles.shadow_view, {marginTop: 20}]}>
                <this.commonView
                  attr={Locale('Amount')}
                  value={campaignDetail ? campaignDetail.amount + ' THB' : ''}
                />
                <this.commonView
                  attr={Locale('Campaign Name')}
                  value={campaignDetail ? campaignDetail.campaign.name : ''}
                />
                <this.commonView
                  attr={Locale('Bill number')}
                  value={campaignDetail ? campaignDetail.billNumber : ''}
                />
                <View
                  style={[Styles.line_view, {width: '90%', marginVertical: 15}]}
                />
                <this.commonView
                  attr={Locale('Name')}
                  value={
                    this.props.userDetail
                      ? (this.props.userDetail.firstName ?? '') +
                        ' ' +
                        (this.props.userDetail.lastName ?? '')
                      : ''
                  }
                />
                <this.commonView
                  attr={'Email'}
                  value={
                    this.props.userDetail ? this.props.userDetail.email : ''
                  }
                />
                <this.commonView
                  attr={Locale('Customer ID')}
                  value={
                    this.props.userDetail ? this.props.userDetail.bbb_id : ''
                  }
                />
              </View>
            </KeyboardAwareScrollView>
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 20,
    paddingVertical: 15,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.authReducer.userInfo,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserDetail: val => {
      dispatch(AuthenticationAction.getUserInfo(val));
    },
    getCreditRedeemHistoryData: val => {
      dispatch(CampaignActions.getCreditRedeemHistoryData(val));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreditHistoryDetails);
