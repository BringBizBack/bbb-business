import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TextInput,
  Image,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import Styles from '../../../../styles/Styles';
import IMAGES from '../../../../styles/Images';
import NavigationBar from '../../../../commonView/NavigationBar';
import COLOR from '../../../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {CustomButton} from '../../../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import AlertPopup from '../../../../commonView/AlertPopup';
import {FloatingTitleTextInputField} from '../../../../commonView/FloatingTitleTextInputField';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  getDateFromTimeStamp,
  Locale,
  showAlertMessage,
} from '../../../../commonView/Helpers';
import ActivityIndicator from '../../../../commonView/ActivityIndicator';
import API from '../../../../api/Api';
import CampaignActions from '../../../../redux/action/CampaignActions';
import {connect} from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';
import {U_BASE, U_FILE_UPLOAD} from '../../../../commonView/Constants';
import ActionSheet from '../../../../commonView/ActionSheet';

import Lightbox from 'react-native-lightbox';

let api = new API();

class GiveCredits extends React.Component {
  constructor(props) {
    super();
    this.state = {
      userDetail: null,
      alertModalVisible: false,
      operationType: 0,
      amountRequested: '',
      billNo: '',
      memberCount: 1,
      isLoading: false,
      selectedCampaign: null,
      actionModalVisible: false,
      billImage: null,
      isCustomerRequest: false,
      customerRequestId: null,
      isCreditConfirmation: false,
      isHomeButtonActive: false,
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.callCreditAPI = this.callCreditAPI.bind(this);
    this.handleDropdown = this.handleDropdown.bind(this);
    this.closeVerifyModal = this.closeVerifyModal.bind(this);
    this.handleSubmitButton = this.handleSubmitButton.bind(this);
  }

  componentDidMount() {
    if (this.props.route.params) {
      if (this.props.route.params.type) {
        this.setState({
          amountRequested: this.props.route.params.amount
            ? this.props.route.params.amount
            : '',
          operationType: this.props.route.params.type,
          userDetail: this.props.route.params.data,
          billNo: this.props.route.params.billNumber
            ? this.props.route.params.billNumber
            : '',
          billImage: this.props.route.params.billImage
            ? this.props.route.params.billImage
            : null,
          selectedCampaign: this.props.route.params.campaign
            ? this.props.route.params.campaign
            : null,
          isCustomerRequest: this.props.route.params.isRequest,
          customerRequestId: this.props.route.params.requestId
            ? this.props.route.params.requestId
            : null,
          memberCount: this.props.route.params.memberCount
            ? this.props.route.params.memberCount
            : '1',
        });
      }
    }
  }

  updateMasterState = (props, value) => {
    if (props == 'amountRequested') {
      let data = JSON.stringify({
        user: this.state.userDetail ? this.state.userDetail.id : 0,
        amount: value,
        type: this.state.operationType ? this.state.operationType : 1,
      });
      if (this.state.operationType == 2) {
        api
          .getCampaingForRedeem(data)
          .then(json => {
            if (json.status == 200) {
              if (json.data.status == 200) {
                console.log('selected campaign', json.data.response);
                let selectedCampaign = json.data.response.campaign;
                selectedCampaign.campaignTemplate =
                  json.data.response.campaignTemplate;
                selectedCampaign.trading = json.data.response.trading;
                this.setState({
                  selectedCampaign: selectedCampaign,
                });
              } else {
                this.setState({
                  selectedCampaign: null,
                });
                showAlertMessage(
                  json.data.message ? json.data.message : 'No campaign found',
                  2,
                );
              }
            } else if (json.status == 400) {
              this.setState({
                selectedCampaign: null,
              });
              showAlertMessage(json.data.message, 2);
            } else {
              showAlertMessage(json.data.message, 2);
            }
          })
          .catch(error => {
            console.log('campaign error is:-', error);
            showAlertMessage(error.response.data.message, 2);
          });
      }
    }
    if (props == 'billNo') {
      this.setState({
        [props]: value.replace(',', ''),
      });
    } else {
      this.setState({
        [props]: value,
      });
    }
  };

  handleDropdown = props => {
    this.setState({
      selectedCampaign: this.props.campaignData.filter(item => {
        if (item.id === props) {
          return item;
        }
      })[0],
    });
  };

  commonView = props => {
    return (
      <View
        pointerEvents="none"
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: 10,
        }}>
        <Text style={[Styles.bold_body_label, style.titleLabel]}>
          {props.title}
        </Text>
        <TextInput
          multiline={true}
          style={[
            Styles.bold_body_label,
            {color: COLOR.WHITE, flex: 1, margin: 0, fontSize: 13},
          ]}
          value={props.value}
        />
      </View>
    );
  };

  closeVerifyModal(val) {
    if (this.state.isHomeButtonActive) {
      this.props.navigation.navigate('MyTabs', {index: 0});
    }
    if (val == '1') {
      this.setState({alertModalVisible: false}, () => {
        if (this.state.isCreditConfirmation) {
          this.setState({isCreditConfirmation: false}, () => {
            this.callCreditAPI();
          });
        } else {
          this.props.navigation.navigate('MyTabs', {index: 0});
        }
      });
    } else {
      this.setState({alertModalVisible: false});
    }
  }

  handleSheet = val => {
    if (val == 1) {
      this.chooseImage(1);
    } else if (val == 2) {
      this.chooseImage(2);
    } else if (val == 3) {
      this.setState({
        actionModalVisible: false,
      });
    }
  };

  chooseImage = val => {
    let options = {
      title: Locale('Select Image'),
      compressImageMaxWidth: 1500,
      compressImageMaxHeight: 700,
      multiple: false,
      cropping: true,
      freeStyleCropEnabled: true,
    };

    if (val == 1) {
      ImagePicker.openCamera({
        title: Locale('Select Image'),
        compressImageMaxWidth: 1500,
        compressImageMaxHeight: 700,
        cropping: true,
        freeStyleCropEnabled: true,
      })
        .then(response => {
          if (response.didCancel) {
            console.log('User cancelled  Camera');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            showAlertMessage(response.customButton);
          } else {
            this.setState({
              isLoading: false,
              actionModalVisible: false,
            });
            this.callUploadImage(response, val);
          }
        })
        .catch(error => {
          if (error.message) {
            showAlertMessage(error.message);
          }
        });
    } else if (val == 2) {
      ImagePicker.openPicker(options)
        .then(response => {
          console.log('image local reposnse', response);
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            alert(response.customButton);
          } else {
            this.setState({
              //isLoading: true,
              actionModalVisible: false,
            });

            this.callUploadImage(response, val);
          }
        })
        .catch(error => {
          if (error.message) {
            showAlertMessage(error.message);
          }
        });
    }
  };

  callUploadImage(res, val) {
    var data = new FormData();
    var photo = {
      uri: res.path,
      type: res.type ? res.type : res.mime,
      name: 'image.jpg',
    };
    console.log('image uploading data is:-', photo);
    data.append('file', photo);
    fetch(U_BASE + U_FILE_UPLOAD, {
      body: data,
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Authorization: 'Bearer ' + this.state.token,
      },
    })
      .then(response => response.json())
      .catch(error => {
        console.log('error data is:-', error);
        this.setState({isLoading: false});
      })
      .then(responseData => {
        this.setState({isLoading: false});
        console.log('response data is:-', responseData);
        if (responseData.status == 200) {
          showAlertMessage('Image uploaded successfully', 1);
          this.setState({billImage: responseData.response.imageUrl});
        } else {
          this.setState({isLoading: false});
          showAlertMessage('Image uploading failed');
        }
      })
      .done();
  }

  callCreditAPI() {
    if (!this.state.selectedCampaign) {
      showAlertMessage(Locale('No campaign found'));
    } else if (this.state.userDetail == null) {
      showAlertMessage(Locale('Select user'));
    } else if (this.state.memberCount == 0) {
      showAlertMessage(Locale('Enter member count'));
    } else if (this.state.amountRequested == '') {
      showAlertMessage(Locale('Enter amount requested'));
    } else if (this.state.billNo == '') {
      showAlertMessage(Locale('Enter bill number'));
    } else if (!this.state.billImage) {
      showAlertMessage(Locale('Upload bill image'));
    } else {
      // this.setState({isLoading: true});
      let data = JSON.stringify({
        trading: this.state.selectedCampaign.trading.id,
        billNumber: this.state.billNo,
        campaign: this.state.selectedCampaign.id,
        amount: this.state.amountRequested,
        visitors: this.state.memberCount,
        bill: this.state.billImage ?? '',
        user: this.state.userDetail.id,
      });
      console.log(this.state.userDetail, this.state.selectedCampaign);
      api
        .giveCreditCustomer(
          this.state.isCustomerRequest ? this.state.customerRequestId : data,
          this.state.operationType,
          this.state.isCustomerRequest,
        )
        .then(json => {
          console.log('credit response is:-', json);
          this.setState({isLoading: false});
          if (json.status == 200) {
            if (json.data.status == 200) {
              // this.props.getCampaignData(1, 1);
              showAlertMessage(Locale('Successfully transferred credit'), 1);
              this.setState({
                alertModalVisible: true,
                isHomeButtonActive: true,
              });
            } else {
              showAlertMessage(json.data.message, 2);
            }
          } else if (json.data.status == 400) {
            showAlertMessage(json.data.message, 2);
          } else {
            showAlertMessage(json.data.message, 2);
          }
        })
        .catch(error => {
          console.log('error is:-', error);
          this.setState({isLoading: false});
          if (error.response.data) {
            showAlertMessage(error.response.data.message, 2);
          }
        });
    }
  }

  handleSubmitButton() {
    if (!this.state.selectedCampaign) {
      return;
    }
    let visitorEligible = parseInt(
      parseInt(this.state.amountRequested) /
        this.state.selectedCampaign.minSpent,
    );

    if (
      this.state.userDetail &&
      !this.state.userDetail.cardNumber &&
      this.state.operationType == 2
    ) {
      showAlertMessage(
        'Customer needs to fill in profile details before requesting for redeem',
      );
    } else if (!this.state.amountRequested) {
      showAlertMessage('Enter amount', 2);
    } else if (
      this.state.amountRequested < this.state.selectedCampaign.minSpent &&
      this.state.operationType != 2
    ) {
      showAlertMessage(
        `Entered amount should be more than minimum spent.(${this.state.selectedCampaign.minSpent} THB)`,
      );
    } else if (
      visitorEligible < this.state.memberCount &&
      this.state.memberCount > 1 &&
      this.state.operationType != 2
    ) {
      Alert.alert(
        'Alert',
        `Only ${visitorEligible} visits will be counted for this transaction as per criteria for minimum spent per visit under this campaign.`,
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: 'OK',
            onPress: () => {
              this.setState({
                memberCount: visitorEligible,
              });
            },
          },
        ],
      );
    } else if (!this.state.billImage) {
      showAlertMessage(Locale('Upload bill image'));
    } else {
      this.setState({
        isCreditConfirmation: true,
        alertModalVisible: true,
      });
    }
  }

  render() {
    let {operationType, isCreditConfirmation} = this.state;
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <AlertPopup
            image={IMAGES.security}
            title={
              operationType == 1
                ? 'GIVE ' + `${this.state.amountRequested}` + ' POINTS'
                : `${this.state.amountRequested}` + ' CREDITS'
            }
            buttonText={
              isCreditConfirmation ? Locale('Confirm') : Locale('Home')
            }
            desc={
              isCreditConfirmation
                ? Locale('Are you sure to redeem points, please confirm?')
                : operationType == 1
                ? `${Locale('Given to')} ${
                    this.state.userDetail
                      ? (this.state.userDetail.firstName ?? '') +
                        ' ' +
                        (this.state.userDetail.lastName ?? '')
                      : ''
                  }`
                : `${Locale('Credits redeemed to')} ${
                    this.state.userDetail
                      ? (this.state.userDetail.firstName ?? '') +
                        ' ' +
                        (this.state.userDetail.lastName ?? '')
                      : ''
                  }.\n` +
                  // 'Transaction ID - #017615\n' +
                  `${Locale('to')} ${
                    this.state.selectedCampaign
                      ? this.state.selectedCampaign.campaign
                        ? this.state.selectedCampaign.campaign.name
                        : ''
                      : ''
                  }`
            }
            closeVerifyModal={this.closeVerifyModal}
            modalVisible={this.state.alertModalVisible}
          />
          <ActivityIndicator loading={this.state.isLoading} />
          <SafeAreaView style={Styles.container}>
            <NavigationBar
              title={
                operationType == 1
                  ? Locale('Customer seeking points')
                  : Locale('Redeem customer credits')
              }
            />
            <KeyboardAwareScrollView>
              <View
                style={[
                  style.shadowView,
                  Styles.shadow_view,
                  {paddingBottom: 10},
                ]}>
                <View
                  style={[
                    Styles.left_corner_view,
                    {marginVertical: 15, position: 'absolute'},
                  ]}>
                  <Text style={[Styles.extra_small_label]}>
                    {Locale('Customer details')}
                  </Text>
                </View>
                <View style={{marginTop: 35}}>
                  <this.commonView
                    title={Locale('Name')}
                    value={
                      this.state.userDetail
                        ? this.state.userDetail.firstName +
                          ' ' +
                          this.state.userDetail.lastName
                        : ''
                    }
                  />
                  <this.commonView
                    title={Locale('Goal Prize ID')}
                    value={
                      this.state.userDetail ? this.state.userDetail.bbb_id : ''
                    }
                  />
                  <this.commonView
                    title={Locale('Email')}
                    value={
                      this.state.userDetail ? this.state.userDetail.email : ''
                    }
                  />

                  <this.commonView
                    title={Locale('Phone number')}
                    value={
                      this.state.userDetail ? this.state.userDetail.phone : ''
                    }
                  />
                </View>
              </View>
              {operationType == 2 && (
                <View
                  style={[
                    style.shadowView,
                    Styles.shadow_view,
                    {paddingBottom: 10},
                  ]}>
                  <View
                    style={[
                      Styles.left_corner_view,
                      {marginVertical: 15, position: 'absolute'},
                    ]}>
                    <Text style={[Styles.extra_small_label]}>
                      Id verification
                    </Text>
                  </View>
                  <View style={{marginTop: 35}}>
                    <this.commonView
                      title={Locale('ID card')}
                      value={
                        this.state.userDetail
                          ? this.state.userDetail.cardType
                            ? this.state.userDetail.cardType.value
                            : ''
                          : ''
                      }
                    />
                    <this.commonView
                      title={Locale('ID number')}
                      value={
                        this.state.userDetail
                          ? this.state.userDetail.cardNumber
                          : ''
                      }
                    />
                    <this.commonView
                      title={Locale('Verified by')}
                      value={Locale('Admin')}
                    />

                    <this.commonView
                      title={Locale('Date verified')}
                      value={
                        this.state.userDetail
                          ? getDateFromTimeStamp(
                              this.state.userDetail.createdAt,
                              'MMM DD, YYYY',
                            )
                          : ''
                      }
                    />
                  </View>
                </View>
              )}
              <View
                style={[Styles.shadow_view, style.shadowView, {marginTop: 0}]}>
                <View
                  style={[
                    Styles.left_corner_view,
                    {marginTop: 15, position: 'absolute'},
                  ]}>
                  <Text style={[Styles.extra_small_label]}>
                    {operationType == 1
                      ? Locale('Points requested on this bill')
                      : Locale('Credit redeemed on this bill')}
                  </Text>
                </View>
                <View
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    padding: 15,
                    paddingBottom: 0,
                    marginTop: 30,
                  }}>
                  <View style={{marginBottom: 15}}>
                    <FloatingTitleTextInputField
                      isWhite={true}
                      floatKeyboardType={'decimal-pad'}
                      attrName={'amountRequested'}
                      value={
                        this.state.amountRequested
                          ? this.state.amountRequested
                          : ''
                      }
                      updateMasterState={(val, b) =>
                        this.updateMasterState(val, b)
                      }
                      title={
                        operationType == 1
                          ? Locale('Amount Requested')
                          : Locale('Amount Redeemed')
                      }
                    />
                    <Text
                      style={[
                        Styles.extra_small_label,
                        {
                          color: COLOR.WHITE,
                          alginSelf: 'flex-start',
                          marginTop: -15,
                          textAlign: 'left',
                        },
                      ]}>
                      {Locale('Do not include tax & tip')}
                    </Text>
                  </View>

                  <FloatingTitleTextInputField
                    isWhite={true}
                    editable={false}
                    value={
                      this.state.selectedCampaign
                        ? this.state.selectedCampaign.campaignTemplate.name
                        : ''
                    }
                    title={Locale('Campaign name')}
                  />
                  <View style={{marginVertical: 15}}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginBottom: 10,
                        paddingHorizontal: 10,
                      }}>
                      <TouchableWithoutFeedback
                        onPress={() => {
                          if (this.state.memberCount > 1) {
                            this.setState({
                              memberCount: parseInt(this.state.memberCount) - 1,
                            });
                          }
                        }}>
                        <Image
                          source={IMAGES.minus}
                          style={style.icon_image}
                          resizeMode={'contain'}
                        />
                      </TouchableWithoutFeedback>
                      <View
                        style={{
                          alignItems: 'center',
                          justifyContent: 'center',
                          flex: 1,
                        }}>
                        <Text
                          style={[
                            Styles.extra_small_label,
                            {color: COLOR.WHITE},
                          ]}>
                          {Locale('Visitor(s) in group')}
                        </Text>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                          }}>
                          <Image
                            source={IMAGES.user_yellow}
                            style={{height: 12, width: 12, marginRight: 5}}
                            resizeMode={'contain'}
                          />
                          <Text
                            style={[Styles.button_font, {color: COLOR.WHITE}]}>
                            {this.state.memberCount} {Locale('members')}
                          </Text>
                        </View>
                      </View>
                      <TouchableWithoutFeedback
                        onPress={() => {
                          this.setState({
                            memberCount: parseInt(this.state.memberCount) + 1,
                          });
                        }}>
                        <Image
                          source={IMAGES.plus}
                          style={style.icon_image}
                          resizeMode={'contain'}
                        />
                      </TouchableWithoutFeedback>
                    </View>
                    <View style={Styles.line_view} />
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1}}>
                      <FloatingTitleTextInputField
                        isWhite={true}
                        attrName={'billNo'}
                        value={this.state.billNo}
                        updateMasterState={this.updateMasterState}
                        title={Locale('Bill number')}
                      />
                    </View>
                    {/*<View*/}
                    {/*  style={[*/}
                    {/*    Styles.shadow_view,*/}
                    {/*    {*/}
                    {/*      flex: 1,*/}
                    {/*      alignItems: 'center',*/}
                    {/*      justifyContent: 'center',*/}
                    {/*      backgroundColor: COLOR.LIGHT_BLUE,*/}
                    {/*      padding: 0,*/}
                    {/*      height: 40,*/}
                    {/*    },*/}
                    {/*  ]}>*/}
                    {/*  <Text*/}
                    {/*    style={[*/}
                    {/*      Styles.extra_small_label,*/}
                    {/*      {*/}
                    {/*        color: COLOR.WHITE,*/}
                    {/*        textDecorationLine: 'underline',*/}
                    {/*        margin: 0,*/}
                    {/*      },*/}
                    {/*    ]}>*/}
                    {/*    Where to find bill number*/}
                    {/*  </Text>*/}
                    {/*</View>*/}
                  </View>
                  <View>
                    <View
                      style={{
                        height: 40,
                        width: '60%',
                        alignSelf: 'center',
                        marginBottom: 20,
                      }}>
                      <CustomButton
                        onPress={() =>
                          this.setState({actionModalVisible: true})
                        }
                        bg={COLOR.WHITE}
                        image={IMAGES.camera}
                        text={Locale('Photograph Bill')}
                      />
                    </View>
                    {this.state.billImage && (
                      <Lightbox
                        navigator={this.props.navigator}
                        underlayColor={'transparent'}
                        backgroundColor={'#ffffff'}
                        activeProps={{
                          style: {
                            width: 500,
                            height: 500,
                            alignSelf: 'center',
                          },
                          resizeMode: 'contain',
                        }}>
                        <Image
                          style={{
                            height: 100,
                            width: 100,
                            alignSelf: 'center',
                            marginBottom: 15,
                          }}
                          source={{uri: this.state.billImage}}
                        />
                      </Lightbox>
                    )}
                    <View style={{flex: 1}}>
                      <CustomButton
                        onPress={() => this.handleSubmitButton()}
                        text={
                          operationType == 1
                            ? Locale('Give points')
                            : Locale('Redeem Credit')
                        }
                      />
                    </View>
                  </View>
                </View>
              </View>
            </KeyboardAwareScrollView>
            <ActionSheet
              modalVisible={this.state.actionModalVisible}
              handleSheet={this.handleSheet}
            />
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 15,
    padding: 15,
  },
  bottomButton: {marginVertical: 20, width: wp(90), alignSelf: 'center'},
  titleLabel: {color: COLOR.WHITE, width: 120, margin: 0, fontSize: 13},
  icon_image: {
    height: 40,
    width: 40,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    campaignData: state.campaignReducer.currentCampaign,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCampaignData: val => {
      dispatch(CampaignActions.getCampaignData(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GiveCredits);
