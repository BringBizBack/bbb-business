import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  FlatList,
  Image,
  TouchableWithoutFeedback,
  Linking,
  RefreshControl,
} from 'react-native';
import Styles from '../../../../styles/Styles';
import IMAGES from '../../../../styles/Images';
import NavigationBar from '../../../../commonView/NavigationBar';
import COLOR from '../../../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {CustomButton} from '../../../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import {FloatingTitleTextInputField} from '../../../../commonView/FloatingTitleTextInputField';
import {
  getDateFromTimeStamp,
  getTimestampFromDate,
  insertComma,
  Locale,
  showAlertMessage,
  validateEmail,
} from '../../../../commonView/Helpers';
import ActivityIndicator from '../../../../commonView/ActivityIndicator';
import API from '../../../../api/Api';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import QRCodeView from '../../../../commonView/QRCodeView';
import CampaignActions from '../../../../redux/action/CampaignActions';
import {connect} from 'react-redux';
import AuthenticationAction from '../../../../redux/action/AuthenticationAction';

let api = new API();

class GiveCustomerCredit extends React.Component {
  constructor(props) {
    super();
    this.state = {
      bbbNumber: '',
      customerEmail: '',
      phoneNumber: '',
      operationType: '',
      billImage: '',
      isLoading: false,
      billNumber: '',
      qrModalVisible: false,
      amount: '',
      campaignData: [],
      requestData: null,
      refreshing: false,
      memberCount: '',
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.callCreditAPI = this.callCreditAPI.bind(this);
    this.handleQRModal = this.handleQRModal.bind(this);
    this.searchUserRecord = this.searchUserRecord.bind(this);
    this.handleRequestData = this.handleRequestData.bind(this);
  }

  componentDidMount() {
    if (this.props.route.params) {
      if (this.props.route.params.type) {
        this.setState({operationType: this.props.route.params.type});
        if (this.props.route.params.type == 1) {
          this.props.getCampaignData(2, 1);
          this.handleRequestData(1);
        } else {
          this.props.getCampaignData(3, 1);
          this.handleRequestData(2);
        }
      }
    }
    if (this.props.operationType) {
      if (this.props.operationType == 1) {
        this.handleRequestData(1);
      } else {
        this.handleRequestData(2);
      }
    }
  }

  componentWillUnmount() {
    this.setState({
      bbbNumber: '',
      customerEmail: '',
      phoneNumber: '',
      operationType: '',
      billImage: '',
      isLoading: false,
      billNumber: '',
      qrModalVisible: false,
      amount: '',
      requestData: null,
    });
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ) {
    if (
      prevProps.campaignData != this.props.campaignData &&
      this.state.operationType == 1
    ) {
      this.setState({campaignData: this.props.campaignData});
    } else if (
      prevProps.pastCampaign != this.props.pastCampaign &&
      this.state.operationType == 2
    ) {
      this.setState({campaignData: this.props.pastCampaign});
    }
  }

  updateMasterState = (props, value) => {
    if (props == 'phoneNumber') {
      if (value.length <= 10) {
        this.setState({
          [props]: value,
        });
      }
    } else {
      this.setState({
        [props]: value,
      });
    }
  };

  handleRequestData(val) {
    this.setState({isLoading: true});
    let that = this;
    if (val == 1) {
      api
        .getCampaignCreditRequest()
        .then(json => {
          console.log('tes data is:-', json);
          if (json.data.status == 200) {
            console.log('credit request data.response', json.data.response);
            this.setState({requestData: json.data.response, isLoading: false});
          } else {
            that.setState({isLoading: false});
            console.log('error is', json.data);
            // showAlertMessage(json);
          }
        })
        .catch(function (error) {
          that.setState({isLoading: false});
          console.log('error is', error);
          // showAlertMessage(error.response.data.message);
        });
    } else {
      api
        .getCampaignRedeemRequest()
        .then(json => {
          console.log('credit redeem test data is:-', json);
          if (json.data.status == 200) {
            that.setState({requestData: json.data.response, isLoading: false});
          } else {
            that.setState({isLoading: false});
            showAlertMessage(json.data.message);
          }
        })
        .catch(function (error) {
          console.log('error is', error);
          that.setState({isLoading: false});
          //showAlertMessage(error.response.data.message);
        });
    }
  }

  callCreditAPI() {
    if (this.state.campaignData ? this.state.campaignData.length == 0 : false) {
      showAlertMessage(Locale('No campaign found'));
    } else if (
      this.state.bbbNumber == '' &&
      this.state.customerEmail == '' &&
      this.state.phoneNumber == '' &&
      this.state.billNumber == ''
    ) {
      showAlertMessage(Locale('Enter any one of the above details'), '2');
    } else {
      let query = '';
      if (this.state.bbbNumber != '' && this.state.bbbNumber.length == 9) {
        query = this.state.bbbNumber;
      } else if (
        this.state.customerEmail != '' &&
        validateEmail(this.state.customerEmail)
      ) {
        query = this.state.customerEmail;
      } else if (
        this.state.phoneNumber != '' &&
        this.state.phoneNumber.length >= 9
      ) {
        query = this.state.phoneNumber;
      } else if (this.state.billNumber != '') {
        query = this.state.billNumber;
      } else {
        showAlertMessage(Locale('Enter correct details'));
        return;
      }
      if (this.state.campaignData.length > 0) {
        this.searchUserRecord(
          query,
          this.state.operationType,
          this.state.operationType == 1 ? this.state.campaignData[0] : null,
        );
      }
    }
  }

  searchUserRecord(query, type, campaign) {
    if (query != '') {
      this.setState({isLoading: true});
      api
        .searchUserRecord(JSON.stringify({query: query}))
        .then(json => {
          this.setState({isLoading: false});
          if (json.data.status == 200) {
            console.log(
              'user record data is:-',
              json.data.response,
              this.state,
            );
            this.props.navigation.navigate('GiveCredits', {
              data: json.data.response,
              type: type ? type : this.state.operationType,
              amount: (this.state.amount ?? '').replace(',', ''),
              billImage: this.state.billImage ?? '',
              billNumber: this.state.billNumber ?? '',
              campaign: campaign,
              isRequest: false,
              memberCount: this.state.memberCount ?? '',
            });
            this.setState({
              customerEmail: '',
              bbbNumber: '',
              billNumber: '',
              billImage: '',
            });
          } else if (json.data.status == 400) {
            showAlertMessage(json.data.message, 2);
          } else {
            showAlertMessage(json.data.message, 2);
          }
        })
        .catch(error => {
          console.log('user record error is:-', error);
          this.setState({isLoading: false});
          showAlertMessage(error.response.data.message, 2);
        });
    }
  }

  onSuccess = e => {
    Linking.openURL(e.data).catch(err =>
      console.error('An error occured', err),
    );
  };

  getCampaignForRedeem = (amount, camp) => {
    if (this.state.campaignData.length > 0) {
      let selectedCampaign = [];
      for (let i = 0; i < this.state.campaignData.length; i++) {
        if (camp == this.state.campaignData[i].id) {
          selectedCampaign.push(this.state.campaignData[i]);
        }
      }
      if (selectedCampaign.length == 0) {
        return null;
      } else if (selectedCampaign.length == 1) {
        return selectedCampaign[0];
      } else if (selectedCampaign.length > 1) {
        let campaign = selectedCampaign.find(val => {
          return val.id == camp.id;
        });
        if (campaign) {
          return campaign;
        } else {
          return null;
        }
      }
    } else {
      console.log(
        'data is:-',
        amount,
        camp,
        this.state.campaignData,
        this.state.operationType,
      );
      return null;
    }
  };

  handleQRModal = val => {
    if (val == 1) {
      this.setState({
        qrModalVisible: false,
        isLoading: false,
      });
    } else {
      let data = val.data.split(',');
      if (data.length > 4) {
        let campaign = this.getCampaignForRedeem(data[1], data[3]);
        console.log(
          'qr scan value is:-',
          val,
          campaign,
          this.state.campaignData,
        );
        if (campaign != null) {
          //0:email,1:amount,2:type,3:campaignid,4:billnumber,5:billimage
          this.searchUserRecord(data[0], data[2], campaign);
          this.setState({
            qrModalVisible: false,
            amount: data[1],
            isLoading: false,
            billImage: data[5],
            billNumber: data[4],
            memberCount: data.length > 6 ? String(data[6]) : '1',
          });
        } else {
          this.setState({
            qrModalVisible: false,
            isLoading: false,
          });
          showAlertMessage(Locale('No campaign found'));
        }
      }
    }
  };

  render() {
    let {operationType, qrModalVisible} = this.state;
    let {userBusinessDetail} = this.props;
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <ActivityIndicator loading={this.state.isLoading} />
          <QRCodeView
            handleQRModal={this.handleQRModal}
            qrModalVisible={qrModalVisible}
          />
          <SafeAreaView style={Styles.container}>
            <NavigationBar
              title={
                operationType == 1
                  ? Locale('Give customer points')
                  : Locale('Redeem customer credits')
              }
            />
            <KeyboardAwareScrollView
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={() => {
                    this.setState({
                      refreshing: false,
                    });

                    if (this.state.operationType == 1) {
                      this.handleRequestData(1);
                      this.props.getCampaignData(2, 1);
                    } else {
                      this.handleRequestData(2);
                      this.props.getCampaignData(3, 1);
                    }
                  }}
                />
              }>
              {userBusinessDetail && (
                <View
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    marginTop: 10,
                  }}>
                  {userBusinessDetail.legalInfo &&
                    userBusinessDetail.legalInfo.id && (
                      <Text
                        style={[
                          Styles.extra_small_label,
                          {color: COLOR.WHITE},
                        ]}>
                        Business Id:
                        {userBusinessDetail.legalInfo.id}
                      </Text>
                    )}
                  {userBusinessDetail.tradingInfo &&
                    userBusinessDetail.tradingInfo.id && (
                      <Text
                        style={[
                          Styles.extra_small_label,
                          {color: COLOR.WHITE},
                        ]}>
                        Trade Id:
                        {userBusinessDetail.tradingInfo.id}
                      </Text>
                    )}
                </View>
              )}
              {this.state.requestData && this.state.requestData.length ? (
                <Text
                  style={[
                    Styles.extra_small_label,
                    {
                      marginHorizontal: 15,
                      marginVertical: 10,
                      color: COLOR.WHITE,
                    },
                  ]}>
                  {Locale(
                    'Please scroll down to see requests for credit/redeem',
                  )}
                </Text>
              ) : null}
              <View style={[Styles.shadow_view, style.shadowView]}>
                <View style={[Styles.left_corner_view, {marginVertical: 15}]}>
                  <Text style={[Styles.extra_small_label]}>
                    {Locale('Bringup customer record')}
                  </Text>
                </View>
                <View style={{padding: 15, alignItems: 'center'}}>
                  <TouchableWithoutFeedback
                    onPress={() => this.setState({qrModalVisible: true})}>
                    <Image
                      style={{height: 150, width: 150}}
                      source={IMAGES.scan_yellow}
                    />
                  </TouchableWithoutFeedback>
                  <Text
                    style={[
                      Styles.extra_small_label,
                      {
                        color: COLOR.WHITE,
                        alignSelf: 'center',
                        marginTop: 10,
                      },
                    ]}>
                    {Locale("Scan customer's QR")}
                  </Text>
                  <View style={{marginTop: 15, width: '100%'}}>
                    <FloatingTitleTextInputField
                      isWhite={true}
                      attrName={'bbbNumber'}
                      floatKeyboardType={'number-pad'}
                      value={this.state.bbbNumber}
                      updateMasterState={this.updateMasterState}
                      title={Locale('Or type in customer’s GP number')}
                    />
                  </View>
                  <View style={{width: '100%'}}>
                    <FloatingTitleTextInputField
                      isWhite={true}
                      attrName={'customerEmail'}
                      floatKeyboardType={'email-address'}
                      value={this.state.customerEmail}
                      updateMasterState={this.updateMasterState}
                      title={Locale('Or type in customer’s email')}
                    />
                  </View>
                  <View style={{width: '100%'}}>
                    <FloatingTitleTextInputField
                      isWhite={true}
                      floatKeyboardType={'number-pad'}
                      attrName={'phoneNumber'}
                      value={this.state.phoneNumber}
                      updateMasterState={this.updateMasterState}
                      title={Locale('Or type in customer’s number')}
                    />
                  </View>
                  {/* <View style={{width: '100%'}}>
                    <FloatingTitleTextInputField
                      isWhite={true}
                      attrName={'billNumber'}
                      value={this.state.billNumber}
                      updateMasterState={this.updateMasterState}
                      title={Locale('Or type in bill number')}
                    />
                  </View> */}
                  <View style={{width: '100%', marginTop: 50}}>
                    <CustomButton
                      onPress={this.callCreditAPI}
                      text={Locale('Search record')}
                    />
                  </View>
                </View>
              </View>
              {this.state.requestData && this.state.requestData.length > 0 && (
                <View style={[Styles.shadow_view, style.shadowView]}>
                  <View style={[Styles.left_corner_view, {marginVertical: 15}]}>
                    <Text style={[Styles.extra_small_label]}>
                      {Locale('Request history')}
                    </Text>
                  </View>
                  <View
                    style={{
                      paddingHorizontal: 15,
                      alignSelf: 'center',
                      flex: 1,

                      width: wp(90),
                    }}>
                    <FlatList
                      style={style.flatlist_view}
                      data={this.state.requestData}
                      renderItem={({item, index}) => (
                        <TouchableWithoutFeedback
                          onPress={() => {
                            this.props.navigation.navigate('GiveCredits', {
                              data: item.user,
                              type: this.state.operationType,
                              amount: String(item.amount).replace(',', ''),
                              billImage: item.bill,
                              billNumber: item.billNumber ?? '',
                              campaign: item,
                              isRequest: true,
                              requestId: item.id,
                              memberCount: item.visitors ?? '',
                            });
                          }}
                          style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <View
                            style={[
                              Styles.shadow_view,
                              {
                                width: '100%',
                                marginBottom: 5,
                              },
                            ]}>
                            <View
                              style={{
                                marginBottom: 10,
                                marginHorizontal: 0,
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                              }}>
                              <View style={{margin: 0, flex: 1}}>
                                <Text
                                  style={[
                                    Styles.button_font,
                                    {
                                      color: COLOR.WHITE,
                                      margin: 0,
                                      alignSelf: 'flex-start',
                                    },
                                  ]}>
                                  {insertComma(item.amount)} THB
                                </Text>
                                <Text
                                  style={[
                                    Styles.small_label,
                                    {
                                      color: COLOR.WHITE,
                                      margin: 0,
                                      alignSelf: 'flex-start',
                                    },
                                  ]}>
                                  {item.campaignTemplate.name}
                                </Text>
                              </View>
                              <View style={{margin: 0}}>
                                {/*<Text*/}
                                {/*  style={[*/}
                                {/*    Styles.button_font,*/}
                                {/*    {*/}
                                {/*      color: COLOR.WHITE,*/}
                                {/*      margin: 0,*/}
                                {/*      alignSelf: 'flex-end',*/}
                                {/*    },*/}
                                {/*  ]}>*/}
                                {/*  {item.type}*/}
                                {/*</Text>*/}
                                <Text
                                  style={[
                                    Styles.small_label,
                                    {margin: 0, color: COLOR.WHITE},
                                  ]}>
                                  {getDateFromTimeStamp(
                                    item.updatedAt,
                                    'MMM DD, YYYY',
                                  )}
                                </Text>
                              </View>
                            </View>
                          </View>
                        </TouchableWithoutFeedback>
                      )}
                      showsVerticalScrollIndicator={false}
                      showsHorizontalScrollIndicator={false}
                      keyExtractor={(item, index) => index.toString()}
                    />
                  </View>
                </View>
              )}
            </KeyboardAwareScrollView>
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginTop: 15,
  },
  bottomButton: {marginVertical: 20, width: wp(90), alignSelf: 'center'},
  titleLabel: {color: COLOR.WHITE, width: 120, margin: 0, fontSize: 13},
  icon_image: {
    height: 40,
    width: 40,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    campaignData: state.campaignReducer.currentCampaign,
    pastCampaign: state.campaignReducer.pastCampaign,
    userBusinessDetail: state.authReducer.userBusinessDetail,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCampaignData: (type, val) => {
      dispatch(CampaignActions.getCampaignData(type, val));
    },
    getUserBusinessDetail: val => {
      dispatch(AuthenticationAction.getBusinessDetail(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GiveCustomerCredit);
