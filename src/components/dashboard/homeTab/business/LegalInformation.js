import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import Styles from '../../../../styles/Styles';
import IMAGES from '../../../../styles/Images';
import NavigationBar from '../../../../commonView/NavigationBar';
import COLOR from '../../../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {CustomButton} from '../../../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import {FloatingTitleTextInputField} from '../../../../commonView/FloatingTitleTextInputField';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Locale, showAlertMessage} from '../../../../commonView/Helpers';
import {
  AS_USER_TOKEN,
  U_ADD_BUSINESS_IMAGES,
  U_BASE,
} from '../../../../commonView/Constants';
import API from '../../../../api/Api';
import ActivityIndicator from '../../../../commonView/ActivityIndicator';
import ImagePicker from 'react-native-image-crop-picker';
import AuthenticationAction from '../../../../redux/action/AuthenticationAction';
import {connect} from 'react-redux';
import {Dropdown} from '../../../../commonView/Dropdown';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import FONTS from '../../../../styles/Fonts';
import Auth from '../../../../auth';
import axios from 'axios';

let api = new API();
let auth = new Auth();

class LegalInformation extends React.Component {
  constructor() {
    super();
    this.state = {
      legalName: '',
      legalRegistrationNumber: '',
      legalStreetAddress: '',
      legalSubdistrict: null,
      legalDistrict: null,
      legalCity: null,
      legalNation: null,
      legalPostalCode: '',
      tradePostalCode: '',
      isLoading: false,
      actionModalVisible: false,
      currentImage: 1,
      userBusinessInfo: null,
      provinceData: null,
      nationData: null,
      alertModalVisible: false,
      imageIndex: 0,
      tickSelected: false,
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.callUploadImage = this.callUploadImage.bind(this);
    this.getProvinceData = this.getProvinceData.bind(this);
    this.handleDropdown = this.handleDropdown.bind(this);
    this.closeVerifyModal = this.closeVerifyModal.bind(this);
  }

  componentDidMount() {
    this.props.getUserDetail();
    this.props.getUserBusinessDetail();
    this.props.getBusinessImages();
    this.getProvinceData(8, null);
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ) {
    if (prevProps.userBusinessDetail != this.props.userBusinessDetail) {
      let data = this.props.userBusinessDetail;

      this.setState(
        {
          legalName: data.legalInfo.name,
          legalRegistrationNumber: data.legalInfo.registrationNumber,
          legalStreetAddress: data.legalInfo.address,
          legalSubdistrict: data.legalInfo
            ? data.legalInfo.subDistrict && {
                id: data.legalInfo.subDistrict.id,
                label: data.legalInfo.subDistrict.subdistrict,
                value: data.legalInfo.subDistrict.subdistrict,
                postalCode: data.legalInfo.subDistrict.postalCode,
              }
            : null,
          legalNation: data.legalInfo
            ? data.legalInfo.nation && {
                id: data.legalInfo.nation.id,
                label: data.legalInfo.nation.label,
                value: data.legalInfo.nation.value,
              }
            : null,
          legalDistrict: data.legalInfo
            ? data.legalInfo.district && {
                id: data.legalInfo.district.id,
                label: data.legalInfo.district.label,
                value: data.legalInfo.district.value,
              }
            : null,
          legalCity: data.legalInfo
            ? data.legalInfo.city && {
                id: data.legalInfo.city.id,
                label: data.legalInfo.city.label,
                value: data.legalInfo.city.value,
              }
            : null,
          legalPostalCode: data.legalInfo.postalCode,
          userBusinessInfo: data,
        },
        () => {
          if (this.state.legalNation != null) {
            this.getProvinceData(1, this.state.legalNation.id);
            if (
              this.state.legalCity != null &&
              this.state.legalDistrict != null
            ) {
              if (this.state.legalCity.id) {
                this.getProvinceData(2, this.state.legalCity.id);
              }
              if (this.state.legalDistrict.id) {
                this.getProvinceData(3, this.state.legalDistrict.id);
              }
            }
          }
          if (this.props.userDetail && this.props.userDetail.role) {
            setTimeout(() => {
              console.log(this.props.userDetail.role);
              if (
                this.props.userDetail.role &&
                this.props.userDetail.role == 2
              ) {
                if (this.legalLocationRef) {
                  this.legalLocationRef.setAddressText(
                    this.state.legalStreetAddress ?? '',
                  );
                }
              } else {
                if (this.legalLocationRef) {
                  this.legalLocationRef.setAddressText(
                    this.state.legalStreetAddress ?? '',
                  );
                }
              }
            }, 1000);
          } else {
            setTimeout(() => {
              if (this.legalLocationRef) {
                this.legalLocationRef.setAddressText(
                  this.state.legalStreetAddress ?? '',
                );
              }
            }, 1000);
          }
          if (this.state.nation != null) {
            this.getProvinceData(1, this.state.nation.id);
          }
        },
      );
    }
  }

  topRoundView = props => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <View style={[Styles.left_corner_view, {marginVertical: 15}]}>
          <Text style={[Styles.extra_small_label]}>{props.title}</Text>
        </View>
      </View>
    );
  };

  updateMasterState = (attrName, value) => {
    this.setState({
      [attrName]: value,
    });
  };

  handleSheet = val => {
    if (val == 1) {
      this.chooseImage(1);
    } else if (val == 2) {
      this.chooseImage(2);
    } else if (val == 3) {
      this.setState({
        actionModalVisible: false,
      });
    }
  };

  chooseImage = val => {
    let options = {
      title: Locale('Select Image'),
      compressImageMaxWidth: 1500,
      compressImageMaxHeight: 700,
      multiple: false,
      cropping: true,
      freeStyleCropEnabled: true,
    };

    if (val == 1) {
      ImagePicker.openCamera({
        title: Locale('Select Image'),
        compressImageMaxWidth: 1500,
        compressImageMaxHeight: 700,
        cropping: true,
        freeStyleCropEnabled: true,
      })
        .then(response => {
          if (response.didCancel) {
            console.log('User cancelled  Camera');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            showAlertMessage(response.customButton);
          } else {
            this.setState({
              isLoading: false,
              actionModalVisible: false,
            });
            this.callUploadImage(response, val);
          }
        })
        .catch(error => {
          if (error.message) {
            showAlertMessage(error.message);
          }
        });
    } else if (val == 2) {
      ImagePicker.openPicker(options)
        .then(response => {
          console.log('image local reposnse', response);
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            alert(response.customButton);
          } else {
            this.setState({
              isLoading: false,
              actionModalVisible: false,
            });

            this.callUploadImage(response, val);
          }
        })
        .catch(error => {
          if (error.message) {
            showAlertMessage(error.message);
          }
        });
    }
  };

  callUploadImage = async (res, val) => {
    let {currentImage} = this.state;
    let data = new FormData();
    let token = await auth.getValue(AS_USER_TOKEN);
    //1: ID Front, 2:Profile, 3:Logo, 4: Business Image
    let photo = {
      uri: res.path,
      type: res.type ? res.type : res.mime,
      name: 'image.jpg',
    };
    data.append('file', photo);
    data.append(
      'type',
      currentImage == 3 ? 2 : currentImage == 4 ? 3 : currentImage == 5 ? 4 : 1,
    );
    const config = {
      headers: {Authorization: `Bearer ${token}`},
    };
    axios
      .post(U_BASE + U_ADD_BUSINESS_IMAGES, data, config)
      .then(responseData => {
        this.setState({isLoading: false});
        console.log(
          'response data is:-',
          responseData.data.response,
          responseData.data.response.file,
        );
        if (responseData.data.status == 200) {
          showAlertMessage(Locale('Image uploaded successfully'), 1);
          this.props.getBusinessImages(1);
        } else {
          this.setState({isLoading: false});
          if (responseData.data.message) {
            showAlertMessage(responseData.data.message);
          }
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  saveLegalInfo = () => {
    if (this.state.legalName == '') {
      showAlertMessage(Locale('Enter Legal name'));
    } else if (this.state.legalRegistrationNumber == '') {
      showAlertMessage(Locale('Enter registration number'));
    } else if (this.state.legalStreetAddress == '') {
      showAlertMessage(Locale('Enter street address'));
    } else if (this.state.legalCity == '') {
      showAlertMessage(Locale('Enter city'));
    } else if (this.state.legalSubdistrict == '') {
      showAlertMessage(Locale('Enter sub-district'));
    } else if (this.state.legalDistrict == '') {
      showAlertMessage(Locale('Enter district'));
    } else if (this.state.legalPostalCode == '') {
      showAlertMessage(Locale('Enter postal code'));
    } else {
      let data = JSON.stringify({
        name: this.state.legalName,
        registrationNumber: this.state.legalRegistrationNumber,
        address: this.state.legalStreetAddress,
        subDistrict: this.state.legalSubdistrict,
        district: this.state.legalDistrict,
        city: this.state.legalCity,
        postalCode: this.state.legalPostalCode,
        nation: this.state.legalNation,
      });
      this.submitResponse(data, 2);
    }
  };

  submitResponse = (data, type) => {
    let that = this;
    that.setState({isLoading: true});
    api
      .updatePersonalDetail(data, type)
      .then(json => {
        console.log('response is:-', json);
        that.setState({isLoading: false});
        if (json.status == 200) {
          if (json.data.status == 200) {
            if (type == 3) {
              this.setState({businessImage: []});
            }
            this.props.getUserBusinessDetail(1);
            if (type == 3) {
              this.setState({alertModalVisible: true});
            } else {
              showAlertMessage(
                Locale('Successfully updated Legal Information'),
                1,
              );
            }
          } else {
            showAlertMessage(json.data.message, 2);
          }
        } else if (json.status == 400) {
          showAlertMessage(json.data.message, 2);
        } else {
          showAlertMessage(json.data.message, 2);
        }
      })
      .catch(error => {
        console.log('response is:-', error);
        that.setState({isLoading: false});
        showAlertMessage(error.response.data.message, 2);
      });
  };

  getProvinceData(type, id) {
    api
      .getProvince(type, id)
      .then(json => {
        console.log('response is:-', json);
        this.setState({isLoading: false});
        if (json.status == 200) {
          if (type == 1) {
            let data = [];
            for (let i = 0; i < json.data.response.length; i++) {
              data.push({
                id: json.data.response[i].id,
                label: json.data.response[i].province,
                value: json.data.response[i].province,
              });
            }

            this.setState({
              provinceData: data,
              subdistrict: null,
              district: null,
            });
          } else if (type == 2) {
            let data = [];
            for (let i = 0; i < json.data.response.length; i++) {
              data.push({
                id: json.data.response[i].id,
                label: json.data.response[i].district,
                value: json.data.response[i].district,
              });
            }

            this.setState({
              district: data,
              subdistrict: null,
            });
          } else if (type == 3) {
            let data = [];
            for (let i = 0; i < json.data.response.length; i++) {
              data.push({
                id: json.data.response[i].id,
                label: json.data.response[i].subdistrict,
                value: json.data.response[i].subdistrict,
                postalCode: json.data.response[i].postcode,
              });
            }
            this.setState({
              subdistrict: data,
            });
          } else if (type == 8) {
            let data = [];
            for (let i = 0; i < json.data.response.length; i++) {
              data.push({
                id: json.data.response[i].id,
                label: json.data.response[i].nation,
                value: json.data.response[i].nation,
              });
            }
            this.setState({
              nationData: data,
              provinceData: null,
              subdistrict: null,
              district: null,
            });
          }
        } else if (json.status == 400) {
          showAlertMessage(json.data.message, 2);
        } else {
          showAlertMessage(json.data.message, 2);
        }
      })
      .catch(error => {
        console.log('response is:-', error);
        this.setState({isLoading: false});
        showAlertMessage(error.response.data.message, 2);
      });
  }

  commonView = props => {
    if (props.data) {
      return (
        <View
          style={{
            height: 45,
            marginBottom: 20,
            ...(Platform.OS === 'ios' && {
              zIndex: props.zIndex,
            }),
          }}>
          <Dropdown
            placeholder={props.placeholder}
            type={props.type}
            items={props.data}
            value={props.value}
            zIndex={props.zIndex}
            handleDropdown={props.handleDropdown}
          />
        </View>
      );
    }
  };

  handleDropdown = (val, type) => {
    if (type == 1) {
      let selectedValue = this.state.provinceData.find(
        data => data.value === val,
      );
      if (selectedValue) {
        this.setState({
          legalCity: selectedValue,
          legalDistrict: null,
          legalSubdistrict: null,
          isLoading: true,
        });
        this.getProvinceData(2, selectedValue.id);
      }
    } else if (type == 2) {
      let selectedValue = this.state.district.find(data => data.value === val);
      if (selectedValue) {
        this.setState({
          legalDistrict: selectedValue,
          legalSubdistrict: null,
          isLoading: true,
        });
        this.getProvinceData(3, selectedValue.id);
      }
    } else if (type == 3) {
      let selectedValue = this.state.subdistrict.find(
        data => data.value === val,
      );
      console.log('selected subdistrict is', selectedValue);
      this.setState({
        legalSubdistrict: selectedValue,
        legalPostalCode: selectedValue.postalCode,
        isLoading: false,
      });
    } else if (type == 8) {
      let selectedValue = this.state.nationData.find(
        data => data.value === val,
      );
      if (selectedValue) {
        this.setState({
          legalNation: selectedValue,
          legalCity: null,
          legalDistrict: null,
          legalSubdistrict: null,
          isLoading: true,
        });
        this.getProvinceData(1, selectedValue.id);
      }
    }
  };

  getBusinessStatus = props => {
    if (props && props.tradingInfo) {
      switch (props.tradingInfo.tradeStatus) {
        case 1:
          return Locale('Waiting');
          break;
        case 2:
          return Locale('Approved');
          break;
        case 3:
          return Locale('Rejected');
          break;
        default:
          return 'Add Business';
          break;
      }
    } else {
      return Locale('Add Business');
    }
  };

  onGoogleInputChangeLegal = text => {
    this.setState({legalStreetAddress: text});
  };

  GooglePlacesInput = props => {
    return (
      <View
        style={{
          width: wp(90),
          flex: 1,
          paddingHorizontal: 15,
          marginBottom: 15,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <View
          style={{
            width: '100%',
            borderBottomWidth: 1,
            borderBottomColor: COLOR.WHITE,
            flexDirection: 'row',
            flex: 1,
          }}>
          <GooglePlacesAutocomplete
            ref={instance => {
              props.type != 1
                ? (this.locationRef = instance)
                : (this.legalLocationRef = instance);
            }}
            fetchDetails={false}
            isFocused={false}
            placeholder={'Street address'}
            enablePoweredByContainer={false}
            textInputProps={{
              placeholderTextColor: COLOR.WHITE,
              fontSize: 13,
              marginLeft: -20,
              selectTextOnFocus: true,
              autoFocus: false,
              onChangeText:
                props.type == 1
                  ? this.onGoogleInputChangeLegal
                  : this.onGoogleInputChangeTrade,
            }}
            onPress={(data, details = null) => {
              // 'details' is provided when fetchDetails = true
              console.log(props.type, data.description);
              if (props.type == 1) {
                this.setState({legalStreetAddress: data.description});
              } else {
                this.setState({streetAddress: data.description});
              }
            }}
            query={{
              key: 'AIzaSyBvOO-m6sV_jFWGUWIx5aPqBGfgI8d3HeI',
              language: 'en',
              components: 'country:th',
            }}
            styles={{
              container: {
                flex: 1,
              },
              textInputContainer: {
                flexDirection: 'row',
                color: COLOR.WHITE,
                marginLeft: 10,
              },
              textInput: {
                backgroundColor: 'transparent',
                height: 35,
                fontSize: 16,
                fontWeight: '500',
                fontFamily: FONTS.FAMILY_REGULAR,
                color: COLOR.WHITE,
              },

              row: {
                backgroundColor: COLOR.WHITE,
                padding: 10,

                flexDirection: 'row',
                color: COLOR.WHITE,
              },
              separator: {
                height: 0.5,
                backgroundColor: '#c8c7cc',
                color: COLOR.WHITE,
              },
              loader: {
                flexDirection: 'row',
                justifyContent: 'flex-end',
                height: 0,
                color: COLOR.WHITE,
              },
            }}
          />
        </View>
        <TouchableWithoutFeedback
          onPress={() => {
            showAlertMessage(
              props.type == 1
                ? Locale('Legal registration address')
                : Locale('Trade registration address'),
              1,
            );
          }}
          style={{height: 40, width: 40}}>
          <Image style={{height: 15, width: 15}} source={IMAGES.info} />
        </TouchableWithoutFeedback>
      </View>
    );
  };

  closeVerifyModal = val => {
    if (val == 1) {
      this.setState({alertModalVisible: false, isLoading: false});
      this.props.navigation.goBack();
    } else {
      this.setState({alertModalVisible: false, isLoading: false});
    }
  };

  thirdTab = props => {
    return (
      <View>
        <Text
          style={[
            Styles.extra_small_label,
            {color: COLOR.WHITE, marginTop: 5},
          ]}>
          {Locale('Your Legal name is used for your contact with us')}
        </Text>

        <View
          style={[
            Styles.shadow_view,
            style.shadowView,
            {
              ...(Platform.OS === 'ios' && {
                zIndex: 1000,
              }),
            },
          ]}>
          <props.this.topRoundView id={3} title={Locale('Legal information')} />
          <View
            style={{
              paddingHorizontal: 15,
              alignItems: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                alignSelf: 'center',
                marginHorizontal: 10,
              }}>
              <FloatingTitleTextInputField
                isWhite={true}
                attrName={'legalName'}
                value={this.state.legalName}
                updateMasterState={this.updateMasterState}
                title={Locale('Legal name')}
              />
              <TouchableWithoutFeedback
                onPress={() => {
                  showAlertMessage(Locale('Legal info'), 1);
                }}
                style={{height: 40, width: 40}}>
                <Image style={{height: 15, width: 15}} source={IMAGES.info} />
              </TouchableWithoutFeedback>
            </View>

            <FloatingTitleTextInputField
              isWhite={true}
              attrName={'legalRegistrationNumber'}
              value={this.state.legalRegistrationNumber}
              floatKeyboardType={'number-pad'}
              updateMasterState={this.updateMasterState}
              title={Locale('Registration #')}
            />

            <props.this.commonView
              placeholder={Locale('Nation')}
              placeholdeTextColor={COLOR.WHITE}
              type={8}
              zIndex={3001}
              value={
                this.state.legalNation
                  ? this.state.legalNation.value
                    ? this.state.legalNation.value
                    : ''
                  : ''
              }
              data={this.state.nationData ? this.state.nationData : []}
              handleDropdown={this.handleDropdown}
            />
            <props.this.commonView
              placeholder={Locale('City')}
              placeholdeTextColor={COLOR.WHITE}
              type={1}
              zIndex={3000}
              value={
                this.state.legalCity
                  ? this.state.legalCity.value
                    ? this.state.legalCity.value
                    : ''
                  : ''
              }
              data={this.state.provinceData ? this.state.provinceData : []}
              handleDropdown={this.handleDropdown}
            />
            <props.this.commonView
              placeholder={Locale('District')}
              type={2}
              zIndex={2999}
              value={
                this.state.legalDistrict
                  ? this.state.legalDistrict.value
                    ? this.state.legalDistrict.value
                    : ''
                  : ''
              }
              data={this.state.district ? this.state.district : []}
              handleDropdown={this.handleDropdown}
            />
            <props.this.commonView
              placeholder={Locale('Subdistrict')}
              type={3}
              zIndex={2998}
              value={
                this.state.legalSubdistrict
                  ? this.state.legalSubdistrict.value
                    ? this.state.legalSubdistrict.value
                    : ''
                  : ''
              }
              data={this.state.subdistrict ? this.state.subdistrict : []}
              handleDropdown={this.handleDropdown}
            />

            <FloatingTitleTextInputField
              isWhite={true}
              attrName={'legalPostalCode'}
              value={
                this.state.legalSubdistrict
                  ? this.state.legalSubdistrict.postalCode
                    ? String(this.state.legalSubdistrict.postalCode)
                    : this.state.legalPostalCode
                  : this.state.legalPostalCode
              }
              floatKeyboardType={'number-pad'}
              updateMasterState={this.updateMasterState}
              title={Locale('Postal code')}
            />
            <props.this.GooglePlacesInput type={1} />
          </View>
        </View>
      </View>
    );
  };

  render() {
    let {userDetail, userBusinessDetail} = this.props;
    return (
      <View style={[Styles.container]}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <NavigationBar title={Locale('Legal Information')} />
            <KeyboardAwareScrollView
              keyboardShouldPersistTaps={'always'}
              enableResetScrollToCoords={false}
              style={{
                flex: 1,
                width: wp(90),
                alignSelf: 'center',
                showsVerticalScrollIndicator: false,
              }}>
              {userDetail && userDetail.role ? (
                userDetail.role == 1 ? null : (
                  <View pointerEvents="none">
                    <this.thirdTab this={this} />
                  </View>
                )
              ) : (
                <this.thirdTab this={this} />
              )}
            </KeyboardAwareScrollView>
            {userDetail && userDetail.role ? null : (
              <View
                style={{
                  width: '90%',
                  marginVertical: 15,
                  alignSelf: 'center',
                }}>
                <CustomButton
                  onPress={this.saveLegalInfo}
                  text={
                    this.getBusinessStatus(userBusinessDetail) == 'Approved'
                      ? Locale('Save & Submit')
                      : Locale('Save')
                  }
                />
              </View>
            )}
            <ActivityIndicator loading={this.state.isLoading} />
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginTop: 15,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.authReducer.userDetail,
    userBusinessDetail: state.authReducer.userBusinessDetail,
    userBusinessImages: state.authReducer.businessImages,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserDetail: val => {
      dispatch(AuthenticationAction.getUserInfo(val));
    },
    getUserBusinessDetail: val => {
      dispatch(AuthenticationAction.getBusinessDetail(val));
    },
    getBusinessImages: val => {
      dispatch(AuthenticationAction.getBusinessImages(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LegalInformation);
