import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';
import Styles from '../../../../styles/Styles';
import IMAGES from '../../../../styles/Images';
import NavigationBar from '../../../../commonView/NavigationBar';
import COLOR from '../../../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {CustomButton} from '../../../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import {MobileView} from '../../../campaign/TopWhiteView';
import {
  getDateFromTimeStamp,
  Locale,
  navigate,
  showAlertMessage,
} from '../../../../commonView/Helpers';

import API from '../../../../api/Api';
import ActivityIndicator from '../../../../commonView/ActivityIndicator';
import AuthenticationAction from '../../../../redux/action/AuthenticationAction';
import {connect} from 'react-redux';
import AlertPopup from '../../../../commonView/AlertPopup';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

let api = new API();

class BusinessInformation extends React.Component {
  constructor() {
    super();
    this.state = {
      alertModalVisible: false,
      isLoading: false,
    };

    this.handleRedirection = this.handleRedirection.bind(this);
    this.submitResponse = this.submitResponse.bind(this);
  }

  componentDidMount() {
    this.props.getUserDetail();
    this.props.getUserBusinessDetail();
    this.props.getBusinessImages();
  }

  submitResponse() {
    let that = this;
    that.setState({isLoading: true});
    api
      .submitBusinessForApproval()
      .then(json => {
        console.log('response is:-', json);
        that.setState({isLoading: false});
        if (json.status == 200) {
          if (json.data.status == 200) {
            this.setState({alertModalVisible: true});
            this.props.getUserBusinessDetail(1);
          } else {
            showAlertMessage(json.data.message, 2);
          }
        } else if (json.status == 400) {
          showAlertMessage(json.data.message, 2);
        } else {
          showAlertMessage(json.data.message, 2);
        }
      })
      .catch(error => {
        console.log('response is:-', error);
        that.setState({isLoading: false});
        showAlertMessage(error.response.data.message, 2);
      });
  }

  getBusinessStatus = props => {
    if (props && props.tradingInfo) {
      switch (props.tradingInfo.tradeStatus) {
        case 0:
          return Locale('Submitted');
          break;
        case 1:
          return Locale('Waiting');
          break;
        case 2:
          return Locale('Approved');
          break;
        case 3:
          return Locale('Rejected');
          break;
        default:
          return 'Add Business';
          break;
      }
    } else {
      return Locale('Submitted');
    }
  };

  closeVerifyModal = val => {
    if (val == 1) {
      this.setState({alertModalVisible: false, isLoading: false});
      this.props.navigation.goBack();
    } else {
      this.setState({alertModalVisible: false, isLoading: false});
    }
  };

  handleRedirection = props => {
    switch (String(props)) {
      case '1':
        this.props.navigation.navigate('PersonalDetails');
        break;
      case '2':
        if (this.props.userBusinessDetail && this.props.userBusinessDetail.id) {
          this.props.navigation.navigate('LegalInformation');
        } else {
          showAlertMessage('Please enter Personal Details.');
        }
        break;
      case '3':
        if (
          this.props.userBusinessDetail &&
          this.props.userBusinessDetail.legalInfo.id
        ) {
          this.props.navigation.navigate('TradeInformation');
        } else {
          showAlertMessage('Please enter Legal Information.');
        }
        break;
      case '4':
        this.props.navigation.navigate('LogoBusinessImages');
        break;
      default:
        break;
    }
  };

  campaignView = props => {
    return (
      <View>
        <TouchableWithoutFeedback
          onPress={() => {
            this.handleRedirection(props.id);
          }}>
          <View style={styles.campaign_main_view}>
            <Image
              source={props.image}
              style={{height: 35, width: 35}}
              resizeMode={'contain'}
            />
            <View style={{flex: 1, marginHorizontal: 15}}>
              <Text style={[Styles.bold_body_label, {color: COLOR.WHITE}]}>
                {props.title}
              </Text>
            </View>
            <Image
              source={IMAGES.keyboard_arrow_right}
              style={{height: 12, width: 12, tintColor: COLOR.WHITE}}
              resizeMode={'contain'}
            />
          </View>
        </TouchableWithoutFeedback>
        {props.showLine && (
          <View
            style={[
              Styles.line_view,
              {
                borderBottomWidth: 0.4,
                marginVertical: 15,
                borderBottomColor: COLOR.DARK_BLUE,
              },
            ]}
          />
        )}
      </View>
    );
  };

  render() {
    let {userDetail, userBusinessDetail} = this.props;
    return (
      <View style={[Styles.container]}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <NavigationBar title={Locale('Business Information')} />
            <AlertPopup
              image={IMAGES.security}
              title={Locale('Business created')}
              buttonText={Locale('Home')}
              desc={Locale(
                'Business registered successfully, waiting for approval from admin You can edit your legal information before approval from admin',
              )}
              closeVerifyModal={val => this.closeVerifyModal(val)}
              modalVisible={this.state.alertModalVisible}
            />
            {userBusinessDetail && (
              <View
                style={{
                  width: wp(90),
                  alignSelf: 'center',
                  marginTop: 0,
                }}>
                {userBusinessDetail.legalInfo &&
                  userBusinessDetail.legalInfo.id && (
                    <Text
                      style={[Styles.extra_small_label, {color: COLOR.WHITE}]}>
                      Business Id:
                      {userBusinessDetail.legalInfo.id}
                    </Text>
                  )}
                {userBusinessDetail.tradingInfo &&
                  userBusinessDetail.tradingInfo.id && (
                    <Text
                      style={[Styles.extra_small_label, {color: COLOR.WHITE}]}>
                      Trade Id:
                      {userBusinessDetail.tradingInfo.id}
                    </Text>
                  )}
              </View>
            )}
            <KeyboardAwareScrollView
              keyboardShouldPersistTaps={'always'}
              enableResetScrollToCoords={false}
              style={{
                flex: 1,
                width: wp(90),
                alignSelf: 'center',
                showsVerticalScrollIndicator: false,
              }}>
              <View>
                <MobileView
                  title={'Submitted'}
                  time={
                    userBusinessDetail
                      ? userBusinessDetail.tradingInfo.businessUpdatedDate
                        ? 'Latest: ' +
                          getDateFromTimeStamp(
                            userBusinessDetail.tradingInfo.businessUpdatedDate,
                            'MMM DD, YYYY h:mm a',
                          )
                        : Locale('Not yet submitted')
                      : Locale('Not yet submitted')
                  }
                  subTitle={
                    userBusinessDetail
                      ? userBusinessDetail.tradingInfo
                          .previousBusinessUpdateDate
                        ? userBusinessDetail.tradingInfo
                            .previousBusinessUpdateDate != 0
                          ? 'Last: ' +
                            getDateFromTimeStamp(
                              userBusinessDetail.tradingInfo
                                .previousBusinessUpdateDate,
                              'MMM DD, YYYY h:mm a',
                            )
                          : null
                        : null
                      : null
                  }
                  image={IMAGES.rocket}
                />

                <MobileView
                  title={
                    this.getBusinessStatus(userBusinessDetail) == 'Rejected'
                      ? 'Rejected'
                      : 'Approved'
                  }
                  time={
                    userBusinessDetail
                      ? userBusinessDetail.tradingInfo.businessApprovalDate
                        ? 'Latest: ' +
                          getDateFromTimeStamp(
                            userBusinessDetail.tradingInfo.businessApprovalDate,
                            'MMM DD, YYYY h:mm a',
                          )
                        : Locale('Pending for approval')
                      : Locale('Not yet submitted')
                  }
                  subTitle={
                    userBusinessDetail
                      ? userBusinessDetail.tradingInfo
                          .previousBusinessApprovalDate
                        ? userBusinessDetail.tradingInfo
                            .previousBusinessApprovalDate != 0
                          ? 'Last: ' +
                            getDateFromTimeStamp(
                              userBusinessDetail.tradingInfo
                                .previousBusinessApprovalDate,
                              'MMM DD, YYYY h:mm a',
                            )
                          : null
                        : null
                      : null
                  }
                  image={IMAGES.mobile_tick}
                />
                {userBusinessDetail &&
                userBusinessDetail.tradingInfo.tradeStatus == 3 ? (
                  <View
                    style={[
                      Styles.shadow_view,
                      styles.shadowView,
                      {backgroundColor: COLOR.RED, padding: 15},
                    ]}>
                    <Text
                      style={[Styles.bold_body_label, {color: COLOR.WHITE}]}>
                      {userBusinessDetail
                        ? userBusinessDetail.tradingInfo.reason
                        : ''}
                    </Text>
                  </View>
                ) : null}
                <Text
                  style={[
                    Styles.subheading_label,
                    {color: COLOR.WHITE, marginTop: 15, fontWeight: '600'},
                  ]}>
                  {this.props.userDetail
                    ? this.props.userDetail.role
                      ? this.props.userDetail.role == 1
                        ? Locale('Server')
                        : Locale('Manager')
                      : Locale('Owner')
                    : Locale('Owner')}
                </Text>

                <View
                  style={[
                    styles.shadowView,
                    Styles.shadow_view,
                    styles.customer_credit_view,
                    {marginVertical: 15},
                  ]}>
                  {userDetail && userDetail.role ? (
                    userDetail.role == 1 ? null : (
                      <View>
                        <this.campaignView
                          id={1}
                          showLine={true}
                          image={
                            this.props.userBusinessDetail
                              ? this.props.userBusinessDetail.isSubmitted == 1
                                ? IMAGES.green_tick
                                : IMAGES.tick_gray
                              : IMAGES.tick_gray
                          }
                          title={Locale('Personal & Verification Details')}
                        />
                      </View>
                    )
                  ) : (
                    <this.campaignView
                      id={1}
                      showLine={true}
                      image={
                        this.props.userBusinessDetail
                          ? this.props.userBusinessDetail.isSubmitted == 1
                            ? IMAGES.green_tick
                            : IMAGES.tick_gray
                          : IMAGES.tick_gray
                      }
                      title={Locale('Personal & Verification Details')}
                    />
                  )}

                  {userDetail && userDetail.role ? (
                    userDetail.role == 1 ? null : (
                      <View>
                        <this.campaignView
                          id={2}
                          showLine={true}
                          image={
                            this.props.userBusinessDetail
                              ? this.props.userBusinessDetail.legalInfo
                                ? this.props.userBusinessDetail.legalInfo
                                    .isSubmitted == 1
                                  ? IMAGES.green_tick
                                  : IMAGES.tick_gray
                                : IMAGES.tick_gray
                              : IMAGES.tick_gray
                          }
                          title={Locale('Legal Information')}
                        />
                      </View>
                    )
                  ) : (
                    <this.campaignView
                      id={2}
                      showLine={true}
                      image={
                        this.props.userBusinessDetail
                          ? this.props.userBusinessDetail.legalInfo
                            ? this.props.userBusinessDetail.legalInfo
                                .isSubmitted == 1
                              ? IMAGES.green_tick
                              : IMAGES.tick_gray
                            : IMAGES.tick_gray
                          : IMAGES.tick_gray
                      }
                      title={Locale('Legal Information')}
                    />
                  )}
                  <this.campaignView
                    id={3}
                    showLine={true}
                    image={
                      this.props.userBusinessDetail
                        ? this.props.userBusinessDetail.tradingInfo
                          ? this.props.userBusinessDetail.tradingInfo
                              .isSubmitted == 1
                            ? IMAGES.green_tick
                            : IMAGES.tick_gray
                          : IMAGES.tick_gray
                        : IMAGES.tick_gray
                    }
                    title={Locale('Trade Information')}
                  />

                  <this.campaignView
                    id={4}
                    showLine={false}
                    image={
                      this.props.userBusinessImages
                        ? this.props.userBusinessImages.logo != '' &&
                          this.props.userBusinessImages.images.length
                          ? IMAGES.green_tick
                          : IMAGES.tick_gray
                        : IMAGES.tick_gray
                    }
                    title={Locale('Logo & Images')}
                  />
                </View>
              </View>
            </KeyboardAwareScrollView>
            {this.props.userDetail && this.props.userDetail.role ? (
              this.props.userDetail.role == 1 ? null : this.getBusinessStatus(
                  userBusinessDetail,
                ) != 'Submitted' ? null : (
                <View
                  style={{
                    width: '90%',
                    alignSelf: 'center',
                    marginTop: 0,
                  }}>
                  <CustomButton
                    onPress={this.submitResponse}
                    labelColor={COLOR.WHITE}
                    bg={COLOR.GREEN}
                    text={
                      this.getBusinessStatus(userBusinessDetail) == 'Approved'
                        ? Locale('Submit for update approval')
                        : Locale('Submit for approval')
                    }
                  />
                </View>
              )
            ) : this.getBusinessStatus(userBusinessDetail) !=
              'Submitted' ? null : (
              <View
                style={{
                  width: '90%',
                  alignSelf: 'center',
                  marginTop: 0,
                }}>
                <CustomButton
                  onPress={this.submitResponse}
                  labelColor={COLOR.WHITE}
                  bg={COLOR.GREEN}
                  text={
                    this.getBusinessStatus(userBusinessDetail) == 'Approved'
                      ? Locale('Submit for update approval')
                      : Locale('Submit for approval')
                  }
                />
              </View>
            )}
            <TouchableWithoutFeedback
              onPress={() => {
                navigate('PrivacyPolicyScreen');
              }}>
              <Text
                style={{
                  textDecorationLine: 'underline',
                  alignSelf: 'center',
                  marginTop: 10,
                  marginBottom: 10,
                  color: COLOR.WHITE,
                }}>
                {Locale('Privacy Policy')}
              </Text>
            </TouchableWithoutFeedback>
            <ActivityIndicator loading={this.state.isLoading} />
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 15,
    paddingVertical: 15,
  },
  campaign_view: {
    color: COLOR.WHITE,
    margin: 0,
    alignSelf: 'flex-start',
    flex: 1,
    fontSize: 9,
    fontWeight: '600',
  },
  campaign_main_view: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 5,
  },
  nav_bar_view: {
    width: wp(100),
    alignSelf: 'center',
    marginTop: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  profile_image_view: {
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 15,
    marginBottom: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  add_business_view: {
    height: 150,
    padding: 0,
    marginVertical: 0,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 0,
  },
  customer_credit_view: {
    marginVertical: 15,
    padding: 15,
    backgroundColor: COLOR.MEDIUM_BLUE,
    paddingVertical: 20,
  },
  logout_label: {
    color: COLOR.WHITE,
    textDecorationLine: 'underline',
    marginVertical: 15,
    height: 35,
    alignSelf: 'center',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.authReducer.userDetail,
    userBusinessDetail: state.authReducer.userBusinessDetail,
    userBusinessImages: state.authReducer.businessImages,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserDetail: val => {
      dispatch(AuthenticationAction.getUserInfo(val));
    },
    getUserBusinessDetail: val => {
      dispatch(AuthenticationAction.getBusinessDetail(val));
    },
    getBusinessImages: val => {
      dispatch(AuthenticationAction.getBusinessImages(val));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BusinessInformation);
