import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import Styles from '../../../../styles/Styles';
import IMAGES from '../../../../styles/Images';
import NavigationBar from '../../../../commonView/NavigationBar';
import COLOR from '../../../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {CustomButton} from '../../../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import {FloatingTitleTextInputField} from '../../../../commonView/FloatingTitleTextInputField';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Locale, showAlertMessage} from '../../../../commonView/Helpers';
import {
  AS_USER_TOKEN,
  U_ADD_BUSINESS_IMAGES,
  U_BASE,
} from '../../../../commonView/Constants';
import API from '../../../../api/Api';
import ActivityIndicator from '../../../../commonView/ActivityIndicator';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from '../../../../commonView/ActionSheet';
import AuthenticationAction from '../../../../redux/action/AuthenticationAction';
import {connect} from 'react-redux';
import {Dropdown} from '../../../../commonView/Dropdown';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import FONTS from '../../../../styles/Fonts';
import Auth from '../../../../auth';
import axios from 'axios';

let api = new API();
let auth = new Auth();

class TradeInformation extends React.Component {
  constructor() {
    super();
    this.state = {
      tradeName: '',
      streetAddress: '',
      city: 'City',
      tradingSubdistrict: null,
      tradingDistrict: null,
      nation: '',
      tradePhoneNumber: '',
      tradeWebsite: 'https://www.',
      tradeStyle: '',
      tradeSpecial: '',
      tradePostalCode: '',
      isLoading: false,
      actionModalVisible: false,
      currentImage: 1,
      userBusinessInfo: null,
      provinceData: null,
      nationData: null,
      district: null,
      subdistrict: null,
      alertModalVisible: false,
      imageIndex: 0,
      tickSelected: false,
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.getProvinceData = this.getProvinceData.bind(this);
    this.handleDropdown = this.handleDropdown.bind(this);
    this.closeVerifyModal = this.closeVerifyModal.bind(this);
    this.handleLegalTradingInfo = this.handleLegalTradingInfo.bind(this);
  }

  componentDidMount() {
    this.props.getUserDetail();
    this.props.getUserBusinessDetail();
    this.props.getBusinessImages();
    this.getProvinceData(8, null);
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ) {
    if (prevProps.userBusinessDetail != this.props.userBusinessDetail) {
      let data = this.props.userBusinessDetail;

      this.setState(
        {
          tradeName: data.tradingInfo ? data.tradingInfo.name : '',
          streetAddress: data.tradingInfo ? data.tradingInfo.address : '',
          city: data.tradingInfo ? data.tradingInfo.city : '',
          tradePhoneNumber: data.tradingInfo ? data.tradingInfo.phone : '',
          tradeWebsite: data.tradingInfo
            ? data.tradingInfo.website
            : 'https://www.',
          tradeStyle: data.tradingInfo ? data.tradingInfo.style : '',
          tradeSpecials: data.tradingInfo ? data.tradingInfo.specials : '',
          tradingSubdistrict: data.tradingInfo
            ? data.tradingInfo.subDistrict && {
                id: data.tradingInfo.subDistrict.id,
                label: data.tradingInfo.subDistrict.subdistrict,
                value: data.tradingInfo.subDistrict.subdistrict,
                postalCode: data.tradingInfo.subDistrict.postalCode,
              }
            : null,
          tradingDistrict: data.tradingInfo
            ? data.tradingInfo.district && {
                id: data.tradingInfo.district.id,
                label: data.tradingInfo.district.label,
                value: data.tradingInfo.district.value,
              }
            : null,
          nation: data.tradingInfo
            ? data.tradingInfo.nation && {
                id: data.tradingInfo.nation.id,
                label: data.tradingInfo.nation.label,
                value: data.tradingInfo.nation.value,
              }
            : null,
          tradePostalCode: data.tradingInfo.postalCode,
          userBusinessInfo: data,
        },
        () => {
          if (data.tradingInfo.address == data.legalInfo.address) {
            this.setState(
              {
                tickSelected: false,
              },
              () => {
                this.handleLegalTradingInfo();
              },
            );
          }
          if (this.state.nation != null) {
            this.getProvinceData(1, this.state.nation.id);
            if (this.state.city != null && this.state.tradingDistrict != null) {
              if (this.state.city.id) {
                this.getProvinceData(2, this.state.city.id);
              }
              if (this.state.tradingDistrict.id) {
                this.getProvinceData(3, this.state.tradingDistrict.id);
              }
            }
          }
          if (this.props.userDetail && this.props.userDetail.role) {
            setTimeout(() => {
              console.log(this.props.userDetail.role);
              if (
                this.props.userDetail.role &&
                this.props.userDetail.role == 2
              ) {
                if (this.locationRef) {
                  this.locationRef.setAddressText(
                    this.state.streetAddress ?? '',
                  );
                }
              } else {
                if (this.locationRef) {
                  this.locationRef.setAddressText(
                    this.state.streetAddress ?? '',
                  );
                }
              }
            }, 1000);
          } else {
            setTimeout(() => {
              if (this.locationRef) {
                this.locationRef.setAddressText(this.state.streetAddress ?? '');
              }
            }, 1000);
          }
          if (this.state.nation != null) {
            this.getProvinceData(1, this.state.nation.id);
          }
        },
      );
    }
  }

  handleLegalTradingInfo() {
    if (this.state.tickSelected) {
      this.setState(
        {
          tickSelected: !this.state.tickSelected,
          streetAddress: '',
          city: null,
          tradingSubdistrict: null,
          tradingDistrict: null,
          nation: '',
          tradePostalCode: '',
        },
        () => {
          if (this.locationRef) {
            this.locationRef.setAddressText('');
          }
        },
      );
    } else {
      this.setState(
        {
          tickSelected: !this.state.tickSelected,
          streetAddress: this.props.userBusinessDetail
            ? this.props.userBusinessDetail.legalInfo
              ? this.props.userBusinessDetail.legalInfo.address
              : ''
            : '',
          city: this.props.userBusinessDetail
            ? this.props.userBusinessDetail.legalInfo
              ? this.props.userBusinessDetail.legalInfo.city
              : null
            : null,
          tradingSubdistrict: this.props.userBusinessDetail
            ? this.props.userBusinessDetail.legalInfo
              ? this.props.userBusinessDetail.legalInfo.subDistrict
              : null
            : null,
          tradingDistrict: this.props.userBusinessDetail
            ? this.props.userBusinessDetail.legalInfo
              ? this.props.userBusinessDetail.legalInfo.district
              : null
            : null,
          nation: this.props.userBusinessDetail
            ? this.props.userBusinessDetail.legalInfo
              ? this.props.userBusinessDetail.legalInfo.nation
              : ''
            : '',
          tradePostalCode: this.props.userBusinessDetail
            ? this.props.userBusinessDetail.legalInfo
              ? this.props.userBusinessDetail.legalInfo.postalCode
              : ''
            : '',
        },
        () => {
          if (this.locationRef) {
            this.locationRef.setAddressText(this.state.streetAddress ?? '');
          }
        },
      );
    }
  }

  topRoundView = props => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <View style={[Styles.left_corner_view, {marginVertical: 15}]}>
          <Text style={[Styles.extra_small_label]}>{props.title}</Text>
        </View>
      </View>
    );
  };

  updateMasterState = (attrName, value) => {
    if (attrName == 'phoneNumber' || attrName == 'tradePhoneNumber') {
      if (value.length <= 10) {
        this.setState({
          [attrName]: value,
        });
      }
    } else {
      this.setState({
        [attrName]: value,
      });
    }
  };

  saveTradeInfo = () => {
    console.log(this.state);
    if (!this.state.tradeName) {
      showAlertMessage(Locale('Enter trade name'));
    } else if (!this.state.streetAddress) {
      showAlertMessage(Locale('Enter street address'));
    } else if (!this.state.nation) {
      showAlertMessage(Locale('Enter nation'));
    } else if (!this.state.city) {
      showAlertMessage(Locale('Enter city'));
    } else if (!this.state.tradePhoneNumber) {
      showAlertMessage(Locale('Enter phone number'));
    } else if (!this.state.tradeWebsite) {
      showAlertMessage(Locale('Enter website'));
    } else if (
      this.state.userBusinessInfo && this.state.userBusinessInfo.legalInfo
        ? this.state.userBusinessInfo.legalInfo.id == null
        : true
    ) {
      showAlertMessage(Locale('Please submit legal information'));
    } else {
      let data = JSON.stringify({
        legal: this.state.userBusinessInfo.legalInfo.id,
        name: this.state.tradeName,
        address: this.state.streetAddress,
        nation: this.state.nation,
        city: this.state.city,
        phone: this.state.tradePhoneNumber,
        website: this.state.tradeWebsite,
        style: this.state.tradeStyle,
        specials: this.state.tradeSpecials,
        logo: this.state.businessLogo,
        images: this.state.businessImage,
        subDistrict: this.state.tradingSubdistrict,
        district: this.state.tradingDistrict,
        postalCode: this.state.tradePostalCode,
      });

      console.log('sent data is:-', data);
      this.submitResponse(data, 3);
    }
  };

  submitResponse = (data, type) => {
    let that = this;
    that.setState({isLoading: true});
    api
      .updatePersonalDetail(data, type)
      .then(json => {
        console.log('response is:-', json);
        that.setState({isLoading: false});
        if (json.status == 200) {
          if (json.data.status == 200) {
            if (type == 3) {
              this.setState({businessImage: []});
            }
            showAlertMessage('Successfully saved Trade Information', 1);
            this.props.getUserBusinessDetail(1);
            if (type == 3) {
              this.setState({alertModalVisible: true});
            } else {
              showAlertMessage(Locale('Successfully updated profile'), 1);
            }
          } else {
            showAlertMessage(json.data.message, 2);
          }
        } else if (json.status == 400) {
          showAlertMessage(json.data.message, 2);
        } else {
          showAlertMessage(json.data.message, 2);
        }
      })
      .catch(error => {
        console.log('response is:-', error);
        that.setState({isLoading: false});
        showAlertMessage(error.response.data.message, 2);
      });
  };

  getProvinceData(type, id) {
    api
      .getProvince(type, id)
      .then(json => {
        console.log('response is:-', json);
        this.setState({isLoading: false});
        if (json.status == 200) {
          if (type == 1) {
            let data = [];
            for (let i = 0; i < json.data.response.length; i++) {
              data.push({
                id: json.data.response[i].id,
                label: json.data.response[i].province,
                value: json.data.response[i].province,
              });
            }

            this.setState({
              provinceData: data,
              subdistrict: null,
              district: null,
            });
          } else if (type == 2) {
            let data = [];
            for (let i = 0; i < json.data.response.length; i++) {
              data.push({
                id: json.data.response[i].id,
                label: json.data.response[i].district,
                value: json.data.response[i].district,
              });
            }

            this.setState({
              district: data,
              subdistrict: null,
            });
          } else if (type == 3) {
            let data = [];
            for (let i = 0; i < json.data.response.length; i++) {
              data.push({
                id: json.data.response[i].id,
                label: json.data.response[i].subdistrict,
                value: json.data.response[i].subdistrict,
                postalCode: json.data.response[i].postcode,
              });
            }
            this.setState({
              subdistrict: data,
            });
          } else if (type == 8) {
            let data = [];
            for (let i = 0; i < json.data.response.length; i++) {
              data.push({
                id: json.data.response[i].id,
                label: json.data.response[i].nation,
                value: json.data.response[i].nation,
              });
            }
            this.setState({
              nationData: data,
              provinceData: null,
              subdistrict: null,
              district: null,
            });
          }
        } else if (json.status == 400) {
          showAlertMessage(json.data.message, 2);
        } else {
          showAlertMessage(json.data.message, 2);
        }
      })
      .catch(error => {
        console.log('response is:-', error);
        this.setState({isLoading: false});
        showAlertMessage(error.response.data.message, 2);
      });
  }

  commonView = props => {
    if (props.data) {
      return (
        <View
          style={{
            height: 45,
            marginBottom: 20,
            ...(Platform.OS === 'ios' && {
              zIndex: props.zIndex,
            }),
          }}>
          <Dropdown
            placeholder={props.placeholder}
            type={props.type}
            items={props.data}
            value={props.value}
            zIndex={props.zIndex}
            handleDropdown={props.handleDropdown}
          />
        </View>
      );
    }
  };

  handleDropdown = (val, type) => {
    if (type == 4) {
      let selectedValue = this.state.provinceData.find(
        data => data.value === val,
      );

      if (selectedValue) {
        this.setState({
          city: selectedValue,
          tradingDistrict: null,
          tradingSubdistrict: null,
          isLoading: true,
        });
      }
      this.getProvinceData(2, selectedValue.id);
    } else if (type == 5) {
      this.setState({
        nation: val,
      });
    } else if (type == 6) {
      let selectedValue = this.state.district.find(data => data.value === val);
      if (selectedValue) {
        this.setState({
          tradingDistrict: selectedValue,
          tradingSubdistrict: null,
          isLoading: true,
        });
        this.getProvinceData(3, selectedValue.id);
      }
    } else if (type == 7) {
      let selectedValue = this.state.subdistrict.find(
        data => data.value === val,
      );
      this.setState({
        tradingSubdistrict: selectedValue,
        tradePostalCode: selectedValue.postalCode,
        isLoading: false,
      });
    } else if (type == 9) {
      let selectedValue = this.state.nationData.find(
        data => data.value === val,
      );
      if (selectedValue) {
        this.setState({
          nation: selectedValue,
          city: null,
          tradingDistrict: null,
          tradingSubdistrict: null,
          isLoading: true,
        });
        this.getProvinceData(1, selectedValue.id);
      }
    }
  };

  getBusinessStatus = props => {
    if (props && props.tradingInfo) {
      switch (props.tradingInfo.tradeStatus) {
        case 1:
          return Locale('Waiting');
          break;
        case 2:
          return Locale('Approved');
          break;
        case 3:
          return Locale('Rejected');
          break;
        default:
          return 'Add Business';
          break;
      }
    } else {
      return Locale('Add Business');
    }
  };
  onGoogleInputChangeTrade = text => {
    this.setState({streetAddress: text});
  };

  GooglePlacesInput = props => {
    return (
      <View
        style={{
          width: wp(90),
          flex: 1,
          paddingHorizontal: 15,
          marginBottom: 15,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <View
          style={{
            width: '100%',
            borderBottomWidth: 1,
            borderBottomColor: COLOR.WHITE,
            flexDirection: 'row',
            flex: 1,
          }}>
          <GooglePlacesAutocomplete
            ref={instance => {
              props.type != 1
                ? (this.locationRef = instance)
                : (this.legalLocationRef = instance);
            }}
            fetchDetails={false}
            isFocused={false}
            placeholder={'Street address'}
            enablePoweredByContainer={false}
            textInputProps={{
              placeholderTextColor: COLOR.WHITE,
              fontSize: 13,
              marginLeft: -20,
              selectTextOnFocus: true,
              autoFocus: false,
              onChangeText:
                props.type == 1
                  ? this.onGoogleInputChangeLegal
                  : this.onGoogleInputChangeTrade,
            }}
            onPress={(data, details = null) => {
              // 'details' is provided when fetchDetails = true
              console.log(props.type, data.description);
              if (props.type == 1) {
                this.setState({legalStreetAddress: data.description});
              } else {
                this.setState({streetAddress: data.description});
              }
            }}
            query={{
              key: 'AIzaSyBvOO-m6sV_jFWGUWIx5aPqBGfgI8d3HeI',
              language: 'en',
              components: 'country:th',
            }}
            styles={{
              container: {
                flex: 1,
              },
              textInputContainer: {
                flexDirection: 'row',
                color: COLOR.WHITE,
                marginLeft: 10,
              },
              textInput: {
                backgroundColor: 'transparent',
                height: 35,
                fontSize: 16,
                fontWeight: '500',
                fontFamily: FONTS.FAMILY_REGULAR,
                color: COLOR.WHITE,
              },

              row: {
                backgroundColor: COLOR.WHITE,
                padding: 10,

                flexDirection: 'row',
                color: COLOR.WHITE,
              },
              separator: {
                height: 0.5,
                backgroundColor: '#c8c7cc',
                color: COLOR.WHITE,
              },
              loader: {
                flexDirection: 'row',
                justifyContent: 'flex-end',
                height: 0,
                color: COLOR.WHITE,
              },
            }}
          />
        </View>
        <TouchableWithoutFeedback
          onPress={() => {
            showAlertMessage(
              props.type == 1
                ? Locale('Legal registration address')
                : Locale('Trade registration address'),
              1,
            );
          }}
          style={{height: 40, width: 40}}>
          <Image style={{height: 15, width: 15}} source={IMAGES.info} />
        </TouchableWithoutFeedback>
      </View>
    );
  };

  closeVerifyModal = val => {
    if (val == 1) {
      this.setState({alertModalVisible: false, isLoading: false});
      this.props.navigation.goBack();
    } else {
      this.setState({alertModalVisible: false, isLoading: false});
    }
  };

  fourthTab = props => {
    return (
      <View style={[Styles.shadow_view, style.shadowView]}>
        <props.this.topRoundView id={4} title={Locale('Trade information')} />

        <View style={{paddingHorizontal: 15, alignItems: 'center'}}>
          <FloatingTitleTextInputField
            isWhite={true}
            attrName={'tradeName'}
            value={this.state.tradeName}
            updateMasterState={this.updateMasterState}
            title={Locale('Trade Name')}
          />

          <props.this.commonView
            placeholder={Locale('Nation')}
            placeholdeTextColor={COLOR.WHITE}
            type={9}
            zIndex={3000}
            value={
              this.state.nation
                ? this.state.nation.value
                  ? this.state.nation.value
                  : ''
                : ''
            }
            data={this.state.nationData ? this.state.nationData : []}
            handleDropdown={this.handleDropdown}
          />
          <props.this.commonView
            placeholder={Locale('City')}
            type={4}
            zIndex={2999}
            value={
              this.state.city
                ? this.state.city.value
                  ? this.state.city.value
                  : ''
                : ''
            }
            data={this.state.provinceData ? this.state.provinceData : []}
            handleDropdown={this.handleDropdown}
          />
          <props.this.commonView
            placeholder={Locale('District')}
            type={6}
            zIndex={2998}
            value={
              this.state.tradingDistrict
                ? this.state.tradingDistrict.value
                  ? this.state.tradingDistrict.value
                  : ''
                : ''
            }
            data={this.state.district ? this.state.district : []}
            handleDropdown={this.handleDropdown}
          />
          <props.this.commonView
            placeholder={Locale('Subdistrict')}
            type={7}
            zIndex={2997}
            value={
              this.state.tradingSubdistrict
                ? this.state.tradingSubdistrict.value
                  ? this.state.tradingSubdistrict.value
                  : ''
                : ''
            }
            data={this.state.subdistrict ? this.state.subdistrict : []}
            handleDropdown={this.handleDropdown}
          />
          <FloatingTitleTextInputField
            isWhite={true}
            attrName={'tradePostalCode'}
            value={
              this.state.tradingSubdistrict
                ? this.state.tradingSubdistrict.postalCode
                  ? String(this.state.tradingSubdistrict.postalCode)
                  : this.state.tradePostalCode
                : this.state.tradePostalCode
            }
            floatKeyboardType={'number-pad'}
            updateMasterState={this.updateMasterState}
            title={Locale('Postal code')}
          />
          <props.this.GooglePlacesInput type={2} />
          <FloatingTitleTextInputField
            isWhite={true}
            floatKeyboardType={'number-pad'}
            attrName={'tradePhoneNumber'}
            value={this.state.tradePhoneNumber}
            updateMasterState={this.updateMasterState}
            title={Locale('Phone number')}
          />
          <FloatingTitleTextInputField
            isWhite={true}
            attrName={'tradeWebsite'}
            value={this.state.tradeWebsite ?? 'https://www.'}
            updateMasterState={this.updateMasterState}
            title={Locale('Website')}
          />
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              alignSelf: 'center',
              marginHorizontal: 10,
            }}>
            <FloatingTitleTextInputField
              isWhite={true}
              attrName={'tradeStyle'}
              value={this.state.tradeStyle}
              updateMasterState={this.updateMasterState}
              title={Locale('Style')}
            />
            <TouchableWithoutFeedback
              onPress={() => {
                showAlertMessage(Locale('What kind of food you will serve'), 1);
              }}
              style={{height: 40, width: 40}}>
              <Image style={{height: 15, width: 15}} source={IMAGES.info} />
            </TouchableWithoutFeedback>
          </View>
        </View>
      </View>
    );
  };

  render() {
    let {userDetail, userBusinessDetail} = this.props;
    return (
      <View style={[Styles.container]}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <NavigationBar title={Locale('Trade Information')} />

            <KeyboardAwareScrollView
              keyboardShouldPersistTaps={'always'}
              enableResetScrollToCoords={false}
              style={{
                flex: 1,
                width: wp(90),
                alignSelf: 'center',
                showsVerticalScrollIndicator: false,
              }}>
              <View>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.WHITE, marginTop: 15},
                  ]}>
                  Users will see the information below.
                </Text>

                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 15,
                  }}>
                  <TouchableWithoutFeedback
                    onPress={this.handleLegalTradingInfo}>
                    <Image
                      source={
                        this.state.tickSelected
                          ? IMAGES.react_selected
                          : IMAGES.rectangle7
                      }
                      resizeMode={'contain'}
                      style={{
                        height: 15,
                        width: 15,
                        marginRight: 10,
                        tintColor: COLOR.YELLOW,
                      }}
                    />
                  </TouchableWithoutFeedback>
                  <Text style={[Styles.small_label, {color: COLOR.WHITE}]}>
                    {Locale('Same as Legal Information')}
                  </Text>
                </View>

                <View>
                  {this.props.userDetail && this.props.userDetail.role ? (
                    this.props.userDetail.role == 1 ? (
                      <View pointerEvents="none">
                        <this.fourthTab this={this} />
                      </View>
                    ) : (
                      <this.fourthTab this={this} />
                    )
                  ) : (
                    <this.fourthTab this={this} />
                  )}
                </View>
              </View>
            </KeyboardAwareScrollView>
            {this.props.userDetail && this.props.userDetail.role ? (
              this.props.userDetail.role == 1 ? null : (
                <View
                  style={{
                    width: '90%',
                    alignSelf: 'center',
                    marginTop: 20,
                    marginBottom: 10,
                  }}>
                  <CustomButton
                    onPress={this.saveTradeInfo}
                    text={
                      this.getBusinessStatus(userBusinessDetail) == 'Approved'
                        ? Locale('Save & Submit')
                        : Locale('Save')
                    }
                  />
                </View>
              )
            ) : (
              <View
                style={{
                  width: '90%',
                  alignSelf: 'center',
                  marginTop: 20,
                  marginBottom: 10,
                }}>
                <CustomButton
                  onPress={this.saveTradeInfo}
                  text={
                    this.getBusinessStatus(userBusinessDetail) == 'Approved'
                      ? Locale('Save & Submit')
                      : Locale('Save')
                  }
                />
              </View>
            )}
            <ActivityIndicator loading={this.state.isLoading} />
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginTop: 15,
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.authReducer.userDetail,
    userBusinessDetail: state.authReducer.userBusinessDetail,
    userBusinessImages: state.authReducer.businessImages,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserDetail: val => {
      dispatch(AuthenticationAction.getUserInfo(val));
    },
    getUserBusinessDetail: val => {
      dispatch(AuthenticationAction.getBusinessDetail(val));
    },
    getBusinessImages: val => {
      dispatch(AuthenticationAction.getBusinessImages(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TradeInformation);
