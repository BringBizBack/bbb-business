import React, {useEffect, useRef} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  Platform,
  Alert,
} from 'react-native';
import Styles from '../../../../styles/Styles';
import IMAGES from '../../../../styles/Images';
import NavigationBar from '../../../../commonView/NavigationBar';
import COLOR from '../../../../styles/Color';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {CustomButton} from '../../../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import {FloatingTitleTextInputField} from '../../../../commonView/FloatingTitleTextInputField';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  getDateFromTimeStamp,
  Locale,
  navigate,
  showAlertMessage,
} from '../../../../commonView/Helpers';
import {
  AS_USER_TOKEN,
  U_ADD_BUSINESS_IMAGES,
  U_BASE,
} from '../../../../commonView/Constants';
import API from '../../../../api/Api';
import ActivityIndicator from '../../../../commonView/ActivityIndicator';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from '../../../../commonView/ActionSheet';
import AuthenticationAction from '../../../../redux/action/AuthenticationAction';
import {connect} from 'react-redux';
import FastImage from 'react-native-fast-image';
import {Dropdown} from '../../../../commonView/Dropdown';
import Auth from '../../../../auth';
import axios from 'axios';

let api = new API();
let auth = new Auth();

class PersonalDetails extends React.Component {
  constructor() {
    super();
    this.state = {
      fName: '',
      lName: '',
      email: '',
      idCard: '',
      idNumber: '',
      idFrontImage: '',
      idBackImage: '',
      profileImage: '',
      phoneNumber: '',
      isLoading: false,
      actionModalVisible: false,
      currentImage: 1,
      alertModalVisible: false,
      imageIndex: 0,
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.saveBusinessInfo = this.saveBusinessInfo.bind(this);
    this.callUploadImage = this.callUploadImage.bind(this);
    this.handleDropdown = this.handleDropdown.bind(this);
    this.closeVerifyModal = this.closeVerifyModal.bind(this);
  }

  componentDidMount() {
    this.props.getUserDetail();
    this.props.getUserBusinessDetail();
    this.props.getBusinessImages();
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ) {
    if (prevProps.userBusinessDetail != this.props.userBusinessDetail) {
      console.log(
        'previous current props',
        prevProps.userBusinessDetail,
        this.props.userBusinessDetail,
      );
      let data = this.props.userBusinessDetail;
      this.setState({
        fName: this.state.fName ? this.state.fName : data.firstName,
        lName: this.state.lName ? this.state.lName : data.lastName,
        idCard: this.state.idCard ? this.state.idCard : data.cardType,
        idNumber: this.state.idNumber ? this.state.idNumber : data.cardNumber,
        idFrontImage: this.state.idFrontImage
          ? this.state.idFrontImage
          : data.idFront,
        idBackImage: this.state.idBackImage
          ? this.state.idBackImage
          : data.idBack,
        profileImage: this.state.profileImage
          ? this.state.profileImage
          : data.profile,
        phoneNumber: this.state.phoneNumber
          ? this.state.phoneNumber
          : data.phone,
        userBusinessInfo: this.state.userBusinessInfo
          ? this.state.userBusinessInfo
          : data,
      });
    }
  }

  topRoundView = props => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <View style={[Styles.left_corner_view, {marginVertical: 15}]}>
          <Text style={[Styles.extra_small_label]}>{props.title}</Text>
        </View>
        {/*<View style={{height: 30, width: 65, marginRight: 15}}>*/}
        {/*  <CustomButton height={30} text={'Edit'} />*/}
        {/*</View>*/}
      </View>
    );
  };

  updateMasterState = (attrName, value) => {
    if (attrName == 'phoneNumber' || attrName == 'tradePhoneNumber') {
      if (value.length <= 10) {
        this.setState({
          [attrName]: value,
        });
      }
    } else {
      this.setState({
        [attrName]: value,
      });
    }
  };

  handleSheet = val => {
    if (val == 1) {
      this.chooseImage(1);
    } else if (val == 2) {
      this.chooseImage(2);
    } else if (val == 3) {
      this.setState({
        actionModalVisible: false,
      });
    }
  };

  chooseImage = val => {
    let options = {
      title: Locale('Select Image'),
      compressImageMaxWidth: 1500,
      compressImageMaxHeight: 700,
      multiple: false,
      cropping: true,
      freeStyleCropEnabled: true,
    };

    if (val == 1) {
      ImagePicker.openCamera({
        title: Locale('Select Image'),
        compressImageMaxWidth: 1500,
        compressImageMaxHeight: 700,
        cropping: true,
        freeStyleCropEnabled: true,
      })
        .then(response => {
          if (response.didCancel) {
            console.log('User cancelled  Camera');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            showAlertMessage(response.customButton);
          } else {
            this.setState({
              isLoading: false,
              actionModalVisible: false,
            });
            this.callUploadImage(response, val);
          }
        })
        .catch(error => {
          if (error.message) {
            showAlertMessage(error.message);
          }
        });
    } else if (val == 2) {
      ImagePicker.openPicker(options)
        .then(response => {
          console.log('image local reposnse', response);
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            alert(response.customButton);
          } else {
            this.setState({
              isLoading: false,
              actionModalVisible: false,
            });

            this.callUploadImage(response, val);
          }
        })
        .catch(error => {
          if (error.message) {
            showAlertMessage(error.message);
          }
        });
    }
  };

  callUploadImage = async (res, val) => {
    let {currentImage} = this.state;
    let data = new FormData();
    let token = await auth.getValue(AS_USER_TOKEN);
    //1: ID Front, 2:Profile, 3:Logo, 4: Business Image

    let photo = {
      uri: res.path,
      type: res.type ? res.type : res.mime,
      name: 'image.jpg',
    };
    data.append(
      'type',
      currentImage == 3 ? 2 : currentImage == 4 ? 3 : currentImage == 5 ? 4 : 1,
    );
    data.append('file', photo);
    const config = {
      headers: {Authorization: `Bearer ${token}`},
    };
    axios
      .post(U_BASE + U_ADD_BUSINESS_IMAGES, data, config)
      .then(responseData => {
        this.setState({isLoading: false});
        if (responseData.data.status == 200) {
          console.log('] data is:-_-', responseData.data.response);
          showAlertMessage(Locale('Image uploaded successfully'), 1);
          this.props.getBusinessImages(1);
        } else {
          this.setState({isLoading: false});
          if (responseData.data.message) {
            showAlertMessage(responseData.data.message);
          }
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  saveBusinessInfo = () => {
    if (this.state.fName == '') {
      showAlertMessage(Locale('Enter first name'));
    } else if (this.state.lName == '') {
      showAlertMessage(Locale('Enter last name'));
    } else if (this.state.contactphone == '') {
      showAlertMessage(Locale('Enter phone number'));
    } else if (this.state.idCard == '') {
      showAlertMessage(Locale('Enter ID card'));
    } else if (this.state.idNumber == '') {
      showAlertMessage(Locale('Enter ID number'));
    } else {
      let data = JSON.stringify({
        firstName: this.state.fName,
        lastName: this.state.lName,
        email: this.props.userDetail ? this.props.userDetail.email : '',
        phone: this.state.phoneNumber,
        cardType: this.state.idCard,
        cardNumber: this.state.idNumber,
        idFront: this.state.idFrontImage,
        idBack: this.state.idBackImage,
        profile: this.state.profileImage,
      });
      this.submitResponse(data, 1);
    }
  };

  submitResponse = (data, type) => {
    let that = this;
    that.setState({isLoading: true});
    api
      .updatePersonalDetail(data, type)
      .then(json => {
        console.log('response is:-', json);
        that.setState({isLoading: false});
        if (json.status == 200) {
          if (json.data.status == 200) {
            if (type == 3) {
              this.setState({businessImage: []});
            }
            this.props.getUserBusinessDetail(1);
            if (type == 3) {
              this.setState({alertModalVisible: true});
            } else {
              showAlertMessage(Locale('Successfully updated profile'), 1);
            }
          } else {
            showAlertMessage(json.data.message, 2);
          }
        } else if (json.status == 400) {
          showAlertMessage(json.data.message, 2);
        } else {
          showAlertMessage(json.data.message, 2);
        }
      })
      .catch(error => {
        console.log('response is:-', error);
        that.setState({isLoading: false});
        showAlertMessage(error.response.data.message, 2);
      });
  };

  commonView = props => {
    if (props.data) {
      return (
        <View
          style={{
            height: 45,
            marginBottom: 20,
            ...(Platform.OS === 'ios' && {
              zIndex: props.zIndex,
            }),
          }}>
          <Dropdown
            placeholder={props.placeholder}
            type={props.type}
            items={props.data}
            value={props.value}
            zIndex={props.zIndex}
            handleDropdown={props.handleDropdown}
          />
        </View>
      );
    }
  };

  handleDropdown = (val, type) => {
    if (type == 10) {
      this.setState({
        idCard: {id: val == 'Passport' ? 1 : 0, label: val, value: val},
        isLoading: false,
      });
    }
  };

  getBusinessStatus = props => {
    if (props && props.tradingInfo) {
      switch (props.tradingInfo.tradeStatus) {
        case 1:
          return Locale('Waiting');
          break;
        case 2:
          return Locale('Approved');
          break;
        case 3:
          return Locale('Rejected');
          break;
        default:
          return 'Add Business';
          break;
      }
    } else {
      return Locale('Add Business');
    }
  };

  handleLegalTradingInfo() {
    if (this.state.tickSelected) {
      this.setState(
        {
          tickSelected: !this.state.tickSelected,
          streetAddress: '',
          city: null,
          tradingSubdistrict: null,
          tradingDistrict: null,
          nation: '',
          tradePostalCode: '',
        },
        () => {
          if (this.locationRef) {
            this.locationRef.setAddressText('');
          }
        },
      );
    } else {
      this.setState(
        {
          tickSelected: !this.state.tickSelected,
          streetAddress: this.state.legalStreetAddress,
          city: this.state.legalCity,
          tradingSubdistrict: this.state.legalSubdistrict,
          tradingDistrict: this.state.legalDistrict,
          nation: this.state.legalNation,
          tradePostalCode: this.state.legalPostalCode,
        },
        () => {
          if (this.locationRef) {
            this.locationRef.setAddressText(this.state.streetAddress ?? '');
          }
        },
      );
    }
  }

  onGoogleInputChangeLegal = text => {
    this.setState({legalStreetAddress: text});
  };

  onGoogleInputChangeTrade = text => {
    this.setState({streetAddress: text});
  };

  closeVerifyModal = val => {
    if (val == 1) {
      this.setState({alertModalVisible: false, isLoading: false});
      this.props.navigation.goBack();
    } else {
      this.setState({alertModalVisible: false, isLoading: false});
    }
  };

  firstTab = props => {
    return (
      <View style={[Styles.shadow_view, style.shadowView]}>
        <props.this.topRoundView id={1} title={Locale('Personal Details')} />
        <View style={{paddingHorizontal: 15, alignItems: 'center'}}>
          <FloatingTitleTextInputField
            isWhite={true}
            attrName={'fName'}
            value={this.state.fName}
            updateMasterState={this.updateMasterState}
            title={Locale('First Name')}
          />
          <FloatingTitleTextInputField
            isWhite={true}
            attrName={'lName'}
            value={this.state.lName}
            updateMasterState={this.updateMasterState}
            title={Locale('Last Name')}
          />
          <FloatingTitleTextInputField
            isWhite={true}
            attrName={'email'}
            value={this.props.userDetail ? this.props.userDetail.email : ''}
            editable={false}
            floatKeyboardType={'email-address'}
            updateMasterState={this.updateMasterState}
            title={Locale('Email ID')}
          />
          <FloatingTitleTextInputField
            isWhite={true}
            attrName={'phoneNumber'}
            value={this.state.phoneNumber}
            floatKeyboardType={'number-pad'}
            updateMasterState={this.updateMasterState}
            title={Locale('Phone Number')}
          />
        </View>
      </View>
    );
  };

  render() {
    let {userDetail, userBusinessDetail} = this.props;
    return (
      <View style={[Styles.container]}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <NavigationBar title={Locale('Personal & Verification Details')} />
            <KeyboardAwareScrollView
              keyboardShouldPersistTaps={'always'}
              enableResetScrollToCoords={false}
              style={{
                flex: 1,
                width: wp(90),
                alignSelf: 'center',
                showsVerticalScrollIndicator: false,
              }}>
              <View>
                {userDetail && userDetail.role ? (
                  userDetail.role == 1 ? null : (
                    <View pointerEvents="none">
                      <this.firstTab this={this} />
                    </View>
                  )
                ) : (
                  <this.firstTab this={this} />
                )}
                {userDetail && userDetail.role ? null : (
                  <View>
                    <View style={[Styles.shadow_view, style.shadowView]}>
                      <this.topRoundView
                        id={1}
                        title={Locale('Verification Details')}
                      />
                      <View
                        style={{paddingHorizontal: 15, alignItems: 'center'}}>
                        <this.commonView
                          placeholder={Locale('ID Card Type')}
                          placeholdeTextColor={COLOR.WHITE}
                          type={10}
                          zIndex={3002}
                          value={
                            this.state.idCard
                              ? this.state.idCard.value
                                ? this.state.idCard.value
                                : ''
                              : ''
                          }
                          data={[
                            {id: 0, label: 'Thai ID', value: 'Thai ID'},
                            {id: 1, label: 'Passport', value: 'Passport'},
                          ]}
                          handleDropdown={this.handleDropdown}
                        />
                        <FloatingTitleTextInputField
                          isWhite={true}
                          attrName={'idNumber'}
                          value={this.state.idNumber}
                          floatKeyboardType={'number-pad'}
                          updateMasterState={this.updateMasterState}
                          title={Locale('ID Number')}
                        />

                        <View
                          style={{
                            height: 180,
                            flexDirection: 'row',
                            marginBottom: 15,
                            backgroundColor: 'transparent',
                          }}>
                          <View
                            style={{
                              flex: 1,
                              alignItems: 'center',
                              marginRight: 0,
                              backgroundColor: 'transparent',
                            }}>
                            <TouchableWithoutFeedback
                              onPress={() => {
                                this.setState({
                                  actionModalVisible: true,
                                  currentImage: 1,
                                });
                              }}>
                              {this.props.userBusinessImages &&
                              this.props.userBusinessImages.idFront ? (
                                <FastImage
                                  resizeMode={FastImage.resizeMode.cover}
                                  style={[style.card_view, {borderRadius: 10}]}
                                  source={
                                    this.props.userBusinessImages
                                      ? this.props.userBusinessImages.idFront
                                        ? {
                                            uri: this.props.userBusinessImages
                                              .idFront,
                                            priority: FastImage.priority.high,
                                          }
                                        : IMAGES.id_card_front
                                      : IMAGES.id_card_front
                                  }
                                />
                              ) : (
                                <Image
                                  resizeMode={'contain'}
                                  style={[
                                    style.card_view,
                                    {borderRadius: 10, height: 150, width: 150},
                                  ]}
                                  source={IMAGES.id_card_front}
                                />
                              )}
                            </TouchableWithoutFeedback>
                            <Text
                              style={[
                                Styles.extra_small_label,
                                {color: COLOR.WHITE, marginTop: 5},
                              ]}>
                              {Locale('ID Front')}
                            </Text>
                          </View>
                        </View>
                        <View style={{alignItems: 'center'}}>
                          <TouchableWithoutFeedback
                            onPress={() => {
                              this.setState({
                                actionModalVisible: true,
                                currentImage: 3,
                              });
                            }}>
                            <Image
                              resizeMode={'cover'}
                              style={{
                                height: 80,
                                width: 80,
                                borderRadius: 30,
                              }}
                              source={
                                this.props.userBusinessImages
                                  ? this.props.userBusinessImages.profile
                                    ? {
                                        uri: this.props.userBusinessImages
                                          .profile,
                                      }
                                    : IMAGES.user
                                  : IMAGES.user
                              }
                            />
                          </TouchableWithoutFeedback>
                          <Text
                            style={[
                              Styles.extra_small_label,
                              {
                                color: COLOR.WHITE,
                                marginTop: 5,
                                marginBottom: 15,
                              },
                            ]}>
                            {Locale('Profile image')}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                )}

                {this.props.userDetail && this.props.userDetail.role ? (
                  this.props.userDetail.role == 1 ? null : (
                    <ActionSheet
                      modalVisible={this.state.actionModalVisible}
                      handleSheet={this.handleSheet}
                    />
                  )
                ) : (
                  <ActionSheet
                    modalVisible={this.state.actionModalVisible}
                    handleSheet={this.handleSheet}
                  />
                )}
              </View>
            </KeyboardAwareScrollView>
            {this.props.userDetail && this.props.userDetail.role ? (
              this.props.userDetail.role == 1 ? null : null
            ) : (
              <View
                style={{
                  width: '90%',
                  marginVertical: 15,
                  alignSelf: 'center',
                }}>
                <CustomButton
                  onPress={this.saveBusinessInfo}
                  text={
                    this.getBusinessStatus(userBusinessDetail) == 'Approved'
                      ? Locale('Save & Submit')
                      : Locale('Save')
                  }
                />
              </View>
            )}
            <ActivityIndicator loading={this.state.isLoading} />
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginTop: 15,
  },
  bottomButton: {marginVertical: 20, width: wp(90), alignSelf: 'center'},
  titleLabel: {color: COLOR.WHITE, width: 120, margin: 0, fontSize: 13},
  icon_image: {
    height: 40,
    width: 40,
  },
  card_view: {
    flex: 1,
    height: 120,
    width: '100%',
    borderRadius: 5,
  },
  add_business_view: {
    height: 100,
    width: 100,
    padding: 15,
    marginVertical: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.authReducer.userDetail,
    userBusinessDetail: state.authReducer.userBusinessDetail,
    userBusinessImages: state.authReducer.businessImages,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserDetail: val => {
      dispatch(AuthenticationAction.getUserInfo(val));
    },
    getUserBusinessDetail: val => {
      dispatch(AuthenticationAction.getBusinessDetail(val));
    },
    getBusinessImages: val => {
      dispatch(AuthenticationAction.getBusinessImages(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PersonalDetails);
