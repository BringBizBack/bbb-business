import React, {useEffect, useRef} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  Platform,
  Alert,
} from 'react-native';
import Styles from '../../../../styles/Styles';
import IMAGES from '../../../../styles/Images';
import NavigationBar from '../../../../commonView/NavigationBar';
import COLOR from '../../../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Locale, showAlertMessage} from '../../../../commonView/Helpers';
import {
  AS_USER_TOKEN,
  U_ADD_BUSINESS_IMAGES,
  U_BASE,
} from '../../../../commonView/Constants';
import API from '../../../../api/Api';
import ActivityIndicator from '../../../../commonView/ActivityIndicator';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from '../../../../commonView/ActionSheet';
import AuthenticationAction from '../../../../redux/action/AuthenticationAction';
import CampaignActions from '../../../../redux/action/CampaignActions';
import {connect} from 'react-redux';
import {Pages} from 'react-native-pages';
import FastImage from 'react-native-fast-image';
import Auth from '../../../../auth';
import axios from 'axios';
import Swiper from 'react-native-swiper';

let api = new API();
let auth = new Auth();

class LogoBusinessImages extends React.Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,
      actionModalVisible: false,
      currentImage: 1,
      businessImage: [],
      businessLogo: '',
      userBusinessInfo: null,
      imageIndex: 0,
      tickSelected: false,
    };

    this.callUploadImage = this.callUploadImage.bind(this);
    this.closeVerifyModal = this.closeVerifyModal.bind(this);
    this.deleteBusinessImage = this.deleteBusinessImage.bind(this);
  }

  componentDidMount() {
    this.props.getUserDetail();
    this.props.getUserBusinessDetail();
    this.props.getBusinessImages();
    this.props.getCampaignData(2, 1);
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ) {
    if (prevProps.userBusinessDetail != this.props.userBusinessDetail) {
      let data = this.props.userBusinessDetail;
      this.setState({
        businessLogo: data.tradingInfo ? data.tradingInfo.logo : [],
        userBusinessInfo: data,
      });
    }
  }

  topRoundView = props => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <View style={[Styles.left_corner_view, {marginVertical: 15}]}>
          <Text style={[Styles.extra_small_label]}>{props.title}</Text>
        </View>
        {/*<View style={{height: 30, width: 65, marginRight: 15}}>*/}
        {/*  <CustomButton height={30} text={'Edit'} />*/}
        {/*</View>*/}
      </View>
    );
  };

  handleSheet = val => {
    if (val == 1) {
      this.chooseImage(1);
    } else if (val == 2) {
      this.chooseImage(2);
    } else if (val == 3) {
      this.setState({
        actionModalVisible: false,
      });
    }
  };

  chooseImage = val => {
    let options = {
      title: Locale('Select Image'),
      compressImageMaxWidth: 1500,
      compressImageMaxHeight: 700,
      multiple: false,
      cropping: true,
      freeStyleCropEnabled: true,
    };

    if (val == 1) {
      ImagePicker.openCamera({
        title: Locale('Select Image'),
        compressImageMaxWidth: 1500,
        compressImageMaxHeight: 700,
        cropping: true,
        freeStyleCropEnabled: true,
      })
        .then(response => {
          if (response.didCancel) {
            console.log('User cancelled  Camera');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            showAlertMessage(response.customButton);
          } else {
            this.setState({
              isLoading: false,
              actionModalVisible: false,
            });
            this.callUploadImage(response, val);
          }
        })
        .catch(error => {
          if (error.message) {
            showAlertMessage(error.message);
          }
        });
    } else if (val == 2) {
      ImagePicker.openPicker(options)
        .then(response => {
          console.log('image local reposnse', response);
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            alert(response.customButton);
          } else {
            this.setState({
              isLoading: false,
              actionModalVisible: false,
            });

            this.callUploadImage(response, val);
          }
        })
        .catch(error => {
          if (error.message) {
            showAlertMessage(error.message);
          }
        });
    }
  };

  callUploadImage = async (res, val) => {
    let {currentImage} = this.state;
    let data = new FormData();
    let token = await auth.getValue(AS_USER_TOKEN);
    //1: ID Front, 2:Profile, 3:Logo, 4: Business Image
    let photo = {
      uri: res.path,
      type: res.type ? res.type : res.mime,
      name: 'image.jpg',
    };

    data.append(
      'type',
      currentImage == 3 ? 2 : currentImage == 4 ? 3 : currentImage == 5 ? 4 : 1,
    );
    data.append('file', photo);
    const config = {
      headers: {Authorization: `Bearer ${token}`},
    };

    axios
      .post(U_BASE + U_ADD_BUSINESS_IMAGES, data, config)
      .then(responseData => {
        this.setState({isLoading: false});
        console.log('responsedata is', responseData);
        if (responseData.data.status == 200) {
          showAlertMessage(Locale('Image uploaded successfully'), 1);
          this.props.getBusinessImages(1);
          this.props.getUserBusinessDetail(1);
        } else {
          this.setState({isLoading: false});
          if (responseData.data.message) {
            showAlertMessage(responseData.data.message);
          }
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  deleteBusinessImage() {
    let that = this;
    that.setState({isLoading: true});
    let imagePath = this.props.userBusinessImages.images[this.state.imageIndex];
    let data = JSON.stringify({file: imagePath});
    console.log('data is:-', data, imagePath, this.props.userBusinessImages);
    api
      .deleteBusinessImage(data)
      .then(json => {
        console.log('response is:-', json);
        that.setState({isLoading: false});
        if (json.status == 200) {
          if (json.data.status == 200) {
            this.props.getBusinessImages(1);
          } else {
            showAlertMessage(json.data.message, 2);
          }
        } else if (json.status == 400) {
          showAlertMessage(json.data.message, 2);
        } else {
          showAlertMessage(json.data.message, 2);
        }
      })
      .catch(error => {
        console.log('response is:-', error);
        that.setState({isLoading: false});
        showAlertMessage(error.response.data.message, 2);
      });
  }

  getBusinessStatus = props => {
    if (props && props.tradingInfo) {
      switch (props.tradingInfo.tradeStatus) {
        case 1:
          return Locale('Waiting');
          break;
        case 2:
          return Locale('Approved');
          break;
        case 3:
          return Locale('Rejected');
          break;
        default:
          return 'Add Business';
          break;
      }
    } else {
      return Locale('Add Business');
    }
  };

  closeVerifyModal = val => {
    if (val == 1) {
      this.setState({alertModalVisible: false, isLoading: false});
      this.props.navigation.goBack();
    } else {
      this.setState({alertModalVisible: false, isLoading: false});
    }
  };

  render() {
    return (
      <View style={[Styles.container]}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <NavigationBar title={Locale('Logo & Images')} />

            <KeyboardAwareScrollView
              keyboardShouldPersistTaps={'always'}
              enableResetScrollToCoords={false}
              style={{
                flex: 1,
                width: wp(90),
                alignSelf: 'center',
                showsVerticalScrollIndicator: false,
              }}>
              <View>
                <View style={[Styles.shadow_view, style.shadowView]}>
                  <this.topRoundView id={5} title={'Logo'} />
                  <View
                    style={{
                      margin: 15,
                      alignItems: 'center',
                      flexDirection: 'row',
                    }}>
                    <TouchableWithoutFeedback
                      onPress={() => {
                        if (
                          this.props.currentCampaign &&
                          !this.props.currentCampaign.length
                        ) {
                          this.setState({
                            actionModalVisible: true,
                            currentImage: 4,
                          });
                        } else {
                          showAlertMessage(
                            'You cannot edit information if campaign is running',
                          );
                        }
                      }}>
                      <View
                        style={[
                          style.shadowView,
                          Styles.shadow_view,
                          style.add_business_view,
                        ]}>
                        <Image
                          source={
                            this.props.userBusinessImages
                              ? this.props.userBusinessImages.logo
                                ? {uri: this.props.userBusinessImages.logo}
                                : IMAGES.plus
                              : IMAGES.plus
                          }
                          style={
                            this.props.userBusinessImages
                              ? this.props.userBusinessImages.logo
                                ? {height: 100, width: 100}
                                : {height: 50, width: 50}
                              : {height: 50, width: 50}
                          }
                          resizeMode={'contain'}
                        />
                        {this.props.userBusinessImages &&
                          !this.props.userBusinessImages.logo && (
                            <Text
                              style={[
                                Styles.extra_small_label,
                                {color: COLOR.WHITE, marginTop: 0},
                              ]}>
                              {Locale('Click to upload')}
                            </Text>
                          )}
                      </View>
                    </TouchableWithoutFeedback>
                    <View style={{marginLeft: 10, justifyContent: 'center'}}>
                      <Text
                        style={[
                          Styles.bold_body_label,
                          {color: COLOR.WHITE, marginBottom: 10},
                        ]}>
                        {Locale('Required')}
                      </Text>
                      <Text
                        style={[
                          Styles.extra_small_label,
                          {color: COLOR.WHITE},
                        ]}>
                        {Locale('Minumum 400 pixels X 400 pixels')}
                      </Text>
                      <Text
                        style={[
                          Styles.extra_small_label,
                          {color: COLOR.WHITE, marginVertical: 5},
                        ]}>
                        {Locale('Size 4 mb or less')}
                      </Text>
                      <Text
                        style={[
                          Styles.extra_small_label,
                          {color: COLOR.WHITE},
                        ]}>
                        {Locale('Type PNG, JPEG, JPG')}
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={[Styles.shadow_view, style.shadowView]}>
                  <this.topRoundView id={4} title={Locale('Photo of food')} />
                  <View style={{margin: 15, alignItems: 'center'}}>
                    <View
                      style={[
                        style.shadowView,
                        Styles.shadow_view,
                        {
                          width: '100%',
                          flex: 1,
                          alignItems: 'center',
                          padding: 0,
                          height: 180,
                          justifyContent: 'center',
                        },
                      ]}>
                      {this.props.userBusinessImages &&
                      this.props.userBusinessImages.images.length == 0 ? (
                        <TouchableWithoutFeedback
                          style={{flex: 1}}
                          onPress={() => {
                            if (
                              this.props.currentCampaign &&
                              !this.props.currentCampaign.length
                            ) {
                              this.setState({
                                actionModalVisible: true,
                                currentImage: 5,
                              });
                            } else {
                              showAlertMessage(
                                'You cannot edit information if campaign is running',
                              );
                            }
                          }}>
                          <View
                            style={{
                              alignItems: 'center',
                            }}>
                            <Image
                              source={IMAGES.plus}
                              style={{height: 50, width: 50}}
                              resizeMode={'contain'}
                            />
                            <Text
                              style={[
                                Styles.extra_small_label,
                                {color: COLOR.WHITE, marginTop: 0},
                              ]}>
                              {Locale('Click to upload')}
                            </Text>
                          </View>
                        </TouchableWithoutFeedback>
                      ) : (
                        <View
                          style={{
                            flex: 1,
                            height: '100%',
                            width: '100%',
                          }}>
                          <TouchableWithoutFeedback
                            onPress={() => {
                              if (
                                this.props.currentCampaign &&
                                !this.props.currentCampaign.length
                              ) {
                                this.setState({
                                  actionModalVisible: true,
                                  currentImage: 5,
                                });
                              } else {
                                showAlertMessage(
                                  'You cannot edit information if campaign is running',
                                );
                              }
                            }}>
                            <Text
                              style={{
                                height: 30,
                                color: COLOR.WHITE,
                                alignSelf: 'flex-end',
                                marginRight: 10,
                                marginTop: 10,
                              }}>
                              {Locale('Add more +')}
                            </Text>
                          </TouchableWithoutFeedback>
                          <Swiper
                            keyboardShouldPersistTaps="handled"
                            key={
                              this.props.userBusinessImages
                                ? this.props.userBusinessImages.images.length
                                : 0
                            }
                            showsPagination={true}
                            containerStyle={{flex: 1}}
                            loop={false}
                            loadMinimalSize={1}
                            onIndexChanged={index => {
                              this.setState({
                                imageIndex: index,
                              });
                            }}
                            paginationStyle={{
                              bottom: 10,
                            }}
                            loadMinimal={true}
                            activeDotColor="#FFFFFF"
                            dotColor="#FFFFFF50">
                            {this.props.userBusinessImages &&
                              this.props.userBusinessImages.images.map(
                                element => {
                                  return (
                                    <FastImage
                                      resizeMode={FastImage.resizeMode.cover}
                                      style={{
                                        overflow: 'hidden',
                                        width: '100%',
                                        alignSelf: 'center',
                                        height: '100%',
                                        borderRadius: 5,
                                      }}
                                      source={{
                                        uri: element,
                                        priority: FastImage.priority.high,
                                      }}
                                    />
                                  );
                                },
                              )}
                          </Swiper>
                          {this.props.userDetail &&
                          this.props.userDetail.role ? (
                            this.props.userDetail.role == 1 ? null : (
                              <TouchableWithoutFeedback
                                style={{
                                  height: 50,
                                  width: 50,
                                  backgroundColor: COLOR.WHITE,
                                  zIndex: 1000,
                                }}
                                onPress={() => this.deleteBusinessImage()}>
                                <Image
                                  style={{
                                    height: 20,
                                    width: 20,
                                    borderWidth: 1,
                                    borderRadius: 10,
                                    borderColor: COLOR.RED,
                                    top: 45,
                                    right: 5,
                                    position: 'absolute',
                                  }}
                                  source={IMAGES.minus}
                                />
                              </TouchableWithoutFeedback>
                            )
                          ) : (
                            <TouchableWithoutFeedback
                              style={{
                                height: 50,
                                width: 50,
                                backgroundColor: COLOR.WHITE,
                                zIndex: 1000,
                              }}
                              onPress={() => this.deleteBusinessImage()}>
                              <Image
                                style={{
                                  height: 20,
                                  width: 20,
                                  borderWidth: 1,
                                  borderRadius: 10,
                                  borderColor: COLOR.RED,
                                  top: 45,
                                  right: 5,
                                  position: 'absolute',
                                }}
                                source={IMAGES.minus}
                              />
                            </TouchableWithoutFeedback>
                          )}
                        </View>
                      )}
                    </View>

                    <View style={{marginTop: 15, width: '100%'}}>
                      <Text
                        style={[
                          Styles.bold_body_label,
                          {color: COLOR.WHITE, marginBottom: 10},
                        ]}>
                        {Locale('Required')}
                      </Text>
                      <Text
                        style={[
                          Styles.extra_small_label,
                          {color: COLOR.WHITE},
                        ]}>
                        {Locale('Minumum 800 pixels X 400 pixels')}
                      </Text>
                      <Text
                        style={[
                          Styles.extra_small_label,
                          {color: COLOR.WHITE, marginVertical: 5},
                        ]}>
                        {Locale('Size 8 mb or less')}
                      </Text>
                      <Text
                        style={[
                          Styles.extra_small_label,
                          {color: COLOR.WHITE},
                        ]}>
                        {Locale('Type PNG, JPEG, JPG')}
                      </Text>
                    </View>
                  </View>
                </View>
                {this.props.userDetail && this.props.userDetail.role ? (
                  this.props.userDetail.role == 1 ? null : (
                    <ActionSheet
                      modalVisible={this.state.actionModalVisible}
                      handleSheet={this.handleSheet}
                    />
                  )
                ) : (
                  <ActionSheet
                    modalVisible={this.state.actionModalVisible}
                    handleSheet={this.handleSheet}
                  />
                )}
              </View>
            </KeyboardAwareScrollView>
            <ActivityIndicator loading={this.state.isLoading} />
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginTop: 15,
  },
  bottomButton: {marginVertical: 20, width: wp(90), alignSelf: 'center'},
  titleLabel: {color: COLOR.WHITE, width: 120, margin: 0, fontSize: 13},
  icon_image: {
    height: 40,
    width: 40,
  },
  card_view: {
    flex: 1,
    height: 120,
    width: '100%',
    borderRadius: 5,
  },
  add_business_view: {
    height: 100,
    width: 100,
    padding: 15,
    marginVertical: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    userDetail: state.authReducer.userDetail,
    userBusinessDetail: state.authReducer.userBusinessDetail,
    userBusinessImages: state.authReducer.businessImages,
    currentCampaign: state.campaignReducer.currentCampaign,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserDetail: val => {
      dispatch(AuthenticationAction.getUserInfo(val));
    },
    getUserBusinessDetail: val => {
      dispatch(AuthenticationAction.getBusinessDetail(val));
    },
    getBusinessImages: val => {
      dispatch(AuthenticationAction.getBusinessImages(val));
    },
    getCampaignData: (type, val, city) => {
      dispatch(CampaignActions.getCampaignData(type, val, city));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LogoBusinessImages);
