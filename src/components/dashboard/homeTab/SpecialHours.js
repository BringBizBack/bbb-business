import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  FlatList,
  TextInput,
  Alert,
} from 'react-native';
import Styles from '../../../styles/Styles';
import IMAGES from '../../../styles/Images';
import NavigationBar from '../../../commonView/NavigationBar';
import COLOR from '../../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {CustomButton} from '../../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import DateTimePicker from 'react-native-modal-datetime-picker';

import moment from 'moment';
import API from '../../../api/Api';
import {
  getDateFromTimeStamp,
  Locale,
  showAlertMessage,
} from '../../../commonView/Helpers';
import ActionSheet from '../../../commonView/ActionSheet';
import ActivityIndicator from '../../../commonView/ActivityIndicator';
import BusinessAction from '../../../redux/action/BusinessAction';
import {connect} from 'react-redux';

let api = new API();

class SpecialHours extends React.Component {
  constructor() {
    super();
    this.state = {
      weekDay: [
        [{day: 0, opening: null, closing: null, is_opened: 1}],
        [{day: 1, opening: null, closing: null, is_opened: 1}],
        [{day: 2, opening: null, closing: null, is_opened: 1}],
        [{day: 3, opening: null, closing: null, is_opened: 1}],
        [{day: 4, opening: null, closing: null, is_opened: 1}],
        [{day: 5, opening: null, closing: null, is_opened: 1}],
        [{day: 6, opening: null, closing: null, is_opened: 1}],
      ],
      dayName: [
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday',
      ],
      specialText: '',
      isLoading: false,
      isChangesMade: false,
      openDatePicker: false,
      minDate: null,
      selectedDateType: 0,
      selectedDateIndex: 0,
      selectedPick: 1,
    };

    this.addNewSlot = this.addNewSlot.bind(this);
    this.saveBusinessHours = this.saveBusinessHours.bind(this);
    this.handleBack = this.handleBack.bind(this);
    this.initializeView = this.initializeView.bind(this);
    this.getJsonData = this.getJsonData.bind(this);
    this.onPickupTimePick = this.onPickupTimePick.bind(this);
  }

  componentDidMount() {
    this.props.getBusinessHours();
    if (this.props.businessHourData) {
      this.initializeView();
    }
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ) {
    if (prevProps.businessHourData != this.props.businessHourData) {
      this.initializeView();
    }
  }

  initializeView = () => {
    let weeksTime = [];
    for (var i = 0; i < this.props.businessHourData.days.length; i++) {
      let currentSlots = [];
      for (var j = 0; j < this.props.businessHourData.days[i].length; j++) {
        currentSlots.push({
          day: i,
          opening: getDateFromTimeStamp(
            this.props.businessHourData.days[i][j].opening,
            'h:mm a',
          ),
          closing: getDateFromTimeStamp(
            this.props.businessHourData.days[i][j].closing,
            'h:mm a',
          ),
          is_opened:
            this.props.businessHourData.days[i][j].is_opened == true ? 1 : 0,
        });
      }
      if (currentSlots != []) {
        weeksTime.push(currentSlots);
      } else {
        weeksTime.push([{day: i, opening: null, closing: null, is_opened: 1}]);
      }
    }
    this.setState({
      weekDay: weeksTime,
      specialText: this.props.businessHourData.special
        ? this.props.businessHourData.special.special
        : '',
    });
  };

  onPickupTimePick = time => {
    let data = this.state.weekDay;
    if (this.state.selectedPick == 1) {
      data[this.state.selectedDateIndex][this.state.selectedDateType].opening =
        getDateFromTimeStamp(time, 'h:mm a');
    } else {
      data[this.state.selectedDateIndex][this.state.selectedDateType].closing =
        getDateFromTimeStamp(time, 'h:mm a');
    }
    this.setState({openDatePicker: false, isChangesMade: true});
  };

  addNewSlot = val => {
    let data = this.state.weekDay;
    data[val].push({day: val, opening: null, closing: null, is_opened: 1});
    this.setState({
      weekDay: data,
      isChangesMade: true,
    });
  };

  handleBack() {
    if (this.state.isChangesMade) {
      Alert.alert('Discard changes', 'Are you sure?', [
        {
          text: 'Discard',
          onPress: () => {
            this.props.navigation.goBack();
          },
          style: 'cancel',
        },
        {
          text: 'Save & Exit',
          onPress: () => {
            this.saveBusinessHours(true);
          },
        },
      ]);
    } else {
      this.props.navigation.goBack();
    }
  }

  getJsonData(array) {
    let errorCount = 0;
    let data = array;
    for (var i = 0; i < data.length; i++) {
      for (var j = 0; j < data[i].length; j++) {
        if (isNaN(data[i][j].opening) && isNaN(data[i][j].closing)) {
          let open = moment(data[i][j].opening, 'h:mm a', true).toDate();
          data[i][j].opening = open.getTime() / 1000;
          let close = moment(data[i][j].closing, 'h:mm a', true).toDate();
          data[i][j].closing = close.getTime() / 1000;
        }
        if (!data[i][j].opening) {
          showAlertMessage(
            Locale('Select start time') + ' ' + this.state.dayName[i],
          );
          errorCount += 1;
        }
        if (!data[i][j].closing) {
          showAlertMessage(
            Locale('Select end time') + ' ' + this.state.dayName[i],
          );
          errorCount += 1;
        }
      }
    }
    if (errorCount > 0) {
      return null;
    } else {
      return data;
    }
  }

  saveBusinessHours(props) {
    let data = this.getJsonData(this.state.weekDay);
    if (!data) {
      return;
    }
    this.setState({isLoading: true});
    api
      .addBusinessHours(
        JSON.stringify({data: data, special: this.state.specialText}),
      )
      .then(json => {
        this.setState({isLoading: false, isChangesMade: false});
        console.log('json response is', json);
        if (json.data.status == 200) {
          showAlertMessage(Locale('Successfully saved business hours'), 1);
          this.props.getBusinessHours(1);
          if (props) {
            this.props.navigation.goBack();
          }
        } else if (json.status == 400) {
          showAlertMessage(json.data.message, 2);
        } else {
          showAlertMessage(json.data.message, 2);
        }
      })
      .catch(error => {
        this.setState({isLoading: false});
        console.log('error is:-', error);
        //showAlertMessage(error.response.data.message, 2);
      });
  }

  render() {
    let {weekDay} = this.state;
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <ActivityIndicator loading={this.state.isLoading} />
          <SafeAreaView style={Styles.container}>
            <NavigationBar title={Locale('Special and hours')} />
            <TouchableWithoutFeedback onPress={this.handleBack}>
              <Text
                style={{
                  height: 45,
                  width: 45,
                  marginLeft: 20,
                  marginTop: -45,
                }}
              />
            </TouchableWithoutFeedback>

            <Text
              style={[
                Styles.small_label,
                {
                  width: wp(90),
                  alignSelf: 'center',
                  marginVertical: 15,
                  color: COLOR.WHITE,
                },
              ]}>
              {Locale('K_HOURS_HEADING')}
            </Text>
            <View style={[Styles.shadow_view, style.shadowView]}>
              <View style={[Styles.left_corner_view, {marginVertical: 15}]}>
                <Text style={[Styles.extra_small_label]}>
                  Specials(200 characters)
                </Text>
              </View>
              <View
                style={{
                  width: wp(90),
                  paddingHorizontal: 15,
                  alignSelf: 'center',
                }}>
                <TextInput
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: COLOR.WHITE,
                    marginBottom: 20,
                    color: COLOR.WHITE,
                  }}
                  onChangeText={text => {
                    if (text.length < 200) {
                      this.setState({specialText: text});
                    } else {
                      showAlertMessage(
                        Locale('Specials imposes a limit of 200 characters'),
                      );
                    }
                  }}
                  value={this.state.specialText}
                  multiline={true}
                  placeholder={Locale(
                    'What is special about your menu or place',
                  )}
                  placeholderTextColor={COLOR.WHITE}
                  returnKeyLabel="Done"
                  returnKeyType="done"
                  blurOnSubmit={true}
                />
              </View>
            </View>
            <FlatList
              style={style.flatlist_view}
              data={weekDay}
              renderItem={({item, index}) => (
                <TouchableWithoutFeedback
                  onPress={() => this.props.navigation.navigate('')}
                  style={{justifyContent: 'center', alignItems: 'center'}}>
                  <View style={[Styles.shadow_view, style.shadowView]}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}>
                      <View
                        style={[Styles.left_corner_view, {marginVertical: 15}]}>
                        <Text style={[Styles.extra_small_label]}>
                          {this.state.dayName[index]}
                        </Text>
                      </View>

                      <TouchableWithoutFeedback
                        onPress={() => this.addNewSlot(index)}>
                        <Text
                          style={[
                            Styles.extra_small_label,
                            {color: COLOR.WHITE, marginRight: 15},
                          ]}>
                          {Locale('Add new slot')}
                        </Text>
                      </TouchableWithoutFeedback>
                    </View>
                    {item.length > 0 &&
                      item.map((val, slot) => {
                        return (
                          <View style={style.time_view}>
                            <TouchableWithoutFeedback
                              onPress={() =>
                                this.setState({
                                  openDatePicker: true,
                                  selectedDateType: slot,
                                  selectedDateIndex: index,
                                  selectedPick: 1,
                                })
                              }>
                              <View
                                style={{
                                  flex: 1,
                                  marginRight: 10,
                                  marginBottom: 10,
                                }}>
                                <Text
                                  style={[
                                    Styles.body_label,
                                    {color: COLOR.WHITE},
                                  ]}>
                                  {isNaN(val.opening)
                                    ? val.opening
                                      ? val.opening
                                      : 'Start Time'
                                    : getDateFromTimeStamp(
                                        val.opening,
                                        'h:mm a',
                                      ) ?? 'Start Time'}
                                </Text>
                                <View
                                  style={[
                                    Styles.line_view,
                                    {
                                      color: COLOR.WHITE,
                                      marginBottom: 10,
                                      marginTop: 5,
                                    },
                                  ]}
                                />
                              </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback
                              onPress={() =>
                                this.setState({
                                  openDatePicker: true,
                                  // minDate: val.opening
                                  //   ? moment(
                                  //       val.opening,
                                  //       'h:mm a',
                                  //       true,
                                  //     ).toDate()
                                  //   : new Date(),
                                  selectedDateType: slot,
                                  selectedDateIndex: index,
                                  selectedPick: 2,
                                })
                              }>
                              <View
                                style={{
                                  flex: 1,
                                  marginRight: 10,
                                  marginBottom: 10,
                                }}>
                                <Text
                                  style={[
                                    Styles.body_label,
                                    {color: COLOR.WHITE},
                                  ]}>
                                  {isNaN(val.closing)
                                    ? val.closing
                                      ? val.closing
                                      : 'End Time'
                                    : getDateFromTimeStamp(
                                        val.closing,
                                        'h:mm a',
                                      ) ?? 'End Time'}
                                </Text>
                                <View
                                  style={[
                                    Styles.line_view,
                                    {
                                      color: COLOR.WHITE,
                                      marginBottom: 10,
                                      marginTop: 5,
                                    },
                                  ]}
                                />
                              </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback
                              onPress={() => {
                                let data = this.state.weekDay;
                                data[index].splice(slot, 1);
                                this.setState({weekDay: data});
                              }}>
                              <Image
                                source={IMAGES.minus}
                                style={{
                                  height: 20,
                                  width: 20,
                                  borderRadius: 10,
                                  borderWidth: 1,
                                  borderColor: COLOR.WHITE,
                                  marginBottom: 25,
                                }}
                              />
                            </TouchableWithoutFeedback>
                          </View>
                        );
                      })}
                    {/*<View*/}
                    {/*  style={{*/}
                    {/*    height: 30,*/}
                    {/*    alignItems: 'center',*/}
                    {/*    margin: 10,*/}
                    {/*    flexDirection: 'row',*/}
                    {/*  }}>*/}
                    {/*  <Text*/}
                    {/*    style={[*/}
                    {/*      Styles.bold_body_label,*/}
                    {/*      {flex: 1, color: COLOR.WHITE},*/}
                    {/*    ]}>*/}
                    {/*    {' '}*/}
                    {/*    Shop opened*/}
                    {/*  </Text>*/}
                    {/*  <Switch*/}
                    {/*    trackColor={COLOR.LIGHT_TEXT}*/}
                    {/*    ios_backgroundColor={COLOR.LIGHT_TEXT}*/}
                    {/*    tintColor={COLOR.GREEN}*/}
                    {/*    style={{height: 30}}*/}
                    {/*    value={*/}
                    {/*      item.length && item[0].is_opened == 1 ? true : false*/}
                    {/*    }*/}
                    {/*    onValueChange={val => {*/}
                    {/*      let data = this.state.weekDay;*/}
                    {/*      for (var i = 0; i < data[index].length; i++) {*/}
                    {/*        if (data[index][i].is_opened) {*/}
                    {/*          data[index][i].is_opened = 0;*/}
                    {/*        } else {*/}
                    {/*          data[index][i].is_opened = 1;*/}
                    {/*        }*/}
                    {/*      }*/}
                    {/*      if (data[index].length == 0) {*/}
                    {/*        this.addNewSlot(index);*/}
                    {/*      }*/}
                    {/*      this.setState({weekDay: data});*/}
                    {/*    }}*/}
                    {/*  />*/}
                    {/*</View>*/}
                  </View>
                </TouchableWithoutFeedback>
              )}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
            />
            <View style={style.bottom_button}>
              <CustomButton onPress={this.saveBusinessHours} text={'Save'} />
            </View>
            <DateTimePicker
              isVisible={this.state.openDatePicker}
              mode="time"
              date={new Date()}
              minimumDate={this.state.minDate ? this.state.minDate : null}
              onConfirm={this.onPickupTimePick.bind(this)}
              onCancel={() => this.setState({openDatePicker: false})}
            />
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginTop: 15,
  },
  flatlist_view: {
    marginVertical: 0,
    width: wp(90),
    marginHorizontal: 15,
    alignSelf: 'center',

    borderRadius: 5,
    paddingVertical: 15,
    paddingHorizontal: 10,
  },
  time_view: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
  },
  bottom_button: {width: wp(90), marginTop: 20, alignSelf: 'center'},
});

const mapStateToProps = (state, ownProps) => {
  return {
    businessHourData: state.businessReducer.businessHourData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getBusinessHours: val => {
      dispatch(BusinessAction.getBusinessHourData(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SpecialHours);
