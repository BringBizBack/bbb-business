import React, {Component} from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  ScrollView,
  Text,
  Alert,
  RefreshControl,
  Platform,
} from 'react-native';
import Styles from '../../../styles/Styles';
import IMAGES from '../../../styles/Images';
import COLOR from '../../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import {TouchableOpacity} from 'react-native-gesture-handler';
import AuthenticationAction from '../../../redux/action/AuthenticationAction';
import {connect} from 'react-redux';
import {
  AS_FCM_TOKEN,
  AS_INITIAL_ROUTE,
  AS_USER_DETAIL,
  AS_USER_TOKEN,
  U_IMAGE_BASE,
} from '../../../commonView/Constants';
import {Pages} from 'react-native-pages';
import FastImage from 'react-native-fast-image';
import {Locale, showAlertMessage} from '../../../commonView/Helpers';
import {CommonActions} from '@react-navigation/native';
import Auth from '../../../auth';
import BusinessAction from '../../../redux/action/BusinessAction';
import API from '../../../api/Api';
import CampaignActions from '../../../redux/action/CampaignActions';

let auth = new Auth();
let api = new API();

class DashboardScreen extends React.Component {
  constructor(props) {
    super();
    this.state = {
      isTabbar: false,
      businessImage: [],
      refreshing: false,
      creditRequestCount: 0,
      redeeemRequestCount: 0,
      showBusinessTick: false,
    };
    this.updateMasterState = this.updateMasterState.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    this.handleRequestData = this.handleRequestData.bind(this);
  }

  componentDidMount() {
    this.props.getUserDetail();
    this.props.getMemberData();
    this.props.getBusinessImages();
    this.props.getUserBusinessDetail();
    this.props.getBusinessHours();
    this.props.getPaymentMethod();
    this.props.getCampaignData(2, 1);
    this.props.getCampaignData(3, 1);
    this.props.getCampaignData(4, 1);
    this.handleRequestData(1);
    this.handleRequestData(2);
    if (this.props.route.params) {
      if (this.props.route.params.tabScreen != null) {
        this.setState({
          isTabbar: true, //this.props.route.params.tabScreen,
        });
      }
    }
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ) {
    if (
      prevProps.userBusinessDetail != this.props.userBusinessDetail &&
      this.props.userBusinessDetail
    ) {
      if (this.props.userBusinessDetail) {
        this.setState({
          businessImage: this.props.userBusinessDetail.tradingInfo.images ?? [],
        });
      }
    }
    if (prevProps.businessHourData != this.props.businessHourData) {
      if (this.props.businessHourData) {
        if (this.props.businessHourData.days.length) {
          let showTick = false;
          if (
            this.props.businessHourData.days[0][0] ||
            this.props.businessHourData.days[1][0] ||
            this.props.businessHourData.days[2][0] ||
            this.props.businessHourData.days[3][0] ||
            this.props.businessHourData.days[4][0] ||
            this.props.businessHourData.days[5][0] ||
            this.props.businessHourData.days[6][0]
          ) {
            this.setState({showBusinessTick: true});
          } else {
            this.setState({showBusinessTick: false});
          }
        } else {
          this.setState({showBusinessTick: false});
        }
      }
    }

    console.log(this.props.userBusinessImages);
  }

  updateMasterState = (props, value) => {
    this.setState({
      [props]: value,
    });
  };

  handleRequestData(val) {
    this.setState({isLoading: true});
    let that = this;
    if (val == 1) {
      api
        .getCampaignCreditRequest()
        .then(json => {
          console.log('tes data is:-', json);
          if (json.data.status == 200) {
            console.log('credit request data.response', json.data.response);
            this.setState({
              creditRequestCount: json.data.response.length,
              isLoading: false,
            });
          } else {
            that.setState({isLoading: false});
            console.log('error is', json.data);
            // showAlertMessage(json);
          }
        })
        .catch(function (error) {
          that.setState({isLoading: false});
          console.log('error is', error);
          // showAlertMessage(error.response.data.message);
        });
    } else {
      api
        .getCampaignRedeemRequest()
        .then(json => {
          console.log('credit redeem test data is:-', json);
          if (json.data.status == 200) {
            that.setState({
              redeeemRequestCount: json.data.response.length,
              isLoading: false,
            });
          } else {
            that.setState({isLoading: false});
            showAlertMessage(json.data.message);
          }
        })
        .catch(function (error) {
          console.log('error is', error);
          that.setState({isLoading: false});
          //showAlertMessage(error.response.data.message);
        });
    }
  }

  isBusinessApproved = () => {
    return this.props.userBusinessDetail
      ? this.props.userBusinessDetail.tradingInfo
        ? this.props.userBusinessDetail.tradingInfo.tradeStatus == 2
          ? true
          : false
        : false
      : false;
  };

  handleRedirection = id => {
    switch (id) {
      case 1:
        if (this.isBusinessApproved()) {
          if (
            this.props.currentCampaign &&
            this.props.currentCampaign.length > 0
          ) {
            this.props.navigation.navigate('GiveCustomerCredit', {type: 1});
          } else {
            showAlertMessage(Locale('No campaign(s) found'));
          }
        } else {
          showAlertMessage(
            Locale('Please get your business verified before giving points'),
          );
        }
        break;
      case 2:
        if (this.isBusinessApproved()) {
          if (this.props.pastCampaign && this.props.pastCampaign.length > 0) {
            this.props.navigation.navigate('GiveCustomerCredit', {
              type: 2,
              isRedeemScreen: true,
            });
          } else {
            showAlertMessage(Locale('No campaign(s) found'));
          }
        } else {
          showAlertMessage(
            Locale('Please get your business verified before giving points'),
          );
        }
        break;
      case 3:
        this.props.navigation.navigate('BusinessInformation');
        break;
      case 4:
        if (this.isBusinessApproved()) {
          this.props.navigation.navigate('SpecialHours');
        } else {
          showAlertMessage(
            Locale('Please get your business verified before adding hours'),
          );
        }
        break;
      case 5:
        if (this.isBusinessApproved()) {
          this.props.navigation.navigate('PaymentCards');
        } else {
          showAlertMessage(
            Locale('Please get your business verified before adding cards'),
          );
        }
        break;
      case 6:
        if (this.isBusinessApproved()) {
          this.props.navigation.navigate('MyPeople');
        } else {
          showAlertMessage(
            Locale('Please get your business verified before adding members'),
          );
        }
        break;
      case 7:
        if (
          this.props.currentCampaign &&
          this.props.currentCampaign.length > 0
        ) {
          showAlertMessage(
            'Cannot Start campaign if current campaign running or future campaign is approved',
          );
        } else if (
          this.props.futureCampaign &&
          this.props.futureCampaign.length > 0
        ) {
          showAlertMessage(
            'Cannot Start campaign if current campaign running or future campaign is approved',
          );
        } else {
          if (this.isBusinessApproved()) {
            this.props.navigation.navigate('CreateNewCampaign');
          } else {
            showAlertMessage(
              Locale(
                'Please get your business verified before adding campaign',
              ),
            );
          }
        }
        break;
      case 8:
        if (this.isBusinessApproved()) {
          this.props.navigation.navigate('ExploreCampaign', {
            type: 3,
          });
        } else {
          showAlertMessage(
            Locale('Please get your business verified to view past campaign'),
          );
        }
        break;
      case 9:
        if (this.isBusinessApproved()) {
          this.props.navigation.navigate('ExploreCampaign', {
            type: 4,
          });
        } else {
          showAlertMessage(
            Locale('Please get your business verified to view future campaign'),
          );
        }
        break;
      default:
        break;
    }
  };

  campaignView = props => {
    return (
      <View
        style={{opacity: this.isBusinessApproved() || props.id == 3 ? 1 : 0.4}}>
        <TouchableWithoutFeedback
          onPress={() => {
            this.handleRedirection(props.id);
          }}>
          <View style={style.campaign_main_view}>
            <Image
              source={props.image}
              style={{height: 35, width: 35}}
              resizeMode={'contain'}
            />
            <View style={{flex: 1, marginHorizontal: 15}}>
              <Text style={[Styles.bold_body_label, {color: COLOR.WHITE}]}>
                {props.title}
              </Text>
            </View>
            <Image
              source={IMAGES.keyboard_arrow_right}
              style={{height: 12, width: 12, tintColor: COLOR.WHITE}}
              resizeMode={'contain'}
            />
          </View>
        </TouchableWithoutFeedback>
        {props.showLine && (
          <View
            style={[
              Styles.line_view,
              {
                borderBottomWidth: 0.4,
                marginVertical: 15,
                borderBottomColor: COLOR.DARK_BLUE,
              },
            ]}
          />
        )}
      </View>
    );
  };

  handleLogout = async () => {
    const token = await auth.getValue(AS_FCM_TOKEN);
    api
      .logoutUser(token)
      .then(json => {
        console.log('logout response is:-', json);
        this.setState({isLoading: true});
        showAlertMessage(Locale('Successfully logged out'), 1);
        this.setState({isLoading: false});
        auth.remove(AS_USER_TOKEN);
        auth.remove(AS_USER_DETAIL);
        auth.setValue(AS_INITIAL_ROUTE, 'LoginScreen');
        this.props.logoutUser();
        showAlertMessage(Locale('Successfully logged out user'));
        this.props.navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{name: 'LoginScreen'}],
          }),
        );
      })
      .catch(error => {
        this.setState({isLoading: false});
        console.log('error is:-', error);
        showAlertMessage(error.response.data.message, 2);
      });
  };

  render() {
    const {isTabbar} = this.state;
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <View style={style.nav_bar_view}>
              <Image
                source={IMAGES.logo}
                style={{height: 35, width: 120, marginLeft: 5}}
                resizeMode={'contain'}
              />
              <View
                style={[
                  Styles.right_corner_view,
                  {backgroundColor: COLOR.YELLOW},
                ]}>
                <Text style={Styles.extra_small_label}>
                  {this.props.userDetail && this.props.userDetail.role != 1
                    ? Locale('Business Manager')
                    : Locale('Server')}
                </Text>
              </View>
            </View>
            <ScrollView
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={() => {
                    this.setState({
                      refreshing: false,
                    });
                    this.props.getUserDetail(1);
                    this.props.getMemberData(1);
                    this.props.getUserBusinessDetail(1);
                    this.props.getBusinessHours(1);
                    this.props.getPaymentMethod(1);
                    this.props.getCampaignData(2, 1);
                    this.props.getCampaignData(3, 1);
                    this.props.getCampaignData(4, 1);
                    this.handleRequestData(1);
                    this.handleRequestData(2);
                  }}
                />
              }>
              <View style={style.profile_image_view}>
                <View style={{flex: 1}}>
                  <Text style={[Styles.subheading_label, {color: COLOR.WHITE}]}>
                    Welcome{' '}
                    {this.props.userBusinessDetail
                      ? this.props.userBusinessDetail.tradingInfo
                        ? this.props.userBusinessDetail.tradingInfo.name
                        : ''
                      : ''}
                    ,
                  </Text>
                  <Text
                    style={[
                      Styles.extra_small_label,
                      {color: COLOR.WHITE, marginTop: 0, marginBottom: 10},
                    ]}>
                    {this.props.userDetail ? this.props.userDetail.email : ''}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    margin: 0,
                  }}>
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.props.navigation.navigate('NotificationScreen')
                    }>
                    <View>
                      <Image
                        source={IMAGES.alarm}
                        style={{
                          height: 30,
                          width: 30,
                          borderRadius: 20,
                          marginRight: 10,
                          tintColor: COLOR.WHITE,
                        }}
                        resizeMode={'contain'}
                      />
                    </View>
                  </TouchableWithoutFeedback>
                  {this.props.userBusinessImages &&
                  this.props.userBusinessImages.profile ? (
                    <View>
                      <TouchableWithoutFeedback
                        onPress={() =>
                          this.props.navigation.navigate('PersonalDetails')
                        }>
                        <Image
                          source={{
                            uri: this.props.userDetail
                              ? this.props.userDetail.role == null
                                ? this.props.userBusinessImages.profile
                                : this.props.userBusinessImages.logo
                              : '',
                          }}
                          style={{
                            height: 40,
                            width: 40,
                            borderRadius: 20,
                            margin: 0,
                          }}
                          resizeMode={'cover'}
                        />
                      </TouchableWithoutFeedback>
                    </View>
                  ) : null}
                </View>
              </View>
              {this.props.userBusinessDetail && (
                <View
                  style={{
                    width: wp(90),
                    alignSelf: 'center',
                    marginBottom: 10,
                  }}>
                  {this.props.userBusinessDetail.legalInfo &&
                    this.props.userBusinessDetail.legalInfo.id && (
                      <Text
                        style={[
                          Styles.extra_small_label,
                          {color: COLOR.WHITE},
                        ]}>
                        Business Id:
                        {this.props.userBusinessDetail.legalInfo.id}
                      </Text>
                    )}
                  {this.props.userBusinessDetail.tradingInfo &&
                    this.props.userBusinessDetail.tradingInfo.id && (
                      <Text
                        style={[
                          Styles.extra_small_label,
                          {color: COLOR.WHITE},
                        ]}>
                        Trade Id:
                        {this.props.userBusinessDetail.tradingInfo.id}
                      </Text>
                    )}
                </View>
              )}
              <View
                style={[
                  Styles.shadow_view,
                  style.shadowView,
                  style.add_business_view,
                ]}>
                {this.props.userBusinessImages &&
                  this.props.userBusinessImages.images.length > 0 && (
                    <View
                      style={{
                        flex: 1,
                        width: '100%',
                        height: '100%',
                      }}>
                      <Pages
                        indicatorColor={COLOR.WHITE}
                        indicatorOpacity={0.5}>
                        {this.props.userBusinessImages &&
                          this.props.userBusinessImages.images.map(element => {
                            return (
                              <FastImage
                                resizeMode={FastImage.resizeMode.cover}
                                style={{
                                  overflow: 'hidden',
                                  width: '100%',
                                  alignSelf: 'center',
                                  height: '100%',
                                }}
                                source={{
                                  uri: element,
                                  priority: FastImage.priority.high,
                                }}
                              />
                            );
                          })}
                      </Pages>
                    </View>
                  )}
                {this.props.userBusinessImages ? (
                  this.props.userBusinessImages.images.length == 0 && (
                    <TouchableWithoutFeedback
                      onPress={() =>
                        this.props.navigation.navigate('BusinessInformation')
                      }>
                      <View
                        style={{
                          alignItems: 'center',
                        }}>
                        <Image
                          source={IMAGES.plus}
                          style={{height: 50, width: 50}}
                          resizeMode={'contain'}
                        />
                        <Text
                          style={[
                            Styles.body_label,
                            {color: COLOR.WHITE, marginTop: 0},
                          ]}>
                          {Locale('Add your business info')}
                        </Text>
                      </View>
                    </TouchableWithoutFeedback>
                  )
                ) : (
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.props.navigation.navigate('BusinessInformation')
                    }>
                    <View
                      style={{
                        alignItems: 'center',
                      }}>
                      <Image
                        source={IMAGES.plus}
                        style={{height: 50, width: 50}}
                        resizeMode={'contain'}
                      />
                      <Text
                        style={[
                          Styles.body_label,
                          {color: COLOR.WHITE, marginTop: 0},
                        ]}>
                        {Locale('Add your business info')}
                      </Text>
                    </View>
                  </TouchableWithoutFeedback>
                )}
              </View>

              <View
                style={[
                  style.shadowView,
                  Styles.shadow_view,
                  style.customer_credit_view,
                ]}>
                <this.campaignView
                  id={1}
                  showLine={false}
                  image={IMAGES.coin_collection}
                  title={
                    +this.state.creditRequestCount != 0
                      ? Locale('Give customer points') +
                        `(${this.state.creditRequestCount})`
                      : Locale('Give customer points')
                  }
                />
              </View>
              <View
                style={[
                  style.shadowView,
                  Styles.shadow_view,
                  style.customer_credit_view,
                  {marginVertical: 0},
                ]}>
                <this.campaignView
                  id={2}
                  showLine={false}
                  image={IMAGES.coin_collection}
                  title={
                    this.state.redeeemRequestCount != 0
                      ? Locale('Redeem customer credits') +
                        `(${this.state.redeeemRequestCount})`
                      : Locale('Redeem customer credits')
                  }
                />
              </View>

              <View
                style={[
                  style.shadowView,
                  Styles.shadow_view,
                  style.customer_credit_view,
                  {marginTop: 15, backgroundColor: COLOR.LIGHT_BLUE},
                ]}>
                <this.campaignView
                  id={3}
                  showLine={
                    this.props.userDetail
                      ? this.props.userDetail.role
                        ? this.props.userDetail.role != 1
                          ? true
                          : false
                        : true
                      : true
                  }
                  image={
                    this.props.userBusinessDetail
                      ? this.props.userBusinessDetail.tradingInfo
                        ? this.props.userBusinessDetail.tradingInfo
                            .tradeStatus == 2
                          ? IMAGES.green_tick
                          : IMAGES.tick_gray
                        : IMAGES.tick_gray
                      : IMAGES.tick_gray
                  }
                  title={Locale('Business Information')}
                />
                {this.props.userDetail ? (
                  this.props.userDetail.role != 1 && (
                    <View>
                      <this.campaignView
                        id={4}
                        showLine={true}
                        image={
                          this.state.showBusinessTick
                            ? IMAGES.green_tick
                            : IMAGES.tick_gray
                        }
                        title={Locale('Special and hours')}
                      />
                      {this.props.userDetail &&
                      this.props.userDetail.role ? null : (
                        <this.campaignView
                          id={5}
                          showLine={true}
                          image={
                            this.props.cardData
                              ? this.props.cardData.length > 0
                                ? IMAGES.green_tick
                                : IMAGES.tick_gray
                              : IMAGES.tick_gray
                          }
                          title={Locale('Payment Details')}
                        />
                      )}
                      <this.campaignView
                        id={6}
                        showLine={false}
                        image={
                          this.props.membersData
                            ? this.props.membersData.length > 0
                              ? IMAGES.green_tick
                              : IMAGES.tick_gray
                            : IMAGES.tick_gray
                        }
                        title={Locale('My People')}
                      />
                    </View>
                  )
                ) : (
                  <View />
                )}
              </View>

              <View
                style={[
                  style.shadowView,
                  Styles.shadow_view,
                  style.customer_credit_view,
                  {
                    marginVertical: 0,
                    marginTop: 15,
                    backgroundColor: COLOR.MEDIUM_BLUE,
                  },
                ]}>
                {this.props.userDetail ? (
                  this.props.userDetail.role == null && (
                    <this.campaignView
                      id={7}
                      showLine={true}
                      image={IMAGES.campaign_start}
                      title={Locale('Start campaign')}
                    />
                  )
                ) : (
                  <View />
                )}
                <this.campaignView
                  id={8}
                  showLine={true}
                  image={IMAGES.campaign_start}
                  title={Locale('Past campaign')}
                />
                <this.campaignView
                  id={9}
                  showLine={false}
                  image={IMAGES.campaign_start}
                  title={Locale('Future campaign')}
                />
              </View>
              <TouchableOpacity onPress={() => this.handleLogout()}>
                <Text
                  style={[
                    Styles.small_label,
                    style.logout_label,
                    {marginBottom: 0},
                  ]}>
                  {Locale('Logout')}
                </Text>
              </TouchableOpacity>
              <Text
                style={[
                  Styles.small_label,
                  {color: COLOR.WHITE, alignSelf: 'center', marginBottom: 10},
                ]}>
                {Platform.OS == 'ios' ? 'version-1.0.27' : 'version-1.0.9'}
              </Text>
            </ScrollView>
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 15,
    paddingVertical: 15,
  },
  campaign_view: {
    color: COLOR.WHITE,
    margin: 0,
    alignSelf: 'flex-start',
    flex: 1,
    fontSize: 9,
    fontWeight: '600',
  },
  campaign_main_view: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 5,
  },
  nav_bar_view: {
    width: wp(100),
    alignSelf: 'center',
    marginTop: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  profile_image_view: {
    alignSelf: 'center',
    marginVertical: 15,
    marginBottom: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: wp(90),
  },
  add_business_view: {
    height: 150,
    padding: 0,
    marginVertical: 0,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 0,
  },
  customer_credit_view: {
    marginVertical: 15,
    padding: 15,
    backgroundColor: COLOR.MEDIUM_BLUE,
    paddingVertical: 20,
  },
  logout_label: {
    color: COLOR.WHITE,
    textDecorationLine: 'underline',
    marginVertical: 15,
    height: 35,
    alignSelf: 'center',
  },
});

const mapStateToProps = (state, ownProps) => {
  console.log('state.authReducer.userDetail', state.authReducer.userDetail);
  return {
    userDetail: state.authReducer.userDetail,
    membersData: state.businessReducer.membersData,
    userBusinessDetail: state.authReducer.userBusinessDetail,
    businessHourData: state.businessReducer.businessHourData,
    cardData: state.businessReducer.paymentCardData,
    currentCampaign: state.campaignReducer.currentCampaign,
    futureCampaign: state.campaignReducer.futureCampaign,
    pastCampaign: state.campaignReducer.pastCampaign,
    userBusinessImages: state.authReducer.businessImages,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserDetail: val => {
      dispatch(AuthenticationAction.getUserInfo(val));
    },
    getMemberData: val => {
      dispatch(BusinessAction.getMemberData(val));
    },
    getUserBusinessDetail: val => {
      dispatch(AuthenticationAction.getBusinessDetail(val));
    },
    logoutUser: val => {
      dispatch(AuthenticationAction.logoutUser());
    },
    getBusinessHours: val => {
      dispatch(BusinessAction.getBusinessHourData(val));
    },
    getPaymentMethod: val => {
      dispatch(BusinessAction.getPaymentMethod(val));
    },
    getCampaignData: (type, val, city) => {
      dispatch(CampaignActions.getCampaignData(type, val, city));
    },
    getBusinessImages: val => {
      dispatch(AuthenticationAction.getBusinessImages(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DashboardScreen);
