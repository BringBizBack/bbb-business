import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  FlatList,
} from 'react-native';
import Styles from '../../../../styles/Styles';
import IMAGES from '../../../../styles/Images';
import NavigationBar from '../../../../commonView/NavigationBar';
import COLOR from '../../../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {
  getDateFromTimeStamp,
  insertComma,
  Locale,
  showAlertMessage,
} from '../../../../commonView/Helpers';
import API from '../../../../api/Api';

let api = new API();

export default class BillingHistory extends React.Component {
  constructor() {
    super();
    this.state = {
      campaignData: [],
      historyData: [],
      isTabbar: false,
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.getCreditRedeemData = this.getCreditRedeemData.bind(this);
  }

  componentDidMount(props) {
    if (this.props.route.params) {
      if (this.props.route.params.tabScreen != null) {
        this.setState(
          {
            isTabbar: this.props.route.params.tabScreen,
            campaignData: this.props.route.params.campaignData,
          },
          () => {
            this.getCreditRedeemData();
          },
        );
      }
    }
  }

  getCreditRedeemData() {
    try {
      api
        .getRedeemCreditHistoryCampaign(
          JSON.stringify({
            campaign: this.state.campaignData ? this.state.campaignData.id : '',
          }),
        )
        .then(json => {
          console.log('tes data is:-', json);
          if (json.data.status == 200) {
            console.log('billing history  data.response', json.data.response);
            this.setState({historyData: json.data.response});
          } else {
            showAlertMessage(json.data.message);
          }
        })
        .catch(function (error) {
          showAlertMessage(error.response.data.message);
        });
    } catch (error) {
      console.log('error getting campaign detail');
    }
  }

  updateMasterState = (props, value) => {
    this.setState({
      [props]: value,
    });
  };

  commonView = props => {
    return (
      <View>
        <TouchableWithoutFeedback
          onPress={() => this.handleRedirection(props.id)}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              paddingHorizontal: 5,
            }}>
            <Image
              source={props.image}
              style={{height: 40, width: 40}}
              resizeMode={'cover'}
            />
            <View style={{flex: 1, marginHorizontal: 15}}>
              <Text
                style={[
                  Styles.small_label,
                  {color: COLOR.BLACK, alignSelf: 'flex-start'},
                ]}>
                {Locale('Terms & Conditions')}
              </Text>

              <Text
                style={[
                  Styles.bold_body_label,
                  {
                    alignSelf: 'flex-start',
                  },
                ]}>
                {props.title}
              </Text>
              {props.description && (
                <Text
                  style={[
                    Styles.small_label,
                    {color: COLOR.BLACK, alignSelf: 'flex-start'},
                  ]}>
                  {props.description}
                </Text>
              )}
            </View>
            <Image
              source={IMAGES.keyboard_arrow_right}
              style={{height: 12, width: 12}}
              resizeMode={'contain'}
            />
          </View>
        </TouchableWithoutFeedback>
        {props.showLine && (
          <View
            style={[
              Styles.line_view,
              {
                borderBottomWidth: 0.3,
                marginVertical: 15,
                borderBottomColor: COLOR.DARK_BLUE,
              },
            ]}
          />
        )}
      </View>
    );
  };

  handleRedirection = id => {
    switch (id) {
      case 1:
        this.props.navigation.navigate('PrizeDetailScreen');
        break;
      case 2:
        this.props.navigation.navigate('CampaignRules');
        break;
      default:
        break;
    }
  };

  render() {
    const {isTabbar} = this.state;
    return (
      <View style={[Styles.container, {backgroundColor: COLOR.DARK_BLUE}]}>
        <SafeAreaView style={Styles.container}>
          <NavigationBar title={'Billing History'} />
          <View style={[Styles.shadow_view, style.shadowView]}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <View style={Styles.left_corner_view}>
                <Text style={[Styles.small_label]}>
                  {Locale('Transactions')}
                </Text>
              </View>
              <TouchableWithoutFeedback
                onPress={() => {
                  this.setState({showCalendar: true});
                }}>
                {/*<View*/}
                {/*  style={{*/}
                {/*    flexDirection: 'row',*/}
                {/*    alignItems: 'center',*/}
                {/*    borderBottomColor: COLOR.WHITE,*/}
                {/*    borderBottomWidth: 1,*/}
                {/*    marginRight: 15,*/}
                {/*  }}>*/}
                {/*  <Image*/}
                {/*    source={IMAGES.calendar}*/}
                {/*    style={{height: 15, width: 15, marginRight: 10}}*/}
                {/*    resizeMode={'contain'}*/}
                {/*  />*/}
                {/*  <Text*/}
                {/*    style={[Styles.extra_small_label, {color: COLOR.WHITE}]}>*/}
                {/*    Feb 01, 2021 - May 15, 2021*/}
                {/*  </Text>*/}
                {/*</View>*/}
              </TouchableWithoutFeedback>
            </View>
            <FlatList
              style={style.flatlist_view}
              data={this.state.historyData}
              renderItem={({item, index}) => (
                <TouchableWithoutFeedback
                  onPress={() =>
                    this.props.navigation.navigate('CreditHistoryDetails', {
                      data: item,
                    })
                  }
                  style={{justifyContent: 'center', alignItems: 'center'}}>
                  <View
                    style={[
                      Styles.shadow_view,
                      {
                        width: '100%',
                        marginBottom: 5,
                      },
                    ]}>
                    <View
                      style={{
                        marginBottom: 10,
                        marginHorizontal: 0,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                      }}>
                      <View style={{margin: 0, flex: 1}}>
                        <Text
                          style={[
                            Styles.button_font,
                            {
                              color: COLOR.WHITE,
                              margin: 0,
                              alignSelf: 'flex-start',
                            },
                          ]}>
                          {insertComma(item.amount)} THB
                        </Text>
                        <Text
                          style={[
                            Styles.small_label,
                            {
                              color: COLOR.WHITE,
                              margin: 0,
                              alignSelf: 'flex-start',
                            },
                          ]}>
                          #{item.billNumber}
                        </Text>
                      </View>
                      <View style={{margin: 0}}>
                        <Text
                          style={[
                            Styles.button_font,
                            {
                              color: COLOR.WHITE,
                              margin: 0,
                              alignSelf: 'flex-end',
                            },
                          ]}>
                          {item.type}
                        </Text>
                        <Text
                          style={[
                            Styles.small_label,
                            {margin: 0, color: COLOR.WHITE},
                          ]}>
                          {getDateFromTimeStamp(item.createdAt, 'MMM DD, YYYY')}
                        </Text>
                      </View>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              )}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          {/*<View style={{width: wp(90), alignSelf: 'center'}}>*/}
          {/*  <CustomButton text={'Download'} />*/}
          {/*</View>*/}
        </SafeAreaView>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.MEDIUM_BLUE,
    borderRadius: 5,
    paddingVertical: 15,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 15,
  },
  flatlist_view: {
    width: wp(90),
    alignSelf: 'center',
    padding: 15,
    paddingBottom: 0,
  },
  common_view: {
    flex: 1,
    backgroundColor: COLOR.MEDIUM_BLUE,
    borderRadius: 10,
    padding: 7,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text_one: {
    color: COLOR.WHITE,
    textAlign: 'center',
    fontWeight: '500',
  },
  text_two: {
    color: COLOR.WHITE,
    textAlign: 'center',
    alignSelf: 'center',
    fontWeight: '700',
    marginVertical: 5,
  },
  text_three: {
    color: COLOR.WHITE,
    fontSize: 10,
    textAlign: 'center',
  },
  main_view: {
    marginTop: 10,
    marginBottom: 0,
    paddingVertical: 10,
    width: wp(100),
    backgroundColor: COLOR.WHITE,
    alignSelf: 'center',
  },
});
