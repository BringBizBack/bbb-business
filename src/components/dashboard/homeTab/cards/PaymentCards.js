import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  FlatList,
} from 'react-native';
import Styles from '../../../../styles/Styles';
import IMAGES from '../../../../styles/Images';
import NavigationBar from '../../../../commonView/NavigationBar';
import COLOR from '../../../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {CustomButton} from '../../../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import BusinessAction from '../../../../redux/action/BusinessAction';
import {connect} from 'react-redux';
import API from '../../../../api/Api';
import {Locale, showAlertMessage} from '../../../../commonView/Helpers';
import ActivityIndicator from '../../../../commonView/ActivityIndicator';
import AlertPopup from '../../../../commonView/AlertPopup';

let api = new API();

class PaymentCards extends React.Component {
  constructor() {
    super();
    this.state = {
      startTime: '',
      endTime: '',
      isLoading: false,
      alertModalVisible: false,
      selectedCard: null,
    };

    this.closeVerifyModal = this.closeVerifyModal.bind(this);
  }

  componentDidMount() {
    this.props.getPaymentMethod();
  }

  handleCard = (card, type) => {
    let that = this;
    that.setState({isLoading: false});
    api
      .handlePaymentMethod(card.id, type)
      .then(json => {
        console.log('member response is:-', json);
        that.setState({isLoading: false});
        let stringData = JSON.stringify(json.data.response);
        if (json.data.status == 200) {
          showAlertMessage(
            type == 1
              ? Locale('Successfully removed card')
              : card.isDefaultCard
              ? Locale('Card removed from default')
              : Locale('Card set to default'),
            1,
          );
          that.setState({isLoading: false});
          this.props.getPaymentMethod(1);
        } else if (json.status == 400) {
          showAlertMessage(json.data.message, 2);
        } else {
          showAlertMessage(json.data.message, 2);
        }
      })
      .catch(error => {
        that.setState({isLoading: false});
        showAlertMessage(error.response.data.message, 2);
      });
  };

  noPaymentCards() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
        }}>
        <Text style={[Styles.subheading_label, {color: COLOR.WHITE}]}>
          {Locale('No payment method added!')}
        </Text>
      </View>
    );
  }

  closeVerifyModal(val) {
    this.setState({alertModalVisible: false, isLoading: false});
    if (val == 1) {
      this.handleCard(this.state.selectedCard, 1);
    }
  }

  render() {
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <AlertPopup
              image={IMAGES.security}
              title={Locale('CONFIRMATION')}
              buttonText={Locale('Confirm')}
              desc={Locale('Are you sure you want to delete card?')}
              closeVerifyModal={val => this.closeVerifyModal(val)}
              modalVisible={this.state.alertModalVisible}
            />
            <NavigationBar title={Locale('Payment Details')} />

            {this.props.cardData && this.props.cardData.length ? (
              <FlatList
                style={style.flatlist_view}
                data={this.props.cardData}
                renderItem={({item, index}) => (
                  <View style={[Styles.shadow_view, style.shadowView]}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        marginBottom: 15,
                      }}>
                      <Image
                        style={{height: 30, width: 55}}
                        resizeMode={'contain'}
                        source={IMAGES.mastercard}
                      />
                      <TouchableWithoutFeedback
                        onPress={() => {
                          this.setState({
                            alertModalVisible: true,
                            selectedCard: item,
                          });
                        }}>
                        <Text
                          style={[
                            Styles.extra_small_label,
                            {color: COLOR.WHITE},
                          ]}>
                          {Locale('Delete')}
                        </Text>
                      </TouchableWithoutFeedback>
                    </View>

                    <Text
                      style={[
                        Styles.bold_body_label,
                        {marginVertical: 10, color: COLOR.WHITE},
                      ]}>
                      {item.cardNumber}
                    </Text>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}>
                      <View style={{flex: 1, marginRight: 10}}>
                        <Text
                          style={[
                            Styles.extra_small_label,
                            {marginBottom: 5, color: COLOR.WHITE},
                          ]}>
                          {Locale('Card Holder Name')}
                        </Text>
                        <Text
                          style={[
                            Styles.bold_body_label,
                            {color: COLOR.WHITE},
                          ]}>
                          {item.cardName}
                        </Text>
                      </View>
                      <View style={{flex: 1}}>
                        <Text
                          style={[
                            Styles.extra_small_label,
                            {marginBottom: 5, color: COLOR.WHITE},
                          ]}>
                          {Locale('Expiration Date')}
                        </Text>
                        <Text
                          style={[
                            Styles.bold_body_label,
                            {color: COLOR.WHITE},
                          ]}>
                          {item.cardExpiry}
                        </Text>
                      </View>
                    </View>
                    <TouchableWithoutFeedback
                      onPress={() => {
                        this.handleCard(item, 2);
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginTop: 15,
                        }}>
                        <Image
                          style={{height: 15, width: 15, marginRight: 10}}
                          source={
                            item.isDefaultCard
                              ? IMAGES.react_selected
                              : IMAGES.rectangle7
                          }
                        />
                        <Text style={[Styles.body_label, {color: COLOR.WHITE}]}>
                          {Locale('Primary Card')}
                        </Text>
                      </View>
                    </TouchableWithoutFeedback>
                  </View>
                )}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
              />
            ) : (
              <this.noPaymentCards />
            )}
            {/*<View style={{width: wp(60), marginTop: 20, alignSelf: 'center'}}>*/}
            {/*  <CustomButton*/}
            {/*    bg={COLOR.WHITE}*/}
            {/*    onPress={() => this.props.navigation.navigate('BillingHistory')}*/}
            {/*    text={'View billing history'}*/}
            {/*  />*/}
            {/*</View>*/}
            <View
              style={{width: wp(90), marginVertical: 15, alignSelf: 'center'}}>
              <CustomButton
                onPress={() => this.props.navigation.navigate('AddCard')}
                text={Locale('Add payment method')}
              />
            </View>
          </SafeAreaView>
          <ActivityIndicator loading={this.state.isLoading} />
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    padding: 15,
    marginBottom: 15,
  },
  flatlist_view: {
    marginVertical: 0,
    width: wp(90),
    marginHorizontal: 15,
    alignSelf: 'center',

    borderRadius: 5,
    paddingVertical: 15,
    paddingHorizontal: 10,
  },
});

const mapStateToProps = state => {
  return {
    cardData: state.businessReducer.paymentCardData,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    getPaymentMethod: val => {
      dispatch(BusinessAction.getPaymentMethod(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentCards);
