import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';
import Styles from '../../../../styles/Styles';
import IMAGES from '../../../../styles/Images';
import NavigationBar from '../../../../commonView/NavigationBar';
import COLOR from '../../../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {CustomButton} from '../../../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import {FloatingTitleTextInputField} from '../../../../commonView/FloatingTitleTextInputField';
import {
  Locale,
  navigate,
  showAlertMessage,
} from '../../../../commonView/Helpers';
import API from '../../../../api/Api';
import {connect} from 'react-redux';
import BusinessAction from '../../../../redux/action/BusinessAction';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

let api = new API();

class AddCard extends React.Component {
  constructor() {
    super();
    this.state = {
      name: '',
      cardNumber: '',
      expiryDate: '',
      securityCode: '',
    };
    this.updateMasterState = this.updateMasterState.bind(this);
  }

  updateMasterState = (props, value) => {
    if (props == 'expiryDate') {
      let val = value;
      if (val.length == 2 && this.state.expiryDate.length == 1) {
        val += '/';
      } else if (val.length == 2 && this.state.expiryDate.length == 3) {
        val = val.substring(0, val.length - 1);
      }
      if (val.length <= 5) {
        this.setState({expiryDate: val});
      }
    } else if (props == 'cardNumber') {
      if (value.length <= 19) {
        this.setState({
          cardNumber: value
            .replace(/\s?/g, '')
            .replace(/(\d{4})/g, '$1 ')
            .trim(),
        });
      }
    } else if (props == 'securityCode') {
      if (value.length <= 3) {
        this.setState({
          securityCode: value,
        });
      }
    } else {
      this.setState({
        [props]: value,
      });
    }
  };

  submitAddCard() {
    let that = this;
    const {name, cardNumber, expiryDate, securityCode} = that.state;
    if (name.length == 0) {
      showAlertMessage('Enter name on card', 2);
    } else if (cardNumber.length == 0) {
      showAlertMessage('Enter card number', 2);
    } else if (expiryDate.length == 0) {
      showAlertMessage('Enter expiry date', 2);
    } else if (securityCode.length == 0) {
      showAlertMessage('Enter security code', 2);
    } else {
      let data = JSON.stringify({
        cardName: name,
        cardNumber: cardNumber.replace(/ /g, ''),
        cardSecurityCode: securityCode,
        cardExpiry: expiryDate,
      });

      that.setState({isLoading: true});
      api
        .addPaymentMethod(data)
        .then(json => {
          console.log('add card response is:-', json);
          that.setState({isLoading: false});
          if (json.status == 200) {
            this.props.getPaymentMethod(1);
            this.props.navigation.navigate('SuccessScreen', {
              cardNumber: cardNumber,
            });
          } else if (json.status == 400) {
            showAlertMessage(json.data.message, 2);
          } else {
            showAlertMessage(json.data.message, 2);
          }
        })
        .catch(error => {
          that.setState({isLoading: false});
          showAlertMessage(error.response.data.message, 2);
        });
    }
  }

  render() {
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <NavigationBar title={'Add payment method'} />
            <Image
              style={{
                height: 60,
                marginTop: 15,
                width: wp(90),
                alignSelf: 'center',
              }}
              source={IMAGES.managerInfo}
            />
            <KeyboardAwareScrollView>
              <View style={[Styles.shadow_view, style.shadowView]}>
                <View style={[Styles.left_corner_view, {marginVertical: 15}]}>
                  <Text style={[Styles.extra_small_label]}>
                    {Locale('Credit Debit card details')}
                  </Text>
                </View>
                <View style={{padding: 15, alignItems: 'center'}}>
                  <View style={{marginTop: 15, width: '100%'}}>
                    <FloatingTitleTextInputField
                      isWhite={true}
                      attrName={'name'}
                      value={this.state.name}
                      updateMasterState={this.updateMasterState}
                      title={Locale('Name on card')}
                    />
                    <FloatingTitleTextInputField
                      isWhite={true}
                      attrName={'cardNumber'}
                      floatKeyboardType={'number-pad'}
                      value={this.state.cardNumber}
                      updateMasterState={this.updateMasterState}
                      title={Locale('Card Number')}
                    />
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1, marginRight: 10}}>
                        <FloatingTitleTextInputField
                          isWhite={true}
                          floatKeyboardType={'number-pad'}
                          attrName={'expiryDate'}
                          value={this.state.expiryDate}
                          updateMasterState={this.updateMasterState}
                          title={Locale('Expiration Date')}
                        />
                      </View>
                      <View style={{flex: 1}}>
                        <FloatingTitleTextInputField
                          isWhite={true}
                          attrName={'securityCode'}
                          floatKeyboardType={'number-pad'}
                          value={this.state.securityCode}
                          updateMasterState={this.updateMasterState}
                          title={Locale('Security Code')}
                        />
                      </View>
                    </View>
                  </View>
                </View>
              </View>
              <View style={{width: '90%', marginTop: 50, alignSelf: 'center'}}>
                <CustomButton
                  onPress={() => this.submitAddCard()}
                  text={Locale('Save and verify')}
                />
              </View>
            </KeyboardAwareScrollView>
            <View
              style={{
                flexDirection: 'row',
                marginVertical: 10,
                marginHorizontal: 15,
                justifyContent: 'center',
              }}>
              <TouchableWithoutFeedback
                onPress={() => {
                  navigate('PrivacyPolicyScreen');
                }}>
                <Text
                  style={{
                    textDecorationLine: 'underline',
                    alignSelf: 'center',

                    color: COLOR.WHITE,
                  }}>
                  {Locale('Privacy Policy')}
                </Text>
              </TouchableWithoutFeedback>
              <Text
                style={{
                  textDecorationLine: 'underline',
                  alignSelf: 'center',

                  color: COLOR.WHITE,
                }}>
                {'  '}|{'  '}
              </Text>
              <TouchableWithoutFeedback
                onPress={() => {
                  navigate('TermsConditionScreen');
                }}>
                <Text
                  style={{
                    textDecorationLine: 'underline',
                    alignSelf: 'center',

                    color: COLOR.WHITE,
                  }}>
                  {Locale('Terms and conditions')}
                </Text>
              </TouchableWithoutFeedback>
            </View>
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginTop: 15,
  },
  bottomButton: {marginVertical: 20, width: wp(90), alignSelf: 'center'},
  titleLabel: {color: COLOR.WHITE, width: 120, margin: 0, fontSize: 13},
  icon_image: {
    height: 40,
    width: 40,
  },
});

const mapDispatchToProps = dispatch => {
  return {
    getPaymentMethod: val => {
      dispatch(BusinessAction.getPaymentMethod(val));
    },
  };
};

export default connect(null, mapDispatchToProps)(AddCard);
