import React from 'react';
import {View, ImageBackground, Text} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {CommonActions} from '@react-navigation/native';
import {AS_INITIAL_ROUTE, AS_USER_TOKEN} from '../../commonView/Constants';
import IMAGES from '../../styles/Images';

export default class InitialLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      initialRoute: 'SplashScreen',
    };
  }

  async componentDidMount() {
    const token = await AsyncStorage.getItem(AS_USER_TOKEN);
    const routeName = await AsyncStorage.getItem(AS_INITIAL_ROUTE);
    if (routeName === null || routeName === '' || token === null) {
      this.props.navigation.navigate(this.state.initialRoute);
    } else {
      this.props.navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{name: routeName}],
        }),
      );
    }
  }

  // Render any loading content that you like here
  render() {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          alignSelf: 'center',
        }}
      />
    );
  }
}
