import React, {Component} from 'react';
import {
  StatusBar,
  Text,
  View,
  Image,
  SafeAreaView,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
  Platform,
  Alert,
} from 'react-native';
import {Pages, Indicator} from 'react-native-pages';
import COLOR from '../../styles/Color';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import LinearGradient from 'react-native-linear-gradient';
import {CustomButton} from '../../commonView/CustomButton';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {Locale} from '../../commonView/Helpers';

Platform.select({
  ios: () => StatusBar.setBarStyle('default'),
  android: () => StatusBar.setBackgroundColor('#263238'),
})();

export default class SplashScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      headingText: [
        Locale('Create Business'),
        Locale('Invite your team'),
        Locale('Create campaign'),
        Locale('Gain visitors'),
      ],
      descText: [
        Locale(
          'Lorem ipsum dolar sit amen  Lorem ipsum dolar sit amen Lorem ipsum dolar sit amen',
        ),
        Locale(
          'Lorem ipsum dolar sit amen  Lorem ipsum dolar sit amen Lorem ipsum dolar sit amen',
        ),
        Locale(
          'Lorem ipsum dolar sit amen  Lorem ipsum dolar sit amen Lorem ipsum dolar sit amen',
        ),
        Locale(
          'Lorem ipsum dolar sit amen  Lorem ipsum dolar sit amen Lorem ipsum dolar sit amen',
        ),
      ],
      splashImg: [
        IMAGES.splash1,
        IMAGES.splash2,
        IMAGES.splash3,
        IMAGES.splash4,
      ],
    };

    this.updateRef = this.updateRef.bind(this);
  }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.setState({index: 0});
    });
  }

  updateRef(ref) {
    this._pages = ref;
  }

  changePage = direc => {
    // console.log('reference is:-', index);
    const {index} = this.state;
    if (this._pages) {
      this.setState(
        {
          index: direc == 1 ? index - 1 : index + 1,
        },
        () => {
          if (index == 3 && direc == 2) {
            this.props.navigation.navigate('LoginScreen');
          } else {
            this._pages.scrollToPage(this.state.index);
          }
        },
      );
    }
  };

  render() {
    let {index, headingText, descText, splashImg} = this.state;
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={{flex: 1}}>
            <Image
              style={{
                height: 40,
                alignSelf: 'center',
                marginTop: 20,
              }}
              resizeMode={'contain'}
              source={IMAGES.logo}
            />
            <View style={{flex: 1, alignContent: 'center'}}>
              <Pages
                onScrollEnd={index => this.setState({index})}
                ref={this.updateRef}
                indicatorColor="#FFFFFF00"
                indicatorOpacity={0}>
                <Splash image={IMAGES.splash1} />
                <Splash image={IMAGES.splash2} />
                <Splash image={IMAGES.splash3} />
                <Splash image={IMAGES.splash4} />
              </Pages>
            </View>
          </SafeAreaView>
          <View style={styles.yellow_view}>
            <ImageBackground
              resizeMode={'stretch'}
              source={IMAGES.yellow_gradient}
              style={{flex: 1}}>
              <SafeAreaView style={{flex: 1}}>
                <View style={styles.indicator_view}>
                  <View
                    style={[
                      styles.dot_view,
                      {
                        backgroundColor:
                          index == 0 ? COLOR.DARK_BLUE : COLOR.WHITE,
                      },
                    ]}
                  />
                  <View
                    style={[
                      styles.dot_view,
                      {
                        backgroundColor:
                          index == 1 ? COLOR.DARK_BLUE : COLOR.WHITE,
                      },
                    ]}
                  />
                  <View
                    style={[
                      styles.dot_view,
                      {
                        backgroundColor:
                          index == 2 ? COLOR.DARK_BLUE : COLOR.WHITE,
                      },
                    ]}
                  />
                  <View
                    style={[
                      styles.dot_view,
                      {
                        backgroundColor:
                          index == 3 ? COLOR.DARK_BLUE : COLOR.WHITE,
                        marginRight: 0,
                      },
                    ]}
                  />
                </View>
                <Text
                  style={[
                    Styles.heading_label,
                    {alignSelf: 'center', marginTop: 10},
                  ]}>
                  {headingText[index]}
                </Text>
                <Text
                  style={[Styles.body_label, styles.desc_text, {fontSize: 14}]}>
                  {descText[index]}
                </Text>
                <View style={styles.button_view}>
                  <CustomButton
                    onPress={() => {
                      this.props.navigation.navigate('SignupScreen');
                    }}
                    bg={COLOR.DARK_BLUE}
                    labelColor={COLOR.WHITE}
                    text={Locale('Register your business')}
                  />
                </View>
                <View style={styles.prev_nxt}>
                  {index != 0 && (
                    <TouchableWithoutFeedback
                      onPress={() => this.changePage(1)}>
                      <Text
                        style={[
                          Styles.body_label,
                          {width: 60, textAlign: 'center'},
                        ]}>
                        {Locale('Previous')}
                      </Text>
                    </TouchableWithoutFeedback>
                  )}
                  <TouchableWithoutFeedback onPress={() => this.changePage(2)}>
                    <Text
                      style={[
                        Styles.body_label,
                        {width: 60, textAlign: 'center'},
                      ]}>
                      {Locale('Next')}
                    </Text>
                  </TouchableWithoutFeedback>
                </View>
              </SafeAreaView>
            </ImageBackground>
          </View>
        </LinearGradient>
      </View>
    );
  }
}

const Splash = props => {
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
      }}>
      <View style={[Styles.shadow_view, {marginVertical: 20}]}>
        <Image style={styles.image} source={props.image} contain={'contain'} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  image: {width: wp(80), height: hp(55), resizeMode: 'contain'},
  yellow_view: {
    width: wp(100),
    height: 260,
  },
  button_view: {
    marginVertical: 20,
    alignItems: 'center',
    justifyContent: 'center',
    width: wp(65),
    alignSelf: 'center',
  },
  prev_nxt: {
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
    height: 30,
    marginBottom: 0,
  },
  desc_text: {
    alignSelf: 'center',
    marginVertical: 0,
    textAlign: 'center',
    width: wp(85),
  },
  dot_view: {
    height: 8,
    width: 8,
    borderRadius: 4,
    marginRight: 5,
  },
  indicator_view: {
    marginTop: 20,
    height: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
