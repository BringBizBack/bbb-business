/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import 'react-native-gesture-handler';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import IMAGES from '../../styles/Images';
import {StyleSheet, View, TouchableOpacity, Image, SafeAreaView, Text} from 'react-native';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import COLOR from '../../styles/Color';
import Styles from '../../styles/Styles';
import CampaignDetail from '../campaign/CampaignDetail';
import PrizeDetailScreen from '../campaign/PrizeDetailScreen';
import DashboardScreen from '../dashboard/homeTab/DashboardScreen';

import NotificationScreen from '../dashboard/NotificationScreen';
import SuccessScreen from '../dashboard/homeTab/SuccessScreen';
import GiveCredits from '../dashboard/homeTab/credits/GiveCredit';
import GiveCustomerCredit from '../dashboard/homeTab/credits/GiveCustomerCredit';
import BusinessInformation from '../dashboard/homeTab/business/BusinessInformation';
import SpecialHours from '../dashboard/homeTab/SpecialHours';
import PaymentCards from '../dashboard/homeTab/cards/PaymentCards';
import AddCard from '../dashboard/homeTab/cards/AddCard';
import BillingHistory from '../dashboard/homeTab/cards/BillingHistory';
import CreateNewCampaign from '../campaign/CreateNewCampaign';
import AddCampaignDetail from '../campaign/AddCampaignDetail';
import MyPeople from '../dashboard/member/MyPeople';
import AddMember from '../dashboard/member/AddMember';

import LoginScreen from '../registration/LoginScreen';
import AboutScreen from '../registration/AboutScreen';
import ExploreCampaign from '../campaign/ExploreCampaign';
import SignupScreen from '../registration/SignupScreen';
import InitialLoadingScreen from '../splash/InitialLoadingScreen';
import SplashScreen from '../splash/SplashView';
import ForgotPasswordScreen from '../registration/ForgotPassswordScreen';
import VerifyEmail from '../registration/VerifyEmail';
import App from '../../../App';
import {setNavigationRef} from '../../commonView/Helpers';
import PrivacyPolicyScreen from '../registration/PrivacyPolicyScreen';
import TermsConditionScreen from '../registration/TermsConditionScreen';
import CreditHistoryDetails from '../dashboard/homeTab/credits/CreditHistoryDetails';
import LegalInformation from '../dashboard/homeTab/business/LegalInformation';
import LogoBusinessImages from '../dashboard/homeTab/business/LogoBusinessImages';
import PersonalDetails from '../dashboard/homeTab/business/PersonalDetails';
import TradeInformation from '../dashboard/homeTab/business/TradeInformation';
import BusinessDetail from '../campaign/BusinessDetail';
import LeaderboardScreen from '../campaign/LeaderboardScreen';

const Tab = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const CreditStack = createStackNavigator();
const CampaignStack = createStackNavigator();
const Stack = createStackNavigator();

const images = [
  IMAGES.home,
  IMAGES.plus_circle,
  IMAGES.campaign,
];

const titles = [
  'Home',
  'Give Points',
  'Campaigns',
];

export function MyTabBar({ state, descriptors, navigation }) {
  return (

    <View style={style.bottom_tab_container}>

      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
              ? options.title
              : route.name;
        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
            <SafeAreaView style={{flex:1,backgroundColor: COLOR.DARK_BLUE}}>
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}>
            <View style={{
              flexDirection:'row',alignItems:'center', justifyContent: 'space-between'}}>
            <View style={{marginVertical:5,flex:1}}>
              <Image
                style={{
                  width: 25,
                  height: 25,
                  alignItems: 'center',
                  alignSelf: 'center',
                  tintColor: isFocused ? COLOR.YELLOW : COLOR.WHITE,
                }}
                resizeMode={'cover'}
                source={images[index]}
              />
              <Text style={[Styles.small_label,{ marginTop:5,color: isFocused ? COLOR.YELLOW : COLOR.WHITE, textAlign:'center'}]}>{titles[index]}</Text>
            </View>
              {index != 2 && <View style={{width: 1.5, height: 30, backgroundColor: COLOR.WHITE}}/>}
            </View>
          </TouchableOpacity>
            </SafeAreaView>
        );
      })}
    </View>

        );
}

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: '#ffffff',
  },
};

export default function RootStack(props) {
  return (

    <NavigationContainer ref={ref=>{
      setNavigationRef(ref);
    }} theme={MyTheme}>
      <Stack.Navigator   initialRouteName={'InitialLoadingScreen'} headerMode="none"  screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        gestureEnabled:false,
      }}>
        <Stack.Screen name="InitialLoadingScreen"component={InitialLoadingScreen}/>
        <Stack.Screen name="SplashScreen"component={SplashScreen}/>
        <Stack.Screen name="LoginScreen"component={LoginScreen}/>
        <Stack.Screen name="ForgotPasswordScreen"component={ForgotPasswordScreen}/>
        <Stack.Screen name="VerifyEmail"component={VerifyEmail}/>
        <Stack.Screen name="AboutScreen"component={AboutScreen}/>
        <Stack.Screen name="SignupScreen"component={SignupScreen}/>
        <Stack.Screen name="ExploreCampaign"component={ExploreCampaign}/>
        <Stack.Screen name="DashboardScreen"component={DashboardScreen}/>


        <Stack.Screen name="SuccessScreen"component={SuccessScreen}/>
        <Stack.Screen name="PrizeDetailScreen"component={PrizeDetailScreen}/>

        <Stack.Screen name="NotificationScreen"component={NotificationScreen}/>
        <Stack.Screen  name="MyTabs"component={MyTabs}/>
        <Stack.Screen name="GiveCredits" component={GiveCredits}/>
        <Stack.Screen name="GiveCustomerCredit" component={GiveCustomerCredit}/>

        <Stack.Screen name="BusinessInformation" component={BusinessInformation}/>
        <Stack.Screen name="LegalInformation" component={LegalInformation}/>
        <Stack.Screen name="LogoBusinessImages" component={LogoBusinessImages}/>
        <Stack.Screen name="PersonalDetails" component={PersonalDetails}/>
        <Stack.Screen name="TradeInformation" component={TradeInformation}/>

        <Stack.Screen name="SpecialHours" component={SpecialHours} />
        <Stack.Screen name="PaymentCards" component={PaymentCards}/>
        <Stack.Screen name="AddCard" component={AddCard} />
        <Stack.Screen name="BillingHistory" component={BillingHistory} />
        <Stack.Screen name="CreateNewCampaign" component={CreateNewCampaign} />
        <Stack.Screen name="AddCampaignDetail" component={AddCampaignDetail} />
        <Stack.Screen name="MyPeople" component={MyPeople} />
        <Stack.Screen name="AddMember" component={AddMember} />
        <Stack.Screen name="PrivacyPolicyScreen" component={PrivacyPolicyScreen} />
        <Stack.Screen name="TermsConditionScreen" component={TermsConditionScreen} />
        <Stack.Screen name="CreditHistoryDetails" component={CreditHistoryDetails} />

          <Stack.Screen name="CampaignDetail"component={CampaignDetail}/>
          <Stack.Screen name="BusinessDetail"component={BusinessDetail}/>
          <Stack.Screen name="LeaderboardScreen"component={LeaderboardScreen}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

function MyTabs(props) {
  return (
      <Tab.Navigator
      
          style={{  alignItems: 'center' }}
          tabBar={props => <MyTabBar {...props} />}>
        <Tab.Screen key={1} name="Home" component={HomeScreen} initialParams={{tabScreen:true}} />
        <Tab.Screen key={2} name="Give Points" component={CreditScreen} initialParams={{tabScreen:true}} />
        <Tab.Screen key={3} name="Campaigns" component={CampaignScreen} initialParams={{tabScreen:true,type:1}} />
      </Tab.Navigator>
  );
}

const getInitialScreen = ()  => {

};


function HomeScreen() {
  return (
      <HomeStack.Navigator screenOptions={{ gestureEnabled: false }} initialRouteName={'DashboardScreen'} headerMode="none">
        <Stack.Screen name="DashboardScreen"component={DashboardScreen} initialParams={{tabScreen:true}}/>
        <Stack.Screen name="SuccessScreen" component={SuccessScreen} options={{animationEnabled: false}} initialParams={{tabScreen:true}}/>
        <Stack.Screen name="ExploreCampaign" component={ExploreCampaign}  initialParams={{tabScreen:true,type:4}}/>
      </HomeStack.Navigator>

  );
}

function CreditScreen() {
    return (
        <CreditStack.Navigator screenOptions={{ gestureEnabled: false }} initialRouteName={'GiveCustomerCredit'} headerMode="none">
            <Stack.Screen name="GiveCustomerCredit" component={GiveCustomerCredit} initialParams={{tabScreen:true,type:1}}/>
        </CreditStack.Navigator>

    );
}

function CampaignScreen() {
    return (
        <CampaignStack.Navigator screenOptions={{ gestureEnabled: false }} initialRouteName={'ExploreCampaign'} headerMode="none">
            <Stack.Screen name="ExploreCampaign" component={ExploreCampaign} initialParams={{tabScreen:true,type:2}}/>
            <Stack.Screen name="PrizeDetailScreen" component={PrizeDetailScreen} initialParams={{tabScreen:true}}/>
            <Stack.Screen name="CampaignDetail" component={CampaignDetail} initialParams={{tabScreen:true}}/>
            <Stack.Screen name="BusinessDetail" component={BusinessDetail} initialParams={{tabScreen:true}}/>
            <Stack.Screen name="LeaderboardScreen" component={LeaderboardScreen} initialParams={{tabScreen:true}}/>
        </CampaignStack.Navigator>

    );
}

const style = StyleSheet.create({
  bottom_tab_container: {
    flexDirection: 'row',
    alignSelf: 'center',
    width: widthPercentageToDP(100),
    margin: 0,
    justifyContent: 'center',
    backgroundColor: COLOR.DARK_BLUE,
    alignItems: 'center',
  },

});
