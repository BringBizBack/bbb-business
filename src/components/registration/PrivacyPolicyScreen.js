import React from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  Image,
  ScrollView,
  Text,
} from 'react-native';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import NavigationBar from '../../commonView/NavigationBar';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import FONTS from '../../styles/Fonts';
import LinearGradient from 'react-native-linear-gradient';
import {Locale} from '../../commonView/Helpers';

export default class PrivacyPolicyScreen extends React.Component {
  constructor(ptops) {
    super();
    this.state = {
      isTabbar: false,
    };
    this.updateMasterState = this.updateMasterState.bind(this);
  }

  componentDidMount(props) {
    if (this.props.route.params) {
      if (this.props.route.params.tabScreen != null) {
        this.setState({
          isTabbar: this.props.route.params.tabScreen,
        });
      }
    }
  }

  updateMasterState = (props, value) => {
    this.setState({
      [props]: value,
    });
  };

  render() {
    const {isTabbar} = this.state;
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <NavigationBar
              title={Locale('Privacy and Data Protection Policy')}
            />
            <ScrollView>
              <View style={[style.shadowView, Styles.shadow_view]}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingHorizontal: 5,
                  }}>
                  <Image
                    source={IMAGES.trophy}
                    style={{height: 35, width: 35}}
                    resizeMode={'cover'}
                  />
                  <View style={{flex: 1, marginHorizontal: 15}}>
                    <Text
                      style={[
                        Styles.button_font,
                        {fontWeight: '500', alignSelf: 'flex-start'},
                      ]}>
                      {Locale('Goalprize')}
                    </Text>
                  </View>
                </View>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      marginTop: 15,
                    },
                  ]}
                />
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  FastUsers Co., Ltd. ("Company") is the developer and owner of
                  Goalprize, which is one of the services of the Company (the
                  "Service"). When you establish an account on this Service, you
                  will be considered a User of the Service ("User"). The Company
                  needs to process personal data of users, therefore the Company
                  is announcing this Privacy Policy to protect the personal
                  information of users and clarify rights and duties including
                  conditions relating to the collection, use and disclosure of
                  personal information. The Company will operate the Service
                  this way as the User acknowledges.{'\n\n'}This Privacy Policy
                  only applies to the Company's services. It does not apply to
                  other applications and services or websites that may have a
                  connection which are a third party which the company does not
                  have the power to control and are a part of another service
                  for which the user has to make an agreement and read the
                  Privacy Policy for the use of such other applications,
                  services or websites separately.{'\n\n'}If the User does not
                  agree to the terms of this Privacy Policy or any revised
                  version the Company reserves the right to prohibit or not
                  allow such Users to use the Company's services. Because User
                  agreement with this Privacy Policy is required to provide the
                  Service, the User's continued use of the Service shall be
                  deemed to be proof of acceptance of this Policy.{'\n\n'}The
                  Company may update this Policy from time to time to comply
                  with guidelines, laws, regulations and in accordance with the
                  services of the Company. The Company will notify Users of the
                  updated policy by announcing the revised Policy to the Users,
                  provided that the Policy is deemed to be effective when the
                  Company has announced it.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                    },
                  ]}>
                  Characteristics of personal data processed by the Company in
                  providing the Company's services{'\n\n'}The Company Services
                  will collect and process personal data from the Users as
                  follows:
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  Directly identifiable information such as name, age,
                  nationality, date of birth. Contact information such as
                  address, contact location, telephone number, e-mail address.
                  Payment information, such as payment details, credit cards,
                  and bank accounts. Service usage information such as user
                  account name, password, transaction history that the User
                  performs, including the interests of the User.{'\n\n'}
                  Technical information for identification, such as computer
                  identifier numbers (IP Address), usage data settings and
                  browser connection of the device used by the user to utilize
                  the Company's services. The amount spent through the Service.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                    },
                  ]}>
                  Data processing purposes
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  The Company has a necessity to collect and use personal
                  information of the Users as stated in the Company's Terms Of
                  Service, including for verification purposes; and follow up on
                  the transactions of Users for verifying payment terms, service
                  fees, and for communication with service users, etc.{'\n'}The
                  Company has a necessity to collect and use the personal
                  information of the Users for interest analysis of Users to be
                  able to offer benefits or services according to User interest
                  or to build a better relationship between the Company and its
                  Users. The Company has a necessity to store and use the
                  personal information of the Users for the purpose of providing
                  other support services such as contacting for information,
                  feedback, comments about the Service or submitting various
                  requests. The Company has a necessity to store personal
                  information of the Users to comply with laws and government
                  regulations such as tax documentation, withholding or other
                  actions required by law.{'\n\n'}The Company will keep and
                  collect personal information of the Users throughout the time
                  period that the User is still using a service of the Company.
                  And the Company reserves the right to keep the information for
                  a period of 3 years after the User cancels the service for the
                  sake of protection of the rights of the Company, unless the
                  law requires the Company to keep personal information for
                  another time period, the Company may need to keep the
                  information for a period longer than 3 years.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  Disclosure of personal information
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  To provide the Service according to the specified conditions
                  it may be necessary for the Company to disclose personal
                  information of the User in the following cases:{'\n\n'}The
                  Company may need to disclose it to its external service
                  providers hired by the Company providing assistance and
                  support for the Company's services such as consultants,
                  service contractors, as well as third-party service providers
                  who provide evaluation of the Company's services, such as
                  Google Analytics. The Company will only disclose information
                  as necessary.{'\n'}The Company may disclose it by keeping your
                  personal information in the computer system used by the
                  service, including DigitalOcean.{'\n'}The Company may disclose
                  information for the necessity of protecting the rights of the
                  Company or for the prevention and investigation of wrongdoing
                  related to the use of the Company's services by Users in
                  various ways. The Company will do so only as necessary.{'\n'}
                  In case the Company is legally obligated or subject to
                  judgment or the order of a government agency the Company may
                  be required to disclose information to such agencies in order
                  to perform their duties under the law.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  Cookies that the Company uses to provide services
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  Cookies are text files located on the User's computer; a log
                  of data of Internet usage or behavior of Users in order to
                  guarantee efficiency in providing the Company's services to
                  users.{'\n'}The Company is required to use different types of
                  cookies for different purposes, as listed below.{'\n\n'}
                  Functionality Cookies: Used to remember what users chose or
                  set as platform settings such as user account name, language,
                  fonts, and platform style. And, to present information that
                  meets the needs of individual users according to their
                  selected settings. Advertising Cookies: These are used to
                  remember what Users have visited including the User's use of
                  the service to present products or services that are relevant
                  to the interests of the User and used to assess the
                  effectiveness of the use of the various functions of the
                  system.{'\n'}Strictly Necessary Cookies are cookies necessary
                  for the functioning of Company Service. They are necessary to
                  enable the use of the Service by the User thoroughly and
                  safely.{'\n\n'}Although the use of cookies is useful in
                  enhancing the performance of the Service and work for the
                  Service, if the User wants the User can delete cookie settings
                  on their own browser. However the User must acknowledge that
                  doing so may affect the ability and right to use the Company's
                  services according to the stated purpose of the cookie’s
                  function.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  Guarantees for the implementation of appropriate data security
                  measures
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  The Company ensures that appropriate security measures are in
                  place to prevent unauthorized access, use, editing, change or
                  disclosure of personal data or use of information. The Company
                  will keep User’s personal information private and secure. The
                  Company will arrange to review such measures periodically for
                  appropriateness according to the relevant laws.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  User’s Data Rights{'\n\n'}The Company acknowledges and respect
                  the legal rights of Users in relation to personal data of the
                  Users, which includes the rights as follows:
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  Right to request access and obtain a copy of personal data
                  including the right to request correction of such information
                  in order to keep it current and correct.{'\n'}Right to request
                  personal information in a form that can be read or used for
                  typical tasks with automated tools or devices, including the
                  right to request the transmission or transfer of such forms of
                  data to another data controller. Right to reject the
                  processing of personal data.{'\n'}Right to request removal or
                  destruction or make personal information non-personally
                  identifiable when the data becomes unnecessary or when the
                  subject of personal data withdraws consent.{'\n'}Right to
                  request the suspension of the use of personal data in the case
                  that this is personal data that needs to be deleted or when
                  such data is no longer necessary.{'\n'}Right to withdraw
                  consent of the processing of information previously provided
                  by the service user.{'\n\n'}The Users can contact the company
                  to request the exercise of the above rights according to the
                  contact details the company has set without any cost and the
                  Company will consider and notify the User the result of
                  consideration of rights within 30 days from the date we
                  receive the request.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  Contact:
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  Data Controller{'\n'}Name: Fast Users Company Limited
                  {'\n\n'}
                  Contact address: 252/20 Muang Thai-Phatra Complex Building A,
                  16th Floor, Ratchadaphisek Road, Huai Khwang Sub-district,
                  Huai Khwang District, Bangkok 10310{'\n\n'}Contact: 093 047
                  2330
                  {'\n'}Email: logan@fastusers.com{'\n'}Website:
                  www.fastusers.com
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  Data Protection Officer (DPO) details
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  Name: Mr. Douglas Logan Darrow Clements
                  {'\n\n'}Contact location: 1/391 Whizdom Connect Soi Piyabutr 1
                  Sukhumvit Road, Bang Chak Subdistrict, Phra Khanong District,
                  Bangkok 10260
                  {'\n\n'}
                  Contact: 093 047 2330
                  {'\n'}Email: logan@fastusers.com
                  {'\n\n'}THIS IS THE UNALTERED ORIGINAL THAI VERSION FROM WHICH
                  THE ABOVE ENGLISH VERSION HAS BEEN DERIVED.
                </Text>
              </View>
            </ScrollView>
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 20,
    paddingVertical: 30,
    paddingHorizontal: 15,
  },
});
