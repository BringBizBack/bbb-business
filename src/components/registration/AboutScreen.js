import React, {Component} from 'react';
import {
  View,
  ImageBackground,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  ScrollView,
  Text,
} from 'react-native';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import NavigationBar from '../../commonView/NavigationBar';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import FONTS from '../../styles/Fonts';
import TopView from '../../commonView/TopView';
import LinearGradient from 'react-native-linear-gradient';
import BottomView from '../../commonView/BottomView';

export default class AboutScreen extends React.Component {
  constructor(props) {
    super();
    this.state = {
      isTabbar: false,
    };
    this.updateMasterState = this.updateMasterState.bind(this);
  }

  componentDidMount(props) {
    if (this.props.route.params) {
      if (this.props.route.params.tabScreen != null) {
        this.setState({
          isTabbar: this.props.route.params.tabScreen,
        });
      }
    }
  }

  updateMasterState = (props, value) => {
    this.setState({
      [props]: value,
    });
  };

  render() {
    const {isTabbar} = this.state;
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <NavigationBar title={'GoalPrize: Customers Fast No Risk'} />
            <ScrollView>
              <View style={[style.shadowView, Styles.shadow_view]}>
                <Text style={[Styles.bold_body_label]}>What is GoalPrize?</Text>
                <Text
                  style={[
                    Styles.small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  Goalprize campaign is an attempt by a business to increase the
                  number of customers in a short period of time.
                </Text>
                <Text
                  style={[
                    Styles.small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 15},
                  ]}>
                  1. A business manager sets a goal of the number of qualified
                  customers they want to visitin 30days. They make this goal
                  public through Goalprize.com
                </Text>
                <Text
                  style={[
                    Styles.small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 15},
                  ]}>
                  2. If the goal is achieved before the 30day deadline the
                  business gives digital gift cards to the registered customers
                  who spent the most during the campaign. The top 30% get
                  digital gift cards.
                </Text>
                <Text
                  style={[
                    Styles.small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 15},
                  ]}>
                  3. A business manager sets a goal of the number of qualified
                  customers they want to visitin 30days. They make this goal
                  public through GoalPrize.com
                </Text>
              </View>
            </ScrollView>
            {!isTabbar && <BottomView />}
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 20,
    paddingVertical: 30,
    paddingHorizontal: 15,
  },
});
