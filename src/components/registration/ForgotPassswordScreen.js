import React from 'react';
import {View, Text, SafeAreaView, StyleSheet, ScrollView} from 'react-native';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import NavigationBar from '../../commonView/NavigationBar';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {FloatingTitleTextInputField} from '../../commonView/FloatingTitleTextInputField';
import {CustomButton} from '../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import {
  showAlertMessage,
  validateEmail,
  validatePassword,
  Locale,
} from '../../commonView/Helpers';
import API from '../../api/Api';

let api = new API();

export default class ForgotPasswordScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      email: '',
      codeSent: 1,
      newPassword: '',
      confirmPassword: '',
      otp: '',
      passwordValidate: false,
      emailValidate: false,
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.handleForgotPassword = this.handleForgotPassword.bind(this);
  }

  componentDidMount(props) {
    if (this.props.route.params) {
      if (this.props.route.params.email != null) {
        this.setState({
          email: this.props.route.params.email,
        });
      }
    }
  }

  updateMasterState = (attrName, value) => {
    this.setState({
      [attrName]: value,
      emailValidate: false,
      passwordValidate: false,
    });
  };

  handleForgotPassword(type) {
    let that = this;
    if (type == 2) {
      const {email, newPassword, confirmPassword, otp} = that.state;
      if (email.length == 0) {
        this.setState({emailValidate: true});
        showAlertMessage(Locale('Enter email address'), 2);
      } else if (validateEmail(email) == false) {
        this.setState({emailValidate: true});
        showAlertMessage(Locale('Enter valid email address'), 2);
      } else if (newPassword.length <= 7) {
        this.setState({passwordValidate: true});
      } else if (validatePassword(newPassword) == false) {
        showAlertMessage(
          Locale(
            'Please enter password with 8 characters including a minimum of 1 letter and 1 number',
          ),
          2,
        );
      } else if (confirmPassword.length == 0) {
        showAlertMessage(Locale('Enter confirm password'), 2);
      } else if (confirmPassword !== newPassword) {
        showAlertMessage(Locale('Password not matched'), 2);
      } else {
        let that = this;
        const {email} = that.state;
        let data = JSON.stringify({
          email: email,
          otp: otp,
          password: newPassword,
        });
        that.setState({isLoading: true});
        api
          .resetPassword(data)
          .then(json => {
            console.log('response is:-', json);
            that.setState({isLoading: false});
            let stringData = JSON.stringify(json.data.response);
            if (json.status == 200) {
              showAlertMessage(Locale('Successfully updated password'));
              this.props.navigation.navigate('LoginScreen');
            } else if (json.status == 400) {
              showAlertMessage(json.data.message, 2);
            } else {
              showAlertMessage(json.data.message, 2);
            }
          })
          .catch(error => {
            that.setState({isLoading: false});
            showAlertMessage(error.response.data.message, 2);
          });
      }
    } else {
      const {email} = that.state;
      if (email.length == 0) {
        showAlertMessage(Locale('Enter email address'), 2);
      } else if (validateEmail(email) == false) {
        showAlertMessage(Locale('Enter valid email address'), 2);
      } else {
        let that = this;
        const {email} = that.state;
        let data = JSON.stringify({
          email: email,
        });
        that.setState({isLoading: true});
        api
          .forgotPassword(data)
          .then(json => {
            console.log('response is:-', json);
            that.setState({isLoading: false});
            let stringData = JSON.stringify(json.data.response);
            if (json.status == 200) {
              that.setState({codeSent: 2});
              showAlertMessage(
                Locale('An OTP is sent to the above email address'),
              );
            } else if (json.status == 400) {
              showAlertMessage(json.data.message, 2);
            } else {
              showAlertMessage(json.data.message, 2);
            }
          })
          .catch(error => {
            that.setState({isLoading: false});
            showAlertMessage(error.response.data.message, 2);
          });
      }
    }
  }

  render() {
    let {codeSent} = this.state;

    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <NavigationBar />
            <ScrollView>
              <View style={[style.shadowView, Styles.shadow_view]}>
                <Text
                  style={[
                    Styles.heading_label,
                    {
                      color: COLOR.WHITE,
                      alignSelf: 'flex-start',
                      marginBottom: 20,
                    },
                  ]}>
                  {codeSent == 2
                    ? Locale('Reset password sent to')
                    : Locale('Forgot Password')}
                </Text>

                {codeSent == 2 ? (
                  <View>
                    <Text
                      style={[
                        Styles.bold_body_label,
                        {
                          color: COLOR.WHITE,
                          alignSelf: 'flex-start',
                          marginBottom: 20,
                        },
                      ]}>
                      {this.state.email}
                    </Text>
                    <FloatingTitleTextInputField
                      attrName={'otp'}
                      value={this.state.otp}
                      keyboardType={'number-pad'}
                      updateMasterState={this.updateMasterState}
                      title={Locale('Enter otp')}
                    />
                    <FloatingTitleTextInputField
                      attrName={'newPassword'}
                      value={this.state.newPassword}
                      handleFieldEmpty={this.state.passwordValidate}
                      isPasswordVisible={true}
                      updateMasterState={this.updateMasterState}
                      title={Locale(
                        'Enter 8 digit password with letters and numbers',
                      )}
                      image={IMAGES.visibility_hidden}
                    />
                    <FloatingTitleTextInputField
                      attrName={'confirmPassword'}
                      value={this.state.confirmPassword}
                      handleFieldEmpty={this.state.passwordValidate}
                      isPasswordVisible={true}
                      updateMasterState={this.updateMasterState}
                      title={Locale('Enter confirm password')}
                      image={IMAGES.visibility_hidden}
                    />
                  </View>
                ) : (
                  <FloatingTitleTextInputField
                    attrName={'email'}
                    handleFieldEmpty={this.state.emailValidate}
                    value={this.state.email}
                    keyboardType={'email-address'}
                    updateMasterState={this.updateMasterState}
                    title={Locale('Email Address')}
                  />
                )}
                {codeSent == 2 && (
                  <CustomButton
                    onPress={() => this.handleForgotPassword(2)}
                    text={Locale('Reset password')}
                  />
                )}
                <CustomButton
                  bg={codeSent == 2 ? 'transparent' : null}
                  onPress={() => this.handleForgotPassword(1)}
                  text={
                    codeSent == 2
                      ? Locale('Resend')
                      : Locale('Send OTP to email')
                  }
                />
              </View>
            </ScrollView>
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 10,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 20,
    paddingVertical: 20,
    paddingHorizontal: 15,
  },
});
