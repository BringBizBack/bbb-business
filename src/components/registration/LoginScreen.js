import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  ScrollView,
  Platform,
} from 'react-native';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {FloatingTitleTextInputField} from '../../commonView/FloatingTitleTextInputField';
import {CustomButton} from '../../commonView/CustomButton';

import LinearGradient from 'react-native-linear-gradient';
import {
  handleDispatch,
  Locale,
  redirectToLink,
  showAlertMessage,
  validateEmail,
} from '../../commonView/Helpers';
import {
  AS_INITIAL_ROUTE,
  AS_USER_TOKEN,
  AS_FCM_TOKEN,
  U_APP_STORE_LINK,
  U_PLAY_STORE_LINK,
  AS_USER_EMAIL,
  AS_USER_PASSWORD,
} from '../../commonView/Constants';
import ActivityIndicator from '../../commonView/ActivityIndicator';
import API from '../../api/Api';
import Auth from '../../auth';
import AuthenticationAction from '../../redux/action/AuthenticationAction';
import {connect} from 'react-redux';
import AlertPopup from '../../commonView/AlertPopup';

let api = new API();
let auth = new Auth();

class LoginScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      isLoading: false,
      rememberMeSelected: true,
      emailValidate: false,
      passwordValidate: false,
      alertModalVisible: false,
      title: '',
      description: '',
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.handleRedirection = this.handleRedirection.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.closeVerifyModal = this.closeVerifyModal.bind(this);
    this.handleEmailPassword = this.handleEmailPassword.bind(this);
  }

  componentDidMount(props) {
    if (this.props.route.params) {
      if (this.props.route.params.email != null) {
        this.setState({
          email: this.props.route.params.email,
        });
      }
    }
    this.handleEmailPassword();
  }

  handleEmailPassword = async () => {
    let email = await auth.getValue(AS_USER_EMAIL);
    if (email) {
      this.setState({
        email: email,
      });
    }
    let pass = await auth.getValue(AS_USER_PASSWORD);
    if (pass) {
      this.setState({
        password: pass,
      });
    }
  };

  updateMasterState = (attrName, value) => {
    this.setState({
      [attrName]: value,
      emailValidate: false,
      passwordValidate: false,
    });
  };

  closeVerifyModal = val => {
    this.setState({alertModalVisible: false});
  };

  handleRedirection = id => {
    switch (id) {
      case 2:
        this.props.navigation.navigate('AboutScreen');
        break;
      case 3:
        this.props.navigation.navigate('ExploreCampaign', {tabScreen: true});
        break;
      default:
        break;
    }
  };

  handleLogin = async () => {
    let that = this;
    const {email, password} = that.state;
    if (email.length == 0) {
      this.setState({emailValidate: true});
    } else if (validateEmail(email) == false) {
      this.setState({emailValidate: true});
    } else if (password.length == 0) {
      this.setState({passwordValidate: true});
    } else {
      let that = this;
      const {email, password} = that.state;
      const token = await auth.getValue(AS_FCM_TOKEN);
      let data = JSON.stringify({
        email: email,
        password: password,
        token: token,
        platform: Platform.OS == 'ios' ? 2 : 1,
      });

      that.setState({isLoading: true});
      api
        .loginAPI(data)
        .then(json => {
          console.log('login response is:-', json);
          that.setState({isLoading: false});
          let stringData = JSON.stringify(json.data.response);
          if (json.status == 200) {
            showAlertMessage(Locale('Successfully logged in'), 1);
            auth.setValue(AS_USER_TOKEN, json.data.response.token);
            that.props.saveUserDetail(stringData);
            if (this.state.rememberMeSelected) {
              auth.setValue(AS_USER_EMAIL, email);
              auth.setValue(AS_USER_PASSWORD, password);
            } else {
              auth.remove(AS_USER_EMAIL);
              auth.remove(AS_USER_PASSWORD);
            }
            auth.setValue(AS_INITIAL_ROUTE, 'MyTabs');
            this.props.navigation.navigate('MyTabs');
            handleDispatch(this.props);
          } else if (json.status == 400) {
            showAlertMessage(json.data.message, 2);
          } else {
            showAlertMessage(json.data.message, 2);
          }
        })
        .catch(error => {
          that.setState({isLoading: false});
          showAlertMessage(error.response.data.message, 2);
        });
    }
  };

  commonView = props => {
    return (
      <View>
        <TouchableWithoutFeedback
          onPress={() => {
            this.handleRedirection(props.id);
          }}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              paddingHorizontal: 5,
            }}>
            <Image
              source={props.image}
              style={{height: 40, width: 40}}
              resizeMode={'contain'}
            />
            <View style={{flex: 1, marginHorizontal: 15}}>
              <Text style={[Styles.body_label, {color: COLOR.WHITE}]}>
                {props.description}
              </Text>
            </View>
            <Image
              source={IMAGES.keyboard_arrow_right}
              style={{height: 12, width: 12, tintColor: COLOR.WHITE}}
              resizeMode={'contain'}
            />
          </View>
        </TouchableWithoutFeedback>
        {props.showLine && (
          <View
            style={[
              Styles.line_view,
              {
                borderBottomWidth: 0.4,
                marginVertical: 15,
                borderBottomColor: COLOR.DARK_BLUE,
              },
            ]}
          />
        )}
      </View>
    );
  };

  render() {
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <AlertPopup
            image={IMAGES.security}
            title={this.state.title}
            buttonText={Locale('ok')}
            desc={this.state.description}
            closeVerifyModal={val => this.closeVerifyModal(val)}
            modalVisible={this.state.alertModalVisible}
          />
          <ActivityIndicator loading={this.state.isLoading} />
          <SafeAreaView style={Styles.container}>
            <ScrollView>
              <View style={style.topbar}>
                <Image
                  style={{height: 35, marginLeft: 20, width: 120}}
                  resizeMode={'contain'}
                  source={IMAGES.logo}
                />
                <View style={[Styles.yellow_view]}>
                  <Text style={Styles.extra_small_label}>
                    {Locale('Business owner version')}
                  </Text>
                </View>
              </View>
              <TouchableWithoutFeedback
                onPress={() => {
                  redirectToLink(
                    Platform.OS === 'ios'
                      ? U_APP_STORE_LINK
                      : U_PLAY_STORE_LINK,
                  );
                }}>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {
                      color: COLOR.WHITE,
                      alignSelf: 'flex-end',
                      marginTop: 10,
                      marginRight: 20,
                      textDecorationLine: 'underline',
                    },
                  ]}>
                  {Locale('Seeking Customer version?')}
                </Text>
              </TouchableWithoutFeedback>

              <View style={[style.shadowView, Styles.shadow_view]}>
                <Text
                  style={[
                    Styles.heading_label,
                    {
                      color: COLOR.WHITE,
                      alignSelf: 'flex-start',
                      marginBottom: 20,
                    },
                  ]}>
                  {Locale('Login')}
                </Text>

                <FloatingTitleTextInputField
                  attrName={'email'}
                  handleFieldEmpty={this.state.emailValidate}
                  value={this.state.email}
                  floatKeyboardType={'email-address'}
                  updateMasterState={this.updateMasterState}
                  title={Locale('Email Address')}
                />
                <FloatingTitleTextInputField
                  attrName={'password'}
                  handleFieldEmpty={this.state.passwordValidate}
                  value={this.state.password}
                  isPasswordVisible={true}
                  updateMasterState={this.updateMasterState}
                  title={Locale('Password')}
                  image={IMAGES.visibility_hidden}
                />

                <View
                  style={{
                    flexDirection: 'row',
                    height: 30,
                    alignItems: 'center',
                    marginBottom: 30,
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <TouchableWithoutFeedback
                      onPress={() => {
                        this.setState({
                          rememberMeSelected: !this.state.rememberMeSelected,
                        });
                      }}>
                      <Image
                        source={
                          this.state.rememberMeSelected
                            ? IMAGES.react_selected
                            : IMAGES.rectangle7
                        }
                        resizeMode={'contain'}
                        style={{
                          height: 15,
                          width: 15,
                          marginRight: 10,
                          tintColor: COLOR.YELLOW,
                        }}
                      />
                    </TouchableWithoutFeedback>
                    <Text style={[Styles.small_label, {color: COLOR.WHITE}]}>
                      {Locale('Remember me')}
                    </Text>
                    <TouchableWithoutFeedback
                      style={{
                        height: 45,
                        width: 45,
                      }}
                      onPress={() => {
                        this.setState({
                          alertModalVisible: true,
                          title: Locale('Remember me'),
                          description: Locale('K_REMEMBER_ME'),
                        });
                      }}>
                      <Image
                        source={IMAGES.info}
                        resizeMode={'contain'}
                        style={{
                          height: 15,
                          width: 15,
                          marginLeft: 5,
                        }}
                      />
                    </TouchableWithoutFeedback>
                  </View>

                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.props.navigation.navigate('ForgotPasswordScreen', {
                        email: this.state.email,
                      })
                    }>
                    <Text
                      style={[
                        Styles.bold_body_label,
                        {
                          color: COLOR.WHITE,
                          alignSelf: 'center',
                          textDecorationLine: 'underline',
                        },
                      ]}>
                      {Locale('Forgot password?')}
                    </Text>
                  </TouchableWithoutFeedback>
                </View>
                <CustomButton
                  onPress={() => this.handleLogin()}
                  text={Locale('Login')}
                />
                <View style={{marginVertical: 20}}>
                  <CustomButton
                    bg={COLOR.WHITE}
                    onPress={() =>
                      this.props.navigation.navigate('SignupScreen', {
                        email: this.state.email,
                      })
                    }
                    text={Locale('Create account')}
                  />
                </View>
              </View>
              <View
                style={[
                  style.shadowView,
                  Styles.shadow_view,
                  {
                    borderRadius: 5,
                    backgroundColor: COLOR.MEDIUM_BLUE,
                    marginVertical: 0,
                  },
                ]}>
                <this.commonView
                  id={2}
                  showLine={false}
                  image={IMAGES.trophy}
                  description={Locale('GoalPrize: Customer fast no risk')}
                />
                {/*<this.commonView*/}
                {/*  id={3}*/}
                {/*  showLine={false}*/}
                {/*  image={IMAGES.shop_home}*/}
                {/*  description={Locale('GoalPrizes')}*/}
                {/*/>*/}
              </View>
            </ScrollView>
            <Text
              style={[
                Styles.extra_small_label,
                {color: COLOR.WHITE, alignSelf: 'center', marginVertical: 0},
              ]}>
              © 2021 FastUsers Co. Ltd.
            </Text>
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 10,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 20,
    paddingVertical: 20,
    paddingHorizontal: 15,
  },
  topbar: {
    height: 45,
    width: wp(100),
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

const mapDispatchToProps = dispatch => {
  return {
    saveUserDetail: data => {
      dispatch(AuthenticationAction.saveUserDetail(data));
    },
  };
};

export default connect(null, mapDispatchToProps)(LoginScreen);
