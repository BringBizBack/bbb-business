import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Image,
  TouchableWithoutFeedback,
  ScrollView,
  StyleSheet,
  Platform,
} from 'react-native';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {FloatingTitleTextInputField} from '../../commonView/FloatingTitleTextInputField';
import {CustomButton} from '../../commonView/CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import {
  validateEmail,
  validatePassword,
  showAlertMessage,
  handleDispatch,
  navigate,
  redirectToLink,
} from '../../commonView/Helpers';
import API from '../../api/Api';
import ActivityIndicator from '../../commonView/ActivityIndicator';
import Auth from '../../auth';
import {
  AS_FCM_TOKEN,
  AS_INITIAL_ROUTE,
  AS_USER_EMAIL,
  AS_USER_PASSWORD,
  U_CUSTOMER_APP_STORE_LINK,
  U_CUSTOMER_PLAY_STORE_LINK,
} from '../../commonView/Constants';
import {Locale} from '../../commonView/Helpers';
import AlertPopup from '../../commonView/AlertPopup';

let api = new API();
let auth = new Auth();

export default class SignupScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      rememberMeSelected: true,
      termsConditionSelected: false,
      isLoading: false,
      emailValidate: false,
      passwordValidate: false,
      alertModalVisible: false,
      title: '',
      description: '',
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.handleSignup = this.handleSignup.bind(this);
    this.closeVerifyModal = this.closeVerifyModal.bind(this);
    this.handleRedirection = this.handleRedirection.bind(this);
  }

  componentDidMount(props) {
    if (this.props.route.params) {
      if (this.props.route.params.email != null) {
        this.setState({
          email: this.props.route.params.email,
        });
      }
    }
  }

  updateMasterState = (attrName, value) => {
    this.setState({
      [attrName]: value,
      emailValidate: false,
      passwordValidate: false,
      alertModalVisible: false,
    });
  };

  handleSignup = async () => {
    let that = this;
    const {email, password} = that.state;
    if (email.length == 0) {
      this.setState({emailValidate: true});
      //  showAlertMessage('Enter email address', 2);
    } else if (validateEmail(email) == false) {
      this.setState({emailValidate: true});
      //showAlertMessage('Enter valid email address', 2);
      this.setState({emailValidate: true});
    }  else if (validatePassword(password) == false) {
      showAlertMessage(
        Locale(
          'Please enter password with 8 characters including a minimum of 1 letter and 1 number',
        ),
        2,
      );
    } else if (this.state.termsConditionSelected == false) {
      showAlertMessage(Locale('Please agree with our terms of service'), 2);
    } else {
      let that = this;
      const {email, password} = that.state;
      const token = await auth.getValue(AS_FCM_TOKEN);
      let data = JSON.stringify({
        email: email,
        password: password,
        token: token,
        platform: Platform.OS == 'ios' ? 2 : 1,
      });

      that.setState({isLoading: true});
      api
        .signupAPI(data)
        .then(json => {
          console.log('signup response:-', json);
          that.setState({isLoading: false});
          if (json.status == 200) {
            this.props.navigation.navigate('VerifyEmail', {
              email: this.state.email,
            });
            if (this.state.rememberMeSelected) {
              auth.setValue(AS_USER_EMAIL, email);
              auth.setValue(AS_USER_PASSWORD, password);
            } else {
              auth.remove(AS_USER_EMAIL);
              auth.remove(AS_USER_PASSWORD);
            }
            auth.setValue(AS_INITIAL_ROUTE, 'MyTabs');
          } else if (json.status == 400) {
            showAlertMessage(json.data.message, 2);
          } else {
            showAlertMessage(json.data.message, 2);
          }
        })
        .catch(error => {
          that.setState({isLoading: false});
          showAlertMessage(error.response.data.message, 2);
        });
    }
  };

  closeVerifyModal = val => {
    this.setState({alertModalVisible: false});
  };

  handleRedirection = id => {
    switch (id) {
      case 2:
        this.props.navigation.navigate('AboutScreen');
        break;
      case 3:
        this.props.navigation.navigate('ExploreCampaign', {tabScreen: true});
        break;
      default:
        break;
    }
  };

  commonView = props => {
    return (
      <View>
        <TouchableWithoutFeedback
          onPress={() => {
            this.handleRedirection(props.id);
          }}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              paddingHorizontal: 5,
            }}>
            <Image
              source={props.image}
              style={{height: 40, width: 40}}
              resizeMode={'contain'}
            />
            <View style={{flex: 1, marginHorizontal: 15}}>
              <Text style={[Styles.body_label, {color: COLOR.WHITE}]}>
                {props.description}
              </Text>
            </View>
            <Image
              source={IMAGES.keyboard_arrow_right}
              style={{height: 12, width: 12, tintColor: COLOR.WHITE}}
              resizeMode={'contain'}
            />
          </View>
        </TouchableWithoutFeedback>
        {props.showLine && (
          <View
            style={[
              Styles.line_view,
              {
                borderBottomWidth: 0.4,
                marginVertical: 15,
                borderBottomColor: COLOR.DARK_BLUE,
              },
            ]}
          />
        )}
      </View>
    );
  };

  render() {
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <ActivityIndicator loading={this.state.isLoading} />
          <AlertPopup
            image={IMAGES.security}
            title={this.state.title}
            buttonText={'ok'}
            desc={this.state.description}
            closeVerifyModal={val => this.closeVerifyModal(val)}
            modalVisible={this.state.alertModalVisible}
          />
          <SafeAreaView style={Styles.container}>
            <View style={style.topbar}>
              <Image
                style={{height: 35, marginLeft: 20, width: 120}}
                resizeMode={'contain'}
                source={IMAGES.logo}
              />
              <View style={[Styles.yellow_view]}>
                <Text style={Styles.extra_small_label}>
                  {Locale('Business owner version')}
                </Text>
              </View>
            </View>
            <TouchableWithoutFeedback
              onPress={() => {
                redirectToLink(
                  Platform.OS === 'ios'
                    ? U_CUSTOMER_APP_STORE_LINK
                    : U_CUSTOMER_PLAY_STORE_LINK,
                );
              }}>
              <Text
                style={[
                  Styles.extra_small_label,
                  {
                    color: COLOR.WHITE,
                    alignSelf: 'flex-end',
                    marginTop: 10,
                    marginRight: 20,
                    textDecorationLine: 'underline',
                  },
                ]}>
                {Locale('Seeking Customer version?')}
              </Text>
            </TouchableWithoutFeedback>

            <ScrollView style={Styles.container}>
              <View
                style={[
                  style.shadowView,
                  Styles.shadow_view,
                  {paddingBottom: 20},
                ]}>
                <Text
                  style={[
                    Styles.heading_label,
                    {
                      color: COLOR.WHITE,
                      alignSelf: 'flex-start',
                      marginBottom: 20,
                      marginTop: 10,
                    },
                  ]}>
                  {Locale('Create account')}
                </Text>
                <FloatingTitleTextInputField
                  attrName={'email'}
                  handleFieldEmpty={this.state.emailValidate}
                  value={this.state.email}
                  floatKeyboardType={'email-address'}
                  updateMasterState={this.updateMasterState}
                  title={Locale('Email Address')}
                />
                <FloatingTitleTextInputField
                  attrName={'password'}
                  value={this.state.password}
                  handleFieldEmpty={this.state.passwordValidate}
                  isPasswordVisible={true}
                  updateMasterState={this.updateMasterState}
                  title={Locale(
                    'Enter 8 digit password with letters and numbers',
                  )}
                  image={IMAGES.visibility_hidden}
                />

                <View
                  style={{
                    flexDirection: 'row',
                    height: 30,
                    alignItems: 'center',
                    marginBottom: 5,
                  }}>
                  <TouchableWithoutFeedback
                    onPress={() => {
                      this.setState({
                        rememberMeSelected: !this.state.rememberMeSelected,
                      });
                    }}>
                    <Image
                      source={
                        this.state.rememberMeSelected
                          ? IMAGES.react_selected
                          : IMAGES.rectangle7
                      }
                      resizeMode={'contain'}
                      style={{
                        height: 15,
                        width: 15,
                        marginRight: 10,
                      }}
                    />
                  </TouchableWithoutFeedback>
                  <Text style={[Styles.small_label, {color: COLOR.WHITE}]}>
                    {Locale('Remember me')}
                  </Text>
                  <TouchableWithoutFeedback
                    style={{
                      height: 45,
                      width: 45,
                    }}
                    onPress={() => {
                      this.setState({
                        alertModalVisible: true,
                        title: Locale('Remember me'),
                        description: Locale('K_REMEMBER_ME'),
                      });
                    }}>
                    <Image
                      source={IMAGES.info}
                      resizeMode={'contain'}
                      style={{
                        height: 15,
                        width: 15,
                        marginLeft: 5,
                      }}
                    />
                  </TouchableWithoutFeedback>
                </View>

                <TouchableWithoutFeedback
                  onPress={() => {
                    this.setState({
                      termsConditionSelected:
                        !this.state.termsConditionSelected,
                    });
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginBottom: 20,
                    }}>
                    <Image
                      source={
                        this.state.termsConditionSelected
                          ? IMAGES.react_selected
                          : IMAGES.rectangle7
                      }
                      resizeMode={'contain'}
                      style={{
                        height: 15,
                        width: 15,
                        marginRight: 10,
                      }}
                    />
                    <Text style={[Styles.small_label, {color: COLOR.WHITE}]}>
                      {Locale('I have read and agreed to the ')}
                      {'\n'}
                      <TouchableWithoutFeedback
                        onPress={() => {
                          navigate('TermsConditionScreen');
                        }}>
                        <Text
                          style={{
                            textDecorationLine: 'underline',
                          }}>
                          terms and conditions.
                        </Text>
                      </TouchableWithoutFeedback>
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
                <CustomButton
                  onPress={() => this.handleSignup()}
                  text={Locale('Create Account')}
                />
                <TouchableWithoutFeedback
                  onPress={() =>
                    this.props.navigation.navigate('LoginScreen', {
                      email: this.state.email,
                    })
                  }>
                  <Text
                    style={[
                      Styles.body_label,
                      {color: COLOR.WHITE, marginTop: 20, alignSelf: 'center'},
                    ]}>
                    {Locale('Already registered?')}{' '}
                    <Text
                      style={[
                        Styles.bold_body_label,
                        {textDecorationLine: 'underline', color: COLOR.WHITE},
                      ]}>
                      {Locale('Login')}
                    </Text>
                  </Text>
                </TouchableWithoutFeedback>
              </View>
              <View
                style={[
                  style.shadowView,
                  Styles.shadow_view,
                  {
                    borderRadius: 5,
                    backgroundColor: COLOR.MEDIUM_BLUE,
                    marginVertical: 0,
                  },
                ]}>
                <this.commonView
                  id={2}
                  showLine={false}
                  image={IMAGES.trophy}
                  description={Locale('GoalPrize: Customer fast no risk')}
                />
                {/*<this.commonView*/}
                {/*  id={3}*/}
                {/*  showLine={false}*/}
                {/*  image={IMAGES.shop_home}*/}
                {/*  description={Locale('GoalPrizes')}*/}
                {/*/>*/}
              </View>
            </ScrollView>
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 10,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 20,
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  topbar: {
    height: 45,
    width: wp(100),
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
