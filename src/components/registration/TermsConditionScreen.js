import React from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  Image,
  ScrollView,
  Text,
} from 'react-native';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import NavigationBar from '../../commonView/NavigationBar';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import FONTS from '../../styles/Fonts';
import LinearGradient from 'react-native-linear-gradient';

export default class TermsConditionScreen extends React.Component {
  constructor(ptops) {
    super();
    this.state = {
      isTabbar: false,
    };
    this.updateMasterState = this.updateMasterState.bind(this);
  }

  componentDidMount(props) {
    if (this.props.route.params) {
      if (this.props.route.params.tabScreen != null) {
        this.setState({
          isTabbar: this.props.route.params.tabScreen,
        });
      }
    }
  }

  updateMasterState = (props, value) => {
    this.setState({
      [props]: value,
    });
  };

  render() {
    const {isTabbar} = this.state;
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <SafeAreaView style={Styles.container}>
            <NavigationBar title={'TERMS AND CONDITIONS AGREEMENT'} />
            <ScrollView>
              <View style={[style.shadowView, Styles.shadow_view]}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingHorizontal: 5,
                  }}>
                  <Image
                    source={IMAGES.trophy}
                    style={{height: 35, width: 35}}
                    resizeMode={'cover'}
                  />
                  <View style={{flex: 1, marginHorizontal: 15}}>
                    <Text
                      style={[
                        Styles.button_font,
                        {fontWeight: '500', alignSelf: 'flex-start'},
                      ]}>
                      In effect from August 31, 2021 until updated.
                    </Text>
                  </View>
                </View>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      marginTop: 15,
                    },
                  ]}
                />
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  These terms and conditions ("Agreement") set forth the general
                  terms and conditions of your use of the Goalprize.com website
                  ("Website"), "Goalprize" mobile applications ("Mobile
                  Application") and any of their related products and services
                  (collectively, "Services") and the rules pertaining to the
                  Goalprize campaign hosting by businesses and participation by
                  customers.{'\n\n'}This Agreement is legally binding between
                  you ("User", "you" or "your") and FastUsers Company Limited
                  ("FastUsers Company Limited", "we", "us" or "our"). If you are
                  entering into this Agreement on behalf of a business or other
                  legal entity, you represent that you have the authority to
                  bind such entity to this Agreement, in which case the terms
                  "User", "you" or "your" shall refer to such entity.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                    },
                  ]}>
                  Definitions
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  "A ""Goalprize"" or ""Goalprize campaign"" is an event where a
                  business uses seek to gain a certain number of customer
                  visits, called a ""Goal"" to make purchases above a minimum
                  value after the ""Start"" of the Goalprize and before the
                  ""End"" or ""End Date"" or ""Deadline"" and uses our service
                  to keep track of the various numbers involved with this
                  Goalprize.{'\n\n'}""Visits"" are customer visits at a business
                  hosting a Goalprize where the amount spent is greater than the
                  minimum spending requirement per customer to count as a visit.
                  For example, if the minimum spending requirement is 100 THB
                  per person per visit and 999 THB was spent on a bill by a
                  group of 10 people then it would count as only 9 visits. Each
                  time a customer pays a bill at a restaurant during a Goalprize
                  greater than the minimum spending requirement then it is
                  counted as a visit.{'\n\n'}""Points"" are what are given by a
                  business hosting a Goalprize to a participating customer for
                  qualifying purchases that customer made during the Goalprize.
                  {'\n\n'}A ""Request for Points"" is a procedure in the service
                  where the participating customer requests points for
                  qualifying purchases during a Goalprize from the business
                  after paying their bill.{'\n\n'}A ""Granting of Points"" is a
                  procedure in the service where a representative of the
                  Business hosting the Goalprize grants points for qualifying
                  purchases after a bill is paid and after receiving a Request
                  for Points from a participating customer.{'\n\n'}If a
                  Goalprize achieves its Goal before the End then it is called
                  ""Successful"" and if it does not is it called ""Failed"".
                  {'\n\n'}If a Goalprize is successful the Business hosting the
                  service grants ""Prizes"" to ""Winners"".{'\n\n'}""Winners""
                  are the participating customers who where ranked the highest,
                  the minimum ranking level for being a Winner having been set
                  before the Goalprize started and explicitly stated in the
                  Rules of the Goalprize. The more points a participating
                  customer receives during a Goalprize the higher their ranking.
                  Points only exist during an active Goalprize after the Start
                  and before the End. Points are not kept past the End of a
                  Goalprize.{'\n'}A ""Prize"" is a digital gift card with
                  credits the Winner can use at that business.{'\n\n'}
                  ""Credits"" are the units on a digital gift card that can be
                  used at that business before the expiration date of that
                  digital gift card. ""Redeeming Credits"" is a procedure
                  whereby a customer with a digital gift card requests that
                  their bill be reduced by the number of credits from that
                  business they want to redeem.{'\n\n'}""Expiration Date"" is
                  the last date at which the credits can be used until the
                  normal closing time of the business on that day. "{'\n\n'}By
                  accessing and using the Services, you acknowledge that you
                  have read, understood, and agree to be bound by the terms of
                  this Agreement.{'\n\n'}If you are entering into this Agreement
                  on behalf of a business or other legal entity, you represent
                  that you have the authority to bind such entity to this
                  Agreement, in which case the terms "User", "you" or "your"
                  shall refer to such entity. If you do not have such authority,
                  or if you do not agree with the terms of this Agreement, you
                  must not accept this Agreement and may not access and use the
                  Services.{'\n\n'}You acknowledge that this Agreement is a
                  contract between you and FastUsers Company Limited, even
                  though it is electronic and is not physically signed by you,
                  and it governs your use of the Services.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                    },
                  ]}>
                  A. Accounts and Membership
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  1 You must be at least 18 years of age to use the Services. By
                  using the Services and by agreeing to this Agreement you
                  warrant and represent that you are at least 18 years of age
                  and able to form legally binding contracts.{'\n\n'}2 If you
                  create an account on the Services, you are responsible for
                  maintaining the security of your account and you are fully
                  responsible for all activities that occur under the account
                  and any other actions taken in connection with it. We may, but
                  have no obligation to, monitor and review new accounts before
                  you may sign in and start using the Services. It is a
                  condition of your use of the Services that all the information
                  you provide on the Services is correct, current, and complete.
                  Providing false information of any kind may result in the
                  termination of your account. You must immediately notify us of
                  any unauthorized uses of your account or any other breaches of
                  security. We will not be liable for any acts or omissions by
                  you, including any damages of any kind incurred as a result of
                  such acts or omissions.{'\n\n'}3 We reserve the right to
                  suspend, disable, or delete your account (or any part thereof)
                  suspend or terminate your access to the Services for any
                  reason or for no reason at all, including, but not limited to,
                  if we determine that you have violated any provision of this
                  Agreement or that your conduct or content would tend to damage
                  our reputation and goodwill. You may suspend or terminate, in
                  your sole discretion, your use of the Services at any time and
                  for any reason. If you wish to deactivate your account, please
                  contact us. If we delete your account for any reason, you may
                  not re-register for our Services. We may block your email
                  address and Internet protocol address to prevent further
                  registration.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  B. Participation in Goalprizes as a Customer
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  1 Each Goalprize campaign is subject to the rules of that
                  particular Goalprize campaign which may differ from all other
                  Goalprize campaigns. To participate as a customer in a
                  Goalprize the customer must comply with this Agreeement and
                  also with the "Rules of the Goalprize" of that particular
                  Goalprize which may state, among other things: how to gain
                  points, how to win prizes, the start and end by date and time,
                  the minimum amount to spend to count as a visit, the number of
                  visits that is the goal of this particular Goalprize, the
                  minimum ranking to be a winner, the maximum prize, the
                  expiration date of of gift cards and the credits on those gift
                  cards.{'\n\n'}2 A Goalprize can be put on hold or cancelled if
                  required by law or for any other reason.{'\n\n'}3 Points can
                  not be given or received without a photo of the the bill
                  included in the Request for Points for which the user is
                  seeking points that clearly shows: 1. The business name, 2.
                  the date, 3. The subtotal, service charge, tax, and total.
                  {'\n\n'}4 Points will never be exchanged for cash. Credits
                  will never be exchanged for cash.{'\n\n'}5 Users may not let
                  other people use their account. Users may not transfer points
                  or credits to other users.{'\n\n'}6 Points are spending money
                  on qualifying goods and services during a Goalprize not by the
                  number of visits a customer makes.{'\n\n'}7 On the End day of
                  a Goalprize it is the customer's responsibility to must make a
                  Request for Points sufficiently in advance of the time at
                  which the Goalprize will end so as to give employees or
                  representatives of the business sufficient time to grant the
                  points. No points can be given after the time at which
                  Goalprize ends even if a participating customer made
                  qualifying purchases before the deadline but failed to submit
                  a Request for Points enough in advance of the deadline.
                  Customers should be aware that many customers may submit
                  Requests for Points near the deadline time and thus if they
                  submit a Request for Points too close to the deadline time
                  they may not receive points.{'\n\n'}8 A Goalprize might
                  promote its prize as being "Win What You Spend" but could have
                  a maximum prize limit that may be less than the amount the
                  customer spent during the Goalprize.{'\n\n'}9 A visit will
                  only be counted as a visit when a bill is paid by a
                  participating customer equal to or greater than the minimum
                  spending requirement to count as visit as indicated in that
                  Goalprize.{'\n\n'}10 The customer will not receive points for
                  the qualifying amount they spent at the business unless they
                  request these points at the business where they paid the bill
                  within 4 hours of getting the receipt of the paid bill or the
                  closing of business, whichever is earlier.{'\n\n'}11
                  Leaderboards only show the points a participating customer has
                  earned for a single Goalprize.{'\n\n'}12 The number of
                  visitors that are registered as being in a customer's group
                  shall be the number of customers in that group who are in the
                  restaurant who ordered food on the bill where the total of
                  qualifying paid purchases is greater than the minimum spending
                  amount per customer. For example, if the minimum spending
                  requirement is 100 THB per person per visit and 1,090 THB was
                  paid on a bill by a group of 11 people then it would count as
                  only 10 visits. Each time a customer pays a bill at a
                  restaurant during a Goalprize greater than the minimum
                  spending requirement then it is counted as a visit. If there
                  is a dispute between the business and the customer the
                  decision of the business will prevail.{'\n\n'}13 A customer
                  can either gain points on a bill or redeem credits on a bill
                  but can not use the same bill to both redeem credits and earn
                  points.{'\n\n'}14 Credits on digital gift card prizes must be
                  redeemed before the closing of the business on the expiration
                  date or 11:59 pm whichever is earlier. Any credits not used by
                  the customer will be lost.{'\n\n'}15 Any disagreements between
                  customers and businesses must be arbitrated between customers
                  and businesses. Goalprize will not arbitrate disputes.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  C. Hosting a Goalprize as a Business
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  1 To use the Goalprize service a business needs to submit all
                  the information in the "Business Information" section of the
                  Goalprize for Business application to verify the legal
                  registration of your business and to verify that the person
                  controlling the account for this business has the legal
                  authority to act for the business.{'\n\n'}2 When a Business
                  submits intellectual property (IP) to us such as a trademark
                  or copyrighted image it certifies that it has legal authority
                  to allow us to use these IP materials in its service and it
                  allows us to us the IP materials submitted to us.{'\n\n'}3 To
                  host a Goalprize campaign a business must fill out the "Host a
                  Goalprize" section and receive approval from Goalprize before
                  it can start. We reserve the right to reject any Goalprize for
                  any reason. A request to host a Goalprize may not be made
                  while the business making the request has a Goalprize already
                  scheduled to start in the future or currently running. A
                  business may host only one Goalprize at a time. We reserve the
                  right to take up to 3 days to respond to a request to host a
                  Goalprize. Goalprizes must be requested at least 5 days in the
                  future.{'\n\n'}4 A business must submit a valid form of
                  payment before a Goalprize will be approved. Businesses must
                  pay the amount agreed upon by the Business in a Request to
                  Host a Goalprize for all customer visits what occur during the
                  Goalprize campaign. The business will be charged when the Goal
                  is achieved or when the End of the Goalprize is reached,
                  whichever is sooner.{'\n\n'}5 When a Goalprize starts it can
                  not be cancelled but must continue until the Goal is achieved
                  or the End of the Goalprize is reached.{'\n\n'}6 Points can
                  not give given without a photo of the the bill that clearly
                  shows: 1. The restaurant, 2. the date, 3. The subtotal,
                  service charge, tax, and total{'\n\n'}7 When a customer pays
                  their bill points must be given to the customer using the
                  Goalprize app equal to what they spent on all items except
                  those prohibited by law (alcohol, tobacco, see list below).
                  Points can only be given on the same day the bill was paid. If
                  a mistake is made in the number of points given or customer
                  visits counted edits can only occur within 30 minutes of when
                  it was made or before the deadline whichever is sooner.
                  {'\n\n'}8 When customers request points they will indicate the
                  number of customers in their group attached to the billed they
                  just paid. Each customer is counted as a customer visit or
                  just a "visit". If the customer indicates the wrong number the
                  business must indicate the correct number.{'\n\n'}9 All
                  conditions of a Goalprize campaign are determined by the
                  Goalprize service not by the business hosting the Goalprize.
                  These include: cumulative number of customer visits, time
                  remaining to end, cumulative points earned by customers,
                  ranking of customers, success or failure of Goalprize, number
                  of winners, ID of winners, amount of prizes given to winners
                  and other factors.{'\n\n'}10 Prizes will be given out
                  automatically by the Goalprize service 24 hours after a
                  Goalprize is successful so as to provide time for an audit to
                  remove any accounts that violated this Agreement, fraudent
                  behavior or to fix any errors.{'\n\n'}11 Businesses are
                  legally obligated to redeem credits for all goods and services
                  not prohibited by law (see list below) before the 11:59 or the
                  normal closing time of their business. Businesses must remain
                  open during normal business days and hours to redeem credits
                  until the expiration date of the latest digital gift card of a
                  customer.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  D. Communications
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  By providing us with your wireless phone number, you confirm
                  and consent to FastUsers Company Limited communicating with
                  you about the Services and other products and services that
                  may be of interest to you by SMS, text message, email and
                  other electronic means, including autodialed text messages
                  containing information regarding the Services and/or marketing
                  messages, even if your phone number is on the do-not-call
                  list. Your carrier's normal messaging, data and other rates
                  and fees will apply to these communications. You are not
                  required to provide this consent to receive marketing messages
                  as a condition of using the Services and you may opt-out of
                  receiving these messages at any time (though you may continue
                  to receive messages related to your Account).
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  E. User content
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  1 We do not own any data, information or material
                  (collectively, "Content") that you submit on the Services in
                  the course of using the Service. You shall have sole
                  responsibility for the accuracy, quality, integrity, legality,
                  reliability, appropriateness, and intellectual property
                  ownership or right to use of all submitted Content. We may
                  monitor and review the Content on the Services submitted or
                  created using our Services by you.{'\n\n'}2 You grant us
                  permission to access, copy, distribute, store, transmit,
                  reformat, display and perform the Content of your user account
                  solely as required for the purpose of providing the Services
                  to you. Without limiting any of those representations or
                  warranties, we have the right, though not the obligation, to,
                  in our own sole discretion, refuse or remove any Content that,
                  in our reasonable opinion, violates any of our policies or is
                  in any way harmful or objectionable. You also grant us the
                  license to use, reproduce, adapt, modify, publish or
                  distribute the Content created by you or stored in your user
                  account for commercial, marketing or any similar purpose.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  F. Billing and payments
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  We do not charge fees for customers to use our service as
                  participants in Goalprize campaigns. We charge businesses that
                  host Goalprizes for the customer visits registered on our
                  service. If in the future we offer any services for customers
                  requiring the payment of a fee or subscription we will require
                  customers to agree to such an offer in a separate explicit
                  indication of purchase and agreement.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  G. Accuracy of information
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  Occasionally there may be information on the Services that
                  contains typographical errors, inaccuracies or omissions that
                  may relate to product descriptions, pricing, availability,
                  promotions and offers. We reserve the right to correct any
                  errors, inaccuracies or omissions, and to change or update
                  information or cancel orders if any information on the
                  Services or Services is inaccurate at any time without prior
                  notice (including after you have submitted your order). We
                  undertake no obligation to update, amend or clarify
                  information on the Services including, without limitation,
                  pricing information, except as required by law. No specified
                  update or refresh date applied on the Services should be taken
                  to indicate that all information on the Services or Services
                  has been modified or updated
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  H. Reliance on Information Posted
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  1 The information presented on or through the Services is made
                  available solely for general information purposes. We do not
                  warrant the accuracy, completeness, or usefulness of this
                  information. Any reliance you place on such information is
                  strictly at your own risk. We disclaim all liability and
                  responsibility arising from any reliance placed on such
                  materials by you or any other visitor to the Services, or by
                  anyone who may be informed of any of their contents.{'\n\n'}2
                  The Services include content provided by third parties,
                  including materials provided by other users and third-party
                  licensors, syndicators, aggregators, and/or reporting
                  services. All statements and/or opinions expressed in these
                  materials, and all articles and responses to questions and
                  other content, other than the content provided by the Company,
                  are solely the opinions and the responsibility of the person
                  or entity providing those materials. These materials do not
                  necessarily reflect the opinion of the Company. We are not
                  responsible, or liable to you or any third party, for the
                  content or accuracy of any materials provided by any third
                  parties.{'\n\n'}3 In the event of any inconsistency between
                  this Agreement and the information set forth in the Goalprize
                  Frequently Asked Questions (FAQ), Goalprize promotional
                  content, or any other source, the Agreement shall control.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  I. Third party services
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  1 If you decide to enable, access or use third party services,
                  be advised that your access and use of such other services are
                  governed solely by the terms and conditions of such other
                  services, and we do not endorse, are not responsible or liable
                  for, and make no representations as to any aspect of such
                  other services, including, without limitation, their content
                  or the manner in which they handle data (including your data)
                  or any interaction between you and the provider of such other
                  services. You irrevocably waive any claim against FastUsers
                  Company Limited with respect to such other services. FastUsers
                  Company Limited is not liable for any damage or loss caused or
                  alleged to be caused by or in connection with your enablement,
                  access or use of any such other services, or your reliance on
                  the privacy practices, data security processes or other
                  policies of such other services.{'\n\n'}2 You may be required
                  to register for or log into such other services on their
                  respective platforms. By enabling any other services, you are
                  expressly permitting FastUsers Company Limited to disclose
                  your data as necessary to facilitate the use or enablement of
                  such other service.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  J. Advertisements
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  During your use of the Services, you may enter into
                  correspondence with or participate in promotions of
                  advertisers or sponsors showing their goods or services
                  through the Services. Any such activity, and any terms,
                  conditions, warranties or representations associated with such
                  activity, is solely between you and the applicable third
                  party. We shall have no liability, obligation or
                  responsibility for any such correspondence, purchase or
                  promotion between you and any such third party.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  K. Links to other resources
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  Although the Services may link to other resources (such as
                  websites, mobile applications, etc.), we are not, directly or
                  indirectly, implying any approval, association, sponsorship,
                  endorsement, or affiliation with any linked resource, unless
                  specifically stated herein. Some of the links on the Services
                  may be "affiliate links". This means if you click on the link
                  and purchase an item, FastUsers Company Limited may receive an
                  affiliate commission. We are not responsible for examining or
                  evaluating, and we do not warrant the offerings of, any
                  businesses or individuals or the content of their resources.
                  We do not assume any responsibility or liability for the
                  actions, products, services, and content of any other third
                  parties. You should carefully review the legal statements and
                  other conditions of use of any resource which you access
                  through a link on the Services. Your linking to any other
                  off-site resources is at your own risk.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  L. Prohibited uses
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  In addition to other terms as set forth in the Agreement, you
                  are prohibited from using the Services or Content: (a) for any
                  unlawful purpose; (b) to solicit others to perform or
                  participate in any unlawful acts; (c) to violate any
                  international, federal, provincial or state regulations,
                  rules, laws, or local ordinances; (d) to infringe upon or
                  violate our intellectual property rights or the intellectual
                  property rights of others; (e) to harass, abuse, insult, harm,
                  defame, slander, disparage, intimidate, or discriminate based
                  on gender, sexual orientation, religion, ethnicity, race, age,
                  national origin, or disability; (f) to submit false or
                  misleading information; (g) to upload or transmit viruses or
                  any other type of malicious code that will or may be used in
                  any way that will affect the functionality or operation of the
                  Services, third party products and services, or the Internet;
                  (h) to spam, phish, pharm, pretext, spider, crawl, or scrape;
                  (i) for any obscene or immoral purpose; or (j) to interfere
                  with or circumvent the security features of the Services,
                  third party products and services, or the Internet. We reserve
                  the right to terminate your use of the Services for violating
                  any of the prohibited uses.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  M. Intellectual property rights
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  "Intellectual Property Rights" means all present and future
                  rights conferred by statute, common law or equity in or in
                  relation to any copyright, including all information,
                  software, text, displays, images, video, and audio, and the
                  design, selection, and arrangement thereof and related rights,
                  trademarks, designs, patents, inventions, goodwill and the
                  right to sue for passing off, rights to inventions, rights to
                  use, and all other intellectual property rights, in each case
                  whether registered or unregistered and including all
                  applications and rights to apply for and be granted, rights to
                  claim priority from, such rights and all similar or equivalent
                  rights or forms of protection and any other results of
                  intellectual activity which subsist or will subsist now or in
                  the future in any part of the world. This Agreement does not
                  transfer to you any intellectual property owned by FastUsers
                  Company Limited or third parties, and all rights, titles, and
                  interests in and to such property will remain (as between the
                  parties) solely with FastUsers Company Limited. All
                  trademarks, service marks, graphics and logos used in
                  connection with the Services, are trademarks or registered
                  trademarks of FastUsers Company Limited or its licensors.
                  Other trademarks, service marks, graphics and logos used in
                  connection with the Services may be the trademarks of other
                  third parties. Your use of the Services grants you no right or
                  license to reproduce or otherwise use any of FastUsers Company
                  Limited or third party trademarks.{'\n\n'}If you print, copy,
                  modify, download, or otherwise use or provide any other person
                  with access to any part of the Services in breach of the Terms
                  of Service, your right to use the Services will stop
                  immediately and you must, at our option, return or destroy any
                  copies of the materials you have made. No right, title, or
                  interest in or to the Services or any content on the Services
                  is transferred to you, and all rights not expressly granted
                  are reserved by the Company. Any use of the Services not
                  expressly permitted by these Terms of Service is a breach of
                  these Terms of Service and may violate copyright, trademark,
                  and other laws.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  N. Copyright Infringement
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  "FastUsers Company Limited responds to notices of alleged
                  copyright infringement and terminate accounts of repeat
                  infringers according to the process set out in the U.S.
                  Digital Millennium Copyright Act. If you believe that any
                  material on the Services infringes upon any copyright which
                  you own or control, you may file a DMCA Notice of Alleged
                  Infringement with FastUsers Company Limited at our email
                  address:{'\n\n'}support@goalprize.com{'\n\n'}DMCA Compliance
                  {'\n\n'}Please provide the following information:{'\n'}- A
                  description of the copyrighted work you believe to have been
                  infringed;{'\n'}- A description of the URL or other location
                  on our Services of the material you believe to be infringing;
                  {'\n'}- Your name, mailing address, telephone number and email
                  address;{'\n'}- A statement that you have a good faith belief
                  that the disputed use is not authorized by the copyright
                  owner, its agent, or the law;{'\n\n'}- A statement by you,
                  which you make under penalty of perjury, that the above
                  information in your notice is accurate, and that you are the
                  copyright owner or authorized to act on the copyright owner’s
                  behalf; and - An electronic or physical signature of the
                  person authorized to act on behalf of the copyright owner."
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  O. Disclaimer of warranty
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  "YOU AGREE THAT SUCH SERVICE IS PROVIDED ON AN ""AS IS"" AND
                  ""AS AVAILABLE"" BASIS AND THAT YOUR USE OF THE SERVICES IS
                  SOLELY AT YOUR OWN RISK. WE EXPRESSLY DISCLAIM ALL WARRANTIES
                  OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING BUT NOT
                  LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
                  FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. WE MAKE NO
                  WARRANTY THAT THE SERVICES WILL MEET YOUR REQUIREMENTS, OR
                  THAT THE SERVICE WILL BE UNINTERRUPTED, TIMELY, SECURE, OR
                  ERROR-FREE; NOR DO WE MAKE ANY WARRANTY AS TO THE RESULTS THAT
                  MAY BE OBTAINED FROM THE USE OF THE SERVICE OR AS TO THE
                  ACCURACY OR RELIABILITY OF ANY INFORMATION OBTAINED THROUGH
                  THE SERVICE OR THAT DEFECTS IN THE SERVICE WILL BE CORRECTED.
                  YOU UNDERSTAND AND AGREE THAT ANY MATERIAL AND/OR DATA
                  DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF SERVICE IS
                  DONE AT YOUR OWN DISCRETION AND RISK AND THAT YOU WILL BE
                  SOLELY RESPONSIBLE FOR ANY DAMAGE OR LOSS OF DATA THAT RESULTS
                  FROM THE DOWNLOAD OF SUCH MATERIAL AND/OR DATA. WE MAKE NO
                  WARRANTY REGARDING ANY GOODS OR SERVICES PURCHASED OR OBTAINED
                  THROUGH THE SERVICE OR ANY TRANSACTIONS ENTERED INTO THROUGH
                  THE SERVICE UNLESS STATED OTHERWISE. NO ADVICE OR INFORMATION,
                  WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM US OR THROUGH
                  THE SERVICE SHALL CREATE ANY WARRANTY NOT EXPRESSLY MADE
                  HEREIN.{'\n\n'}YOU UNDERSTAND THAT WE CANNOT AND DO NOT
                  GUARANTEE OR WARRANT THAT FILES AVAILABLE FOR DOWNLOADING FROM
                  THE INTERNET OR THE SERVICES WILL BE FREE OF VIRUSES OR OTHER
                  DESTRUCTIVE CODE. YOU ARE RESPONSIBLE FOR IMPLEMENTING
                  SUFFICIENT PROCEDURES AND CHECKPOINTS TO SATISFY YOUR
                  PARTICULAR REQUIREMENTS FOR ANTI-VIRUS PROTECTION AND ACCURACY
                  OF DATA INPUT AND OUTPUT, AND FOR MAINTAINING A MEANS EXTERNAL
                  TO OUR SITE FOR ANY RECONSTRUCTION OF ANY LOST DATA. TO THE
                  FULLEST EXTENT PROVIDED BY LAW, WE WILL NOT BE LIABLE FOR ANY
                  LOSS OR DAMAGE CAUSED BY A DISTRIBUTED DENIAL-OF-SERVICE
                  ATTACK, VIRUSES, OR OTHER TECHNOLOGICALLY HARMFUL MATERIAL
                  THAT MAY INFECT YOUR COMPUTER EQUIPMENT, COMPUTER PROGRAMS,
                  DATA, OR OTHER PROPRIETARY MATERIAL DUE TO YOUR USE OF THE
                  SERVICES OR ITEMS OBTAINED THROUGH THE SERVICES OR TO YOUR
                  DOWNLOADING OF ANY MATERIAL POSTED ON IT, OR ON ANY SERVICES
                  LINKED TO IT.{'\n\n'}THE FOREGOING DOES NOT AFFECT ANY
                  WARRANTIES THAT CANNOT BE EXCLUDED OR LIMITED UNDER APPLICABLE
                  LAW. "
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  P. Legal compliance
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  "We and all users of the service must comply with the laws in
                  the jurisdictions where the service is used. ""Points"" are
                  what is given to customers participating in a Goalprize
                  campaign and spending money at a business hosting a Goalprize
                  campaign. In some jurisdictions points can not be given out
                  for purchasing certain goods and services. These jurisdictions
                  prohibit the granting of points for the following goods or
                  services:{'\n\n'}THAILAND: alcohol, tobacco, diapers and some
                  other goods and services.{'\n\n'}U.S.A.: (to be determined)"
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  Q. Tax Reporting
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  We may be required to report users who win prizes above a
                  certain level to the government. We are required to submit to
                  any reporting request of a government tax authority regarding
                  spending and revenues of businesses. All users are required to
                  pay any taxes that may result from winning prizes with a
                  Customer or Business.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  R. Geographic Restrictions
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  FastUsers Company Limited is based in Thailand and shall have
                  a location in the United States. We make no claims that the
                  Services or any of its content is accessible or appropriate
                  outside of the Thailand or the United States. Access to the
                  Services may not be legal by certain persons or in certain
                  countries. If you access the Services from outside the
                  Thailand or the United States, you do so on your own
                  initiative and are responsible for compliance with local laws.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  S. Disclaimer to Users
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  1 No prize will be given to any customer if the Goal of a
                  Campaign is not achieved. If the Goal of a Campaign is
                  achieved only those customers who fulfill the requirements to
                  be winners will be owed a prize by the Business operating the
                  Campaign. There is no guarantee that a Goal will be achieved.
                  It is the responsibility of the Business to fulfill the terms
                  of the prizes and to redeem credits but we are not responsible
                  if the Business fails to do this. We do not guarantee that the
                  Business remains in operation or remains open so that winners
                  can redeem prize credits. We are not responsible for the loss
                  of credit value if we or the business operating a campaign are
                  shut down by the government. If you violate the law with your
                  use of our software we reserve the right to terminate your
                  account and terminate your points and credits. You will not be
                  compensated if either situation happens. Protecting your
                  credits from unauthorized use is your responsibility and we
                  will not give credits if someone uses your credits without
                  your approval. If a Customer believes that the credits given
                  by the Business are wrong it should talk to a manager at that
                  Business for fixing this problem not us. We can not fix them.
                  Being ranked in the “winning zone” of the Leaderboard before a
                  Campaign ends does not guarantee winning as the rankings may
                  change before a Campaign ends successfully or the Campaign may
                  not achieve the Goal and thus end unsuccessfully with no
                  prizes given out. The total amount you spent during a campaign
                  will appear on the Leaderboard with your Goalprize ID number.
                  While we hope the use of the Goalprize ID number will keep
                  your actual identity private we do not promise privacy and
                  someone may be able to determine your actual identity from
                  seeing your Goalprize ID on the Leaderboard or otherwise. If
                  you choose to put any part of your name on the Leaderboard you
                  will increase the chance that someone will be able to
                  determine your actual identity. We are not responsible for the
                  ramifications of someone determining your actual identity. We
                  do not promise that campaigns will be held or that they will
                  be held in a location close to you. This application for
                  customers is only for individuals to use it not businesses
                  which must use the business version of the application. Users
                  must not violate any laws including but not limited to laws
                  outlawing certain types of information or images. All credits
                  have an expiration date and must be used before the expiration
                  date and will be lost after the expiration. If an expiration
                  date for credits is not clearly stated then it will be by
                  default 90 days after the date that the credits were earned as
                  winning credits from a successful campaign. Users may not
                  exchange or use expired credits. The expired credits will not
                  be restored for any reason. If a user has credits with
                  different expiration dates from the same business then the
                  credits from the earliest expiration must be used first. Any
                  credits provided by a Business are subject to being reversed
                  when the Customer receives a refund or credit based on the
                  transaction underlying the previously issued credit.{'\n\n'}2
                  Business owners may use the service in an attempt to achieve a
                  goal of customer visits but we make no promise that this goal
                  can be achieved or that more customers can be gained. We make
                  no promise regarding the amount of money, time or effort we
                  will spend to help promote your campaign. We make no promise
                  regarding the number of customers that may participate in a
                  campaign or the amount customers may spend in a campaign. We
                  make no promise regarding the profitability of hosting a
                  Goalprize campaign.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  T. Business Owners Agreement
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  1 Business owners who use this service to increase the number
                  of customers using a Campaign or setting a Goal with prizes to
                  customers must fulfill the terms of those Campaigns and give
                  the prizes to the customers which are digital gift cards and
                  accept the prize credits for payment by customers before the
                  expiration date. Business owners must remain in operation
                  during normal days and normal hours until the expiration date
                  of the prize credits and must not shut down operation after a
                  successful Campaign before the expiration date of the credits
                  given out during that campaign. When a campaign is launched
                  and is past the start time and date the Business owner is not
                  allowed to cancel, withhold, pause or terminate it but must
                  keep it going until the end stated in the campaign settings.
                  {'\n\n'}2 Business owners may generate a significant amount of
                  revenue with a successful Goalprize campaign. A certain
                  percent of revenue from a successful Goalprize campaign should
                  be held by the business to support the future costs of
                  fulfilling prize credit redemptions and be considered earned
                  only when those credits are redeemed. The percent of revenue
                  that is held by the business to cover its own future costs of
                  fulfilling those prize credit redemptions can be set by the
                  business itself in accordance with prevaling proper accounting
                  practices but should at minimum match all future costs for
                  prize credit fullfillment such that a business has sufficient
                  funds held to remain in operation until all prize credits have
                  either been redeemed or reached their expiration date.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  U. Limitation of liability
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  1 To the fullest extent permitted by applicable law, in no
                  event will FastUsers Company Limited, its affiliates,
                  directors, officers, employees, agents, suppliers or licensors
                  be liable to any person for any indirect, incidental, special,
                  punitive, cover or consequential damages (including, without
                  limitation, damages for lost profits, revenue, sales,
                  goodwill, use of content, impact on business, business
                  interruption, loss of anticipated savings, loss of business
                  opportunity) however caused, under any theory of liability,
                  including, without limitation, contract, tort, warranty,
                  breach of statutory duty, negligence or otherwise, even if the
                  liable party has been advised as to the possibility of such
                  damages or could have foreseen such damages. To the maximum
                  extent permitted by applicable law, the aggregate liability of
                  FastUsers Company Limited and its affiliates, officers,
                  employees, agents, suppliers and licensors relating to the
                  services will be limited to an amount NO greater of one dollar
                  or any amounts actually paid in cash by you to FastUsers
                  Company Limited for the prior one month period prior to the
                  first event or occurrence giving rise to such liability. The
                  limitations and exclusions also apply if this remedy does not
                  fully compensate you for any losses or fails of its essential
                  purpose.{'\n\n'}2 "Users understand and agree that FastUsers
                  Company Limited is not a party to any transactions entered
                  into between users and businesses. FastUsers Company Limited
                  has no control over the conduct of users of the Services or
                  any information provided in connection thereto, and disclaims
                  all liability in this regard. If you have a dispute with a
                  business that is running a campaign or setting goals, you
                  agree to release FastUsers Company Limited from all claims,
                  demands and damages of every nature, known and unknown,
                  arising out of or in any way connected with such disputes. In
                  no event will FastUsers Company Limited be liable for direct
                  or indirect consequences of an end user or business failing to
                  comply with applicable laws and regulations.{'\n\n'}The
                  limitation of liability set out above does not apply to
                  liability resulting from our gross negligence or willful
                  misconduct.{'\n\n'}The foregoing does not affect any liability
                  that cannot be excluded or limited under applicable law. "
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  V. Indemnification
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  You agree to indemnify and hold FastUsers Company Limited and
                  its affiliates, directors, officers, employees, agents,
                  suppliers and licensors harmless from and against any
                  liabilities, losses, damages or costs, including reasonable
                  attorneys’ fees, incurred in connection with or arising from
                  any third party allegations, claims, actions, disputes, or
                  demands asserted against any of them as a result of or
                  relating to your Content, your use of the Services or any
                  willful misconduct on your part.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  W. Severability
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  All rights and restrictions contained in this Agreement may be
                  exercised and shall be applicable and binding only to the
                  extent that they do not violate any applicable laws and are
                  intended to be limited to the extent necessary so that they
                  will not render this Agreement illegal, invalid or
                  unenforceable. If any provision or portion of any provision of
                  this Agreement shall be held to be illegal, invalid or
                  unenforceable by a court of competent jurisdiction, it is the
                  intention of the parties that the remaining provisions or
                  portions thereof shall constitute their agreement with respect
                  to the subject matter hereof, and all such remaining
                  provisions or portions thereof shall remain in full force and
                  effect.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  X. Dispute resolution
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  1 The formation, interpretation, and performance of this
                  Agreement and any disputes arising out of it shall be governed
                  by the substantive and procedural laws of Thailand without
                  regard to its rules on conflicts or choice of law and, to the
                  extent applicable, the laws of Thailand. The exclusive
                  jurisdiction and venue for actions related to the subject
                  matter hereof shall be the courts located in Thailand, and you
                  hereby submit to the personal jurisdiction of such courts. You
                  hereby waive any right to a jury trial in any proceeding
                  arising out of or related to this Agreement. The United
                  Nations Convention on Contracts for the International Sale of
                  Goods does not apply to this Agreement.{'\n\n'}2 In the event
                  of a conflict between the English version of the Agreement and
                  any other language version or a conflict between two
                  non-English versions, the English version shall control.
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  Y Changes and amendments
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  "We reserve the right to modify this Agreement or its terms
                  relating to the Services at any time, effective upon posting
                  of an updated version of this Agreement on the Services. When
                  we do, we will revise the updated date at the bottom of this
                  page. Continued use of the Services after any such changes
                  shall constitute your consent to such changes. You are
                  expected to check this page from time to time so you are aware
                  of any changes, as they are binding on you. "
                </Text>
                <Text
                  style={[
                    Styles.bold_body_label,
                    {
                      color: COLOR.BLACK,
                      marginTop: 15,
                      fontFamily: FONTS.FAMILY_MEDIUM,
                    },
                  ]}>
                  z. Acceptance of these terms
                </Text>
                <Text
                  style={[
                    Styles.extra_small_label,
                    {color: COLOR.LIGHT_TEXT, marginTop: 5},
                  ]}>
                  You acknowledge that you have read this Agreement and agree to
                  all its terms and conditions. By accessing and using the
                  Services you agree to be bound by this Agreement. If you do
                  not agree to abide by the terms of this Agreement, you are not
                  authorized to access or use the Services.{'\n\n'}If you would
                  like to contact us to understand more about this Agreement or
                  wish to contact us concerning any matter relating to it, you
                  may send an email to info@FastUsers.com.{'\n\n'}Terms of
                  Service in effect from August 31, 2021 until updated.
                </Text>
              </View>
            </ScrollView>
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.WHITE,
    borderRadius: 5,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 20,
    paddingVertical: 30,
    paddingHorizontal: 15,
  },
});
