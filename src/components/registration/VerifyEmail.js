import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';
import Styles from '../../styles/Styles';
import IMAGES from '../../styles/Images';
import NavigationBar from '../../commonView/NavigationBar';
import COLOR from '../../styles/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {CustomButton} from '../../commonView/CustomButton';
import CodeInput from 'react-native-confirmation-code-input';
import LinearGradient from 'react-native-linear-gradient';
import {
  handleDispatch,
  Locale,
  showAlertMessage,
} from '../../commonView/Helpers';
import ActivityIndicator from '../../commonView/ActivityIndicator';
import API from '../../api/Api';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import AuthenticationAction from '../../redux/action/AuthenticationAction';
import {connect} from 'react-redux';
import {AS_INITIAL_ROUTE, AS_USER_TOKEN} from '../../commonView/Constants';
import Auth from '../../auth';

let api = new API();
let auth = new Auth();

class VerifyEmail extends React.Component {
  constructor() {
    super();
    this.state = {
      email: '',
      value: '',
      isLoading: false,
      otp: '',
    };

    this.updateMasterState = this.updateMasterState.bind(this);
    this.sendVeriyOtp = this.sendVeriyOtp.bind(this);
    this.veriyOtp = this.veriyOtp.bind(this);
  }

  componentDidMount(props) {
    if (this.props.route.params) {
      if (this.props.route.params.email != null) {
        this.setState({
          email: this.props.route.params.email,
        });
      }
    }
  }

  sendVeriyOtp = () => {
    let that = this;
    that.setState({isLoading: true});
    api
      .generateOtp(this.state.email)
      .then(json => {
        console.log('otp response:-', json);
        that.setState({isLoading: false});
        let stringData = JSON.stringify(json.data);
        if (json.status == 200) {
          that.setState({otp: json.data.response.otp});
          showAlertMessage('Otp sent successfully', 1);
        } else if (json.status == 400) {
          showAlertMessage(json.data.message, 2);
        } else {
          showAlertMessage(json.data.message, 2);
        }
      })
      .catch(error => {
        that.setState({isLoading: false});
        console.log('otp response:-', error);
        showAlertMessage(error.response.data.message, 2);
      });
  };

  veriyOtp = async () => {
    let that = this;
    that.setState({isLoading: true});
    let data = JSON.stringify({
      email: that.state.email,
      otp: this.state.value,
      type: 1,
    });
    api
      .verifyOtp(data)
      .then(json => {
        console.log('signup  response:-', json, json.status);
        that.setState({isLoading: false});
        let stringData = JSON.stringify(json.data.response);
        if (json.data.status == 200) {
          showAlertMessage(Locale('Successfully verified email'), 1);
          auth.setValue(AS_USER_TOKEN, json.data.response.token);
          that.props.saveUserDetail(stringData);
          this.props.navigation.navigate('MyTabs');
          handleDispatch(this.props);
        } else if (json.data.status == 400) {
          showAlertMessage(json.data.message, 2);
        } else {
          showAlertMessage(json.data.message, 2);
        }
      })
      .catch(error => {
        that.setState({isLoading: false});
        showAlertMessage(error.response.data.message, 2);
      });
  };

  updateMasterState = (attrName, value) => {
    this.setState({[attrName]: value});
  };

  render() {
    return (
      <View style={Styles.container}>
        <LinearGradient
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
          colors={[COLOR.TOP_GRADIENT, COLOR.BOTTOM_GRADIENT]}
          style={Styles.container}>
          <ActivityIndicator loading={this.state.isLoading} />
          <SafeAreaView style={Styles.container}>
            <NavigationBar />
            <KeyboardAwareScrollView style={Styles.container}>
              <View
                style={[style.shadowView, {backgroundColor: 'transparent'}]}>
                <Image
                  source={IMAGES.banner}
                  style={{width: '100%', height: 200, marginTop: 20}}
                  resizeMode={'contain'}
                />
                <Text
                  style={[
                    Styles.heading_label,
                    {
                      color: COLOR.WHITE,
                      alignSelf: 'center',
                      fontSize: 16,
                    },
                  ]}>
                  {Locale('Verify your email')}
                </Text>
                <Text
                  style={[
                    Styles.small_label,
                    {
                      color: COLOR.WHITE,
                      alignSelf: 'center',
                      marginTop: 10,

                      textAlign: 'center',
                    },
                  ]}>
                  {Locale('K_VERIFY_EMAIL')}
                </Text>
                {/*<Text*/}
                {/*  style={[*/}
                {/*    Styles.subheading_label,*/}
                {/*    {*/}
                {/*      color: COLOR.WHITE,*/}
                {/*      alignSelf: 'center',*/}
                {/*      marginTop: 10,*/}
                {/*      textAlign: 'center',*/}
                {/*    },*/}
                {/*  ]}>*/}
                {/*  {this.state.otp}*/}
                {/*</Text>*/}
                <CodeInput
                  ref="codeInputRef1"
                  onFulfill={val => {
                    console.log('val is val is:-', val);
                    this.setState({value: val});
                  }}
                  value={this.state.value}
                  className={'border-b'}
                  space={10}
                  codeLength={4}
                  autoFocus={true}
                  size={50}
                  codeInputStyle={[Styles.heading_label, {color: COLOR.WHITE}]}
                  containerStyle={{marginBottom: 20}}
                  cellBorderWidth={2}
                  inputPosition="center"
                  keyboardType={'decimal-pad'}
                />

                <View
                  style={{
                    width: wp(60),
                    alignSelf: 'center',
                    marginVertical: 20,
                  }}>
                  <CustomButton
                    onPress={this.veriyOtp}
                    text={Locale('Verify email')}
                  />
                </View>
                <TouchableWithoutFeedback onPress={this.sendVeriyOtp}>
                  <Text
                    style={[
                      Styles.extra_small_label,
                      {
                        color: COLOR.WHITE,
                        marginTop: 10,
                        alignSelf: 'center',
                        textDecorationLine: 'underline',
                      },
                    ]}>
                    {Locale('Resend OTP')}
                  </Text>
                </TouchableWithoutFeedback>
              </View>
            </KeyboardAwareScrollView>
          </SafeAreaView>
        </LinearGradient>
      </View>
    );
  }
}

const style = StyleSheet.create({
  shadowView: {
    backgroundColor: COLOR.LIGHT_BLUE,
    borderRadius: 10,
    width: wp(90),
    alignSelf: 'center',
    marginVertical: 20,
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  codeFieldRoot: {
    marginTop: 20,
    width: wp(70),
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  cellRoot: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#ccc',
    borderBottomWidth: 4,
  },
  cellText: {
    color: COLOR.BLACK,
    fontSize: 26,
    textAlign: 'center',
  },
  focusCell: {
    borderBottomColor: COLOR.GREEN,
    borderBottomWidth: 4,
  },
});

const mapDispatchToProps = dispatch => {
  return {
    saveUserDetail: data => {
      dispatch(AuthenticationAction.saveUserDetail(data));
    },
  };
};

export default connect(null, mapDispatchToProps)(VerifyEmail);
