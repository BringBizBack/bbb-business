import React, {useEffect} from 'react';
import {
  LogBox,
  Text,
  View,
  Image,
  SafeAreaView,
  Platform,
  Alert,
} from 'react-native';
import RootStack from './src/components/routes/Routes';
import {StatusBar} from 'react-native';
import FlashMessage from 'react-native-flash-message';
import Styles from './src/styles/Styles';
import COLOR from './src/styles/Color';
import messaging from '@react-native-firebase/messaging';
import {AS_FCM_TOKEN} from './src/commonView/Constants';
import Auth from './src/auth/index';
import NotificationPopup from 'react-native-push-notification-popup';
import IMAGES from './src/styles/Images';
import CampaignActions from './src/redux/action/CampaignActions';
import {connect} from 'react-redux';
import AuthenticationAction from './src/redux/action/AuthenticationAction';
import {Locale, navigate, setI18nConfig} from './src/commonView/Helpers';
import * as RNLocalize from 'react-native-localize';
import memoize from 'lodash.memoize';
import i18n from 'i18n-js';
import NetInfo, {useNetInfo} from '@react-native-community/netinfo';

let auth = new Auth();

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      noInternet: false,
    };

    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps.allowFontScaling = false;

    this.handleLocalizationChange();

    this.refreshData = this.refreshData.bind(this);
  }

  componentDidMount() {
    this.addFirebaseListner();
    this.requestUserPermission();

    NetInfo.addEventListener(state => {
      // Alert.alert('cal')
      this.setState({
        noInternet: !state.isConnected,
      });
    });

    this.handleRedirection = this.handleRedirection.bind(this);
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
  }

  componentWillUnmount() {
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }

  handleLocalizationChange = () => {
    setI18nConfig();
    this.forceUpdate();
  };

  renderCustomPopup = ({appIconSource, appTitle, timeText, title, body}) => (
    <View
      style={{
        backgroundColor: '#ffffffef',
        justifyContent: 'center',
        padding: 10,
        borderRadius: 10,
      }}>
      <View
        style={{
          flexDirection: 'row',
          height: 20,
          alignItems: 'center',
          marginBottom: 10,
        }}>
        <Image
          source={IMAGES.app_icon}
          style={{height: 15, width: 15, marginRight: 10}}
        />
        <Text style={[Styles.small_label]}>GP Business</Text>
      </View>
      <Text style={[Styles.bold_body_label]}>{title}</Text>
      <Text style={[Styles.small_label]}>{body}</Text>
    </View>
  );

  refreshData = msg => {
    this.props.getUserNotification(1);
    switch (msg.data.notificationType) {
      //1: Trade Approval
      case '1':
        this.props.getUserBusinessDetail(1);
        break;
      //2: Trade Rejection
      case '2':
        this.props.getUserBusinessDetail(1);
        break;
      //3: Campaign Approval
      case '3':
        this.props.getCampaignData(4, 1, null);
        break;
      //4: Campaign Rejection
      case '4':
        this.props.getCampaignData(4, 1, null);
        break;
      //  12: New Campaign Started(Business)
      case '12':
        this.props.getCampaignData(2, 1, null);
        this.props.getCampaignData(4, 1, null);
        break;
      //  13: Campaign Ended(Business)
      case '13':
        this.props.getCampaignData(3, 1, null);
        this.props.getCampaignData(2, 1, null);
        break;
      //20: New template added
      case '20':
        this.props.getTemplates(1);
        break;
      default:
        break;
    }
  };

  addFirebaseListner = async () => {
    let that = this;
    messaging().onMessage(async remoteMessage => {
      console.log(remoteMessage);
      this.refreshData(remoteMessage);
      this.popup.show({
        onPress: function () {
          that.handleRedirection(remoteMessage);
        },
        appTitle: 'GP Business',
        timeText: 'Now',
        val: 'called',
        title: remoteMessage
          ? remoteMessage.notification
            ? remoteMessage.notification.title
            : 'test'
          : 'test',
        body: remoteMessage
          ? remoteMessage.notification
            ? remoteMessage.notification.body
            : 'test'
          : 'teste',
        slideOutTime: 3000,
      });
    });

    messaging().setBackgroundMessageHandler(async remoteMessage => {
      console.log('Message handled in the background!', remoteMessage);
    });

    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage.notification,
      );
      //Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
      // navigation.navigate(remoteMessage.data.type);
      that.refreshData(remoteMessage);
      that.handleRedirection(remoteMessage);
    });
    // Check whether an initial notification is available
    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage.notification,
          );
          //  setInitialRoute(remoteMessage.data.type); // e.g. "Settings"
        }
        //  setLoading(false);
        that.refreshData(remoteMessage);
        that.handleRedirection(remoteMessage);
      });
  };

  handleRedirection(val) {
    console.log('val.data.notificationType', val);
    switch (val.data.notificationType) {
      //1: Trade Approval
      case '1':
        navigate('BusinessInformation');
        break;
      //2: Trade Rejection
      case '2':
        navigate('BusinessInformation');
        break;
      //3: Campaign Approval
      case '3':
        navigate('ExploreCampaign', {
          type: 4,
          tabScreen: true,
        });
        break;
      //4: Campaign Rejection
      case '4':
        navigate('ExploreCampaign', {
          type: 4,
          tabScreen: true,
        });
        break;
      //Credit request from customer
      case '5':
        navigate('GiveCustomerCredit', {
          type: 1,
        });
        break;
      //Redeem request from customer
      case '8':
        navigate('GiveCustomerCredit', {
          type: 2,
          isRedeemScreen: true,
        });
        break;
      //  12: New Campaign Started(Business)
      case '12':
        navigate('ExploreCampaign', {
          type: 2,
          tabScreen: true,
        });
        break;
      //  13: Campaign Ended(Business)
      case '13':
        navigate('ExploreCampaign', {
          type: 3,
          tabScreen: true,
        });
        break;
      default:
        break;
    }
  }

  requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    await messaging().requestPermission({
      sound: true,
      alert: true,
      provisional: true,
      announcement: true,
    });

    if (enabled) {
      this.getFcmToken(); //<---- Add this
      console.log('Authorization status:', authStatus);
    } else {
      //Alert.alert('error1');
    }
  };

  getFcmToken = async () => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
      auth.setValue(AS_FCM_TOKEN, fcmToken);
      console.log('Your Firebase Token is:', fcmToken);
    } else {
      console.log('Failed', 'No token received');
    }
  };

  render() {
    StatusBar.setBarStyle('light-content', true);
    LogBox.ignoreAllLogs();

    return (
      <View style={{flex: 1}}>
        <FlashMessage
          titleStyle={(Styles.bold_body_label, {color: COLOR.WHITE})}
          floating={true}
          style={{alignItems: 'center'}}
          position="center"
        />

        {this.state.noInternet && (
          <SafeAreaView>
            <View
              style={{
                margin: 0,
                height: 25,
                backgroundColor: !this.state.noInternet
                  ? COLOR.GREEN
                  : COLOR.LIGHT_TEXT,

                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
                width: '100%',
              }}>
              <Text style={[Styles.extra_small_label, {color: COLOR.WHITE}]}>
                NO INTERNET
              </Text>
            </View>
          </SafeAreaView>
        )}

        <RootStack />
        <NotificationPopup
          renderPopupContent={this.renderCustomPopup}
          ref={ref => (this.popup = ref)}
        />
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getCampaignData: (type, val, city) => {
      dispatch(CampaignActions.getCampaignData(type, val, city));
    },
    getUserBusinessDetail: val => {
      dispatch(AuthenticationAction.getBusinessDetail(val));
    },
    getUserNotification: val => {
      dispatch(AuthenticationAction.getNotifications(val));
    },
    getTemplates: val => {
      dispatch(CampaignActions.getTemplatenData(val));
    },
  };
};

export default connect(null, mapDispatchToProps)(App);
